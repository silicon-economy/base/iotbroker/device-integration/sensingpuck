[[section-design-decisions]]
== Design Decisions

=== Other Ingress Aspects

Since often power consumption, which directly correlates with transmission duration, is a critical factor when designing IoT devices, not all incoming traffic can be feasibly transmitted over HTTP or WS connections.
Therefore, means of network communication other than HTTP/WS (which are routable on application layer protocol level) must be possible.
This can be achieved by using TCP traffic on dedicated ports for incoming data.

Currently, the _SensingPuck Adapter_ service requires accessibility over TCP/5683 to achieve communication with SensingPuck devices.
Additionally, communication with SensingPuck devices is secured via the Transport Layer Security (TLS) protocol with authentication based on pre-shared keys (PSKs).
