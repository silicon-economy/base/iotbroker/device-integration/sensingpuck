[[section-introduction-and-goals]]
== Introduction and Goals

=== Device Requirements

==== SensingPuck Requirements

A SensingPuck is a mobile (battery power supply) IoT Devices for observing the temperature of goods during transport ("in motion") and storage ("not in motion").
The observation is based on a Job (e.g.SensingPuck configuration).

A configuration includes the normal range of values for monitoring consisting of lower and upper limit.
If the limits are exceeded or not reached, an event is generated and sent.
In addition, a time interval is defined for monitoring "in motion", i.e. during transport, and for "not in motion" during storage.
During normal monitoring, temperature readings are taken according to the time interval.
The number of measured values is entered by another value in the configuration.
All normal measured values are transmitted together and not during one measurement.
The SensingPuck detects via an acceleration sensor whether the puck is currently being transported or stored.

.SensingPuck Adapter context
image::images/01_sensingpuck_adapter_context.png[sensingpuck adapter]

This section describes general requirements applicable to the SensingPuck device.
It describes all SensingPuck specifics.

*Abbreviations:*

* *SPI* = SensingPuck Infrastructure
* *SPG* = SensingPuck General
* *SPC* = SensingPuck Function

[cols="1,1,3,7",options="header"]
|===
|Id
|Added on
|Property
|Value

|SPG1
|25.01.2021
|communication protocol
|TCP

|SPG2
|25.01.2021
|message input format
|JSON

|SPG3
|25.01.2021
|message output format
|JSON

|SPI1
|25.01.2021
|infrastructure requirements
|Public route (port and a URI) of the SELE infrastructure for communication with the IoT Broker.

|SPC1
|22.02.2021
|functional requirement
|Only a SensingPuck initiates communication.

|SPC2
|22.02.2021
|functional requirement
|The IoT Broker answers on the open TCP connection.

|SPC3
|22.02.2021
|functional requirement
|The IoT Broker answers always the current or updated SensingPuck Job to any message from that SensingPuck.

|SPC4
|22.02.2021
|functional requirement
|The IoT Broker must store all SensingPuck Jobs.

|SPC5
|22.02.2021
|functional requirement
|The IoT Broker must store a history of all SensingPuck Jobs of a device.

|SPC6
|22.02.2021
|functional requirement
|The IoT Broker must accept SensingPuck Jobs from external Services without assignment to a SensingPuck (deviceId is empty).

|SPC7
|22.02.2021
|functional requirement
|The IoT Broker must accept mappings from external services from a SensingPuck Job with a SensingPuck (deviceId is set).
|===
