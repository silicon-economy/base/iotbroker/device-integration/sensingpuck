/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.*;

/**
 * Test cases for the {@link JobDtoToEntityConverter}.
 *
 * @author M. Grzenia
 */
class JobDtoToEntityConverterTest {

  /**
   * Class under test.
   */
  private JobDtoToEntityConverter converter;

  @BeforeEach
  void setUp() {
    converter = new JobDtoToEntityConverter(TestConstants.DEVICE_INTEGRATION_PROPERTIES);
  }

  @Test
  void convert_whenV1Dto_thenReturnsV1Entity() {
    // Arrange
    V1SensingPuckJobDto jobDto = v1JobDtoWithDefaults();

    // Act
    SensingPuckJobEntityBase jobEntity = converter.convert(jobDto);

    // Assert
    assertThat(jobEntity).isInstanceOf(V1SensingPuckJobEntity.class);
    V1SensingPuckJobEntity v1JobEntity = (V1SensingPuckJobEntity) jobEntity;
    SoftAssertions.assertSoftly(softly -> {
      // Check database-specific fields
      softly.assertThat(v1JobEntity.getId()).isEqualTo(String.valueOf(jobDto.getJobId()));
      softly.assertThat(v1JobEntity.getRevision()).isNull();
      softly.assertThat(v1JobEntity.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      softly.assertThat(v1JobEntity.getTenant()).isEqualTo(String.valueOf(jobDto.getDeviceId()));
      softly.assertThat(v1JobEntity.getCreatedTimestamp()).isEqualTo(Instant.EPOCH);
      softly.assertThat(v1JobEntity.getSentTimestamp()).isEqualTo(Instant.EPOCH);

      // Check job-specific fields
      softly.assertThat(v1JobEntity.getProtocolVersion()).isEqualTo(jobDto.getProtocolVersion());
      softly.assertThat(v1JobEntity.getJobId()).isEqualTo(jobDto.getJobId());
      softly.assertThat(v1JobEntity.getJobState()).isEqualTo(jobDto.getJobState());
      softly.assertThat(v1JobEntity.getDeviceId()).isEqualTo(jobDto.getDeviceId());
      softly.assertThat(v1JobEntity.isClearAlarm()).isEqualTo(jobDto.isClearAlarm());
      softly.assertThat(v1JobEntity.getMeasurementCycles()).isEqualTo(jobDto.getMeasurementCycles());
      softly.assertThat(v1JobEntity.getDataAmount()).isEqualTo(jobDto.getDataAmount());
      softly.assertThat(v1JobEntity.getTemperatureAlarm()).isEqualTo(jobDto.getTemperatureAlarm());
      softly.assertThat(v1JobEntity.getHumidityAlarm()).isEqualTo(jobDto.getHumidityAlarm());
      softly.assertThat(v1JobEntity.getLedTimings()).isEqualTo(jobDto.getLedTimings());
    });
  }


  @Test
  void convert_whenV2Dto_thenReturnsV2Entity() {
    // Arrange
    V2SensingPuckJobDto jobDto = v2JobDtoWithDefaults();

    // Act
    SensingPuckJobEntityBase jobEntity = converter.convert(jobDto);

    // Assert
    assertThat(jobEntity).isInstanceOf(V2SensingPuckJobEntity.class);
    V2SensingPuckJobEntity v2JobEntity = (V2SensingPuckJobEntity) jobEntity;
    SoftAssertions.assertSoftly(softly -> {
      // Check database-specific fields
      softly.assertThat(v2JobEntity.getId()).isEqualTo(String.valueOf(jobDto.getJobId()));
      softly.assertThat(v2JobEntity.getRevision()).isNull();
      softly.assertThat(v2JobEntity.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      softly.assertThat(v2JobEntity.getTenant()).isEqualTo(String.valueOf(jobDto.getDeviceId()));
      softly.assertThat(v2JobEntity.getCreatedTimestamp()).isEqualTo(Instant.EPOCH);
      softly.assertThat(v2JobEntity.getSentTimestamp()).isEqualTo(Instant.EPOCH);

      // Check job-specific fields
      softly.assertThat(v2JobEntity.getProtocolVersion()).isEqualTo(jobDto.getProtocolVersion());
      softly.assertThat(v2JobEntity.getJobId()).isEqualTo(jobDto.getJobId());
      softly.assertThat(v2JobEntity.getJobState()).isEqualTo(jobDto.getJobState());
      softly.assertThat(v2JobEntity.getDeviceId()).isEqualTo(jobDto.getDeviceId());
      softly.assertThat(v2JobEntity.isClearAlarm()).isEqualTo(jobDto.isClearAlarm());
      softly.assertThat(v2JobEntity.getMeasurementCycles()).isEqualTo(jobDto.getMeasurementCycles());
      softly.assertThat(v2JobEntity.getDataAmount()).isEqualTo(jobDto.getDataAmount());
      softly.assertThat(v2JobEntity.getTemperatureAlarm()).isEqualTo(jobDto.getTemperatureAlarm());
      softly.assertThat(v2JobEntity.getHumidityAlarm()).isEqualTo(jobDto.getHumidityAlarm());
      softly.assertThat(v2JobEntity.getVoltageAlarm()).isEqualTo(jobDto.getVoltageAlarm());
      softly.assertThat(v2JobEntity.getLedTimings()).isEqualTo(jobDto.getLedTimings());
    });
  }

  @Test
  void convert_whenV3Dto_thenReturnsV3Entity() {
    // Arrange
    V3SensingPuckJobDto jobDto = v3JobDtoWithDefaults();

    // Act
    SensingPuckJobEntityBase jobEntity = converter.convert(jobDto);

    // Assert
    assertThat(jobEntity).isInstanceOf(V3SensingPuckJobEntity.class);
    V3SensingPuckJobEntity v3JobEntity = (V3SensingPuckJobEntity) jobEntity;
    SoftAssertions.assertSoftly(softly -> {
      // Check database-specific fields
      softly.assertThat(v3JobEntity.getId()).isEqualTo(String.valueOf(jobDto.getJobId()));
      softly.assertThat(v3JobEntity.getRevision()).isNull();
      softly.assertThat(v3JobEntity.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      softly.assertThat(v3JobEntity.getTenant()).isEqualTo(String.valueOf(jobDto.getDeviceId()));
      softly.assertThat(v3JobEntity.getCreatedTimestamp()).isEqualTo(Instant.EPOCH);
      softly.assertThat(v3JobEntity.getSentTimestamp()).isEqualTo(Instant.EPOCH);

      // Check job-specific fields
      softly.assertThat(v3JobEntity.getProtocolVersion()).isEqualTo(jobDto.getProtocolVersion());
      softly.assertThat(v3JobEntity.getJobId()).isEqualTo(jobDto.getJobId());
      softly.assertThat(v3JobEntity.getJobState()).isEqualTo(jobDto.getJobState());
      softly.assertThat(v3JobEntity.getDeviceId()).isEqualTo(jobDto.getDeviceId());
      softly.assertThat(v3JobEntity.isClearAlarm()).isEqualTo(jobDto.isClearAlarm());
      softly.assertThat(v3JobEntity.getMeasurementCycles()).isEqualTo(jobDto.getMeasurementCycles());
      softly.assertThat(v3JobEntity.getDataAmount()).isEqualTo(jobDto.getDataAmount());
      softly.assertThat(v3JobEntity.getPackagingTime()).isEqualTo(jobDto.getPackagingTime());
      softly.assertThat(v3JobEntity.getTemperatureAlarm()).isEqualTo(jobDto.getTemperatureAlarm());
      softly.assertThat(v3JobEntity.getHumidityAlarm()).isEqualTo(jobDto.getHumidityAlarm());
      softly.assertThat(v3JobEntity.getVoltageAlarm()).isEqualTo(jobDto.getVoltageAlarm());
      softly.assertThat(v3JobEntity.getLightAlarm()).isEqualTo(jobDto.getLightAlarm());
      softly.assertThat(v3JobEntity.getLedTimings()).isEqualTo(jobDto.getLedTimings());
    });
  }

  @Test
  void convert_whenUnsupportedDtoType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckJobDtoBase jobDto = new UnsupportedSensingPuckJobDto();

    // Act & Assert
    assertThatThrownBy(() -> converter.convert(jobDto))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckJobDto
      extends SensingPuckJobDtoBase {
  }
}
