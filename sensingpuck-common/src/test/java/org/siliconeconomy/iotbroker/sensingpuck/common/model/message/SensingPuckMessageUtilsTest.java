/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.message;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.utils.SensingPuckMessageUtils;

import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

/**
 * Test cases for {@link SensingPuckMessageUtils}.
 *
 * @author M. Grzenia
 */
class SensingPuckMessageUtilsTest {

  /**
   * Class under test.
   */
  private SensingPuckMessageUtils sensingPuckMessageUtils;

  @BeforeEach
  void setUp() {
    sensingPuckMessageUtils
        = new SensingPuckMessageUtils(TestConstants.DEVICE_INTEGRATION_PROPERTIES);
  }

  @Test
  void generateEntityId() {
    // Arrange
    var deviceId = 42;

    // Act
    String result = sensingPuckMessageUtils.generateEntityId(deviceId);

    // Assert
    assertThat(result).startsWith("sensingpuck-42:");
    String messageId = result.split("sensingpuck-42:")[1];
    assertThatNoException().isThrownBy(() -> UUID.fromString(messageId));
  }

  @Test
  void extractMessageId() {
    // Arrange
    var id = "sensingpuck-42:36a83ab4-cf8f-416a-b226-801af1080913";

    // Act
    String result = sensingPuckMessageUtils.extractMessageId(id);

    // Assert
    assertThat(result).isEqualTo("36a83ab4-cf8f-416a-b226-801af1080913");
  }

  @Test
  void extractMessageId_invalidEntityIdPattern() {
    // Arrange
    var invalidId = "some-invalid-pattern";

    // Act & Assert
    assertThatThrownBy(() -> sensingPuckMessageUtils.extractMessageId(invalidId))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
