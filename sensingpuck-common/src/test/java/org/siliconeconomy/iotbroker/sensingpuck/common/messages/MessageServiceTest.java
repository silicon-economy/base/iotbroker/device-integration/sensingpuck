/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.messages;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.siliconeconomy.iotbroker.sensingpuck.common.converter.MessageDtoToEntityConverter;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.util.List;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.*;

/**
 * Test cases for {@link MessageService}.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@SpringBootTest(classes = {MessageService.class})
class MessageServiceTest {

  /**
   * Class under test.
   */
  @Autowired
  @InjectMocks
  private MessageService messageService;
  /**
   * Test dependencies.
   */
  @MockBean
  private MessageRepository messageRepository;
  @MockBean
  private MessageDtoToEntityConverter messageDtoToEntityConverter;

  @Test
  void testFindAllByDeviceIdAndJobIdAndTimestamp() {
    // Arrange
    List<SensingPuckMessageDtoBase> messages = List.of(v1MessageDtoWithDefaults(),
                                                       v2MessageDtoWithDefaults(),
                                                       v1MessageDtoWithDefaults(),
                                                       v2MessageDtoWithDefaults());
    List<SensingPuckMessageEntityBase> messageEntities = List.of(v1MessageEntityWithDefaults(),
                                                                 v2MessageEntityWithDefaults(),
                                                                 v1MessageEntityWithDefaults(),
                                                                 v2MessageEntityWithDefaults());
    when(messageRepository.findAllByJobIdAndTimestamp(anyString(), anyString(), anyBoolean(),
                                                      anyInt(), anyInt(), any(Instant.class),
                                                      any(Instant.class)))
        .thenReturn(messageEntities);

    // Act
    List<SensingPuckMessageDtoBase> result
        = messageService.findAllByDeviceIdAndJobIdAndTimestamp(1,
                                                               1,
                                                               Sort.Direction.ASC,
                                                               100,
                                                               50,
                                                               Instant.EPOCH,
                                                               Instant.ofEpochMilli(Long.MAX_VALUE));

    // Assert
    assertThat(result)
        .hasSize(4)
        .isEqualTo(messages);
  }

  @Test
  void testFindAllByDeviceIdAndTimestamp() {
    // Arrange
    List<SensingPuckMessageDtoBase> messages = List.of(v2MessageDtoWithDefaults(),
                                                       v1MessageDtoWithDefaults(),
                                                       v2MessageDtoWithDefaults());
    List<SensingPuckMessageEntityBase> messageEntities = List.of(v2MessageEntityWithDefaults(),
                                                                 v1MessageEntityWithDefaults(),
                                                                 v2MessageEntityWithDefaults());
    when(messageRepository.findAllByTimestamp(anyString(), anyBoolean(), anyInt(), anyInt(),
                                              any(Instant.class), any(Instant.class)))
        .thenReturn(messageEntities);

    // Act
    List<SensingPuckMessageDtoBase> result
        = messageService.findAllByDeviceIdAndTimestamp(2,
                                                       Sort.Direction.ASC,
                                                       100,
                                                       50,
                                                       Instant.EPOCH,
                                                       Instant.ofEpochMilli(Long.MAX_VALUE));

    // Assert
    assertThat(result)
        .hasSize(3)
        .isEqualTo(messages);
  }

  @Test
  void deleteAll_withFilter() {
    Predicate<SensingPuckMessageEntityBase> filter = entity -> false;

    messageService.deleteAll(filter);

    verify(messageRepository).deleteAll(filter);
  }

  @Test
  void deleteAll() {
    messageService.deleteAll();

    verify(messageRepository).deleteAll(isA(AllMessageEntitiesFilter.class));
  }
}
