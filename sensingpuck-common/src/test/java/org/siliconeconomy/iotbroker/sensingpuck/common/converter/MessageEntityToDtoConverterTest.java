/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.*;

/**
 * Test cases for {@link MessageEntityToDtoConverter}.
 *
 * @author M. Grzenia
 */
class MessageEntityToDtoConverterTest {

  /**
   * Class under test.
   */
  private MessageEntityToDtoConverter converter;

  @BeforeEach
  void setUp() {
    converter = new MessageEntityToDtoConverter();
  }

  @Test
  void convert_whenV1Entity_thenReturnsV1Dto() {
    // Arrange
    V1SensingPuckMessageEntity messageEntity = v1MessageEntityWithDefaults();

    // Act
    SensingPuckMessageDtoBase messageDto = converter.convert(messageEntity);

    // Assert
    assertThat(messageDto).isInstanceOf(V1SensingPuckMessageDto.class);
    V1SensingPuckMessageDto v1MessageDto = (V1SensingPuckMessageDto) messageDto;
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(v1MessageDto.getProtocolVersion()).isEqualTo(messageEntity.getProtocolVersion());
      softly.assertThat(v1MessageDto.getJobId()).isEqualTo(messageEntity.getJobId());
      softly.assertThat(v1MessageDto.getDeviceId()).isEqualTo(messageEntity.getDeviceId());
      softly.assertThat(v1MessageDto.getFirmwareDescriptor()).isEqualTo(messageEntity.getFirmwareDescriptor());
      softly.assertThat(v1MessageDto.getComTimestamp()).isEqualTo(messageEntity.getComTimestamp());
      softly.assertThat(v1MessageDto.getLastComCause()).isEqualTo(messageEntity.getLastComCause());
      softly.assertThat(v1MessageDto.getData()).isEqualTo(messageEntity.getData());
    });
  }

  @Test
  void convert_whenV2Entity_thenReturnsV2Dto() {
    // Arrange
    V2SensingPuckMessageEntity messageEntity = v2MessageEntityWithDefaults();

    // Act
    SensingPuckMessageDtoBase messageDto = converter.convert(messageEntity);

    // Assert
    assertThat(messageDto).isInstanceOf(V2SensingPuckMessageDto.class);
    V2SensingPuckMessageDto v2MessageDto = (V2SensingPuckMessageDto) messageDto;
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(v2MessageDto.getProtocolVersion()).isEqualTo(messageEntity.getProtocolVersion());
      softly.assertThat(v2MessageDto.getJobId()).isEqualTo(messageEntity.getJobId());
      softly.assertThat(v2MessageDto.getDeviceId()).isEqualTo(messageEntity.getDeviceId());
      softly.assertThat(v2MessageDto.getFirmwareDescriptor()).isEqualTo(messageEntity.getFirmwareDescriptor());
      softly.assertThat(v2MessageDto.getComTimestamp()).isEqualTo(messageEntity.getComTimestamp());
      softly.assertThat(v2MessageDto.getLastComCause()).isEqualTo(messageEntity.getLastComCause());
      softly.assertThat(v2MessageDto.getData()).isEqualTo(messageEntity.getData());
    });
  }

  @Test
  void convert_whenV3Entity_thenReturnsV3Dto() {
    // Arrange
    V3SensingPuckMessageEntity messageEntity = v3MessageEntityWithDefaults();

    // Act
    SensingPuckMessageDtoBase messageDto = converter.convert(messageEntity);

    // Assert
    assertThat(messageDto).isInstanceOf(V3SensingPuckMessageDto.class);
    V3SensingPuckMessageDto v3MessageDto = (V3SensingPuckMessageDto) messageDto;
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(v3MessageDto.getProtocolVersion()).isEqualTo(messageEntity.getProtocolVersion());
      softly.assertThat(v3MessageDto.getJobId()).isEqualTo(messageEntity.getJobId());
      softly.assertThat(v3MessageDto.getDeviceId()).isEqualTo(messageEntity.getDeviceId());
      softly.assertThat(v3MessageDto.getFirmwareDescriptor()).isEqualTo(messageEntity.getFirmwareDescriptor());
      softly.assertThat(v3MessageDto.getComTimestamp()).isEqualTo(messageEntity.getComTimestamp());
      softly.assertThat(v3MessageDto.getLastComCause()).isEqualTo(messageEntity.getLastComCause());
      softly.assertThat(v3MessageDto.getData()).isEqualTo(messageEntity.getData());
    });
  }

  @Test
  void convert_whenUnsupportedEntityType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckMessageEntityBase messageEntity = new UnsupportedSensingPuckMessageDto();

    // Act & Assert
    assertThatThrownBy(() -> converter.convert(messageEntity))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckMessageDto
      extends SensingPuckMessageEntityBase {
  }
}
