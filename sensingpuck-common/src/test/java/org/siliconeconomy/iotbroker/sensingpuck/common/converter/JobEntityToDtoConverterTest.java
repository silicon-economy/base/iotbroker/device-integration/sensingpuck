/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.*;

/**
 * Test cases for {@link JobEntityToDtoConverter}.
 *
 * @author M. Grzenia
 */
class JobEntityToDtoConverterTest {

  /**
   * Class under test.
   */
  private JobEntityToDtoConverter converter;

  @BeforeEach
  void setUp() {
    converter = new JobEntityToDtoConverter();
  }

  @Test
  void convert_whenV1Entity_thenReturnsV1Dto() {
    // Arrange
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();

    // Act
    SensingPuckJobDtoBase jobDto = converter.convert(jobEntity);

    // Assert
    assertThat(jobDto).isInstanceOf(V1SensingPuckJobDto.class);
    V1SensingPuckJobDto v1JobDto = (V1SensingPuckJobDto) jobDto;
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(v1JobDto.getProtocolVersion()).isEqualTo(jobEntity.getProtocolVersion());
      softly.assertThat(v1JobDto.getJobId()).isEqualTo(jobEntity.getJobId());
      softly.assertThat(v1JobDto.getJobState()).isEqualTo(jobEntity.getJobState());
      softly.assertThat(v1JobDto.getDeviceId()).isEqualTo(jobEntity.getDeviceId());
      softly.assertThat(v1JobDto.isClearAlarm()).isEqualTo(jobEntity.isClearAlarm());
      softly.assertThat(v1JobDto.getMeasurementCycles()).isEqualTo(jobEntity.getMeasurementCycles());
      softly.assertThat(v1JobDto.getDataAmount()).isEqualTo(jobEntity.getDataAmount());
      softly.assertThat(v1JobDto.getTemperatureAlarm()).isEqualTo(jobEntity.getTemperatureAlarm());
      softly.assertThat(v1JobDto.getHumidityAlarm()).isEqualTo(jobEntity.getHumidityAlarm());
      softly.assertThat(v1JobDto.getLedTimings()).isEqualTo(jobEntity.getLedTimings());
    });
  }

  @Test
  void convert_whenV2Entity_thenReturnsV2Dto() {
    // Arrange
    V2SensingPuckJobEntity jobEntity = v2JobEntityWithDefaults();

    // Act
    SensingPuckJobDtoBase jobDto = converter.convert(jobEntity);

    // Assert
    assertThat(jobDto).isInstanceOf(V2SensingPuckJobDto.class);
    V2SensingPuckJobDto v2JobDto = (V2SensingPuckJobDto) jobDto;
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(v2JobDto.getProtocolVersion()).isEqualTo(jobEntity.getProtocolVersion());
      softly.assertThat(v2JobDto.getJobId()).isEqualTo(jobEntity.getJobId());
      softly.assertThat(v2JobDto.getJobState()).isEqualTo(jobEntity.getJobState());
      softly.assertThat(v2JobDto.getDeviceId()).isEqualTo(jobEntity.getDeviceId());
      softly.assertThat(v2JobDto.isClearAlarm()).isEqualTo(jobEntity.isClearAlarm());
      softly.assertThat(v2JobDto.getMeasurementCycles()).isEqualTo(jobEntity.getMeasurementCycles());
      softly.assertThat(v2JobDto.getDataAmount()).isEqualTo(jobEntity.getDataAmount());
      softly.assertThat(v2JobDto.getTemperatureAlarm()).isEqualTo(jobEntity.getTemperatureAlarm());
      softly.assertThat(v2JobDto.getHumidityAlarm()).isEqualTo(jobEntity.getHumidityAlarm());
      softly.assertThat(v2JobDto.getVoltageAlarm()).isEqualTo(jobEntity.getVoltageAlarm());
      softly.assertThat(v2JobDto.getLedTimings()).isEqualTo(jobEntity.getLedTimings());
    });
  }

  @Test
  void convert_whenV3Entity_thenReturnsV3Dto() {
    // Arrange
    V3SensingPuckJobEntity jobEntity = v3JobEntityWithDefaults();

    // Act
    SensingPuckJobDtoBase jobDto = converter.convert(jobEntity);

    // Assert
    assertThat(jobDto).isInstanceOf(V3SensingPuckJobDto.class);
    V3SensingPuckJobDto v3JobDto = (V3SensingPuckJobDto) jobDto;
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(v3JobDto.getProtocolVersion()).isEqualTo(jobEntity.getProtocolVersion());
      softly.assertThat(v3JobDto.getJobId()).isEqualTo(jobEntity.getJobId());
      softly.assertThat(v3JobDto.getJobState()).isEqualTo(jobEntity.getJobState());
      softly.assertThat(v3JobDto.getDeviceId()).isEqualTo(jobEntity.getDeviceId());
      softly.assertThat(v3JobDto.isClearAlarm()).isEqualTo(jobEntity.isClearAlarm());
      softly.assertThat(v3JobDto.getMeasurementCycles()).isEqualTo(jobEntity.getMeasurementCycles());
      softly.assertThat(v3JobDto.getDataAmount()).isEqualTo(jobEntity.getDataAmount());
      softly.assertThat(v3JobDto.getPackagingTime()).isEqualTo(jobEntity.getPackagingTime());
      softly.assertThat(v3JobDto.getTemperatureAlarm()).isEqualTo(jobEntity.getTemperatureAlarm());
      softly.assertThat(v3JobDto.getHumidityAlarm()).isEqualTo(jobEntity.getHumidityAlarm());
      softly.assertThat(v3JobDto.getVoltageAlarm()).isEqualTo(jobEntity.getVoltageAlarm());
      softly.assertThat(v3JobDto.getLightAlarm()).isEqualTo(jobEntity.getLightAlarm());
      softly.assertThat(v3JobDto.getLedTimings()).isEqualTo(jobEntity.getLedTimings());
    });
  }

  @Test
  void convert_whenUnsupportedEntityType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckJobEntityBase jobEntity = new UnsupportedSensingPuckJobDto();

    // Act & Assert
    assertThatThrownBy(() -> converter.convert(jobEntity))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckJobDto
      extends SensingPuckJobEntityBase {
  }
}
