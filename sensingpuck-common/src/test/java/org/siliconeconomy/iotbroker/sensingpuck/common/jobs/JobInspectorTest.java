/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.jobEntityWithDefaults;

/**
 * Test cases for {@link JobInspector}.
 *
 * @author M. Grzenia
 */
class JobInspectorTest {

  /**
   * Class under test.
   */
  private JobInspector jobInspector;
  /**
   * Test dependencies.
   */
  private JobRepository jobRepository;

  @BeforeEach
  void setUp() {
    jobRepository = mock(JobRepository.class);
    jobInspector = new JobInspector(jobRepository);
  }

  @Test
  void checkCreateAllowed_whenDeviceHasNoActiveJob_thenThrowsNoException() {
    // Arrange
    when(jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(anyString(),
                                                                        eq(JobState.ACTIVE),
                                                                        anyBoolean()))
        .thenReturn(List.of());
    SensingPuckJobEntityBase checkEntity = jobEntityWithDefaults();
    checkEntity.setJobState(JobState.ACTIVE);

    // Act & Assert
    assertDoesNotThrow(() -> jobInspector.checkCreateAllowed(checkEntity));
  }

  @Test
  void checkCreateAllowed_whenDeviceHasActiveJob_thenThrowsException() {
    // Arrange
    SensingPuckJobEntityBase existingEntity = jobEntityWithDefaults();
    existingEntity.setJobId(1);
    existingEntity.setJobState(JobState.ACTIVE);
    SensingPuckJobEntityBase checkEntity = jobEntityWithDefaults();
    checkEntity.setJobId(2);
    checkEntity.setJobState(JobState.ACTIVE);
    when(jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(anyString(),
                                                                        eq(JobState.ACTIVE),
                                                                        anyBoolean()))
        .thenReturn(List.of(existingEntity));

    // Act & Assert
    assertThatThrownBy(() -> jobInspector.checkCreateAllowed(checkEntity))
        .isInstanceOf(DeviceHasActiveJobException.class);
  }

  @Test
  void checkUpdateAllowed_whenDeviceHasNoActiveJob_thenThrowsNoException() {
    // Arrange
    when(jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(anyString(),
                                                                        eq(JobState.ACTIVE),
                                                                        anyBoolean()))
        .thenReturn(List.of());
    SensingPuckJobEntityBase checkEntity = jobEntityWithDefaults();
    checkEntity.setJobState(JobState.ACTIVE);

    // Act & Assert
    assertDoesNotThrow(() -> jobInspector.checkUpdateAllowed(checkEntity));
  }

  @Test
  void checkUpdateAllowed_whenDeviceHasActiveJobAndUpdateForSameJob_thenThrowsNoException() {
    // Arrange
    SensingPuckJobEntityBase existingEntity = jobEntityWithDefaults();
    existingEntity.setJobId(3);
    existingEntity.setJobState(JobState.ACTIVE);
    SensingPuckJobEntityBase checkEntity = jobEntityWithDefaults();
    checkEntity.setJobId(3);
    checkEntity.setJobState(JobState.ACTIVE);
    when(jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(anyString(),
                                                                        eq(JobState.ACTIVE),
                                                                        anyBoolean()))
        .thenReturn(List.of(existingEntity));

    // Act & Assert
    assertDoesNotThrow(() -> jobInspector.checkUpdateAllowed(checkEntity));
  }

  @Test
  void checkUpdateAllowed_whenDeviceHasActiveJobAndActiveUpdateForDifferentJob_thenThrowsException() {
    // Arrange
    SensingPuckJobEntityBase existingEntity = jobEntityWithDefaults();
    existingEntity.setJobId(13);
    existingEntity.setJobState(JobState.ACTIVE);
    SensingPuckJobEntityBase checkEntity = jobEntityWithDefaults();
    checkEntity.setJobId(3);
    checkEntity.setJobState(JobState.ACTIVE);
    when(jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(anyString(),
                                                                        eq(JobState.ACTIVE),
                                                                        anyBoolean()))
        .thenReturn(List.of(existingEntity));

    // Act & Assert
    assertThatThrownBy(() -> jobInspector.checkUpdateAllowed(checkEntity))
        .isInstanceOf(DeviceHasActiveJobException.class);
  }
}
