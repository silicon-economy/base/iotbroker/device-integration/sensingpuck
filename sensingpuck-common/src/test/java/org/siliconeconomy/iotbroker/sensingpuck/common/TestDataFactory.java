/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.FirmwareDescriptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;

import java.time.Instant;
import java.util.ArrayList;

/**
 * A factory that provides convenient methods for creating test data/objects.
 *
 * @author M. Grzenia
 */
public class TestDataFactory {

  public static V1SensingPuckMessageDto v1MessageDtoWithDefaults() {
    return new V1SensingPuckMessageDto(
        1,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V1SensingPuckMessageEntity v1MessageEntityWithDefaults() {
    return new V1SensingPuckMessageEntity(
        "sensingpuck-1:36a83ab4-cf8f-416a-b226-801af1080913",
        "some-revision",
        1,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  /**
   * A factory method for instances of {@link SensingPuckJobDtoBase}.
   * <p>
   * This factory method is specifically intended to be used in test cases where the exact
   * specialization of {@link SensingPuckJobDtoBase} doesn't matter.
   *
   * @return A {@link SensingPuckJobDtoBase} instance with defaults.
   */
  public static SensingPuckJobDtoBase jobDtoWithDefaults() {
    return new V1SensingPuckJobDto(
        1,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        new LedTimings(1, 5, 5)
    );
  }

  public static V1SensingPuckJobDto v1JobDtoWithDefaults() {
    return new V1SensingPuckJobDto(
        1,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        new LedTimings(1, 5, 5)
    );
  }

  /**
   * A factory method for instances of {@link SensingPuckJobEntityBase}.
   * <p>
   * This factory method is specifically intended to be used in test cases where the exact
   * specialization of {@link SensingPuckJobEntityBase} doesn't matter.
   *
   * @return A {@link SensingPuckJobEntityBase} instance with defaults.
   */
  public static SensingPuckJobEntityBase jobEntityWithDefaults() {
    return new V1SensingPuckJobEntity(
        "1",
        "rev-1",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        Instant.EPOCH,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        new LedTimings(1, 5, 5)
    );
  }

  public static V1SensingPuckJobEntity v1JobEntityWithDefaults() {
    return new V1SensingPuckJobEntity(
        "1",
        "rev-1",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        Instant.EPOCH,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        new LedTimings(1, 5, 5)
    );
  }

  public static V2SensingPuckMessageDto v2MessageDtoWithDefaults() {
    return new V2SensingPuckMessageDto(
        2,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V2SensingPuckMessageEntity v2MessageEntityWithDefaults() {
    return new V2SensingPuckMessageEntity(
        "sensingpuck-1:36a83ab4-cf8f-416a-b226-801af1080913",
        "some-revision",
        2,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V2SensingPuckJobDto v2JobDtoWithDefaults() {
    return new V2SensingPuckJobDto(
        2,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        3.0f,
        new LedTimings(1, 5, 5)
    );
  }

  public static V2SensingPuckJobEntity v2JobEntityWithDefaults() {
    return new V2SensingPuckJobEntity(
        "1",
        "rev-1",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        Instant.EPOCH,
        2,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        3.0f,
        new LedTimings(1, 5, 5)
    );
  }

  public static V3SensingPuckMessageDto v3MessageDtoWithDefaults() {
    return new V3SensingPuckMessageDto(
        3,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V3SensingPuckMessageEntity v3MessageEntityWithDefaults() {
    return new V3SensingPuckMessageEntity(
        "sensingpuck-1:36a83ab4-cf8f-416a-b226-801af1080913",
        "some-revision",
        3,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V3SensingPuckJobDto v3JobDtoWithDefaults() {
    return new V3SensingPuckJobDto(
        3,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        1000,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        3.0f,
        new LightAlarm(100.0f, 200.0f),
        new LedTimings(1, 5, 5)
    );
  }

  public static V3SensingPuckJobEntity v3JobEntityWithDefaults() {
    return new V3SensingPuckJobEntity(
        "1",
        "rev-1",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        Instant.EPOCH,
        3,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        1000,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        3.0f,
        new LightAlarm(100.0f, 200.0f),
        new LedTimings(1, 5, 5)
    );
  }
}
