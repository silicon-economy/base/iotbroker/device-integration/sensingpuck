/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.ektorp.BulkDeleteDocument;
import org.ektorp.ViewQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.couchdb.PartitionedCouchDbConnector;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.array;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link MessageRepository}.
 *
 * @author M. Grzenia
 */
class MessageRepositoryTest {

  /**
   * Class under test.
   */
  private MessageRepository repository;
  /**
   * Test dependencies.
   */
  private PartitionedCouchDbConnector couchDbConnector;

  @BeforeEach
  void setUp() {
    couchDbConnector = mock(PartitionedCouchDbConnector.class);
    ObjectMapper objectMapper = new ObjectMapper()
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new JavaTimeModule());

    repository = new MessageRepository(couchDbConnector,
                                       objectMapper,
                                       TestConstants.DEVICE_INTEGRATION_PROPERTIES);
  }

  @Test
  void findAllByJobIdAndTimestamp_sortAscending() {
    // Arrange
    Instant startTime = Instant.parse("2021-08-16T08:35:05.0Z");
    Instant endTime = Instant.parse("2021-08-16T08:35:15.0Z");

    // Act
    repository.findAllByJobIdAndTimestamp(
        "sometenant",
        "somejob",
        false,
        0,
        10,
        startTime,
        endTime
    );

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckMessageEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getStartKey,
            ViewQuery::getEndKey,
            ViewQuery::isDescending,
            ViewQuery::getSkip,
            ViewQuery::getLimit,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_partition/sensingpuck-sometenant/_design/messages",
            "by-jobId-and-timestamp",
            array("somejob", startTime),
            array("somejob", endTime),
            false,
            0,
            10,
            true
        );
  }

  @Test
  void findAllByJobIdAndTimestamp_sortDescending() {
    // Arrange
    Instant startTime = Instant.parse("2021-08-16T08:35:05.0Z");
    Instant endTime = Instant.parse("2021-08-16T08:35:15.0Z");

    // Act
    repository.findAllByJobIdAndTimestamp(
        "sometenant",
        "somejob",
        true,
        0,
        10,
        startTime,
        endTime
    );

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckMessageEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getStartKey,
            ViewQuery::getEndKey,
            ViewQuery::isDescending,
            ViewQuery::getSkip,
            ViewQuery::getLimit,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_partition/sensingpuck-sometenant/_design/messages",
            "by-jobId-and-timestamp",
            array("somejob", endTime),
            array("somejob", startTime),
            true,
            0,
            10,
            true
        );
  }

  @Test
  void findAllByTimestamp_sortAscending() {
    // Arrange
    Instant startTime = Instant.parse("2021-08-16T08:35:05.0Z");
    Instant endTime = Instant.parse("2021-08-16T08:35:15.0Z");

    // Act
    repository.findAllByTimestamp(
        "sometenant",
        false,
        0,
        10,
        startTime,
        endTime
    );

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckMessageEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getStartKey,
            ViewQuery::getEndKey,
            ViewQuery::isDescending,
            ViewQuery::getSkip,
            ViewQuery::getLimit,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_partition/sensingpuck-sometenant/_design/messages",
            "by-timestamp",
            startTime,
            endTime,
            false,
            0,
            10,
            true
        );
  }

  @Test
  void findAllByTimestamp_sortDescending() {
    // Arrange
    Instant startTime = Instant.parse("2021-08-16T08:35:05.0Z");
    Instant endTime = Instant.parse("2021-08-16T08:35:15.0Z");

    // Act
    repository.findAllByTimestamp(
        "sometenant",
        true,
        0,
        10,
        startTime,
        endTime
    );

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckMessageEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getStartKey,
            ViewQuery::getEndKey,
            ViewQuery::isDescending,
            ViewQuery::getSkip,
            ViewQuery::getLimit,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_partition/sensingpuck-sometenant/_design/messages",
            "by-timestamp",
            endTime,
            startTime,
            true,
            0,
            10,
            true
        );
  }

  @Test
  void deleteAll() {
    // Arrange
    SensingPuckMessageEntityBase entity = new SensingPuckMessageEntityBase();
    entity.setId("42");
    when(couchDbConnector.getAllDocIds()).thenReturn(List.of("42"));
    when(couchDbConnector.get(any(Class.class), anyString())).thenReturn(entity);

    // Act
    repository.deleteAll(e -> true);

    // Assert & Verify
    ArgumentCaptor<Collection<BulkDeleteDocument>> deleteDocumentsCaptor
        = ArgumentCaptor.forClass(Collection.class);
    verify(couchDbConnector).executeBulk(deleteDocumentsCaptor.capture());
    assertThat(deleteDocumentsCaptor.getValue())
        .hasSize(1)
        .extracting(BulkDeleteDocument::getId)
        .contains("42");
  }
}
