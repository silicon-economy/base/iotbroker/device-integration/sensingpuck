/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.utils.SensingPuckMessageUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.*;

/**
 * Test cases for {@link MessageDtoToEntityConverter}.
 *
 * @author M. Grzenia
 */
class MessageDtoToEntityConverterTest {

  /**
   * Class under test.
   */
  private MessageDtoToEntityConverter converter;
  /**
   * Test dependencies.
   */
  private SensingPuckMessageUtils sensingPuckMessageUtils;

  @BeforeEach
  void setUp() {
    sensingPuckMessageUtils
        = new SensingPuckMessageUtils(TestConstants.DEVICE_INTEGRATION_PROPERTIES);
    converter = new MessageDtoToEntityConverter(sensingPuckMessageUtils);
  }

  @Test
  void convert_whenV1Dto_thenReturnsV1Entity() {
    // Arrange
    V1SensingPuckMessageDto messageDto = v1MessageDtoWithDefaults();

    // Act
    SensingPuckMessageEntityBase messageEntity = converter.convert(messageDto);

    // Assert
    assertThat(messageEntity).isInstanceOf(V1SensingPuckMessageEntity.class);
    V1SensingPuckMessageEntity v1MessageEntity = (V1SensingPuckMessageEntity) messageEntity;
    SoftAssertions.assertSoftly(softly -> {
      // Check database-specific fields
      softly.assertThat(v1MessageEntity.getId())
          .matches(sensingPuckMessageUtils.getEntityIdPattern());
      softly.assertThat(v1MessageEntity.getRevision()).isNull();

      // Check message-specific fields
      softly.assertThat(v1MessageEntity.getProtocolVersion()).isEqualTo(messageDto.getProtocolVersion());
      softly.assertThat(v1MessageEntity.getJobId()).isEqualTo(messageDto.getJobId());
      softly.assertThat(v1MessageEntity.getDeviceId()).isEqualTo(messageDto.getDeviceId());
      softly.assertThat(v1MessageEntity.getFirmwareDescriptor()).isEqualTo(messageDto.getFirmwareDescriptor());
      softly.assertThat(v1MessageEntity.getComTimestamp()).isEqualTo(messageDto.getComTimestamp());
      softly.assertThat(v1MessageEntity.getLastComCause()).isEqualTo(messageDto.getLastComCause());
      softly.assertThat(v1MessageEntity.getData()).isEqualTo(messageDto.getData());
    });
  }

  @Test
  void convert_whenV2Dto_thenReturnsV2Entity() {
    // Arrange
    V2SensingPuckMessageDto messageDto = v2MessageDtoWithDefaults();

    // Act
    SensingPuckMessageEntityBase messageEntity = converter.convert(messageDto);

    // Assert
    assertThat(messageEntity).isInstanceOf(V2SensingPuckMessageEntity.class);
    V2SensingPuckMessageEntity v2MessageEntity = (V2SensingPuckMessageEntity) messageEntity;
    SoftAssertions.assertSoftly(softly -> {
      // Check database-specific fields
      softly.assertThat(v2MessageEntity.getId())
          .matches(sensingPuckMessageUtils.getEntityIdPattern());
      softly.assertThat(v2MessageEntity.getRevision()).isNull();

      // Check message-specific fields
      softly.assertThat(v2MessageEntity.getProtocolVersion()).isEqualTo(messageDto.getProtocolVersion());
      softly.assertThat(v2MessageEntity.getJobId()).isEqualTo(messageDto.getJobId());
      softly.assertThat(v2MessageEntity.getDeviceId()).isEqualTo(messageDto.getDeviceId());
      softly.assertThat(v2MessageEntity.getFirmwareDescriptor()).isEqualTo(messageDto.getFirmwareDescriptor());
      softly.assertThat(v2MessageEntity.getComTimestamp()).isEqualTo(messageDto.getComTimestamp());
      softly.assertThat(v2MessageEntity.getLastComCause()).isEqualTo(messageDto.getLastComCause());
      softly.assertThat(v2MessageEntity.getData()).isEqualTo(messageDto.getData());
    });
  }

  @Test
  void convert_whenV3Dto_thenReturnsV3Entity() {
    // Arrange
    V3SensingPuckMessageDto messageDto = v3MessageDtoWithDefaults();

    // Act
    SensingPuckMessageEntityBase messageEntity = converter.convert(messageDto);

    // Assert
    assertThat(messageEntity).isInstanceOf(V3SensingPuckMessageEntity.class);
    V3SensingPuckMessageEntity v3MessageEntity = (V3SensingPuckMessageEntity) messageEntity;
    SoftAssertions.assertSoftly(softly -> {
      // Check database-specific fields
      softly.assertThat(v3MessageEntity.getId())
          .matches(sensingPuckMessageUtils.getEntityIdPattern());
      softly.assertThat(v3MessageEntity.getRevision()).isNull();

      // Check message-specific fields
      softly.assertThat(v3MessageEntity.getProtocolVersion()).isEqualTo(messageDto.getProtocolVersion());
      softly.assertThat(v3MessageEntity.getJobId()).isEqualTo(messageDto.getJobId());
      softly.assertThat(v3MessageEntity.getDeviceId()).isEqualTo(messageDto.getDeviceId());
      softly.assertThat(v3MessageEntity.getFirmwareDescriptor()).isEqualTo(messageDto.getFirmwareDescriptor());
      softly.assertThat(v3MessageEntity.getComTimestamp()).isEqualTo(messageDto.getComTimestamp());
      softly.assertThat(v3MessageEntity.getLastComCause()).isEqualTo(messageDto.getLastComCause());
      softly.assertThat(v3MessageEntity.getData()).isEqualTo(messageDto.getData());
    });
  }

  @Test
  void convert_whenUnsupportedDtoType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckMessageDtoBase messageDto = new UnsupportedSensingPuckMessageDto();

    // Act & Assert
    assertThatThrownBy(() -> converter.convert(messageDto))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckMessageDto
      extends SensingPuckMessageDtoBase {
  }
}
