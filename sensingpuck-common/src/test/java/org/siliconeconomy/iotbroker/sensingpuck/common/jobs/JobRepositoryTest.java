/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.ektorp.BulkDeleteDocument;
import org.ektorp.CouchDbConnector;
import org.ektorp.DocumentNotFoundException;
import org.ektorp.ViewQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.couchdb.CouchDbQueryConstants;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.array;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link JobRepository}.
 *
 * @author M. Grzenia
 */
class JobRepositoryTest {

  /**
   * Class under test.
   */
  private JobRepository repository;
  /**
   * Test dependencies.
   */
  private CouchDbConnector couchDbConnector;

  @BeforeEach
  void setUp() {
    couchDbConnector = mock(CouchDbConnector.class);
    ObjectMapper objectMapper = new ObjectMapper()
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new JavaTimeModule());

    repository = new JobRepository(couchDbConnector, objectMapper);
  }

  @Test
  void findAllByTenantAndJobStateSortByCreatedTimestamp_sortAscending() {
    // Act
    repository.findAllByTenantAndJobStateSortByCreatedTimestamp(
        "sometenant",
        JobState.ACTIVE,
        false
    );

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckJobEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getStartKey,
            ViewQuery::getEndKey,
            ViewQuery::isDescending,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_design/jobs",
            "by-tenant-and-jobstate",
            array("sometenant", JobState.ACTIVE),
            array("sometenant", JobState.ACTIVE, CouchDbQueryConstants.HIGH_KEY_VALUE),
            false,
            true
        );
  }

  @Test
  void findAllByTenantAndJobStateSortByCreatedTimestamp_sortDescending() {
    // Act
    repository.findAllByTenantAndJobStateSortByCreatedTimestamp(
        "sometenant",
        JobState.ACTIVE,
        true
    );

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckJobEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getStartKey,
            ViewQuery::getEndKey,
            ViewQuery::isDescending,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_design/jobs",
            "by-tenant-and-jobstate",
            array("sometenant", JobState.ACTIVE, CouchDbQueryConstants.HIGH_KEY_VALUE),
            array("sometenant", JobState.ACTIVE),
            true,
            true
        );
  }

  @Test
  void findAllByDeviceId() {
    // Act
    repository.findAllByDeviceId(42);

    // Assert & Verify
    ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
    verify(couchDbConnector)
        .queryView(viewQueryCaptor.capture(), eq(SensingPuckJobEntityBase.class));
    assertThat(viewQueryCaptor.getValue())
        .extracting(
            ViewQuery::getDesignDocId,
            ViewQuery::getViewName,
            ViewQuery::getKey,
            ViewQuery::isIncludeDocs
        )
        .containsExactly(
            "_design/jobs",
            "by-tenant",
            "42",
            true
        );
  }

  @Test
  void findById() {
    // Arrange
    SensingPuckJobEntityBase entity = new SensingPuckJobEntityBase();
    entity.setJobId(42);
    when(couchDbConnector.get(any(Class.class), anyString())).thenReturn(entity);

    // Act
    Optional<SensingPuckJobEntityBase> result = repository.findById("42");

    // Assert & Verify
    verify(couchDbConnector).get(SensingPuckJobEntityBase.class, "42");
    assertThat(result).isPresent().contains(entity);
  }

  @Test
  void findById_jobNotFound() {
    // Arrange
    when(couchDbConnector.get(any(Class.class), anyString()))
        .thenThrow(new DocumentNotFoundException(""));

    // Act
    Optional<SensingPuckJobEntityBase> result = repository.findById("42");

    // Assert & Verify
    verify(couchDbConnector).get(SensingPuckJobEntityBase.class, "42");
    assertThat(result).isEmpty();
  }

  @Test
  void deleteAll() {
    // Arrange
    SensingPuckJobEntityBase entity = new SensingPuckJobEntityBase();
    entity.setJobId(42);
    when(couchDbConnector.getAllDocIds()).thenReturn(List.of("42"));
    when(couchDbConnector.get(any(Class.class), anyString())).thenReturn(entity);

    // Act
    repository.deleteAll(e -> true);

    // Assert & Verify
    ArgumentCaptor<Collection<BulkDeleteDocument>> deleteDocumentsCaptor
        = ArgumentCaptor.forClass(Collection.class);
    verify(couchDbConnector).executeBulk(deleteDocumentsCaptor.capture());
    assertThat(deleteDocumentsCaptor.getValue())
        .hasSize(1)
        .extracting(BulkDeleteDocument::getId)
        .contains("42");
  }
}
