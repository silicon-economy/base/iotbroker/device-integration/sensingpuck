/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.ektorp.DocumentNotFoundException;
import org.ektorp.UpdateConflictException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.converter.JobDtoToEntityConverter;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;

import java.time.Instant;
import java.util.List;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.common.TestDataFactory.*;

/**
 * Test cases for {@link JobService}.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
class JobServiceTest {

  /**
   * Class under test.
   */
  private JobService service;
  /**
   * Test dependencies.
   */
  private JobRepository repository;
  private JobInspector jobInspector;

  @BeforeEach
  void setUp() {
    repository = mock(JobRepository.class);
    jobInspector = mock(JobInspector.class);
    service = new JobService(
        repository,
        jobInspector,
        new JobDtoToEntityConverter(TestConstants.DEVICE_INTEGRATION_PROPERTIES)
    );
  }

  @Test
  void findMostRecentActiveJob() {
    // Act
    service.findMostRecentActiveJob("some-tenant");

    // Assert & Verify
    verify(repository).findAllByTenantAndJobStateSortByCreatedTimestamp(
        "some-tenant",
        JobState.ACTIVE,
        true
    );
  }

  @Test
  void findMostRecentFinishedJob() {
    // Act
    service.findMostRecentFinishedJob("some-tenant");

    // Assert & Verify
    verify(repository).findAllByTenantAndJobStateSortByCreatedTimestamp(
        "some-tenant",
        JobState.FINISHED,
        true
    );
  }

  @Test
  void update() {
    // Arrange
    V1SensingPuckJobDto jobDto = v1JobDtoWithDefaults();
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();
    jobEntity.setRevision("existing-revision");
    Instant instant = Instant.now();
    jobEntity.setCreatedTimestamp(instant.minusSeconds(60));
    jobEntity.setSentTimestamp(instant.minusSeconds(30));

    when(repository.get(anyString())).thenReturn(jobEntity);
    when(repository.contains(jobEntity.getId())).thenReturn(true);

    // Act
    service.update(jobDto);

    // Assert & Verify
    verify(jobInspector).checkUpdateAllowed(any());
    ArgumentCaptor<SensingPuckJobEntityBase> jobCaptor
        = ArgumentCaptor.forClass(SensingPuckJobEntityBase.class);
    verify(repository).update(jobCaptor.capture());
    assertThat(jobCaptor.getValue())
        .extracting(SensingPuckJobEntityBase::getRevision,
                    SensingPuckJobEntityBase::getCreatedTimestamp,
                    SensingPuckJobEntityBase::getSentTimestamp)
        .contains("existing-revision",
                  instant.minusSeconds(60),
                  instant.minusSeconds(30));
  }

  @Test
  void update_whenJobDoesNotExist_thenThrowsException() {
    // Arrange
    SensingPuckJobDtoBase updateJobDto = jobDtoWithDefaults();
    when(repository.get(String.valueOf(updateJobDto.getJobId())))
        .thenThrow(DocumentNotFoundException.class);

    // Act & Assert
    assertThatThrownBy(() -> service.update(updateJobDto))
        .isInstanceOf(JobNotFoundException.class);

    // Verify
    verify(repository, never()).update(any());
  }

  @Test
  void update_whenDeviceHasAlreadyActiveJob_thenThrowsException() {
    // Arrange
    V1SensingPuckJobEntity existingEntity = v1JobEntityWithDefaults();
    V1SensingPuckJobDto updateJobDto = v1JobDtoWithDefaults();
    V1SensingPuckJobEntity updateJobEntity = v1JobEntityWithDefaults();
    when(repository.get(existingEntity.getId())).thenReturn(existingEntity);
    when(repository.contains(existingEntity.getId())).thenReturn(true);
    doThrow(DeviceHasActiveJobException.class)
        .when(jobInspector).checkUpdateAllowed(updateJobEntity);

    // Act & Assert
    assertThatThrownBy(() -> service.update(updateJobDto))
        .isInstanceOf(DeviceHasActiveJobException.class);

    // Verify
    verify(repository, never()).update(any());
  }

  @Test
  void add_whenJobCreateAllowed_thenCallsAddOnRepository() {
    // Act
    SensingPuckJobDtoBase jobDto = jobDtoWithDefaults();
    service.add(jobDto);

    // Assert & Verify
    verify(jobInspector).checkCreateAllowed(any());
    ArgumentCaptor<SensingPuckJobEntityBase> entityCaptor
        = ArgumentCaptor.forClass(SensingPuckJobEntityBase.class);
    verify(repository).add(entityCaptor.capture());
    assertThat(entityCaptor.getValue().getJobId()).isEqualTo(jobDto.getJobId());
    assertThat(entityCaptor.getValue().getCreatedTimestamp())
        .isAfter(Instant.EPOCH);
  }

  @Test
  void add_whenDeviceHasAlreadyActiveJob_thenThrowsException() {
    // Arrange
    SensingPuckJobDtoBase jobDto = jobDtoWithDefaults();
    doThrow(DeviceHasActiveJobException.class)
        .when(jobInspector).checkCreateAllowed(any(SensingPuckJobEntityBase.class));

    // Act & Assert
    assertThatThrownBy(() -> service.add(jobDto))
        .isInstanceOf(DeviceHasActiveJobException.class);

    // Verify
    verify(repository, never()).add(any());
  }

  @Test
  void testAdd_updateConflict() {
    // Arrange
    SensingPuckJobDtoBase jobDto = jobDtoWithDefaults();
    doThrow(UpdateConflictException.class).when(repository).add(any(SensingPuckJobEntityBase.class));

    // Act & Assert
    assertThatThrownBy(() -> service.add(jobDto))
        .isInstanceOf(JobUpdateConflictException.class);
  }

  @Test
  void update_whenJobUpdateAllowed_thenCallsUpdateOnRepository() {
    // Arrange
    SensingPuckJobEntityBase entity = jobEntityWithDefaults();
    when(repository.contains(entity.getId())).thenReturn(true);

    // Act
    service.update(entity);

    // Verify
    verify(repository, times(1)).update(entity);
  }

  @Test
  void testUpdate_updateConflict() {
    // Arrange
    SensingPuckJobEntityBase testEntity = jobEntityWithDefaults();
    when(repository.get(anyString())).thenReturn(testEntity);
    when(repository.contains(testEntity.getId())).thenReturn(true);
    doThrow(UpdateConflictException.class).when(repository).update(any(SensingPuckJobEntityBase.class));

    // Act & Assert
    assertThatThrownBy(() -> service.update(testEntity))
        .isInstanceOf(JobUpdateConflictException.class);
  }

  @Test
  void testUpdateOnNonExistingJob() {
    SensingPuckJobEntityBase testJob = jobEntityWithDefaults();

    when(repository.get(anyString())).thenThrow(new DocumentNotFoundException(""));

    assertThatThrownBy(() -> service.update(testJob)).isInstanceOf(JobNotFoundException.class);
    verify(repository, times(0)).update(testJob);
  }

  @Test
  void testGetJobIdsByDeviceId() {
    SensingPuckJobEntityBase job1 = jobEntityWithDefaults();
    job1.setDeviceId(1337);
    job1.setJobId(1);
    SensingPuckJobEntityBase job2 = jobEntityWithDefaults();
    job2.setDeviceId(1337);
    job2.setJobId(2);

    List<SensingPuckJobEntityBase> jobs = List.of(job1, job2);

    when(repository.findAllByDeviceId(anyLong())).thenReturn(jobs);

    Assertions.assertThat(service.getJobIdsByDeviceId(1337)).isEqualTo(List.of(1L, 2L));
  }

  @Test
  void testGetJobIdsByUnknownDeviceId() {
    List<SensingPuckJobEntityBase> jobs = List.of();
    when(repository.findAllByDeviceId(anyLong())).thenReturn(jobs);

    Assertions.assertThat(service.getJobIdsByDeviceId(1337)).isEqualTo(List.of());
  }

  @Test
  void testFindEntityByJobId() {
    // Arrange
    SensingPuckJobEntityBase job = jobEntityWithDefaults();
    when(repository.get(anyString())).thenReturn(job);

    // Act
    SensingPuckJobEntityBase result = service.findEntityByJobId(123);

    // Assert
    assertThat(result).isEqualTo(job);
  }

  @Test
  void testFindDtoByJobId() {
    // Arrange
    SensingPuckJobEntityBase entity = jobEntityWithDefaults();
    when(repository.get(entity.getId())).thenReturn(entity);

    // Act
    SensingPuckJobDtoBase result = service.findDtoByJobId(entity.getJobId());

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(result.getProtocolVersion()).isEqualTo(entity.getProtocolVersion());
      softly.assertThat(result.getJobId()).isEqualTo(entity.getJobId());
      softly.assertThat(result.getJobState()).isEqualTo(entity.getJobState());
      softly.assertThat(result.getDeviceId()).isEqualTo(entity.getDeviceId());
    });
    verify(repository).get(entity.getId());
  }

  @Test
  void testFindByJobId_jobNotFound() {
    // Arrange
    when(repository.get(anyString())).thenThrow(new DocumentNotFoundException(""));

    // Act & Assert
    assertThatThrownBy(() -> service.findEntityByJobId(123))
        .isInstanceOf(JobNotFoundException.class);
  }

  @Test
  void updateJobStatesFinished_whenJobsExist_thenUpdatesJobStates() {
    // Arrange
    SensingPuckJobEntityBase jobEntity1 = jobEntityWithDefaults();
    jobEntity1.setJobState(JobState.ACTIVE);
    SensingPuckJobEntityBase jobEntity2 = jobEntityWithDefaults();
    jobEntity2.setJobState(JobState.ACTIVE);
    List<SensingPuckJobEntityBase> jobEntities = List.of(jobEntity1, jobEntity2);
    when(repository.findAllByDeviceId(anyLong())).thenReturn(jobEntities);
    when(repository.contains(anyString())).thenReturn(true);

    // Act
    service.updateJobStatesFinished(1);

    // Assert & Verify
    ArgumentCaptor<SensingPuckJobEntityBase> jobEntityCaptor
        = ArgumentCaptor.forClass(SensingPuckJobEntityBase.class);
    verify(repository, times(2)).update(jobEntityCaptor.capture());
    assertThat(jobEntityCaptor.getAllValues())
        .hasSize(2)
        .extracting(SensingPuckJobEntityBase::getJobState)
        .allMatch(jobState -> jobState == JobState.FINISHED);
  }

  @Test
  void updateJobStatesFinished_whenNoJobsExist_thenDoesNothing() {
    // Arrange
    when(repository.findAllByDeviceId(anyLong())).thenReturn(List.of());

    // Act
    service.updateJobStatesFinished(1);

    // Verify
    verify(repository).findAllByDeviceId(1);
    verifyNoMoreInteractions(repository);
  }

  @Test
  void deleteAll_withFilter() {
    Predicate<SensingPuckJobEntityBase> filter = entity -> false;

    service.deleteAll(filter);

    verify(repository).deleteAll(filter);
  }

  @Test
  void deleteAll() {
    service.deleteAll();

    verify(repository).deleteAll(isA(AllJobEntitiesFilter.class));
  }
}
