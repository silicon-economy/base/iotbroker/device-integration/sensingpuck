/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

/**
 * Thrown to indicate a conflict when adding/updating a job because there's already an active job
 * associated with a specific device.
 * <p>
 * Only one active job may be assigned to a device at any time.
 *
 * @author M. Grzenia
 */
public class DeviceHasActiveJobException
    extends RuntimeException {

  public DeviceHasActiveJobException(long deviceId, long jobId) {
    super(String.format("The device with the ID '%d' already has an active job assigned to it: %d",
                        deviceId,
                        jobId));
  }
}
