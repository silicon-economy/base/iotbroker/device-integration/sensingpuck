/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.config;

import lombok.Getter;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import static java.util.Objects.requireNonNull;

/**
 * Defines CouchDB-related (configuration) properties (that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "couchdb")
@ConstructorBinding
@Getter
public class CouchDbProperties {

  private final String url;
  private final String username;
  private final String password;
  private final String jobDatabaseName;
  private final String messageDatabaseName;

  /**
   * Creates a new instance.
   *
   * @param url                 The URL to the CouchDB instance.
   * @param username            The username for the CouchDB instance.
   * @param password            The password for the user of the CouchDB instance.
   * @param jobDatabaseName     The database name in which to store
   *                            {@link SensingPuckJobEntityBase}.
   * @param messageDatabaseName The database name in which to store
   *                            {@link SensingPuckMessageEntityBase}.
   */
  public CouchDbProperties(String url,
                           String username,
                           String password,
                           String jobDatabaseName,
                           String messageDatabaseName) {
    this.url = requireNonNull(url, "url");
    this.username = requireNonNull(username, "username");
    this.password = requireNonNull(password, "password");
    this.jobDatabaseName = requireNonNull(jobDatabaseName, "jobDatabaseName");
    this.messageDatabaseName = requireNonNull(messageDatabaseName, "messageDatabaseName");
  }
}
