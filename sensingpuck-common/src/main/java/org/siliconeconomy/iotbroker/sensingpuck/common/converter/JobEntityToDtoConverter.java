/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

/**
 * Provides methods for converting {@link SensingPuckJobEntityBase} instances to
 * {@link SensingPuckJobDtoBase} instances.
 *
 * @author M. Grzenia
 */
public class JobEntityToDtoConverter {

  /**
   * Converts the given {@link SensingPuckJobEntityBase} to an instance of
   * {@link SensingPuckJobDtoBase} by considering their respective subtypes.
   *
   * @param entity The {@link SensingPuckJobDtoBase} to convert.
   * @return The converted {@link SensingPuckJobDtoBase}.
   * @throws IllegalArgumentException If the given {@link SensingPuckJobEntityBase} is of an
   *                                  unhandled type.
   */
  public SensingPuckJobDtoBase convert(SensingPuckJobEntityBase entity) {
    if (entity instanceof V1SensingPuckJobEntity) {
      return convert((V1SensingPuckJobEntity) entity);
    }
    if (entity instanceof V2SensingPuckJobEntity) {
      return convert((V2SensingPuckJobEntity) entity);
    }
    if (entity instanceof V3SensingPuckJobEntity) {
      return convert((V3SensingPuckJobEntity) entity);
    }

    throw new IllegalArgumentException("Unknown type: " + entity.getClass());
  }

  private V1SensingPuckJobDto convert(V1SensingPuckJobEntity entity) {
    return new V1SensingPuckJobDto(entity.getProtocolVersion(),
                                   entity.getJobId(),
                                   entity.getJobState(),
                                   entity.getDeviceId(),
                                   entity.isClearAlarm(),
                                   entity.getMeasurementCycles(),
                                   entity.getDataAmount(),
                                   entity.getTemperatureAlarm(),
                                   entity.getHumidityAlarm(),
                                   entity.getLedTimings());
  }

  private V2SensingPuckJobDto convert(V2SensingPuckJobEntity entity) {
    return new V2SensingPuckJobDto(entity.getProtocolVersion(),
                                   entity.getJobId(),
                                   entity.getJobState(),
                                   entity.getDeviceId(),
                                   entity.isClearAlarm(),
                                   entity.getMeasurementCycles(),
                                   entity.getDataAmount(),
                                   entity.getTemperatureAlarm(),
                                   entity.getHumidityAlarm(),
                                   entity.getVoltageAlarm(),
                                   entity.getLedTimings());
  }

  private V3SensingPuckJobDto convert(V3SensingPuckJobEntity entity) {
    return new V3SensingPuckJobDto(entity.getProtocolVersion(),
                                   entity.getJobId(),
                                   entity.getJobState(),
                                   entity.getDeviceId(),
                                   entity.isClearAlarm(),
                                   entity.getMeasurementCycles(),
                                   entity.getDataAmount(),
                                   entity.getPackagingTime(),
                                   entity.getTemperatureAlarm(),
                                   entity.getHumidityAlarm(),
                                   entity.getVoltageAlarm(),
                                   entity.getLightAlarm(),
                                   entity.getLedTimings());
  }
}
