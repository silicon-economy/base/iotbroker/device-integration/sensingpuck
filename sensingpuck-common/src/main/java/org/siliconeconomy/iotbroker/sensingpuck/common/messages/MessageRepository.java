/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.BulkDeleteDocument;
import org.ektorp.DocumentOperationResult;
import org.ektorp.ViewQuery;
import org.ektorp.support.View;
import org.siliconeconomy.iotbroker.sensingpuck.common.SensingPuckDbConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.couchdb.PartitionedCouchDbConnector;
import org.siliconeconomy.iotbroker.couchdb.PartitionedRepositorySupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Provides database access for {@link SensingPuckMessageEntityBase}s.
 * <p>
 * Provides basic CRUD operations and methods for more complex database queries using CouchDB views.
 *
 * @author D. Ronnenberg
 */
@Repository
public class MessageRepository
    extends PartitionedRepositorySupport<SensingPuckMessageEntityBase> {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(MessageRepository.class);
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param db                          The database that the operations are being performed on.
   * @param viewQueryObjectMapper       {@link ObjectMapper} to convert parameters of the
   *                                    {@link ViewQuery} to JSON.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  @Autowired
  public MessageRepository(@Qualifier("messageDatabase") PartitionedCouchDbConnector db,
                           @Qualifier("viewQueryObjectMapper") ObjectMapper viewQueryObjectMapper,
                           DeviceIntegrationProperties deviceIntegrationProperties) {
    super(SensingPuckMessageEntityBase.class,
          db,
          SensingPuckDbConstants.MESSAGES_DESIGN_DOC_NAME,
          viewQueryObjectMapper);

    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");

    // Create or update design documents
    initStandardDesignDocument();
  }

  /**
   * Query {@link SensingPuckMessageEntityBase}s based on deviceId (=tenant) and jobId. This
   * function also supports pagination using the additional parameters.
   *
   * @param tenant     The tenant to query, also known as the deviceId.
   * @param jobId      The id of the job that is connected to the wanted messages.
   * @param descending Flag that indicates a descending order should be used.
   * @param offset     Number of elements to ignore at the beginning.
   * @param limit      Maximal number of elements to return.
   * @param startTime  The lower limit of time that is of interest. All messages earlier will be
   *                   filtered out.
   * @param endTime    The upper limit of time that is of interest. All messages later will be
   *                   filtered out.
   * @return A list of {@link SensingPuckMessageEntityBase}.
   */
  @View(name = "by-jobId-and-timestamp",
      file = "by-jobId-and-timestamp.json")
  public List<SensingPuckMessageEntityBase> findAllByJobIdAndTimestamp(String tenant,
                                                                       String jobId,
                                                                       boolean descending,
                                                                       int offset,
                                                                       int limit,
                                                                       Instant startTime,
                                                                       Instant endTime) {
    var partition = String.format("%s-%s", deviceIntegrationProperties.getDeviceSource(), tenant);
    var query = createQuery("by-jobId-and-timestamp", partition)
        .skip(offset)
        .limit(limit)
        .includeDocs(true);

    if (!descending) {
      query = query
          .startKey(new Object[]{jobId, startTime})
          .endKey(new Object[]{jobId, endTime});
    } else {
      query = query
          .startKey(new Object[]{jobId, endTime})
          .endKey(new Object[]{jobId, startTime})
          .descending(true);
    }

    return db.queryView(query, SensingPuckMessageEntityBase.class);
  }

  /**
   * Query {@link SensingPuckMessageEntityBase}s from single device, regardless of the job. This
   * function also supports pagination using the additional parameters.
   *
   * @param tenant     The tenant to query, also known as the deviceId.
   * @param descending Flag that indicates a descending order should be used.
   * @param offset     Number of elements to ignore at the beginning.
   * @param limit      Maximal number of elements to return.
   * @param startTime  The lower limit of time that is of interest. All messages earlier will be
   *                   filtered out.
   * @param endTime    The upper limit of time that is of interest. All messages later will be
   *                   filtered out.
   * @return A list of {@link SensingPuckMessageEntityBase}.
   */
  @View(name = "by-timestamp", file = "by-timestamp.json")
  public List<SensingPuckMessageEntityBase> findAllByTimestamp(String tenant,
                                                               boolean descending,
                                                               int offset,
                                                               int limit,
                                                               Instant startTime,
                                                               Instant endTime) {
    var partition = String.format("%s-%s", deviceIntegrationProperties.getDeviceSource(), tenant);
    var query = createQuery("by-timestamp", partition)
        .skip(offset)
        .limit(limit)
        .includeDocs(true);

    if (!descending) {
      query = query
          .startKey(startTime)
          .endKey(endTime);
    } else {
      query = query
          .startKey(endTime)
          .endKey(startTime)
          .descending(true);
    }

    return db.queryView(query, SensingPuckMessageEntityBase.class);
  }

  /**
   * Deletes all {@link SensingPuckMessageEntityBase}s in the database that evaluate to {@code true}
   * for the given predicate.
   *
   * @param deletePredicate The predicate to use for selecting {@link SensingPuckMessageEntityBase}s
   *                        to delete.
   */
  public void deleteAll(Predicate<SensingPuckMessageEntityBase> deletePredicate) {
    List<BulkDeleteDocument> documentsToDelete = this.getAll().stream()
        .filter(deletePredicate)
        .map(BulkDeleteDocument::of)
        .collect(Collectors.toList());

    List<DocumentOperationResult> operationsResults = db.executeBulk(documentsToDelete);

    if (!operationsResults.isEmpty()) {
      LOG.warn("Errors occurred while deleting documents: {}", operationsResults);
    }
  }
}
