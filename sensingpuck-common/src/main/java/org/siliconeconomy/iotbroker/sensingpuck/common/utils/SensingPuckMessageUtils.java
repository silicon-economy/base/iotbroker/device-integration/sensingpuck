/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.utils;

import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

/**
 * A collection of utility methods for working with {@link SensingPuckMessageDtoBase}s and
 * {@link SensingPuckMessageEntityBase}s.
 *
 * @author M. Grzenia
 */
@Component
public class SensingPuckMessageUtils {

  private final Pattern entityIdPattern;
  private final String entityIdFormat;
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public SensingPuckMessageUtils(DeviceIntegrationProperties deviceIntegrationProperties) {
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");

    this.entityIdPattern = Pattern.compile(
        "(.{"
            + deviceIntegrationProperties.getDeviceSource().length()
            + "}-\\d{1,10}:)(.{36})"
    );
    this.entityIdFormat = "%s-%d:%s";
  }

  /**
   * Returns the pattern to match IDs of {@link SensingPuckMessageEntityBase} instances.
   * <p>
   * Corresponds to {@link #getEntityIdFormat()}.
   * <p>
   * The constraints in this pattern result from:
   * <ul>
   *     <li>The length of {@link DeviceIntegrationProperties#getDeviceSource()}.</li>
   *     <li>The maximum number of digits in a 32-bit unsigned integer.</li>
   *     <li>The number of characters in a type 4 {@link UUID}.</li>
   * </ul>
   *
   * @return A {@link Pattern}.
   */
  public Pattern getEntityIdPattern() {
    return entityIdPattern;
  }

  /**
   * Returns the format to use for generating IDs for {@link SensingPuckMessageEntityBase}
   * instances.
   * <p>
   * Corresponds to {@link #getEntityIdPattern()}.
   *
   * @return A format string.
   */
  public String getEntityIdFormat() {
    return entityIdFormat;
  }

  /**
   * Generates a unique ID that can be used for a {@link SensingPuckMessageEntityBase} that is based
   * of the given device ID.
   *
   * @param deviceId The tenant/id of the device the entity id is being created for.
   * @return A unique ID that can be used for a {@link SensingPuckMessageEntityBase}.
   */
  public String generateEntityId(long deviceId) {
    return String.format(
        entityIdFormat,
        deviceIntegrationProperties.getDeviceSource(),
        deviceId,
        UUID.randomUUID()
    );
  }

  /**
   * Extracts the part in a {@link SensingPuckMessageEntityBase}'s ID that represents the ID of the
   * original {@link SensingPuckMessageDtoBase}.
   *
   * @param entityId The entity ID to extract the message ID from.
   * @return The extracted message ID.
   */
  public String extractMessageId(String entityId) {
    var entityIdMatcher = entityIdPattern.matcher(entityId);
    if (!entityIdMatcher.matches()) {
      throw new IllegalArgumentException(
          String.format("Unexpected entity ID format '%s'", entityId)
      );
    }

    return entityIdMatcher.group(2);
  }
}
