/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

import java.time.Instant;

/**
 * Defines the base information for a sensing puck job in the database.
 * <p>
 * {@link JsonTypeInfo} and {@link JsonSubTypes} annotations are added to support deserialization
 * (via the base type) when reading from the database.
 * <p>
 * For entities of this type, the ID of an entity in the database is equivalent to the ID of the
 * corresponding job. However, <em>only</em> the entity ID is persisted in the database. Since
 * job ID and entity ID are of different types, this class provides some convenience access methods
 * (see {@link #getJobId()} and {@link #setJobId(long)}) but ignores the corresponding JSON property
 * on serialization/deserialization (see the {@link JsonIgnoreProperties} annotation on this class).
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "protocolVersion",
    visible = true
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = V1SensingPuckJobEntity.class, name = "1"),
    @JsonSubTypes.Type(value = V2SensingPuckJobEntity.class, name = "2"),
    @JsonSubTypes.Type(value = V3SensingPuckJobEntity.class, name = "3")
})
@JsonIgnoreProperties({"jobId"})
// When adding new entities to the database, the revision is set to null. Additionally, in CouchDB,
// the revision property is required to not exist when creating new documents. Therefore, properties
// with null values are excluded.
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SensingPuckJobEntityBase {

  /**
   * The unique identifier of the entity in the database.
   */
  @JsonProperty("_id")
  private String id;
  /**
   * The revision of the entity in the database.
   */
  @JsonProperty("_rev")
  private String revision;
  /**
   * The source that the device the job belongs to represents. (E.g. as a source of messages.)
   */
  private String source;
  /**
   * A unique identifier for the device the job belongs to.
   * (E.g. the device ID. Only unique in the context of the device's source.)
   */
  private String tenant;
  /**
   * Creation timestamp.
   */
  private Instant createdTimestamp;
  /**
   * The time the job was sent to the device.
   * May be {@code null}, if the job hasn't yet been sent to the device.
   */
  private Instant sentTimestamp;
  /**
   * The protocol version.
   */
  private int protocolVersion;
  /**
   * The current state of the job.
   */
  private JobState jobState;
  /**
   * The ID of the sensing puck.
   */
  private long deviceId;

  // Suppress "long parameter list" warning for simple data structures.
  @SuppressWarnings("java:S107")
  public SensingPuckJobEntityBase(String id,
                                  String revision,
                                  String source,
                                  String tenant,
                                  Instant createdTimestamp,
                                  Instant sentTimestamp,
                                  int protocolVersion,
                                  JobState jobState,
                                  long deviceId) {
    // Ensure the entity's ID is a parsable long.
    Long.parseLong(id);
    this.id = id;
    this.revision = revision;
    this.source = source;
    this.tenant = tenant;
    this.createdTimestamp = createdTimestamp;
    this.sentTimestamp = sentTimestamp;
    this.protocolVersion = protocolVersion;
    this.jobState = jobState;
    this.deviceId = deviceId;
  }

  public void setId(String id) {
    // Ensure the entity's ID is a parsable long.
    Long.parseLong(id);
    this.id = id;
  }

  /**
   * Returns the job ID which is equivalent to the entity's ID.
   * <p>
   * This is merely a convenience method for accessing the entity's ID as an integer.
   *
   * @return The job ID.
   */
  public long getJobId() {
    return Long.parseLong(this.getId());
  }

  /**
   * Sets the job ID which is equivalent to the entity's ID.
   * <p>
   * This is merely a convenience method for setting the entity's ID via an integer.
   *
   * @param jobId The new job ID.
   */
  public void setJobId(long jobId) {
    this.setId(String.valueOf(jobId));
  }
}
