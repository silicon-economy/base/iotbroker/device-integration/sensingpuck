/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;

import java.util.function.Predicate;

/**
 * A {@link Predicate} that returns {@code true} for any {@link SensingPuckJobEntityBase} instance.
 *
 * @author M. Grzenia
 */
public class AllJobEntitiesFilter
    implements Predicate<SensingPuckJobEntityBase> {

  @Override
  public boolean test(SensingPuckJobEntityBase sensingPuckJobEntityBase) {
    return true;
  }
}
