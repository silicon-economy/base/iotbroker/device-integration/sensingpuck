/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.utils.SensingPuckMessageUtils;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

/**
 * Provides methods for converting {@link SensingPuckMessageDtoBase} instances to
 * {@link SensingPuckMessageEntityBase} instances.
 *
 * @author M. Grzenia
 */
@Component
public class MessageDtoToEntityConverter {

  private final SensingPuckMessageUtils sensingPuckMessageUtils;

  /**
   * Creates a new instance.
   *
   * @param sensingPuckMessageUtils Provides utility methods for working with
   *                                {@link SensingPuckMessageEntityBase} instances.
   */
  public MessageDtoToEntityConverter(SensingPuckMessageUtils sensingPuckMessageUtils) {
    this.sensingPuckMessageUtils = requireNonNull(sensingPuckMessageUtils,
                                                  "sensingPuckMessageUtils");
  }

  /**
   * Converts the given {@link SensingPuckMessageDtoBase} to an instance of
   * {@link SensingPuckMessageEntityBase} by considering their respective subtypes.
   * <p>
   * On conversion a random ID in a format specific to the database is generated for the
   * {@link SensingPuckMessageEntityBase}.
   * <p>
   * Since a {@link SensingPuckMessageEntityBase} contains some database-specific properties that a
   * {@link SensingPuckMessageDtoBase} does not provide values for, the following default values are
   * use for the converted {@link SensingPuckMessageEntityBase} instance:
   * <ul>
   *   <li>{@link SensingPuckMessageEntityBase#getRevision()} is {@code null}.</li>
   * </ul>
   *
   * @param message The {@link SensingPuckMessageDtoBase} to convert.
   * @return The converted {@link SensingPuckMessageEntityBase}.
   * @throws IllegalArgumentException If the given {@link SensingPuckMessageDtoBase} is of an
   *                                  unhandled type.
   */
  public SensingPuckMessageEntityBase convert(SensingPuckMessageDtoBase message) {
    if (message instanceof V1SensingPuckMessageDto) {
      return convert((V1SensingPuckMessageDto) message);
    }
    if (message instanceof V2SensingPuckMessageDto) {
      return convert((V2SensingPuckMessageDto) message);
    }
    if (message instanceof V3SensingPuckMessageDto) {
      return convert((V3SensingPuckMessageDto) message);
    }

    throw new IllegalArgumentException("Unhandled type: " + message.getClass());
  }

  private V1SensingPuckMessageEntity convert(V1SensingPuckMessageDto message) {
    return new V1SensingPuckMessageEntity(
        sensingPuckMessageUtils.generateEntityId(message.getDeviceId()),
        null,
        message.getProtocolVersion(),
        message.getJobId(),
        message.getDeviceId(),
        message.getFirmwareDescriptor(),
        message.getComTimestamp(),
        message.getLastComCause(),
        message.getData()
    );
  }

  private V2SensingPuckMessageEntity convert(V2SensingPuckMessageDto message) {
    return new V2SensingPuckMessageEntity(
        sensingPuckMessageUtils.generateEntityId(message.getDeviceId()),
        null,
        message.getProtocolVersion(),
        message.getJobId(),
        message.getDeviceId(),
        message.getFirmwareDescriptor(),
        message.getComTimestamp(),
        message.getLastComCause(),
        message.getData()
    );
  }

  private V3SensingPuckMessageEntity convert(V3SensingPuckMessageDto message) {
    return new V3SensingPuckMessageEntity(
        sensingPuckMessageUtils.generateEntityId(message.getDeviceId()),
        null,
        message.getProtocolVersion(),
        message.getJobId(),
        message.getDeviceId(),
        message.getFirmwareDescriptor(),
        message.getComTimestamp(),
        message.getLastComCause(),
        message.getData()
    );
  }
}
