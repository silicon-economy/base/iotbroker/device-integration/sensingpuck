/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.ObjectMapperFactory;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.siliconeconomy.iotbroker.couchdb.PartitionedCouchDbConnector;
import org.siliconeconomy.iotbroker.couchdb.PartitionedCouchDbInstance;
import org.siliconeconomy.iotbroker.couchdb.StdPartitionedCouchDbConnector;
import org.siliconeconomy.iotbroker.couchdb.StdPartitionedCouchDbInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;

import static java.util.Objects.requireNonNull;

/**
 * Spring {@link Configuration} to configure CouchDB connections and client.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Configuration
public class CouchDbConfig {

  private final CouchDbProperties couchDbProperties;

  /**
   * Creates a new instance.
   *
   * @param couchDbProperties The CouchDB-related configuration properties.
   */
  public CouchDbConfig(CouchDbProperties couchDbProperties) {
    this.couchDbProperties = requireNonNull(couchDbProperties, "couchDbProperties");
  }

  /**
   * Configures and provides the HTTP client used for connecting to CouchDB.
   */
  @Bean
  public HttpClient client()
      throws MalformedURLException {
    return new StdHttpClient.Builder()
        .url(couchDbProperties.getUrl())
        .username(couchDbProperties.getUsername())
        .password(couchDbProperties.getPassword())
        // Disabling caching in order to use `initStandardDesignDocument()`
        // see: https://github.com/helun/Ektorp/issues/222#issuecomment-379267999
        .caching(false)
        .build();
  }

  @Bean
  public CouchDbInstance dbInstance(HttpClient httpClient) {
    return new StdCouchDbInstance(httpClient);
  }

  @Bean
  public PartitionedCouchDbInstance partitionedDbInstance(HttpClient httpClient) {
    return new StdPartitionedCouchDbInstance(httpClient);
  }

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper()
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new JavaTimeModule());
  }

  @Bean
  public ObjectMapper viewQueryObjectMapper() {
    return new ObjectMapper()
        // When querying views, empty (JSON) objects can be used for the (start/end)key.
        // This configuration allows simple instances of java.lang.Object to be mapped to those
        // empty JSON objects.
        .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new JavaTimeModule());
  }

  /**
   * The {@link ObjectMapperFactory} to use with Ektorp.
   */
  @Bean
  public ObjectMapperFactory mapperFactory(ObjectMapper objectMapper) {
    return new ObjectMapperFactory() {
      @Override
      public ObjectMapper createObjectMapper() {
        return objectMapper;
      }

      @Override
      public ObjectMapper createObjectMapper(CouchDbConnector couchDbConnector) {
        return objectMapper;
      }
    };
  }

  @Bean
  public PartitionedCouchDbConnector messageDatabase(
      PartitionedCouchDbInstance partitionedDbInstance, ObjectMapperFactory mapperFactory) {
    return new StdPartitionedCouchDbConnector(couchDbProperties.getMessageDatabaseName(),
                                              partitionedDbInstance,
                                              mapperFactory);
  }

  @Bean
  public CouchDbConnector jobDatabase(CouchDbInstance dbInstance,
                                      ObjectMapperFactory mapperFactory) {
    return new StdCouchDbConnector(couchDbProperties.getJobDatabaseName(),
                                   dbInstance,
                                   mapperFactory);
  }
}
