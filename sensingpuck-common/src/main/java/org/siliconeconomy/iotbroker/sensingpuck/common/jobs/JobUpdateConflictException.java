/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

/**
 * Thrown to indicate that there was a conflict trying to update a job.
 *
 * @author M. Grzenia
 */
public class JobUpdateConflictException
    extends RuntimeException {

  public JobUpdateConflictException(long jobId) {
    super(String.format("Could not update job with id '%d'.", jobId));
  }
}
