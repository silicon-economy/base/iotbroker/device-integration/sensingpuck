/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;

/**
 * Defines the base information for a simplified representation of a message in the database that an
 * adapter received from a sensing puck.
 *
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "protocolVersion",
    visible = true
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = V1SensingPuckMessageEntity.class, name = "1"),
    @JsonSubTypes.Type(value = V2SensingPuckMessageEntity.class, name = "2"),
    @JsonSubTypes.Type(value = V3SensingPuckMessageEntity.class, name = "3")
})
// When adding new entities to the database, the revision is set to null. Additionally, in CouchDB,
// the revision property is required to not exist when creating new documents. Therefore, properties
// with null values are excluded.
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SensingPuckMessageEntityBase {

  /**
   * The unique identifier of the entity in the database.
   */
  @JsonProperty("_id")
  private String id;
  /**
   * The revision of the entity in the database.
   */
  @JsonProperty("_rev")
  private String revision;
  /**
   * The protocol version.
   */
  private int protocolVersion;
}
