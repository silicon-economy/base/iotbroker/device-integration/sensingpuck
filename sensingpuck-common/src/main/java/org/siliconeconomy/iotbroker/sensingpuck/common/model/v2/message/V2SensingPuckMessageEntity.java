/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message;

import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.FirmwareDescriptor;

import java.time.Instant;
import java.util.List;

/**
 * A simplified representation of a {@link ProtocolVersion#V2} message in the database that an
 * adapter received from a sensing puck.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class V2SensingPuckMessageEntity
    extends SensingPuckMessageEntityBase {

  /**
   * The ID of the job that the sensing puck last received.
   */
  private long jobId;
  /**
   * The ID of the sensing puck.
   */
  private long deviceId;
  /**
   * The description of the sensing puck adapter's firmware.
   */
  private FirmwareDescriptor firmwareDescriptor;
  /**
   * The timestamp when the request was sent by the sensing puck.
   */
  private Instant comTimestamp;
  /**
   * The identifier for the latest event that triggered a communication.
   */
  private CommCause lastComCause;
  /**
   * The actual data measured by the sending puck.
   */
  private List<V2SensingData> data;

  // Suppress "long parameter list" warning for simple data structures.
  @SuppressWarnings("java:S107")
  public V2SensingPuckMessageEntity(String id,
                                    String revision,
                                    int protocolVersion,
                                    long jobId,
                                    long deviceId,
                                    FirmwareDescriptor firmwareDescriptor,
                                    Instant comTimestamp,
                                    CommCause lastComCause,
                                    List<V2SensingData> data) {
    super(id, revision, protocolVersion);
    this.jobId = jobId;
    this.deviceId = deviceId;
    this.firmwareDescriptor = firmwareDescriptor;
    this.comTimestamp = comTimestamp;
    this.lastComCause = lastComCause;
    this.data = data;
  }
}
