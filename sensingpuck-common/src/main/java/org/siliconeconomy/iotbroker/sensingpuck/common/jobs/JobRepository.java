/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.*;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.View;
import org.siliconeconomy.iotbroker.sensingpuck.common.SensingPuckDbConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.couchdb.CouchDbQueryConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Provides database access for the {@link SensingPuckJobEntityBase} and its subclasses.
 * <p>
 * Provides basic CRUD operations and methods for more complex database queries.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Repository
public class JobRepository
    extends CouchDbRepositorySupport<SensingPuckJobEntityBase> {

  /**
   * This class's logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(JobRepository.class);
  /**
   * The object mapper to use for view queries.
   */
  private final ObjectMapper viewQueryObjectMapper;

  @Autowired
  public JobRepository(@Qualifier("jobDatabase") CouchDbConnector db,
                       @Qualifier("viewQueryObjectMapper") ObjectMapper viewQueryObjectMapper) {
    super(SensingPuckJobEntityBase.class, db, SensingPuckDbConstants.JOB_DESIGN_DOC_NAME);
    this.viewQueryObjectMapper = viewQueryObjectMapper;

    // Create or update design documents
    initStandardDesignDocument();
  }

  /**
   * Retrieves all {@link SensingPuckJobEntityBase}s with the given source, tenant and job state and
   * sorts them by the created timestamp either in ascending or descending order according to the
   * given sort
   * option.
   *
   * @param tenant     The tenant.
   * @param jobState   The job state.
   * @param descending Whether to sort the results in ascending or descending order based on the
   *                   created timestamp.
   * @return A list of {@link SensingPuckJobEntityBase}.
   */
  @View(name = "by-tenant-and-jobstate", file = "by-tenant-and-jobstate.json")
  public List<SensingPuckJobEntityBase> findAllByTenantAndJobStateSortByCreatedTimestamp(
      String tenant,
      JobState jobState,
      boolean descending) {
    ViewQuery query = new ViewQuery(viewQueryObjectMapper)
        .dbPath(db.path())
        .includeDocs(true)
        .designDocId(String.format("_design/%s", "jobs"))
        .viewName("by-tenant-and-jobstate");

    if (!descending) {
      query = query
          .startKey(new Object[]{tenant, jobState})
          .endKey(new Object[]{tenant, jobState, CouchDbQueryConstants.HIGH_KEY_VALUE});
    } else {
      query = query
          .startKey(new Object[]{tenant, jobState, CouchDbQueryConstants.HIGH_KEY_VALUE})
          .endKey(new Object[]{tenant, jobState})
          .descending(true);
    }

    return db.queryView(query, SensingPuckJobEntityBase.class);
  }

  /**
   * Retrieves all {@link SensingPuckJobEntityBase}s associated with the device with the given ID.
   *
   * @param deviceId The device ID.
   * @return A list of {@link SensingPuckJobEntityBase}.
   */
  @View(name = "by-tenant", file = "by-tenant.json")
  public List<SensingPuckJobEntityBase> findAllByDeviceId(long deviceId) {
    return queryView("by-tenant", Long.toString(deviceId));
  }

  /**
   * Retrieves the {@link SensingPuckJobEntityBase} with the given ID.
   *
   * @param id The job ID.
   * @return The {@link SensingPuckJobEntityBase} with the given ID or {@link Optional#empty()}, if
   * there is no such job.
   */
  public Optional<SensingPuckJobEntityBase> findById(String id) {
    try {
      return Optional.of(get(id));

    } catch (DocumentNotFoundException e) {
      return Optional.empty();
    }
  }

  /**
   * Deletes all {@link SensingPuckJobEntityBase}s in the database that evaluate to {@code true}
   * for the given predicate.
   *
   * @param deletePredicate The predicate to use for selecting {@link SensingPuckJobEntityBase}s to
   *                        delete.
   */
  public void deleteAll(Predicate<SensingPuckJobEntityBase> deletePredicate) {
    List<BulkDeleteDocument> documentsToDelete = this.getAll().stream()
        .filter(deletePredicate)
        .map(BulkDeleteDocument::of)
        .collect(Collectors.toList());

    List<DocumentOperationResult> operationsResults = db.executeBulk(documentsToDelete);

    if (!operationsResults.isEmpty()) {
      LOGGER.warn("Errors occurred while deleting documents: {}", operationsResults);
    }
  }
}
