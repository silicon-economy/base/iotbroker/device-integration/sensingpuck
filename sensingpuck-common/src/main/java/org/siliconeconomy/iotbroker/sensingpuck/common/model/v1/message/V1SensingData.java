/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;

import java.time.Instant;

/**
 * A simplified representation of the {@link ProtocolVersion#V1} measurement data that the adapter
 * received from a sensing puck.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class V1SensingData {

  /**
   * The temperature (in °C).
   */
  private float temperature;
  /**
   * The relative humidity (in %RH).
   */
  private float humidity;
  /**
   * The timestamp when the data was captured by the sensing puck.
   */
  private Instant timestamp;
  /**
   * The sensing puck's motion state at the time of measurement.
   */
  private MotionState motionState;
}
