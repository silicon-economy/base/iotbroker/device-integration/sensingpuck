/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Settings of the LED matrix of the sensing puck.
 * <p>
 * This class defines the settings for the LED matrix of a sensing puck device. This defines the
 * number of seconds in which the display remains active for different situations.
 *
 * @author D. Ronnenberg
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LedTimings {
  /**
   * Duration of how long the ID should be displayed.
   */
  private int showId;
  /**
   * Duration of how long the alarm information should be displayed.
   */
  private int showAlarm;
  /**
   * Duration of how long the temperature should be displayed.
   */
  private int showTemp;
}
