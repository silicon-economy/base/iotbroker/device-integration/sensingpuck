/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Provides methods for inspecting jobs (to be created or updated) while respecting the contents
 * currently persisted in the {@link JobRepository}.
 *
 * @author M. Grzenia
 */
@Component
public class JobInspector {

  /**
   * The job repository to use.
   */
  private final JobRepository jobRepository;

  public JobInspector(JobRepository jobRepository) {
    this.jobRepository = jobRepository;
  }

  /**
   * Checks whether the given {@link SensingPuckJobEntityBase} is allowed to be created.
   *
   * @param job The {@link SensingPuckJobEntityBase} to check.
   * @throws DeviceHasActiveJobException If the given job's state is considered "active", but the
   *                                     device to which the job is to be assigned already has an
   *                                     "active" job assigned to it.
   */
  public void checkCreateAllowed(SensingPuckJobEntityBase job) {
    long deviceId = job.getDeviceId();
    Optional<SensingPuckJobEntityBase> existingActiveJob = findActiveJobByDeviceId(deviceId);

    if (existingActiveJob.isPresent() && job.getJobState() == JobState.ACTIVE) {
      throw new DeviceHasActiveJobException(deviceId, existingActiveJob.get().getJobId());
    }
  }

  /**
   * Checks whether the given {@link SensingPuckJobEntityBase} is allowed to be updated.
   *
   * @param job The {@link SensingPuckJobEntityBase} to check.
   * @throws DeviceHasActiveJobException If the given job's state is considered "active", but the
   *                                     device to which the job is assigned already has another
   *                                     "active" job assigned to it.
   */
  public void checkUpdateAllowed(SensingPuckJobEntityBase job) {
    long deviceId = job.getDeviceId();
    Optional<SensingPuckJobEntityBase> existingActiveJob = findActiveJobByDeviceId(deviceId);

    if (existingActiveJob.isPresent()
        && job.getJobState() == JobState.ACTIVE
        && existingActiveJob.get().getJobId() != job.getJobId()) {
      throw new DeviceHasActiveJobException(deviceId, existingActiveJob.get().getJobId());
    }
  }

  private Optional<SensingPuckJobEntityBase> findActiveJobByDeviceId(long deviceId) {
    return jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(
        String.valueOf(deviceId),
        JobState.ACTIVE,
        true
    ).stream().findFirst();
  }
}
