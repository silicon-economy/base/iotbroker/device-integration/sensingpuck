/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model;

/**
 * Defines the existing protocol versions.
 *
 * @author M. Grzenia
 */
public enum ProtocolVersion {

  V1(1),
  V2(2),
  V3(3);

  /**
   * The protocol version's version number.
   */
  private final int versionNumber;

  ProtocolVersion(int versionNumber) {
    this.versionNumber = versionNumber;
  }

  public int getVersionNumber() {
    return versionNumber;
  }

  /**
   * Returns the corresponding {@link ProtocolVersion} instance for the given protocol version
   * number.
   *
   * @param versionNumber The protocol version number.
   * @return The corresponding {@link ProtocolVersion}.
   * @throws IllegalArgumentException If the given protocol version number is unknown.
   */
  public static ProtocolVersion fromVersionNumber(int versionNumber) {
    switch (versionNumber) {
      case 1:
        return V1;
      case 2:
        return V2;
      case 3:
        return V3;
      default:
        throw new IllegalArgumentException("Unknown protocol version number: " + versionNumber);
    }
  }
}
