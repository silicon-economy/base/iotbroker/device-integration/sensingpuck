/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;

/**
 * Definition of intervals for recording data.
 * <p>
 * This class defines intervals for recording sensor data. The intervals can be provided for
 * different states.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementCycles {
  /**
   * The interval (in seconds) to be used when the sensing puck is in {@link MotionState#STATIC}.
   */
  private long storage;
  /**
   * The interval (in seconds) to be used when the sensing puck is in {@link MotionState#MOVING}.
   */
  private long transport;
}
