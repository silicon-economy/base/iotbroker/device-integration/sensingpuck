/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.config;

import lombok.Getter;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import static java.util.Objects.requireNonNull;

/**
 * Defines (configuration) properties related to the devices that are being integrated into the
 * IoT Broker (and that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "deviceintegration")
@ConstructorBinding
@Getter
public class DeviceIntegrationProperties {

  private final String deviceSource;
  private final String deviceTypeDescription;
  private final String deviceInstanceDescription;

  /**
   * Creates a new instance.
   *
   * @param deviceSource              The source that devices (for which the adapter provides an
   *                                  integration) represent. This string is to be used as the
   *                                  'source' property for the adapter's {@link DeviceType} and
   *                                  corresponding {@link DeviceInstance}s as well as
   *                                  {@link SensorDataMessage}s published via AMQP.
   * @param deviceTypeDescription     The description for the adapter's {@link DeviceType}.
   * @param deviceInstanceDescription The description for {@link DeviceInstance}s the adapter
   *                                  provides an integration for.
   */
  public DeviceIntegrationProperties(String deviceSource,
                                     String deviceTypeDescription,
                                     String deviceInstanceDescription) {
    this.deviceSource = requireNonNull(deviceSource, "deviceSource");
    this.deviceTypeDescription = requireNonNull(deviceTypeDescription, "deviceTypeIdentifier");
    this.deviceInstanceDescription = requireNonNull(deviceInstanceDescription,
                                                    "deviceInstanceDescription");
  }
}
