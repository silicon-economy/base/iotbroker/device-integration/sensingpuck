/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;

/**
 * Provides methods for converting {@link SensingPuckMessageEntityBase} instances to
 * {@link SensingPuckMessageDtoBase} instances.
 *
 * @author M. Grzenia
 */
public class MessageEntityToDtoConverter {

  /**
   * Converts the given {@link SensingPuckMessageEntityBase} to an instance of
   * {@link SensingPuckMessageDtoBase} by considering their respective subtypes.
   *
   * @param entity The {@link SensingPuckMessageEntityBase} to convert.
   * @return The converted {@link SensingPuckMessageDtoBase}.
   * @throws IllegalArgumentException If the given {@link SensingPuckMessageEntityBase} is of an
   *                                  unhandled type.
   */
  public SensingPuckMessageDtoBase convert(SensingPuckMessageEntityBase entity) {
    if (entity instanceof V1SensingPuckMessageEntity) {
      return convert((V1SensingPuckMessageEntity) entity);
    }
    if (entity instanceof V2SensingPuckMessageEntity) {
      return convert((V2SensingPuckMessageEntity) entity);
    }
    if (entity instanceof V3SensingPuckMessageEntity) {
      return convert((V3SensingPuckMessageEntity) entity);
    }

    throw new IllegalArgumentException("Unhandled type: " + entity.getClass());
  }

  private V1SensingPuckMessageDto convert(V1SensingPuckMessageEntity entity) {
    return new V1SensingPuckMessageDto(entity.getProtocolVersion(),
                                       entity.getJobId(),
                                       entity.getDeviceId(),
                                       entity.getFirmwareDescriptor(),
                                       entity.getComTimestamp(),
                                       entity.getLastComCause(),
                                       entity.getData());
  }

  private V2SensingPuckMessageDto convert(V2SensingPuckMessageEntity entity) {
    return new V2SensingPuckMessageDto(entity.getProtocolVersion(),
                                       entity.getJobId(),
                                       entity.getDeviceId(),
                                       entity.getFirmwareDescriptor(),
                                       entity.getComTimestamp(),
                                       entity.getLastComCause(),
                                       entity.getData());
  }

  private V3SensingPuckMessageDto convert(V3SensingPuckMessageEntity entity) {
    return new V3SensingPuckMessageDto(entity.getProtocolVersion(),
                                       entity.getJobId(),
                                       entity.getDeviceId(),
                                       entity.getFirmwareDescriptor(),
                                       entity.getComTimestamp(),
                                       entity.getLastComCause(),
                                       entity.getData());
  }
}
