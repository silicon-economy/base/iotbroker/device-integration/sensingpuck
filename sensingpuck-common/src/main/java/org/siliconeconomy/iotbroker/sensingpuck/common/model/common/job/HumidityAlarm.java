/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Ambient humidity alarm settings.
 * <p>
 * This class defines the ambient humidity alarm settings of a sensing puck device. If a sensor
 * reading violates {@link HumidityAlarm#upperBound} or {@link HumidityAlarm#lowerBound} an alarm
 * is triggered.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HumidityAlarm {

  /**
   * The upper humidity threshold (in %RH).
   */
  private float upperBound;
  /**
   * The lower humidity threshold (in %RH).
   */
  private float lowerBound;
}
