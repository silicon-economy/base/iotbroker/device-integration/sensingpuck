/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A simplified representation of the firmware description that the adapter received from a sensing
 * puck.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FirmwareDescriptor {

  /**
   * The version number of the firmware.
   */
  private String versionNumber;
  /**
   * The number of commits since the last version tag.
   */
  private long commitCounter;
  /**
   * Indicates whether the last version tag was a release version tag.
   */
  private boolean releaseFlag;
  /**
   * The first 32 bit of the firmware's commit hash.
   */
  private long commitHash;
  /**
   * Indicates whether the local repository used to build the firmware was dirty.
   */
  private boolean dirtyFlag;
}
