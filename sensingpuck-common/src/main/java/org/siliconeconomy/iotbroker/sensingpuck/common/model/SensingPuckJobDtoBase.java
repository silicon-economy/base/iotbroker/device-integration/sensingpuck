/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;

/**
 * Defines the base information for a sensing puck job DTO.
 *
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "protocolVersion",
    visible = true
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = V1SensingPuckJobDto.class, name = "1"),
    @JsonSubTypes.Type(value = V2SensingPuckJobDto.class, name = "2"),
    @JsonSubTypes.Type(value = V3SensingPuckJobDto.class, name = "3")
})
public class SensingPuckJobDtoBase {

  /**
   * The protocol version.
   */
  private int protocolVersion;
  /**
   * The unique ID of the job.
   */
  private long jobId;
  /**
   * The current state of the job.
   */
  private JobState jobState;
  /**
   * The ID of the sensing puck.
   */
  private long deviceId;
}
