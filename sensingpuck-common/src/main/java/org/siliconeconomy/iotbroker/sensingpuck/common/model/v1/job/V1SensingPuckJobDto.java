/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job;

import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.HumidityAlarm;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.LedTimings;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.MeasurementCycles;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.TemperatureAlarm;

/**
 * Represents a {@link ProtocolVersion#V1} job of a sensing puck.
 * <p>
 * This DTO is used when communicating with an HTTP client.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class V1SensingPuckJobDto
    extends SensingPuckJobDtoBase {

  /**
   * Indicates whether the device should clear its internal alarm flag.
   */
  private boolean clearAlarm;
  /**
   * Defines the intervals in which the sensing puck's sensors will be evaluated.
   */
  private MeasurementCycles measurementCycles;
  /**
   * The amount of evaluated data that will trigger the sensing puck to start communication.
   */
  private long dataAmount;
  /**
   * Defines temperature thresholds that will trigger the sensing puck to start communication.
   */
  private TemperatureAlarm temperatureAlarm;
  /**
   * Defines humidity thresholds that will trigger the sensing puck to start communication.
   */
  private HumidityAlarm humidityAlarm;
  /**
   * Defines timings for information to be shown on the sensing puck's display.
   */
  private LedTimings ledTimings;

  // Suppress "long parameter list" warning for simple data structures.
  @SuppressWarnings("java:S107")
  public V1SensingPuckJobDto(int protocolVersion,
                             long jobId,
                             JobState jobState,
                             long deviceId,
                             boolean clearAlarm,
                             MeasurementCycles measurementCycles,
                             long dataAmount,
                             TemperatureAlarm temperatureAlarm,
                             HumidityAlarm humidityAlarm,
                             LedTimings ledTimings) {
    super(protocolVersion, jobId, jobState, deviceId);
    this.clearAlarm = clearAlarm;
    this.measurementCycles = measurementCycles;
    this.dataAmount = dataAmount;
    this.temperatureAlarm = temperatureAlarm;
    this.humidityAlarm = humidityAlarm;
    this.ledTimings = ledTimings;
  }
}
