/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common;

/**
 * Defines constants related to the sensing puck-specific database.
 *
 * @author M. Grzenia
 */
public class SensingPuckDbConstants {

  public static final String JOB_DESIGN_DOC_NAME = "jobs";
  public static final String MESSAGES_DESIGN_DOC_NAME = "messages";

  private SensingPuckDbConstants() {
  }
}
