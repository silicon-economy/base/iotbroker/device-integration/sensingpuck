/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job;

import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;

import java.time.Instant;

/**
 * Represents a {@link ProtocolVersion#V3} sensing puck job in the database.
 *
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class V3SensingPuckJobEntity
    extends SensingPuckJobEntityBase {

  /**
   * Indicates whether the device should clear its internal alarm flag.
   */
  private boolean clearAlarm;
  /**
   * Defines the intervals in which the sensing puck's sensors will be evaluated.
   */
  private MeasurementCycles measurementCycles;
  /**
   * The amount of evaluated data that will trigger the sensing puck to start communication.
   */
  private long dataAmount;
  /**
   * The duration (in ms) from the activation of the device to the completion of the packaging
   * process.
   * <p>
   * After the device is activated, it won't enter the <em>opened</em> alarm state within this
   * period.
   */
  private long packagingTime;
  /**
   * Defines temperature thresholds that will trigger the sensing puck to start communication.
   */
  private TemperatureAlarm temperatureAlarm;
  /**
   * Defines humidity thresholds that will trigger the sensing puck to start communication.
   */
  private HumidityAlarm humidityAlarm;
  /**
   * Defines the lower battery voltage threshold that will trigger the sensing puck to start
   * communication.
   */
  private float voltageAlarm;
  /**
   * Defines illuminance thresholds that will trigger the device to start communication.
   */
  private LightAlarm lightAlarm;
  /**
   * Defines timings for information to be shown on the sensing puck's display.
   */
  private LedTimings ledTimings;

  // Suppress "long parameter list" warning for simple data structures.
  @SuppressWarnings("java:S107")
  public V3SensingPuckJobEntity(String id,
                                String revision,
                                String source,
                                String tenant,
                                Instant createdTimestamp,
                                Instant sentTimestamp,
                                int protocolVersion,
                                JobState jobState,
                                long deviceId,
                                boolean clearAlarm,
                                MeasurementCycles measurementCycles,
                                long dataAmount,
                                long packagingTime,
                                TemperatureAlarm temperatureAlarm,
                                HumidityAlarm humidityAlarm,
                                float voltageAlarm,
                                LightAlarm lightAlarm,
                                LedTimings ledTimings) {
    super(id, revision, source, tenant, createdTimestamp, sentTimestamp, protocolVersion, jobState,
          deviceId);
    this.clearAlarm = clearAlarm;
    this.measurementCycles = measurementCycles;
    this.dataAmount = dataAmount;
    this.packagingTime = packagingTime;
    this.temperatureAlarm = temperatureAlarm;
    this.humidityAlarm = humidityAlarm;
    this.voltageAlarm = voltageAlarm;
    this.lightAlarm = lightAlarm;
    this.ledTimings = ledTimings;
  }
}
