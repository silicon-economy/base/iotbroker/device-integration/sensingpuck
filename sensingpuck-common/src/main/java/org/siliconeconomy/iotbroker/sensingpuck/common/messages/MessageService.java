/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.messages;

import org.siliconeconomy.iotbroker.sensingpuck.common.converter.MessageDtoToEntityConverter;
import org.siliconeconomy.iotbroker.sensingpuck.common.converter.MessageEntityToDtoConverter;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Provides methods for performing database operations for the {@link SensingPuckMessageDtoBase}.
 * <p>
 * Internally database access is handled through {@link MessageRepository}.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Service
public class MessageService {

  /**
   * Handles the database access and offers methods for all CRUD operations.
   */
  private final MessageRepository messageRepository;
  /**
   * Used to convert {@link SensingPuckMessageEntityBase} instances to
   * {@link SensingPuckMessageDtoBase} instances.
   */
  private final MessageEntityToDtoConverter messageEntityToDtoConverter
      = new MessageEntityToDtoConverter();
  /**
   * Used to convert {@link SensingPuckMessageDtoBase} instances to
   * {@link SensingPuckMessageEntityBase} instances.
   */
  private final MessageDtoToEntityConverter messageDtoToEntityConverter;
  /**
   * The {@link Predicate} to use for filtering all {@link SensingPuckMessageEntityBase} instances.
   */
  private final Predicate<SensingPuckMessageEntityBase> allMessageEntitiesFilter
      = new AllMessageEntitiesFilter();

  @Autowired
  public MessageService(MessageRepository messageRepository,
                        MessageDtoToEntityConverter messageDtoToEntityConverter) {
    this.messageRepository = messageRepository;
    this.messageDtoToEntityConverter = messageDtoToEntityConverter;
  }

  public List<SensingPuckMessageDtoBase> findAllByDeviceIdAndJobIdAndTimestamp(
      long jobId, long deviceId, Sort.Direction sort, int offset, int limit, Instant startTime,
      Instant endTime) {
    return messageRepository
        .findAllByJobIdAndTimestamp(String.valueOf(deviceId), String.valueOf(jobId),
                                    sort.isDescending(), offset, limit, startTime, endTime
        )
        .stream()
        .map(messageEntityToDtoConverter::convert)
        .collect(Collectors.toList());
  }

  public List<SensingPuckMessageDtoBase> findAllByDeviceIdAndTimestamp(
      long deviceId, Sort.Direction sort, int offset, int limit, Instant startTime,
      Instant endTime) {
    return messageRepository
        .findAllByTimestamp(String.valueOf(deviceId), sort.isDescending(), offset, limit,
                            startTime, endTime
        )
        .stream()
        .map(messageEntityToDtoConverter::convert)
        .collect(Collectors.toList());
  }

  /**
   * Persists the given {@link SensingPuckMessageDtoBase} to the database.
   *
   * @param message The message to persist.
   * @return The persisted {@link SensingPuckMessageEntityBase}.
   */
  public SensingPuckMessageEntityBase save(SensingPuckMessageDtoBase message) {
    SensingPuckMessageEntityBase messageEntity
        = messageDtoToEntityConverter.convert(message);
    messageRepository.add(messageEntity);
    return messageEntity;
  }

  /**
   * Deletes all {@link SensingPuckMessageEntityBase}s that evaluate to {@code true} for the given
   * predicate.
   *
   * @param deletePredicate The predicate to use for selecting {@link SensingPuckMessageEntityBase}s
   *                        to delete.
   */
  public void deleteAll(Predicate<SensingPuckMessageEntityBase> deletePredicate) {
    messageRepository.deleteAll(deletePredicate);
  }

  /**
   * Deletes all {@link SensingPuckMessageEntityBase}s.
   */
  public void deleteAll() {
    messageRepository.deleteAll(allMessageEntitiesFilter);
  }
}
