/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.messages;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;

import java.util.function.Predicate;

/**
 * A {@link Predicate} that returns {@code true} for any {@link SensingPuckMessageEntityBase}
 * instance.
 *
 * @author M. Grzenia
 */
public class AllMessageEntitiesFilter
    implements Predicate<SensingPuckMessageEntityBase> {

  @Override
  public boolean test(SensingPuckMessageEntityBase sensingPuckMessageEntityBase) {
    return true;
  }
}
