/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

import org.ektorp.DocumentNotFoundException;
import org.ektorp.UpdateConflictException;
import org.siliconeconomy.iotbroker.sensingpuck.common.converter.JobDtoToEntityConverter;
import org.siliconeconomy.iotbroker.sensingpuck.common.converter.JobEntityToDtoConverter;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Provides methods for performing database operations for the {@link SensingPuckJobEntityBase}.
 * <p>
 * Internally database access is handled through {@link JobRepository}.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
@Service
public class JobService {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(JobService.class);
  /**
   * The repository to use for the database access.
   */
  private final JobRepository jobRepository;
  /**
   * Used to perform checks for jobs to create or update.
   */
  private final JobInspector jobInspector;
  /**
   * Used to convert {@link SensingPuckJobDtoBase} instances to {@link SensingPuckJobEntityBase}
   * instances.
   */
  private final JobDtoToEntityConverter jobDtoToEntityConverter;
  /**
   * Used to convert {@link SensingPuckJobEntityBase} instances to {@link SensingPuckJobDtoBase}
   * instances.
   */
  private final JobEntityToDtoConverter jobEntityToDtoConverter = new JobEntityToDtoConverter();
  /**
   * The {@link Predicate} to use for filtering all {@link SensingPuckJobEntityBase} instances.
   */
  private final Predicate<SensingPuckJobEntityBase> allJobEntitiesFilter
      = new AllJobEntitiesFilter();

  @Autowired
  public JobService(JobRepository jobRepository,
                    JobInspector jobInspector,
                    JobDtoToEntityConverter jobDtoToEntityConverter) {
    this.jobRepository = jobRepository;
    this.jobInspector = jobInspector;
    this.jobDtoToEntityConverter = jobDtoToEntityConverter;
  }

  /**
   * Returns the most recent job (according to its created timestamp) with its {@link JobState} set
   * to {@link JobState#ACTIVE}.
   *
   * @param tenant The tenant.
   * @return The most recent active job.
   */
  public Optional<SensingPuckJobEntityBase> findMostRecentActiveJob(String tenant) {
    return jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(
        tenant,
        JobState.ACTIVE,
        true
    ).stream().findFirst();
  }

  /**
   * Returns the most recent job (according to its created timestamp) with its {@link JobState}
   * set to {@link JobState#FINISHED}.
   *
   * @param tenant The tenant.
   * @return The most recent finished job.
   */
  public Optional<SensingPuckJobEntityBase> findMostRecentFinishedJob(String tenant) {
    return jobRepository.findAllByTenantAndJobStateSortByCreatedTimestamp(
        tenant,
        JobState.FINISHED,
        true
    ).stream().findFirst();
  }

  /**
   * Adds the given job to the database.
   *
   * @param newJob The job to add.
   * @throws JobUpdateConflictException If there was a conflict while trying to update a job.
   */
  public void add(SensingPuckJobDtoBase newJob) {
    SensingPuckJobEntityBase newEntity = jobDtoToEntityConverter.convert(newJob);
    newEntity.setCreatedTimestamp(Instant.now());
    jobInspector.checkCreateAllowed(newEntity);

    try {
      this.jobRepository.add(newEntity);
    } catch (UpdateConflictException e) {
      LOG.error("Could not add job with id '{}'.", newJob.getJobId(), e);
      throw new JobUpdateConflictException(newJob.getJobId());
    }
  }

  /**
   * Uses the given {@link SensingPuckJobDtoBase} to update a corresponding job in the database.
   *
   * @param job The job to use for the update.
   * @throws JobNotFoundException       If a job with the given ID could not be found.
   * @throws JobUpdateConflictException If there was a conflict while trying to update a job.
   */
  public void update(SensingPuckJobDtoBase job) {
    SensingPuckJobEntityBase existingJob;
    try {
      existingJob = jobRepository.get(String.valueOf(job.getJobId()));
    } catch (DocumentNotFoundException e) {
      throw new JobNotFoundException(job.getJobId());
    }

    SensingPuckJobEntityBase newJob = jobDtoToEntityConverter.convert(job);
    // Keep these properties from the existing job since the DTO doesn't provide them.
    newJob.setRevision(existingJob.getRevision());
    newJob.setSentTimestamp(existingJob.getSentTimestamp());
    newJob.setCreatedTimestamp(existingJob.getCreatedTimestamp());

    update(newJob);
  }

  /**
   * Updates the given {@link SensingPuckJobEntityBase} in the database.
   *
   * @throws JobNotFoundException       If the referenced job could not be found.
   * @throws JobUpdateConflictException If there was a conflict while trying to update the job.
   */
  public void update(SensingPuckJobEntityBase job) {
    if (!jobRepository.contains(job.getId())) {
      throw new JobNotFoundException(job.getJobId());
    }

    jobInspector.checkUpdateAllowed(job);

    try {
      jobRepository.update(job);
    } catch (UpdateConflictException e) {
      LOG.error("Could not update job with id '{}'.", job.getId(), e);
      throw new JobUpdateConflictException(job.getJobId());
    }
  }

  /**
   * Returns all jobs associated with the given device ID.
   *
   * @param deviceId The device ID.
   * @return A list of all jobs associated with the given device ID.
   */
  public List<SensingPuckJobDtoBase> findAllByDeviceId(long deviceId) {
    return jobRepository.findAllByDeviceId(deviceId).stream()
        .map(jobEntityToDtoConverter::convert)
        .collect(Collectors.toList());
  }

  /**
   * Returns the IDs of all jobs associated with the given device ID.
   *
   * @param deviceID The device ID.
   * @return A list of the IDs of all jobs associated with the given device ID.
   */
  public List<Long> getJobIdsByDeviceId(long deviceID) {
    return this.findAllByDeviceId(deviceID)
        .stream()
        .map(SensingPuckJobDtoBase::getJobId)
        .collect(Collectors.toList());
  }

  /**
   * Returns the {@link SensingPuckJobEntityBase} with the given ID.
   *
   * @param jobId The job ID.
   * @return The job with the given ID.
   * @throws JobNotFoundException If a job with the given ID could not be found.
   */
  public SensingPuckJobEntityBase findEntityByJobId(long jobId) {
    try {
      return this.jobRepository.get(Long.toString(jobId));
    } catch (DocumentNotFoundException e) {
      throw new JobNotFoundException(jobId);
    }
  }

  /**
   * Returns the {@link SensingPuckJobDtoBase} with the given ID.
   * <p>
   * Convenience method that merely calls, {@link #findEntityByJobId(long)} and converts the result
   * to an instance of {@link SensingPuckJobDtoBase}.
   *
   * @param jobId The job ID.
   * @return The job with the given ID.
   * @throws JobNotFoundException If a job with the given ID could not be found.
   */
  public SensingPuckJobDtoBase findDtoByJobId(long jobId) {
    return jobEntityToDtoConverter.convert(findEntityByJobId(jobId));
  }

  /**
   * Updates jobs associated with the device with the given ID and sets their state to
   * {@link JobState#FINISHED}.
   *
   * @param deviceId The ID of the sensing puck device for which the state of associated jobs should
   *                 be set to {@link JobState#FINISHED}.
   */
  public void updateJobStatesFinished(long deviceId) {
    List<SensingPuckJobEntityBase> jobs = jobRepository.findAllByDeviceId(deviceId);
    for (SensingPuckJobEntityBase job : jobs) {
      job.setJobState(JobState.FINISHED);
      update(job);
    }
  }

  /**
   * Deletes all {@link SensingPuckJobEntityBase}s that evaluate to {@code true} for the given
   * predicate.
   *
   * @param deletePredicate The predicate to use for selecting {@link SensingPuckJobEntityBase}s to
   *                        delete.
   */
  public void deleteAll(Predicate<SensingPuckJobEntityBase> deletePredicate) {
    jobRepository.deleteAll(deletePredicate);
  }

  /**
   * Deletes all {@link SensingPuckJobEntityBase}s.
   */
  public void deleteAll() {
    jobRepository.deleteAll(allJobEntitiesFilter);
  }
}
