/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;

/**
 * Defines the base information for a simplified representation of a message that an adapter
 * received from a sensing puck.
 * <p>
 * {@link JsonTypeInfo} and {@link JsonSubTypes} annotations are added to support deserialization
 * (via the base type) when working with the IoT Broker's Device Message Exchange.
 *
 * @author M. Grzenia
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "protocolVersion",
    visible = true
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = V1SensingPuckMessageDto.class, name = "1"),
    @JsonSubTypes.Type(value = V2SensingPuckMessageDto.class, name = "2"),
    @JsonSubTypes.Type(value = V3SensingPuckMessageDto.class, name = "3")
})
public class SensingPuckMessageDtoBase {

  /**
   * The protocol version.
   */
  private int protocolVersion;
}
