/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.converter;

import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;
import org.springframework.stereotype.Component;

import java.time.Instant;

import static java.util.Objects.requireNonNull;

/**
 * Provides methods for converting {@link SensingPuckJobDtoBase} instances to
 * {@link SensingPuckJobEntityBase} instances.
 *
 * @author D. Ronnenberg
 */
@Component
public class JobDtoToEntityConverter {

  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public JobDtoToEntityConverter(DeviceIntegrationProperties deviceIntegrationProperties) {
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceProperties");
  }

  /**
   * Converts the given {@link SensingPuckJobDtoBase} to an instance of
   * {@link SensingPuckJobEntityBase} by considering their respective subtypes.
   * <p>
   * Since a {@link SensingPuckJobEntityBase} contains some database-specific properties that a
   * {@link SensingPuckJobDtoBase} does not provide values for, the following default values are
   * use for the converted {@link SensingPuckJobEntityBase} instance:
   * <ul>
   *   <li>{@link SensingPuckJobEntityBase#getRevision()} is {@code null}.</li>
   *   <li>{@link SensingPuckJobEntityBase#getCreatedTimestamp()} is {@link Instant#EPOCH}.</li>
   *   <li>{@link SensingPuckJobEntityBase#getSentTimestamp()} is {@link Instant#EPOCH}.</li>
   * </ul>
   *
   * @param job The {@link SensingPuckJobDtoBase} to convert.
   * @return The converted {@link SensingPuckJobEntityBase}.
   * @throws IllegalArgumentException If the given {@link SensingPuckJobDtoBase} is of an unhandled
   *                                  type.
   */
  public SensingPuckJobEntityBase convert(SensingPuckJobDtoBase job) {
    if (job instanceof V1SensingPuckJobDto) {
      return convert((V1SensingPuckJobDto) job);
    }
    if (job instanceof V2SensingPuckJobDto) {
      return convert((V2SensingPuckJobDto) job);
    }
    if (job instanceof V3SensingPuckJobDto) {
      return convert((V3SensingPuckJobDto) job);
    }

    throw new IllegalArgumentException("Unknown type: " + job.getClass());
  }

  private V1SensingPuckJobEntity convert(V1SensingPuckJobDto job) {
    return new V1SensingPuckJobEntity(
        String.valueOf(job.getJobId()),
        null,
        deviceIntegrationProperties.getDeviceSource(),
        Long.toString(job.getDeviceId()),
        Instant.EPOCH,
        Instant.EPOCH,
        job.getProtocolVersion(),
        job.getJobState(),
        job.getDeviceId(),
        job.isClearAlarm(),
        job.getMeasurementCycles(),
        job.getDataAmount(),
        job.getTemperatureAlarm(),
        job.getHumidityAlarm(),
        job.getLedTimings()
    );
  }

  private V2SensingPuckJobEntity convert(V2SensingPuckJobDto job) {
    return new V2SensingPuckJobEntity(
        String.valueOf(job.getJobId()),
        null,
        deviceIntegrationProperties.getDeviceSource(),
        Long.toString(job.getDeviceId()),
        Instant.EPOCH,
        Instant.EPOCH,
        job.getProtocolVersion(),
        job.getJobState(),
        job.getDeviceId(),
        job.isClearAlarm(),
        job.getMeasurementCycles(),
        job.getDataAmount(),
        job.getTemperatureAlarm(),
        job.getHumidityAlarm(),
        job.getVoltageAlarm(),
        job.getLedTimings()
    );
  }

  private V3SensingPuckJobEntity convert(V3SensingPuckJobDto job) {
    return new V3SensingPuckJobEntity(
        String.valueOf(job.getJobId()),
        null,
        deviceIntegrationProperties.getDeviceSource(),
        Long.toString(job.getDeviceId()),
        Instant.EPOCH,
        Instant.EPOCH,
        job.getProtocolVersion(),
        job.getJobState(),
        job.getDeviceId(),
        job.isClearAlarm(),
        job.getMeasurementCycles(),
        job.getDataAmount(),
        job.getPackagingTime(),
        job.getTemperatureAlarm(),
        job.getHumidityAlarm(),
        job.getVoltageAlarm(),
        job.getLightAlarm(),
        job.getLedTimings()
    );
  }
}
