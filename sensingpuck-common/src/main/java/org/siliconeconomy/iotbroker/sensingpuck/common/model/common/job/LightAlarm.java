/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Illuminance alarm settings.
 * <p>
 * This class defines the illuminance alarm settings of a sensing puck device. If a sensor
 * reading violates {@link LightAlarm#upperBound} or {@link LightAlarm#lowerBound} an alarm
 * is triggered.
 *
 * @author M. Grzenia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LightAlarm {

  /**
   * The upper illuminance threshold (in lx).
   */
  private float upperBound;
  /**
   * The lower illuminance threshold (in lx).
   */
  private float lowerBound;
}
