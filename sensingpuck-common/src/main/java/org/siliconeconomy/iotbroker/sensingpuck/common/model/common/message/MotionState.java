/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

/**
 * Motion state of a sensing puck device.
 * <p>
 * This enumeration lists the possible states of motion for a sensing puck device. Since a device is
 * capable of detecting motion the two possible states are no-motion({@link MotionState#STATIC}) or
 * motion({@link MotionState#MOVING}).
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
public enum MotionState {
  /**
   * The state in which a sensing puck is considered moving.
   */
  MOVING("Moving"),
  /**
   * The state in which a sensing puck is considered static/not moving.
   */
  STATIC("Static");

  /**
   * The string representation of a {@link MotionState}.
   */
  @Getter
  @JsonValue
  private final String stringRepresentation;

  MotionState(String stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }
}
