/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

/**
 * Cause of communication of a sensing puck device.
 * <p>
 * In order to conserve energy a sensing puck device communicates rarely with the IoT Broker.
 * This enumeration lists all the possible reasons a sensing puck device initiates communication
 * with the adapter.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
public enum CommCause {

  /**
   * Indicates that communication has been started due to an alarm (e.g. because a configured
   * alarm threshold has been exceeded).
   */
  ALARM("Alarm"),
  /**
   * Indicates that communication has been started due to data (e.g. because the configured amount
   * of data has been captured).
   */
  DATA("Data"),
  /**
   * Indicates that communication has been started due to a job request.
   */
  JOB_REQUEST("JobRequest"),
  /**
   * Indicates that communication has been started due to motion detected by the device.
   */
  MOTION("Motion"),
  /**
   * Indicates that communication has been started due to a pre alarm (e.g. because a configured
   * alarm threshold is about to be exceeded).
   */
  PRE_ALARM("PreAlarm"),
  /**
   * Indicates that communication has been started due to some user interaction.
   */
  USER("User");

  /**
   * The string representation of a {@link CommCause}.
   */
  @Getter
  @JsonValue
  private final String stringRepresentation;

  CommCause(String stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }
}
