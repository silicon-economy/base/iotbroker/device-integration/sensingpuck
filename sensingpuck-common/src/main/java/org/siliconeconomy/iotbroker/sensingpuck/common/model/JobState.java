/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Defines the state a job can take.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@AllArgsConstructor
public enum JobState {
  /**
   * The job is currently active and being worked on.
   */
  ACTIVE("Active"),
  /**
   * The job has been finished and is not longer being worked on.
   */
  FINISHED("Finished");

  @Getter
  @JsonValue
  private final String stringRepresentation;
}
