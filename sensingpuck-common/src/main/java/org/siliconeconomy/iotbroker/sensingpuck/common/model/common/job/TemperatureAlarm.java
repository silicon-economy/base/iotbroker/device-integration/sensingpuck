/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Ambient temperature alarm settings.
 * <p>
 * This class defines the ambient temperature alarm settings of a sensing puck device. If a sensor
 * reading violates {@link TemperatureAlarm#upperBound} or {@link TemperatureAlarm#lowerBound} an
 * alarm is triggered.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemperatureAlarm {

  /**
   * The upper temperature threshold (in degrees Celsius).
   */
  private float upperBound;
  /**
   * The lower temperature threshold (in degrees Celsius).
   */
  private float lowerBound;
}
