/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.common.jobs;

/**
 * Thrown to indicate that a job with a specific ID could not be found.
 *
 * @author M. Grzenia
 */
public class JobNotFoundException
    extends RuntimeException {

  public JobNotFoundException(long jobId) {
    super(String.format("Could not find job with id '%d'.", jobId));
  }
}
