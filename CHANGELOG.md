# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.8.0] - 2023-09-22
### Changed
- Update IoT Broker SDK to 7.0.0.
- Update Device Registry Service Client to 2.2.1.

## [2.7.1] - 2023-09-20
### Fixed
- Fix handling of updates from the Device Registry Service regarding the SensingPuck Adapter's device type or corresponding device instances.
  Previously, such updates were ignored, resulting, for example, in the adapter not being able to be disabled.

## [2.7.0] - 2023-08-22
### Removed
- Remove `LoggingScheduledThreadPoolExecutor`.
  Use implementation from IoT Broker SDK instead.

## [2.6.0] - 2023-08-11
### Changed
- Update IoT Broker SDK to 6.1.0.
- Update Device Registry Service Client to 2.2.0.

## [2.5.2] - 2023-05-03
### Changed
- Relicense under the OLFL-1.3.

## [2.5.1] - 2023-05-03
### Changed
- Delay closing the connection to connected clients, which helps in situations where clients fail to fully read the data they received from the adapter after the adapter has already closed the connection.

## [2.5.0] - 2023-04-13
### Added
- Add support for version 3 of the SensingPuck communication protocol.
  - Extend the SensingPuck Adapter to allow communication with SensingPuck devices in protocol version 1, 2 and 3.
  - Extend the SensingPuck Connector and add a new version of its web API specification (v3.0.0) and a corresponding implementation that supports working with SensingPuck jobs and messages in protocol version 1, 2 and 3.
### Changed
- Adjust the implementation of the SensingPuck Connector's v2.0.0 web API to be explicitly restricted to SensingPuck jobs and messages in protocol version 1 and 2.

## [2.4.2] - 2023-02-14
### Fixed
- Enable artifact deployment for the sensingpuck-common project.
  This fixes a not resolvable transitive dependency when including the SensingPuck Adapter or Connector in other projects.

## [2.4.1] - 2023-02-09
### Changed
- Update Spring Boot to 2.7.8.
  Also, update Maven plugins to versions that are compatible with the new Spring Boot version.
- Add an "exec" classifier to the artifact produced by the spring-boot-maven-plugin.
  In addition to executable artifacts, traditional JAR artifacts (for the SensingPuck Adapter and Connector) are now published for other projects to use as dependencies.

## [2.4.0] - 2023-02-02
### Added
- Add support for publishing (raw) device messages to the IoT Broker's _Device Message Exchange_.

### Changed
- Update IoT Broker SDK to 5.6.0.

## [2.3.0] - 2023-01-20
### Changed
- Adjust the configuration capabilities of the SensingPuck Adapter and SensingPuck Connector applications.
  With the newly added configuration properties listed below, the adapter and connector can be configured for use with a _device label_ other than "sensingpuck".
  - Add the following configuration properties:
    - `ADAPTER_SERVER_TLS_PSK_IDENTITY` - The PSK identity expected to be used by client devices.
    - `ADAPTER_SERVER_TLS_PSK_IDENTITY_HINT` - The PSK identity hint to provide to clients (to help them in selecting which identity to use).
    - `DEVICEINTEGRATION_DEVICE_SOURCE` - The source that devices (for which the adapter provides an IoT Broker integration) represent.
    - `DEVICEINTEGRATION_DEVICE_TYPE_DESCRIPTION` - The description for the adapter's device type.
    - `DEVICEINTEGRATION_DEVICE_INSTANCE_DESCRIPTION` - The description for device instances the adapter provides an IoT Broker integration for.
  - Rename the following configuration properties:
    - `COUCHDB_DATABASENAME_JOBS` to `COUCHDB_JOB_DATABASE_NAME`
    - `COUCHDB_DATABASENAME_MESSAGES` to `COUCHDB_MESSAGE_DATABASE_NAME`
    - `ADAPTER_CONNECTION_THREADPOOL_SIZE` to `ADAPTER_CONNECTION_THREADPOOL_SIZE`
    - `API_DEVICEREGISTRYSERVICE_URL` to `ADAPTER_DEVICE_REGISTRY_SERVICE_API_URL` and `CONNECTOR_DEVICE_REGISTRY_SERVICE_API_URL`
    - `CORS_ALLOWEDORIGINS` to `CONNECTOR_CORS_ALLOWED_ORIGIN_PATTERNS`

## [2.2.0] - 2023-01-19
### Added
- Endpoints for deleting all SensingPuck messages and jobs to the SensingPuck Connector's web API.

## [2.1.2] - 2022-11-25
### Fixed
- Fix handling of devices that are already registered but not associated with the adapter's device type.
  Previously, in such cases, the adapter would (wrongly) attempt to register the devices (and fail).
  Now the adapter closes the connection to such devices immediately and any received messages are discarded.

## [2.1.1] - 2022-11-22
### Changed
- Adjust the adapter's AMQP-related configurations to be more explicit.
  Explicitly configure (and if necessary create) the AMQP exchanges, queues and bindings required/used by the adapter.
  Additionally, the adapter now no longer relies on a Spring message converter to publish Sensor Data Messages via AMQP.

## [2.1.0] - 2022-10-18
### Changed
- Adjust the SensingPuck Connector to no longer use a custom client for accessing the Device Registry Service and instead use the client provided by the service itself.
- Schedule v1.0.0 of the SensingPuck Connector's web API for removal with the next major version (v3.x.x) of the project.

## [2.0.0] - 2022-10-17
### Changed
- Adjust the communication with SensingPuck devices to use the Transport Layer Security (TLS) protocol.
  The authentication is based on a pre-shared key (PSK).

### Removed
- Communication with SensingPuck devices that don't use TLS is no longer supported.

## [1.4.0] - 2022-10-04
### Changed
- Adjust the way the SensingPuck Adapter handles device registration.
  Previously, the SensingPuck Adapter used the Device Registry Service's old web API for registering new devices.
  Now, the SensingPuck Adapter uses the Device Registry Service's new web API and adheres to the device registration process specified in the IoT Broker documentation.
- Introduce an abstraction layer (with interfaces and abstract classes) for IoT Broker device adapters.

## [1.3.1] - 2022-08-09
### Changed
- Improve the adapter's logging in cases where an error occurs during client communication.

## [1.3.0] - 2022-08-02
### Added
- Add support for version 2 of the SensingPuck communication protocol.
  - Extend the SensingPuck Adapter to allow communication with SensingPuck devices in both protocol version 1 and 2.
  - Extend the SensingPuck Connector and add a new version of its web API specification (v2.0.0) and a corresponding implementation that supports working with SensingPuck jobs and messages in both protocol version 1 and 2.

### Changed
- Adjust the implementation of the SensingPuck Connector's old web API specification (v1.0.0) to only support SensingPuck jobs and messages in protocol version 1.
  This ensures compatibility with clients that currently use the older web API.
  Additionally, this is now also documented in the web API specification itself.
- Update the SensingPuck Connector's old web API specification (v1.0.0) to reflect the current implementation state.
- Mark all endpoints in the SensingPuck Connector's old web API specification (v1.0.0) as deprecated in favor of the new web API specification (v2.0.0).

## [1.2.3] - 2022-04-01
### Changed
- Update Spring Boot to v2.5.12.
  This Spring Boot version addresses CVE-2022-22965.

## [1.2.2] - 2022-01-21
### Changed
- Update Spring Boot to v2.5.9.
  This Spring Boot version includes Log4j v2.17.1, which fixes a number of vulnerabilities that have been reported in recent weeks.
  For more information on the reported vulnerabilities, see the following links:
  - CVE-2021-44228, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228
  - CVE-2021-45046, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046
  - CVE-2021-45105, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105
  - CVE-2021-44832, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

## [1.2.1] - 2021-12-14
### Changed
- Update Log4j dependencies (especially `log4j-core`) to v2.15.0.
  A vulnerability has been reported with CVE-2021-44228 against the `log4j-core` jar and has been fixed in Log4j v2.15.0.
  Until Log4j v2.15.0 is picked up by Spring Boot, which is planned for the Spring Boot v2.5.8 & v2.6.2 releases (due Dec 23, 2021), the Log4j dependencies are overridden manually.
  For more information on CVE-2021-44228, see https://nvd.nist.gov/vuln/detail/CVE-2021-44228.

## [1.2.0] - 2021-11-23
### Changed
- Adjust handling of active jobs and allow only one active job to be assigned to a device at a time.
  This rule is now also taken into account in the SensingPuck Connector's web API when creating new jobs or updating existing ones.

## [1.1.2] - 2021-11-22
### Changed
- Adjust exception handling in the SensingPuck Connector's web API, resulting in more expressive error responses for clients.

## [1.1.1] - 2021-11-17
### Changed
- Adjust the style of license headers.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [1.1.0] - 2021-11-03
### Added
- This CHANGELOG file to keep track of the changes in this project.
- SensingPuck Connector: route `/devices/{deviceID}/jobs/finish` to set all jobs of a given device to the state
  `Finished`.

## [1.0.0] - 2021-10-15
This is the first public release.

The device integration for SensingPuck devices adds the following abilities to the Iot Broker:

- Communication with SensingPuck devices
- Integration with the IoT Broker's core services
- Configuration of SensingPuck devices
- Query SensingPuck messages

This release corresponds to IoT Broker v0.9.
