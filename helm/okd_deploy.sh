#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=iot-broker}"
FULLNAME="sensingpuck"

# Upgrade or install
helm --set-string="fullnameOverride=${FULLNAME}" upgrade -n "$NAMESPACE" -i sensingpuck . || exit 1
# Ensure image stream picks up the new docker image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}-adapter"
oc -n "$NAMESPACE" import-image "${FULLNAME}-connector"
