/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v3;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.sensingpuck.connector.CustomResultMatchers.responseBody;
import static org.siliconeconomy.iotbroker.sensingpuck.connector.TestDataFactory.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test cases for {@link V3MessageController}.
 *
 * @author C. Pionzewski
 * @author M. Grzenia
 */
@WebMvcTest(controllers = V3MessageController.class)
@TestPropertySource("/application-test-connector.properties")
class V3MessageControllerTest {

  /**
   * Test dependencies.
   */
  @MockBean
  private MessageService service;
  /**
   * Test environment.
   */
  @Autowired
  private MockMvc mockMvc;

  @Test
  void fetchMessages_whenJobIdNotProvided_thenReturnsDeviceSpecificMessages()
      throws Exception {
    // Arrange
    List<SensingPuckMessageDtoBase> messages = List.of(v1MessageDtoWithDefaults(),
                                                       v2MessageDtoWithDefaults(),
                                                       v3MessageDtoWithDefaults());
    when(service.findAllByDeviceIdAndTimestamp(eq(1L),
                                               any(Sort.Direction.class),
                                               anyInt(),
                                               anyInt(),
                                               any(Instant.class),
                                               any(Instant.class)))
        .thenReturn(messages);

    // Act & Assert
    mockMvc
        .perform(get("/v3/messages")
                     .queryParam("deviceId", "1"))
        .andExpect(status().isOk())
        .andExpect(responseBody().containsObjectAsJson(messages));

    // Verify
    verify(service).findAllByDeviceIdAndTimestamp(eq(1L),
                                                  any(Sort.Direction.class),
                                                  anyInt(),
                                                  anyInt(),
                                                  any(Instant.class),
                                                  any(Instant.class));
  }

  @Test
  void fetchMessages_whenJobIdProvided_thenReturnsJobSpecificMessages()
      throws Exception {
    // Arrange
    List<SensingPuckMessageDtoBase> messages = List.of(v1MessageDtoWithDefaults(),
                                                       v2MessageDtoWithDefaults(),
                                                       v3MessageDtoWithDefaults());
    when(service.findAllByDeviceIdAndJobIdAndTimestamp(eq(1L),
                                                       eq(1L),
                                                       any(Sort.Direction.class),
                                                       anyInt(),
                                                       anyInt(),
                                                       any(Instant.class),
                                                       any(Instant.class)))
        .thenReturn(messages);

    // Act & Assert
    mockMvc
        .perform(get("/v3/messages")
                     .queryParam("deviceId", "1")
                     .queryParam("jobId", "1"))
        .andExpect(status().isOk())
        .andExpect(responseBody().containsObjectAsJson(messages));

    // Verify
    verify(service).findAllByDeviceIdAndJobIdAndTimestamp(eq(1L),
                                                          eq(1L),
                                                          any(Sort.Direction.class),
                                                          anyInt(),
                                                          anyInt(),
                                                          any(Instant.class),
                                                          any(Instant.class));
  }

  @Test
  void fetchMessages_whenOnlyRequiredParamsProvided_thenCallsServiceWithDefaults()
      throws Exception {
    // Act & Assert
    mockMvc
        .perform(get("/v3/messages")
                     .queryParam("deviceId", "1"))
        .andExpect(status().isOk());

    // Verify
    verify(service).findAllByDeviceIdAndTimestamp(1L,
                                                  Sort.Direction.ASC,
                                                  0,
                                                  100,
                                                  Instant.parse("1970-01-01T00:00:00Z"),
                                                  Instant.parse("9999-12-31T23:59:59.999999Z"));
  }

  @Test
  void fetchMessages_whenAllParamsProvided_thenCallsServiceWithParams()
      throws Exception {
    // Act & Assert
    mockMvc
        .perform(get("/v3/messages")
                     .queryParam("deviceId", "1")
                     .queryParam("jobId", "1")
                     .queryParam("startTime", "2022-07-22T13:37:00Z")
                     .queryParam("endTime", "2022-07-23T13:33:37Z")
                     .queryParam("offset", "3")
                     .queryParam("limit", "7")
                     .queryParam("sort", "desc"))
        .andExpect(status().isOk());

    // Verify
    verify(service).findAllByDeviceIdAndJobIdAndTimestamp(1L,
                                                          1L,
                                                          Sort.Direction.DESC,
                                                          3,
                                                          7,
                                                          Instant.parse("2022-07-22T13:37:00Z"),
                                                          Instant.parse("2022-07-23T13:33:37Z"));
  }

  @Test
  void testDeleteAllMessages()
      throws Exception {
    // Act
    mockMvc
        .perform(delete("/v3/messages"))
        .andReturn();

    // Assert & Verify
    verify(service).deleteAll();
  }
}
