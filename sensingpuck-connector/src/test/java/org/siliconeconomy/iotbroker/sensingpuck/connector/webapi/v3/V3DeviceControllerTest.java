/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v3;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.connector.devices.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test cases for {@link V3DeviceController}.
 *
 * @author C. Pionzewski
 * @author M. Grzenia
 */
@WebMvcTest(controllers = V3DeviceController.class)
@TestPropertySource("/application-test-connector.properties")
class V3DeviceControllerTest {

  /**
   * Test dependencies.
   */
  @MockBean
  private DeviceService service;
  /**
   * Test environment.
   */
  @Autowired
  private MockMvc mockMvc;

  @Test
  void fetchDeviceIds_whenDevicesAvailable_thenReturnsDeviceIds()
      throws Exception {
    // Arrange
    when(service.findAllSensingPuckIds()).thenReturn(List.of(1L, 2L, 4L));

    // Act & Assert
    mockMvc
        .perform(get("/v3/devices/ids"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$", hasSize(3)))
        .andExpect(jsonPath("$", hasItems(1, 2, 4)));

    // Verify
    verify(service).findAllSensingPuckIds();
  }
}
