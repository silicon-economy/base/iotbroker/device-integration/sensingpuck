/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;

/**
 * Defines constants of objects that are commonly used across different tests.
 *
 * @author M. Grzenia
 */
public class TestConstants {

  public static final DeviceIntegrationProperties DEVICE_INTEGRATION_PROPERTIES
      = new DeviceIntegrationProperties("sensingpuck",
                                        "",
                                        "");
}
