/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.FirmwareDescriptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;

import java.time.Instant;
import java.util.ArrayList;

/**
 * A factory that provides convenient methods for creating test data/objects.
 *
 * @author M. Grzenia
 */
public class TestDataFactory {

  public static V1SensingPuckMessageDto v1MessageDtoWithDefaults() {
    return new V1SensingPuckMessageDto(
        1,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V1SensingPuckJobDto v1JobDtoWithDefaults() {
    return new V1SensingPuckJobDto(
        1,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        new LedTimings(1, 5, 5)
    );
  }

  public static V2SensingPuckMessageDto v2MessageDtoWithDefaults() {
    return new V2SensingPuckMessageDto(
        2,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V2SensingPuckJobDto v2JobDtoWithDefaults() {
    return new V2SensingPuckJobDto(
        2,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        3.0f,
        new LedTimings(1, 5, 5)
    );
  }

  public static V3SensingPuckMessageDto v3MessageDtoWithDefaults() {
    return new V3SensingPuckMessageDto(
        3,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        new ArrayList<>()
    );
  }

  public static V3SensingPuckJobDto v3JobDtoWithDefaults() {
    return new V3SensingPuckJobDto(
        3,
        1,
        JobState.ACTIVE,
        1,
        false,
        new MeasurementCycles(0, 1),
        1,
        10,
        new TemperatureAlarm(5.0f, 25.0f),
        new HumidityAlarm(30.0f, 60.0f),
        3.0f,
        new LightAlarm(600.0f, 300.0f),
        new LedTimings(1, 5, 5)
    );
  }
}
