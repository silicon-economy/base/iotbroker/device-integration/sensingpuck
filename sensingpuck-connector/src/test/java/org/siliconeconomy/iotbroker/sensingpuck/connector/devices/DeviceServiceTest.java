/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.devices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.sensingpuck.connector.TestConstants;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceService}.
 *
 * @author M. Grzenia
 */
class DeviceServiceTest {

  /**
   * Class under test.
   */
  private DeviceService service;
  /**
   * Test dependencies.
   */
  private DeviceRegistryServiceClient deviceRegistryServiceClient;

  @BeforeEach
  void setUp() {
    deviceRegistryServiceClient = mock(DeviceRegistryServiceClient.class);
    service = new DeviceService(deviceRegistryServiceClient,
                                TestConstants.DEVICE_INTEGRATION_PROPERTIES);
  }

  @Test
  void checkSensingPuckExists_whenSensingPuckExists_thenDoesNothing() {
    // Arrange
    when(deviceRegistryServiceClient.fetchDeviceInstances(any(), any(), any(), any(), any(), any()))
        .thenReturn(List.of(createDeviceInstance("sensingpuck", "1")));

    // Act & Assert
    assertThatCode(() -> service.checkSensingPuckExists(1)).doesNotThrowAnyException();
  }

  @Test
  void checkSensingPuckExists_whenSensingPuckDoesNotExist_thenThrowsException() {
    // Arrange
    when(deviceRegistryServiceClient.fetchDeviceInstances(any(), any(), any(), any(), any(), any()))
        .thenReturn(List.of());

    // Act & Assert
    assertThatThrownBy(() -> service.checkSensingPuckExists(1)).
        isInstanceOf(DeviceNotFoundException.class);
  }

  @Test
  void findAllSensingPuckIds_whenSensingPucksRegistered_thenReturnsTheirIds() {
    // Arrange
    DeviceInstance device1 = createDeviceInstance("sensingpuck", "1");
    DeviceInstance device2 = createDeviceInstance("sensingpuck", "3");
    DeviceInstance device3 = createDeviceInstance("sensingpuck", "7");
    when(deviceRegistryServiceClient.fetchDeviceInstances(
             eq(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource()),
             eq(null),
             any(),
             any(),
             any(),
             any()
         )
    ).thenReturn(List.of(device1, device2, device3));

    // Act
    List<Long> result = service.findAllSensingPuckIds();

    // Assert
    assertThat(result)
        .hasSize(3)
        .contains(1L, 3L, 7L);
  }

  private DeviceInstance createDeviceInstance(String source, String tenant) {
    return new DeviceInstance("",
                              source,
                              tenant,
                              "",
                              Instant.EPOCH,
                              Instant.EPOCH,
                              false,
                              "",
                              "",
                              "");
  }
}
