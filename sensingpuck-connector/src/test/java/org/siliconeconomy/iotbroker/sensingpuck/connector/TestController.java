/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller for testing purposes.
 *
 * @author M. Grzenia
 */
@RestController
public class TestController {

  @GetMapping("/throwUnhandledException")
  public void throwUnhandledException() {
    throw new UnhandledException();
  }

  private static class UnhandledException
      extends RuntimeException {
  }
}
