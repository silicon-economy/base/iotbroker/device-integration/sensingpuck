/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.annotation.RestController;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Defines some {@link ResultMatcher}s to help with testing {@link RestController}s.
 *
 * @author M. Grzenia
 */
public class CustomResultMatchers {

  private final ObjectMapper objectMapper;

  private CustomResultMatchers() {
    // We don't have access to the object mapper instance provided by Spring (which is already
    // configured properly). Therefore, configure the object mapper to allow handling of
    // Java 8 date/time types.
    objectMapper = new ObjectMapper()
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new JavaTimeModule());
  }

  public static CustomResultMatchers responseBody() {
    return new CustomResultMatchers();
  }

  public ResultMatcher containsObjectAsJson(Object expectedObject) {
    return mvcResult -> {
      String responseJson = mvcResult.getResponse().getContentAsString();
      String expectedJson = objectMapper.writeValueAsString(expectedObject);
      assertThat(responseJson).isEqualToIgnoringWhitespace(expectedJson);
    };
  }
}
