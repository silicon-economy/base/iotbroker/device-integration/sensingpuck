/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.ConnectorProperties;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Test cases for {@link V1MessageController}.
 *
 * @author D. Ronnenberg
 */
@SpringBootTest(classes = {V1MessageController.class, WebConfig.class})
@EnableConfigurationProperties(ConnectorProperties.class)
@TestPropertySource("/application-test-connector.properties")
class V1MessageControllerTest {
  private MockMvc mockMvc;

  @MockBean
  private MessageService messageService;

  @Autowired
  @InjectMocks
  private V1MessageController messageController;

  @BeforeEach
  public void init() {
    this.mockMvc = MockMvcBuilders
        .standaloneSetup(messageController)
        .build();
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testGetAllMessagesDefault()
      throws Exception {
    when(messageService.findAllByDeviceIdAndJobIdAndTimestamp(anyLong(), anyLong(),
                                                              any(Sort.Direction.class), anyInt(), anyInt(), any(Instant.class), any(Instant.class)))
        .thenReturn(List.of());

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/messages/{deviceId}/{jobId}", "1", "2"))
        .andDo(mvcResult -> verify(messageService, times(1))
            .findAllByDeviceIdAndJobIdAndTimestamp(2L, 1L, Sort.Direction.ASC, 0, 100,
                                                   Instant.parse("1970-01-01T00:00:00Z"),
                                                   Instant.parse("9999-12-31T23:59:59.999999Z")));
  }

  @Test
  void testGetAllMessagesLimit()
      throws Exception {
    when(messageService.findAllByDeviceIdAndJobIdAndTimestamp(anyLong(), anyLong(),
                                                              any(Sort.Direction.class), anyInt(), anyInt(), any(Instant.class), any(Instant.class)))
        .thenReturn(List.of());

    String starTime = "2001-01-01T00:00:00Z";
    String endTime = "2002-01-01T00:00:00Z";

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/messages/{deviceId}/{jobId}", "1", "2")
                        .param("startTime", starTime)
                        .param("endTime", endTime)
                        .param("offset", "100")
                        .param("limit", "1"))
        .andDo(mvcResult -> verify(messageService, times(1))
            .findAllByDeviceIdAndJobIdAndTimestamp(2L, 1L, Sort.Direction.ASC, 100, 1,
                                                   Instant.parse(starTime),
                                                   Instant.parse(endTime)));
  }

  @Test
  void testGetAllMessagesInvalidTimestamp()
      throws Exception {
    when(messageService.findAllByDeviceIdAndJobIdAndTimestamp(anyLong(), anyLong(),
                                                              any(Sort.Direction.class), anyInt(), anyInt(), any(Instant.class), any(Instant.class)))
        .thenReturn(List.of());

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/messages/{deviceId}/{jobId}", "1", "2")
                        .param("startTime", "INVALID_TIMESTAMP"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void testGetAllMessagesByDeviceIdDefault()
      throws Exception {
    when(messageService.findAllByDeviceIdAndJobIdAndTimestamp(anyLong(), anyLong(),
                                                              any(Sort.Direction.class), anyInt(), anyInt(), any(Instant.class), any(Instant.class)))
        .thenReturn(List.of());

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/messages/{deviceId}", "1"))
        .andDo(mvcResult -> verify(messageService, times(1)).findAllByDeviceIdAndTimestamp(
            1L, Sort.Direction.ASC, 0, 100,
            Instant.parse("1970-01-01T00:00:00Z"),
            Instant.parse("9999-12-31T23:59:59.999999Z")));
  }

  @Test
  void testGetAllMessagesByDeviceIdLimit()
      throws Exception {
    when(messageService.findAllByDeviceIdAndJobIdAndTimestamp(anyLong(), anyLong(),
                                                              any(Sort.Direction.class), anyInt(), anyInt(), any(Instant.class), any(Instant.class)))
        .thenReturn(List.of());

    String starTime = "2001-01-01T00:00:00Z";
    String endTime = "2002-01-01T00:00:00Z";

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/messages/{deviceId}", "1")
                        .param("startTime", starTime)
                        .param("endTime", endTime)
                        .param("offset", "100")
                        .param("limit", "1"))
        .andDo(mvcResult -> verify(messageService, times(1)).findAllByDeviceIdAndTimestamp(
            1L, Sort.Direction.ASC, 100, 1,
            Instant.parse(starTime),
            Instant.parse(endTime)
        ));
  }

  @Test
  void testGetAllMessagesByDeviceIdInvalidTimestamp()
      throws Exception {
    when(messageService.findAllByDeviceIdAndJobIdAndTimestamp(anyLong(), anyLong(),
                                                              any(Sort.Direction.class), anyInt(), anyInt(), any(Instant.class), any(Instant.class)))
        .thenReturn(List.of());

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/messages/{deviceId}", "1")
                        .param("startTime", "INVALID_TIMESTAMP"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }
}
