/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.DeviceHasActiveJobException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobUpdateConflictException;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.connector.CustomResultMatchers.responseBody;
import static org.siliconeconomy.iotbroker.sensingpuck.connector.TestDataFactory.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test cases for {@link V2JobController}.
 *
 * @author M.Grzenia
 */
@WebMvcTest(controllers = V2JobController.class)
@TestPropertySource("/application-test-connector.properties")
class V2JobControllerTest {

  /**
   * Test dependencies.
   */
  @MockBean
  private JobService service;
  /**
   * Test environment.
   */
  @Autowired
  private MockMvc mockMvc;
  private final ObjectMapper objectMapper = new ObjectMapper()
      .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      .registerModule(new JavaTimeModule());

  @Test
  void fetchJobs_whenJobsAvailable_thenReturnsJobs()
      throws Exception {
    // Arrange
    List<SensingPuckJobDtoBase> jobs = List.of(v1JobDtoWithDefaults(),
                                               v2JobDtoWithDefaults());
    when(service.findAllByDeviceId(anyLong())).thenReturn(jobs);

    // Act & Assert
    mockMvc
        .perform(get("/v2/jobs")
                     .queryParam("deviceId", "1"))
        .andExpect(status().isOk())
        .andExpect(responseBody().containsObjectAsJson(jobs));

    // Verify
    verify(service).findAllByDeviceId(1);
  }

  @Test
  void fetchJobs_whenJobsAvailable_thenReturnsJobsWithoutV3()
      throws Exception {
    // Arrange
    List<SensingPuckJobDtoBase> jobs = List.of(v1JobDtoWithDefaults(),
                                               v2JobDtoWithDefaults());
    List<SensingPuckJobDtoBase> jobsWithV3 = List.of(v1JobDtoWithDefaults(),
                                                     v2JobDtoWithDefaults(),
                                                     v3JobDtoWithDefaults());
    when(service.findAllByDeviceId(anyLong())).thenReturn(jobsWithV3);

    // Act & Assert
    mockMvc
        .perform(get("/v2/jobs")
                     .queryParam("deviceId", "1"))
        .andExpect(status().isOk())
        .andExpect(responseBody().containsObjectAsJson(jobs));

    // Verify
    verify(service).findAllByDeviceId(1);
  }

  @Test
  void addJob_whenV1JobThatDoesNotExist_thenReturns201()
      throws Exception {
    // Arrange
    V1SensingPuckJobDto job = v1JobDtoWithDefaults();

    // Act & Assert
    mockMvc
        .perform(post("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isCreated());

    // Verify
    verify(service).add(job);
  }

  @Test
  void addJob_whenV2JobThatDoesNotExist_thenReturns201()
      throws Exception {
    // Arrange
    V2SensingPuckJobDto job = v2JobDtoWithDefaults();

    // Act & Assert
    mockMvc
        .perform(post("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isCreated());

    // Verify
    verify(service).add(job);
  }

  @Test
  void addJob_whenJobThatAlreadyExists_thenReturns409()
      throws Exception {
    // Arrange
    V1SensingPuckJobDto job = v1JobDtoWithDefaults();
    doThrow(JobUpdateConflictException.class).when(service).add(job);

    // Act & Assert
    mockMvc
        .perform(post("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isConflict());

    // Verify
    verify(service).add(job);
  }

  @Test
  void addJob_whenV3Job_thenReturns400()
      throws Exception {
    // Arrange
    V3SensingPuckJobDto job = v3JobDtoWithDefaults();

    // Act & Assert
    mockMvc
        .perform(post("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isBadRequest());

    // Verify
    verify(service, never()).add(job);
  }

  @Test
  void updateJob_whenV1JobThatExists_thenReturns200()
      throws Exception {
    // Arrange
    V1SensingPuckJobDto job = v1JobDtoWithDefaults();

    // Act & Assert
    mockMvc
        .perform(put("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isOk());

    // Verify
    verify(service).update(job);
  }

  @Test
  void updateJob_whenV2JobThatExists_thenReturns200()
      throws Exception {
    // Arrange
    V2SensingPuckJobDto job = v2JobDtoWithDefaults();

    // Act & Assert
    mockMvc
        .perform(put("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isOk());

    // Verify
    verify(service).update(job);
  }

  @Test
  void updateJob_whenV3Job_thenReturns400()
      throws Exception {
    // Arrange
    V3SensingPuckJobDto job = v3JobDtoWithDefaults();

    // Act & Assert
    mockMvc
        .perform(put("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isBadRequest());

    // Verify
    verify(service, never()).update(job);
  }

  @Test
  void fetchJob_whenV1JobThatExists_thenReturnsV1Job()
      throws Exception {
    // Arrange
    SensingPuckJobDtoBase job = v1JobDtoWithDefaults();
    when(service.findDtoByJobId(anyLong())).thenReturn(job);

    // Act & Assert
    mockMvc
        .perform(get("/v2/jobs/{jobId}", 1))
        .andExpect(status().isOk())
        .andExpect(responseBody().containsObjectAsJson(job));

    // Verify
    verify(service).findDtoByJobId(1);
  }

  @Test
  void fetchJob_whenV2JobThatExists_thenReturnsV2Job()
      throws Exception {
    // Arrange
    SensingPuckJobDtoBase job = v2JobDtoWithDefaults();
    when(service.findDtoByJobId(anyLong())).thenReturn(job);

    // Act & Assert
    mockMvc
        .perform(get("/v2/jobs/{jobId}", 1))
        .andExpect(status().isOk())
        .andExpect(responseBody().containsObjectAsJson(job));

    // Verify
    verify(service).findDtoByJobId(1);
  }

  @Test
  void fetchJob_whenV3JobThatExists_thenReturns404()
      throws Exception {
    // Arrange
    SensingPuckJobDtoBase job = v3JobDtoWithDefaults();
    when(service.findDtoByJobId(anyLong())).thenReturn(job);

    // Act & Assert
    mockMvc
        .perform(get("/v2/jobs/{jobId}", 1))
        .andExpect(status().isNotFound());

    // Verify
    verify(service).findDtoByJobId(1);
  }

  @Test
  void fetchJobIds_whenJobsAvailable_thenReturnsJobIds()
      throws Exception {
    // Arrange
    List<SensingPuckJobDtoBase> jobs = List.of(v1JobDtoWithDefaults(),
                                               v2JobDtoWithDefaults(),
                                               v3JobDtoWithDefaults());
    when(service.findAllByDeviceId(anyLong())).thenReturn(jobs);

    // Act & Assert
    mockMvc
        .perform(get("/v2/jobs/ids")
                     .queryParam("deviceId", "1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").isArray())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$", hasItems(1, 1)));

    // Verify
    verify(service).findAllByDeviceId(1L);
  }

  @Test
  void updateJobStatesFinished_whenCalled_thenReturns200()
      throws Exception {
    // Arrange
    List<SensingPuckJobDtoBase> jobs = List.of(v1JobDtoWithDefaults());
    when(service.findAllByDeviceId(anyLong())).thenReturn(jobs);

    // Act & Assert
    mockMvc
        .perform(put("/v2/jobs/finish")
                     .queryParam("deviceId", "1"))
        .andExpect(status().isOk());

    // Verify
    verify(service).update(jobs.get(0));
  }

  @Test
  void whenJobThatDoesNotExist_thenReturns404()
      throws Exception {
    // Arrange
    V2SensingPuckJobDto job = v2JobDtoWithDefaults();
    doThrow(JobNotFoundException.class).when(service).update(job);

    // Act & Assert
    mockMvc
        .perform(put("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isNotFound());

    // Verify
    verify(service).update(job);
  }

  @Test
  void whenActiveJobConflict_thenReturns409()
      throws Exception {
    // Arrange
    V1SensingPuckJobDto job = v1JobDtoWithDefaults();
    job.setJobState(JobState.ACTIVE);
    doThrow(DeviceHasActiveJobException.class).when(service).add(job);

    // Act & Assert
    mockMvc
        .perform(post("/v2/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(toJson(job)))
        .andExpect(status().isConflict());

    // Verify
    verify(service).add(job);
  }

  @Test
  void testDeleteAll()
      throws Exception {
    // Act
    mockMvc
        .perform(delete("/v2/jobs"))
        .andReturn();

    // Assert & Verify
    verify(service).deleteAll(isA(V1AndV2JobEntitiesFilter.class));
  }

  private String toJson(Object object) {
    try {
      return objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Could not parse object to JSON.", e);
    }
  }
}
