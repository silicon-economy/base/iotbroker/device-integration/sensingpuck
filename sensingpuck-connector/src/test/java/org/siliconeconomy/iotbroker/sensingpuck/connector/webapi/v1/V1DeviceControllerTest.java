/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.connector.RequestExceptionHandler;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.ConnectorProperties;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.WebConfig;
import org.siliconeconomy.iotbroker.sensingpuck.connector.devices.DeviceNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.connector.devices.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.connector.TestDataFactory.v1JobDtoWithDefaults;

/**
 * Test cases for {@link V1DeviceController}.
 *
 * @author D. Ronnenberg
 */
@SpringBootTest(classes = {V1DeviceController.class, WebConfig.class})
@EnableWebMvc
@EnableConfigurationProperties(ConnectorProperties.class)
@TestPropertySource("/application-test-connector.properties")
class V1DeviceControllerTest {
  private MockMvc mockMvc;

  @MockBean
  private DeviceService deviceService;

  @MockBean
  private JobService jobService;

  @Autowired
  @InjectMocks
  private V1DeviceController deviceController;

  private final ObjectMapper objectMapper = new ObjectMapper()
      .registerModule(new JavaTimeModule())
      .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);

  @BeforeEach
  public void init() {
    this.mockMvc = MockMvcBuilders
        .standaloneSetup(deviceController)
        .setControllerAdvice(new RequestExceptionHandler())
        .build();
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void getDevices()
      throws Exception {
    when(this.deviceService.findAllSensingPuckIds()).thenReturn(List.of(1L, 2L, 3L, 4L, 5L));

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/devices"))
        .andDo(mvcResult -> {
          String response = mvcResult.getResponse().getContentAsString();
          Integer[] device_ids = objectMapper.readValue(response, Integer[].class);
          Assertions.assertThat(device_ids)
              .hasSize(5)
              .contains(1, 2, 3, 4, 5);
        });
  }

  @Test
  void getJobsOfDevice()
      throws Exception {
    when(this.jobService.findAllByDeviceId(anyLong())).thenReturn(List.of(v1JobDtoWithDefaults()));

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/devices/1337/jobs"))
        .andDo(mvcResult -> {
          String response = mvcResult.getResponse().getContentAsString();
          V1SensingPuckJobDto[] jobs = objectMapper.readValue(response, V1SensingPuckJobDto[].class);
          Assertions.assertThat(jobs)
              .hasSize(1)
              .contains(v1JobDtoWithDefaults());
        });
  }

  @Test
  void getJobIdsOfDevice()
      throws Exception {
    when(this.jobService.findAllByDeviceId(anyLong())).thenReturn(List.of(v1JobDtoWithDefaults()));

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/devices/1337/jobs/ids"))
        .andDo(mvcResult -> {
          String response = mvcResult.getResponse().getContentAsString();
          Long[] jobIds = objectMapper.readValue(response, Long[].class);
          Assertions.assertThat(jobIds)
              .hasSize(1)
              .contains(v1JobDtoWithDefaults().getJobId());
        });
  }

  @Test
  void setAllJobsFinishedOfDevice()
      throws Exception {
    // Arrange
    var job = v1JobDtoWithDefaults();

    doNothing().when(deviceService).checkSensingPuckExists(anyLong());
    when(jobService.findAllByDeviceId(anyLong())).thenReturn(List.of(job));

    // Act & assert
    mockMvc.perform(MockMvcRequestBuilders
                        .put("/devices/42/jobs/finish"))
        .andExpect(MockMvcResultMatchers.status().isCreated());

    // verify that the job is saved to db via update method
    verify(jobService, times(1)).update(job);

    // check that the job has been altered
    Assertions.assertThat(job.getJobState()).isEqualTo(JobState.FINISHED);
  }

  @Test
  void setAllJobsFinishedOfDeviceDeviceNotFound()
      throws Exception {
    // Arrange
    doThrow(DeviceNotFoundException.class).when(deviceService).checkSensingPuckExists(anyLong());

    // Act & assert
    mockMvc.perform(MockMvcRequestBuilders
                        .put("/devices/42/jobs/finish"))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }
}

