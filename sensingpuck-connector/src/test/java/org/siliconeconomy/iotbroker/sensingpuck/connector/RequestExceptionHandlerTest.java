/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test cases for {@link RequestExceptionHandler}.
 * <p>
 * Utilizes {@link TestController}.
 *
 * @author M. Grzenia
 */
@WebMvcTest(controllers = TestController.class)
@TestPropertySource("/application-test-connector.properties")
class RequestExceptionHandlerTest {

  /**
   * Test environment.
   */
  @Autowired
  private MockMvc mockMvc;

  @Test
  void whenUnhandledException_thenReturns500()
      throws Exception {
    // Act & Assert
    mockMvc
        .perform(get("/throwUnhandledException"))
        .andExpect(status().isInternalServerError())
        .andExpect(content().string("An internal error occurred while fetching/processing data."));
  }
}

