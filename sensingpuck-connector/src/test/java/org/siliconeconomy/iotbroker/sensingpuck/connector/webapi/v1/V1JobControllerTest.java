/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.DeviceHasActiveJobException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobUpdateConflictException;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.connector.RequestExceptionHandler;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.ConnectorProperties;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.connector.TestDataFactory.v1JobDtoWithDefaults;

/**
 * Test cases for {@link V1JobController}.
 *
 * @author D. Ronnenberg
 */
@SpringBootTest(classes = {V1JobController.class, WebConfig.class})
@EnableWebMvc
@EnableConfigurationProperties(ConnectorProperties.class)
@TestPropertySource("/application-test-connector.properties")
class V1JobControllerTest {
  private MockMvc mockMvc;

  @MockBean
  private JobService jobService;

  @Autowired
  @InjectMocks
  private V1JobController jobController;

  private final ObjectMapper objectMapper =
      new ObjectMapper().registerModule(new JavaTimeModule())
          .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);

  @BeforeEach
  public void init() {
    this.mockMvc = MockMvcBuilders
        .standaloneSetup(jobController)
        .setControllerAdvice(new RequestExceptionHandler())
        .build();
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testGetJob()
      throws Exception {
    when(jobService.findDtoByJobId(anyLong())).thenReturn(v1JobDtoWithDefaults());

    mockMvc
        .perform(MockMvcRequestBuilders
                     .get("/jobs/123"))
        .andDo(mvcResult -> {
          String response = mvcResult.getResponse().getContentAsString();
          V1SensingPuckJobDto job = objectMapper.readValue(response, V1SensingPuckJobDto.class);
          Assertions.assertThat(job)
              .isEqualTo(v1JobDtoWithDefaults());
        });
  }

  @Test
  void testGetUnknownJob()
      throws Exception {
    when(jobService.findDtoByJobId(anyLong())).thenThrow(new JobNotFoundException(123));

    mockMvc
        .perform(MockMvcRequestBuilders
                     .get("/jobs/123"))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  void testAddJob()
      throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders
                     .post("/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(objectMapper.writeValueAsString(v1JobDtoWithDefaults())))
        .andDo(mvcResult -> {
          verify(jobService, times(1)).add(v1JobDtoWithDefaults());
        });
  }

  @Test
  void testAddJobButDeviceHasAlreadyActiveJob()
      throws Exception {
    doThrow(DeviceHasActiveJobException.class)
        .when(jobService).add(v1JobDtoWithDefaults());

    mockMvc
        .perform(MockMvcRequestBuilders
                     .post("/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(objectMapper.writeValueAsString(v1JobDtoWithDefaults())))
        .andExpect(MockMvcResultMatchers.status().isConflict());
  }

  @Test
  void testAddExistingJob()
      throws Exception {
    doThrow(new JobUpdateConflictException(123))
        .when(jobService).add(any(SensingPuckJobDtoBase.class));

    mockMvc
        .perform(MockMvcRequestBuilders
                     .post("/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(objectMapper.writeValueAsString(v1JobDtoWithDefaults())))
        .andExpect(MockMvcResultMatchers.status().isConflict());
  }

  @Test
  void testUpdateJob()
      throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders
                     .put("/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(objectMapper.writeValueAsString(v1JobDtoWithDefaults())))
        .andDo(mvcResult -> {
          verify(jobService, times(1)).update(v1JobDtoWithDefaults());
        });
  }

  @Test
  void testUpdateJobButDeviceHasAlreadyActiveJob()
      throws Exception {
    doThrow(DeviceHasActiveJobException.class)
        .when(jobService).update(v1JobDtoWithDefaults());

    mockMvc
        .perform(MockMvcRequestBuilders
                     .put("/jobs")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(objectMapper.writeValueAsString(v1JobDtoWithDefaults())))
        .andExpect(MockMvcResultMatchers.status().isConflict());
  }

  @Test
  void testUpdateNonExistingJob()
      throws Exception {
    doThrow(new JobNotFoundException(123))
        .when(jobService).update(any(V1SensingPuckJobDto.class));

    mockMvc.perform(MockMvcRequestBuilders
                        .put("/jobs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(v1JobDtoWithDefaults())))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }
}
