/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of Spring MVC.
 * Enables CORS for the endpoints provided by the backend and adds the frontend's URL to the list of
 * allowed origins.
 *
 * @author M. Grzenia
 */
@Configuration
public class WebConfig
    implements WebMvcConfigurer {

  private final ConnectorProperties connectorProperties;

  /**
   * Creates a new instance.
   *
   * @param connectorProperties The connector's configuration properties.
   */
  public WebConfig(ConnectorProperties connectorProperties) {
    this.connectorProperties = requireNonNull(connectorProperties, "connectorProperties");
  }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
        .allowedOriginPatterns(
            connectorProperties.getCorsAllowedOriginPatterns().toArray(new String[0])
        )
        .allowedMethods("*");
  }
}
