/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import org.siliconeconomy.iotbroker.sensingpuck.common.config.CouchDbProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.connector.config.ConnectorProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * The application starting point.
 *
 * @author D. Ronnenberg
 */
@SpringBootApplication(
    scanBasePackages = {
        "org.siliconeconomy.iotbroker.sensingpuck.common",
        "org.siliconeconomy.iotbroker.sensingpuck.connector"
    }
)
@EnableConfigurationProperties({
    ConnectorProperties.class,
    CouchDbProperties.class,
    DeviceIntegrationProperties.class
})
public class SensingPuckConnectorApplication {
  public static void main(String[] args) {
    SpringApplication.run(SensingPuckConnectorApplication.class, args);
  }
}
