/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;

import org.siliconeconomy.iotbroker.sensingpuck.connector.devices.DeviceNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.DeviceHasActiveJobException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobUpdateConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * Handles exceptions thrown in any rest controller by providing corresponding responses for
 * different kind of exceptions.
 *
 * @author M. Grzenia
 */
@RestControllerAdvice
public class RequestExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(RequestExceptionHandler.class);

  @ExceptionHandler(DeviceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public String handleDeviceNotFoundException(DeviceNotFoundException exception) {
    return exception.getMessage();
  }

  @ExceptionHandler(JobNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public String handleJobNotFoundException(JobNotFoundException exception) {
    return exception.getMessage();
  }

  @ExceptionHandler(JobUpdateConflictException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public String handleJobUpdateConflictException(JobUpdateConflictException exception) {
    return exception.getMessage();
  }

  @ExceptionHandler(DeviceHasActiveJobException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public String handleDeviceHasActiveJobException(DeviceHasActiveJobException exception) {
    return exception.getMessage();
  }

  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public String handleIllegalArgumentException(IllegalArgumentException exception){
    return exception.getMessage();
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public String handleUnhandledException(Exception exception, HttpServletRequest request) {
    LOGGER.error(
        "An unhandled exception occurred while a client accessed: {} {}",
        request.getMethod(),
        request.getRequestURI(),
        exception
    );
    return "An internal error occurred while fetching/processing data.";
  }
}
