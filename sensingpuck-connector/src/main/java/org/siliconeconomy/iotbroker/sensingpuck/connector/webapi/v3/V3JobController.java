/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v3;

import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Provides endpoints to query sensing puck jobs.
 * <p>
 * This implementation supports handling of {@link ProtocolVersion#V1},
 * {@link ProtocolVersion#V2} and {@link ProtocolVersion#V3} jobs.
 *
 * @author C. Pionzewski
 */
@RestController
@RequestMapping("/v3/jobs")
public class V3JobController {

  /**
   * The {@link JobService} to use for performing operations on jobs.
   */
  private final JobService jobService;

  @Autowired
  public V3JobController(JobService jobService) {
    this.jobService = jobService;
  }

  /**
   * Returns a list of {@link SensingPuckJobDtoBase}.
   *
   * @param deviceId The ID of the sensing puck device to fetch jobs for.
   * @return A list of {@link SensingPuckJobDtoBase}.
   */
  @GetMapping
  public List<SensingPuckJobDtoBase> fetchJobs(@RequestParam long deviceId) {
    return jobService.findAllByDeviceId(deviceId);
  }

  /**
   * Adds a new {@link SensingPuckJobDtoBase}.
   *
   * @param job The job to add.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public void addJob(@RequestBody SensingPuckJobDtoBase job) {
    jobService.add(job);
  }

  /**
   * Updates a job in the database using the given one.
   *
   * @param job The updated job.
   */
  @PutMapping
  public void updateJob(@RequestBody SensingPuckJobDtoBase job) {
    jobService.update(job);
  }

  /**
   * Returns the {@link SensingPuckJobDtoBase} with the given job ID.
   *
   * @param jobId The ID of the job to fetch.
   * @return The {@link SensingPuckJobDtoBase} with the given job ID.
   */
  @GetMapping("/{jobId}")
  public SensingPuckJobDtoBase fetchJob(@PathVariable long jobId) {
    return jobService.findDtoByJobId(jobId);
  }

  /**
   * Returns a list of sensing puck job IDs.
   *
   * @param deviceId The ID of the sensing puck device to fetch job IDs for.
   * @return A list of sensing puck job IDs.
   */
  @GetMapping("/ids")
  public List<Long> fetchJobIds(@RequestParam long deviceId) {
    return jobService.getJobIdsByDeviceId(deviceId);
  }

  /**
   * Updates jobs associated with the device with the given ID and sets their state to
   * {@link JobState#FINISHED}.
   *
   * @param deviceId The ID of the sensing puck device for which the state of associated jobs should
   *                 be set to {@link JobState#FINISHED}.
   */
  @PutMapping("/finish")
  public void updateJobStatesFinished(@RequestParam long deviceId) {
    jobService.updateJobStatesFinished(deviceId);
  }

  /**
   * Deletes all jobs in the database.
   */
  @DeleteMapping
  public void deleteJobs() {
    jobService.deleteAll();
  }
}
