/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.config;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of various application beans.
 *
 * @author M. Grzenia
 */
@Configuration
public class ApplicationConfig {

  private final ConnectorProperties connectorProperties;

  /**
   * Creates a new instance.
   *
   * @param connectorProperties The connector's configuration properties.
   */
  public ApplicationConfig(ConnectorProperties connectorProperties) {
    this.connectorProperties = requireNonNull(connectorProperties, "connectorProperties");
  }

  @Bean
  public DeviceRegistryServiceClient deviceRegistryServiceClient() {
    return new DeviceRegistryServiceClientBuilder()
        .baseUrl(connectorProperties.getDeviceRegistryServiceApiUrl())
        .build();
  }
}
