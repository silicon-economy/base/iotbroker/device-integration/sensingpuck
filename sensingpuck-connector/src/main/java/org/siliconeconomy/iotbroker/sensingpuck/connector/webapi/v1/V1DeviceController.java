/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v1;

import org.siliconeconomy.iotbroker.sensingpuck.connector.devices.DeviceService;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides REST endpoints to query Sensing Puck devices and jobs.
 * <p>
 * The implemented endpoints are limited to handling {@link ProtocolVersion#V1} jobs.
 *
 * @author D. Ronnenberg
 */
@RestController
public class V1DeviceController {

  /**
   * Used to retrieve data from the database.
   */
  private final JobService jobService;
  private final DeviceService deviceService;

  @Autowired
  public V1DeviceController(JobService jobService, DeviceService deviceService) {
    this.jobService = jobService;
    this.deviceService = deviceService;
  }

  /**
   * Returns all known Sensing Puck devices.
   *
   * @return A list of device IDs (Integer).
   */
  @GetMapping("/devices")
  public List<Long> devices() {
    return this.deviceService.findAllSensingPuckIds();
  }

  /**
   * Returns all jobs for a given device.
   *
   * @param deviceID The device's ID.
   * @return A list of {@link SensingPuckJobDtoBase}.
   */
  @GetMapping("/devices/{deviceID}/jobs")
  @ResponseStatus(HttpStatus.OK)
  public List<V1SensingPuckJobDto> getAllJobsOfDevice(@PathVariable("deviceID") long deviceID) {
    return jobService.findAllByDeviceId(deviceID).stream()
        .filter(V1SensingPuckJobDto.class::isInstance)
        .map(V1SensingPuckJobDto.class::cast)
        .collect(Collectors.toList());
  }

  /**
   * Sets all jobs for a given device to the status {@link JobState#FINISHED}.
   *
   * @param deviceID The device's ID.
   */
  @PutMapping("/devices/{deviceID}/jobs/finish")
  @ResponseStatus(HttpStatus.CREATED)
  public void setAllJobsFinishedOfDevice(@PathVariable("deviceID") long deviceID) {
    // check if the device exists
    deviceService.checkSensingPuckExists(deviceID);

    // update each job
    List<SensingPuckJobDtoBase> jobs = jobService.findAllByDeviceId(deviceID).stream()
        .filter(V1SensingPuckJobDto.class::isInstance)
        .map(V1SensingPuckJobDto.class::cast)
        .collect(Collectors.toList());
    for (var job : jobs) {
      job.setJobState(JobState.FINISHED);
      jobService.update(job);
    }
  }

  /**
   * Returns all job IDs for a given device.
   *
   * @param deviceID The device's ID.
   * @return A list of IDs (Integer).
   */
  @GetMapping("/devices/{deviceID}/jobs/ids")
  @ResponseStatus(HttpStatus.OK)
  public List<Long> getAllJobIDsOfDevice(@PathVariable("deviceID") long deviceID) {
    return jobService.findAllByDeviceId(deviceID)
        .stream()
        .filter(V1SensingPuckJobDto.class::isInstance)
        .map(SensingPuckJobDtoBase::getJobId)
        .collect(Collectors.toList());
  }

  /**
   * Returns single job for given job ID and device ID.
   *
   * @param jobId    The job's ID.
   * @param deviceID The device's ID.
   * @return The requested {@link V1SensingPuckJobDto}.
   */
  @GetMapping("/devices/{deviceID}/jobs/{jobID}")
  @ResponseStatus(HttpStatus.OK)
  public V1SensingPuckJobDto getJob(@PathVariable("deviceID") long deviceID,
                                    @PathVariable("jobID") long jobId) {
    SensingPuckJobDtoBase job = jobService.findDtoByJobId(jobId);
    if (job instanceof V1SensingPuckJobDto) {
      return (V1SensingPuckJobDto) job;
    }

    throw new JobNotFoundException(jobId);
  }
}
