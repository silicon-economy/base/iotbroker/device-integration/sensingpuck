/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v2;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;

import java.util.function.Predicate;

/**
 * A {@link Predicate} that returns {@code true} for {@link V1SensingPuckJobEntity} and
 * {@link V2SensingPuckJobEntity} instances.
 *
 * @author M. Grzenia
 */
public class V1AndV2JobEntitiesFilter
    implements Predicate<SensingPuckJobEntityBase> {

  @Override
  public boolean test(SensingPuckJobEntityBase sensingPuckJobEntityBase) {
    return sensingPuckJobEntityBase instanceof V1SensingPuckJobEntity
        || sensingPuckJobEntityBase instanceof V2SensingPuckJobEntity;
  }
}
