/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v2;

import org.siliconeconomy.iotbroker.sensingpuck.connector.devices.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Provides endpoints to query sensing puck devices.
 *
 * @author M. Grzenia
 */
@RestController
@RequestMapping("/v2/devices")
public class V2DeviceController {

  /**
   * The {@link DeviceService} to use for performing operations on devices.
   */
  private final DeviceService deviceService;

  @Autowired
  public V2DeviceController(DeviceService deviceService) {
    this.deviceService = deviceService;
  }

  /**
   * Returns a list of all known sensing puck device IDs.
   *
   * @return A list of all known sensing puck device IDs.
   */
  @GetMapping("/ids")
  public List<Long> fetchDeviceIds() {
    return deviceService.findAllSensingPuckIds();
  }
}
