/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v2;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;

import java.util.function.Predicate;

/**
 * A {@link Predicate} that returns {@code true} for {@link V1SensingPuckMessageEntity} and
 * {@link V2SensingPuckMessageEntity} instances.
 *
 * @author M. Grzenia
 */
public class V1AndV2MessageEntitiesFilter
    implements Predicate<SensingPuckMessageEntityBase> {

  @Override
  public boolean test(SensingPuckMessageEntityBase sensingPuckMessageEntityBase) {
    return sensingPuckMessageEntityBase instanceof V1SensingPuckMessageEntity
        || sensingPuckMessageEntityBase instanceof V2SensingPuckMessageEntity;
  }
}
