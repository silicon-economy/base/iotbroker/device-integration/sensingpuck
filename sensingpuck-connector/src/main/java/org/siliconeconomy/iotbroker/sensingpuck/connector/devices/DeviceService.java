/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.devices;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides methods for accessing the IoT Broker's Device Registry Service through its web API.
 *
 * @author M. Grzenia
 */
@Service
public class DeviceService {

  private final DeviceRegistryServiceClient deviceRegistryServiceClient;
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param deviceRegistryServiceClient The client to use for request to the Device Registry
   *                                    Service's web API.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  @Autowired
  public DeviceService(DeviceRegistryServiceClient deviceRegistryServiceClient,
                       DeviceIntegrationProperties deviceIntegrationProperties) {
    this.deviceRegistryServiceClient = deviceRegistryServiceClient;
    this.deviceIntegrationProperties = deviceIntegrationProperties;
  }

  /**
   * Checks whether a sensing puck with the given {@code deviceId} is registered with the
   * IoT Broker.
   *
   * @param deviceId The ID of the sensing puck to check.
   * @throws DeviceNotFoundException If a sensing puck with the given {@code deviceId} could not be
   *                                 found.
   */
  public void checkSensingPuckExists(long deviceId) {
    List<DeviceInstance> deviceInstances = deviceRegistryServiceClient.fetchDeviceInstances(
        deviceIntegrationProperties.getDeviceSource(),
        String.valueOf(deviceId),
        null,
        null,
        null,
        null
    );

    if (deviceInstances.isEmpty()) {
      throw new DeviceNotFoundException(deviceIntegrationProperties.getDeviceSource(),
                                        String.valueOf(deviceId));
    }
  }

  /**
   * Returns a list of IDs of all sensing puck devices registered with the IoT Broker.
   *
   * @return A list of IDs of all sensing puck devices registered with the IoT Broker.
   */
  public List<Long> findAllSensingPuckIds() {
    List<DeviceInstance> deviceInstances = deviceRegistryServiceClient.fetchDeviceInstances(
        deviceIntegrationProperties.getDeviceSource(),
        null,
        0,
        Integer.MAX_VALUE,
        null,
        null
    );

    return deviceInstances.stream()
        .map(DeviceInstance::getTenant)
        .map(Long::parseLong)
        .collect(Collectors.toList());
  }
}
