/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v3;

import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Provides endpoints to query sensing puck messages.
 * <p>
 * This implementation supports handling of {@link ProtocolVersion#V1},
 * {@link ProtocolVersion#V2} and {@link ProtocolVersion#V3} messages.
 *
 * @author C. Pionzewski
 */
@RestController
@RequestMapping("/v3/messages")
public class V3MessageController {

  /**
   * The {@link MessageService} to use for performing operations on messages.
   */
  private final MessageService messageService;

  @Autowired
  public V3MessageController(MessageService messageService) {
    this.messageService = messageService;
  }

  /**
   * Returns a list of {@link SensingPuckMessageDtoBase}s.
   *
   * @param deviceId  The ID of the sensing puck device for which messages are to be queried.
   * @param jobId     The ID of the sensing puck job for which messages are to be queried.
   * @param startTime The lower limit (regarding the {@code comTimestamp}) for which messages are to
   *                  be queried. Messages before this time will be ignored.
   * @param endTime   The upper limit (regarding the {@code comTimestamp}) for which messages are to
   *                  be queried. Messages after this time will be ignored.
   * @param offset    The number of elements to skip.
   * @param limit     The maximum number of elements to return.
   * @param sort      The order in which the returned elements should be sorted (regarding the
   *                  {@code comTimestamp}).
   * @return A list of {@link SensingPuckMessageDtoBase}.
   */
  @GetMapping
  public List<SensingPuckMessageDtoBase> fetchMessages(
      @RequestParam long deviceId,
      @RequestParam Optional<Long> jobId,
      @RequestParam(required = false, defaultValue = "1970-01-01T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant startTime,
      @RequestParam(required = false, defaultValue = "9999-12-31T23:59:59.999999Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant endTime,
      @RequestParam(required = false, defaultValue = "0") int offset,
      @RequestParam(required = false, defaultValue = "100") int limit,
      @RequestParam(required = false, defaultValue = "asc") String sort
  ) {
    if (jobId.isEmpty()) {
      return messageService.findAllByDeviceIdAndTimestamp(deviceId,
                                                          Sort.Direction.fromString(sort),
                                                          offset,
                                                          limit,
                                                          startTime,
                                                          endTime);

    } else {
      return messageService.findAllByDeviceIdAndJobIdAndTimestamp(jobId.get(),
                                                                  deviceId,
                                                                  Sort.Direction.fromString(sort),
                                                                  offset,
                                                                  limit,
                                                                  startTime,
                                                                  endTime);

    }
  }

  /**
   * Deletes all messages in the database.
   */
  @DeleteMapping
  public void deleteMessages() {
    messageService.deleteAll();
  }
}
