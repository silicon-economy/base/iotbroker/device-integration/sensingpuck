/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Defines connector-relevant (configuration) properties (that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "connector")
@ConstructorBinding
@Getter
public class ConnectorProperties {

  private final List<String> corsAllowedOriginPatterns;
  private final String deviceRegistryServiceApiUrl;

  /**
   * Creates a new instance.
   *
   * @param corsAllowedOriginPatterns   The patterns of origins for which cross-origin requests to
   *                                    the connector's web API are allowed from a browser.
   * @param deviceRegistryServiceApiUrl The URL to the Device Registry Service's web API.
   */
  public ConnectorProperties(List<String> corsAllowedOriginPatterns,
                             String deviceRegistryServiceApiUrl) {
    this.corsAllowedOriginPatterns = requireNonNull(corsAllowedOriginPatterns,
                                                    "corsAllowedOriginPatterns");
    this.deviceRegistryServiceApiUrl = requireNonNull(deviceRegistryServiceApiUrl,
                                                      "deviceRegistryServiceApiUrl");
  }
}
