/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v1;

import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides REST endpoints to query messages of sensing pucks.
 * <p>
 * The implemented endpoints are limited to handling {@link ProtocolVersion#V1} messages.
 *
 * @author D. Ronnenberg
 */
@RestController
@RequestMapping("/messages")
public class V1MessageController {

  /**
   * Used to retrieve messages from the database.
   */
  private final MessageService messageService;

  @Autowired
  public V1MessageController(MessageService messageService) {
    this.messageService = messageService;
  }

  /**
   * Get all messages for given job ID and device ID.
   *
   * @param deviceID  The ID of the device that is requested.
   * @param jobID     The ID of the job that is requested.
   * @param startTime The lower limit of time that is of interest. All messages earlier will be
   *                  filtered out.
   * @param endTime   The upper limit of time that is of interest. All messages later will be
   *                  filtered out.
   * @param offset    Number of elements to ignore at the beginning.
   * @param limit     Maximal number of elements to return.
   * @param sort      The order in which the elements are sorted (sorted by time).
   * @return A list of {@link V1SensingPuckMessageDto}.
   */
  @GetMapping("/{deviceId}/{jobId}")
  public List<V1SensingPuckMessageDto> getAllMessages(
      @PathVariable("deviceId") long deviceID,
      @PathVariable("jobId") long jobID,
      @RequestParam(defaultValue = "1970-01-01T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant startTime,
      @RequestParam(defaultValue = "9999-12-31T23:59:59.999999Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant endTime,
      @RequestParam(defaultValue = "0") int offset,
      @RequestParam(defaultValue = "100") int limit,
      @RequestParam(defaultValue = "asc") String sort
  ) {
    return messageService.findAllByDeviceIdAndJobIdAndTimestamp(jobID, deviceID,
                                                                Sort.Direction.fromString(sort),
                                                                offset,
                                                                limit,
                                                                startTime,
                                                                endTime).stream()
        .filter(V1SensingPuckMessageDto.class::isInstance)
        .map(V1SensingPuckMessageDto.class::cast)
        .collect(Collectors.toList());
  }

  /**
   * Get all messages for a given device ID.
   *
   * @param deviceID  The ID of the device that is requested.
   * @param startTime The lower limit of time that is of interest. All messages earlier will be
   *                  filtered out.
   * @param endTime   The upper limit of time that is of interest. All messages later will be
   *                  filtered out.
   * @param offset    Number of elements to ignore at the beginning.
   * @param limit     Maximal number of elements to return.
   * @param sort      The order in which the elements are sorted (sorted by time).
   * @return A list of {@link V1SensingPuckMessageDto}.
   */
  @GetMapping("/{deviceId}")
  public List<V1SensingPuckMessageDto> getAllMessagesByDeviceId(
      @PathVariable("deviceId") long deviceID,
      @RequestParam(defaultValue = "1970-01-01T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant startTime,
      @RequestParam(defaultValue = "9999-12-31T23:59:59.999999Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant endTime,
      @RequestParam(defaultValue = "0") int offset,
      @RequestParam(defaultValue = "100") int limit,
      @RequestParam(defaultValue = "asc") String sort
  ) {
    return messageService.findAllByDeviceIdAndTimestamp(deviceID,
                                                        Sort.Direction.fromString(sort),
                                                        offset,
                                                        limit,
                                                        startTime,
                                                        endTime).stream()
        .filter(V1SensingPuckMessageDto.class::isInstance)
        .map(V1SensingPuckMessageDto.class::cast)
        .collect(Collectors.toList());
  }
}
