/**
 * The root package of the sensing puck connector application.
 * Contains the main class.
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector;
