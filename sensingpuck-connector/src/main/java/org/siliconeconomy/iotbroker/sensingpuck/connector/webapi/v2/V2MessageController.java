/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v2;

import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Provides endpoints to query sensing puck messages.
 * <p>
 * This implementation supports handling of {@link ProtocolVersion#V1} and
 * {@link ProtocolVersion#V2} messages.
 *
 * @author M. Grzenia
 * @author C. Pionzewski
 */
@RestController
@RequestMapping("/v2/messages")
public class V2MessageController {

  /**
   * The {@link MessageService} to use for performing operations on messages.
   */
  private final MessageService messageService;
  /**
   * The {@link Predicate} to use for filtering {@link ProtocolVersion#V1} and
   * {@link ProtocolVersion#V2} messages.
   */
  private final Predicate<SensingPuckMessageEntityBase> messageEntitiesFilter
      = new V1AndV2MessageEntitiesFilter();

  @Autowired
  public V2MessageController(MessageService messageService) {
    this.messageService = messageService;
  }

  /**
   * Returns a list of {@link SensingPuckMessageDtoBase}s.
   *
   * @param deviceId  The ID of the sensing puck device for which messages are to be queried.
   * @param jobId     The ID of the sensing puck job for which messages are to be queried.
   * @param startTime The lower limit (regarding the {@code comTimestamp}) for which messages are to
   *                  be queried. Messages before this time will be ignored.
   * @param endTime   The upper limit (regarding the {@code comTimestamp}) for which messages are to
   *                  be queried. Messages after this time will be ignored.
   * @param offset    The number of elements to skip.
   * @param limit     The maximum number of elements to return.
   * @param sort      The order in which the returned elements should be sorted (regarding the
   *                  {@code comTimestamp}).
   * @return A list of {@link SensingPuckMessageDtoBase}.
   */
  @GetMapping
  public List<SensingPuckMessageDtoBase> fetchMessages(
      @RequestParam long deviceId,
      @RequestParam Optional<Long> jobId,
      @RequestParam(required = false, defaultValue = "1970-01-01T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant startTime,
      @RequestParam(required = false, defaultValue = "9999-12-31T23:59:59.999999Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant endTime,
      @RequestParam(required = false, defaultValue = "0") int offset,
      @RequestParam(required = false, defaultValue = "100") int limit,
      @RequestParam(required = false, defaultValue = "asc") String sort
  ) {
    if (jobId.isEmpty()) {
      return messageService.findAllByDeviceIdAndTimestamp(deviceId,
                                                          Sort.Direction.fromString(sort),
                                                          offset,
                                                          limit,
                                                          startTime,
                                                          endTime).stream()
          .filter(
              job -> job instanceof V1SensingPuckMessageDto
                  || job instanceof V2SensingPuckMessageDto
          )
          .collect(Collectors.toList());

    } else {
      return messageService.findAllByDeviceIdAndJobIdAndTimestamp(jobId.get(),
                                                                  deviceId,
                                                                  Sort.Direction.fromString(sort),
                                                                  offset,
                                                                  limit,
                                                                  startTime,
                                                                  endTime).stream()
          .filter(
              job -> job instanceof V1SensingPuckMessageDto
                  || job instanceof V2SensingPuckMessageDto
          )
          .collect(Collectors.toList());
    }
  }

  /**
   * Deletes all {@link ProtocolVersion#V1} or {@link ProtocolVersion#V2} messages in the database.
   */
  @DeleteMapping
  public void deleteMessages() {
    messageService.deleteAll(messageEntitiesFilter);
  }
}
