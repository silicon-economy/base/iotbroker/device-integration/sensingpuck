/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v2;

import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Provides endpoints to query sensing puck jobs.
 * <p>
 * This implementation supports handling of {@link ProtocolVersion#V1} and
 * {@link ProtocolVersion#V2} jobs.
 *
 * @author M. Grzenia
 * @author C. Pionzewski
 */
@RestController
@RequestMapping("/v2/jobs")
public class V2JobController {

  /**
   * The {@link JobService} to use for performing operations on jobs.
   */
  private final JobService jobService;
  /**
   * The {@link Predicate} to use for filtering {@link ProtocolVersion#V1} and
   * {@link ProtocolVersion#V2} jobs.
   */
  private final Predicate<SensingPuckJobEntityBase> jobEntitiesFilter
      = new V1AndV2JobEntitiesFilter();

  @Autowired
  public V2JobController(JobService jobService) {
    this.jobService = jobService;
  }

  /**
   * Returns a list of {@link SensingPuckJobDtoBase} of either {@link ProtocolVersion#V1} or
   * {@link ProtocolVersion#V2}.
   *
   * @param deviceId The ID of the sensing puck device to fetch jobs for.
   * @return A list of {@link SensingPuckJobDtoBase}.
   */
  @GetMapping
  public List<SensingPuckJobDtoBase> fetchJobs(@RequestParam long deviceId) {
    return jobService.findAllByDeviceId(deviceId).stream()
        .filter(job -> job instanceof V2SensingPuckJobDto || job instanceof V1SensingPuckJobDto)
        .collect(Collectors.toList());
  }

  /**
   * Adds a new {@link SensingPuckJobDtoBase}.
   *
   * @param job The job as {@link V1SensingPuckJobDto} or {@link V2SensingPuckJobDto} to add.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public void addJob(@RequestBody SensingPuckJobDtoBase job) {
    if (job instanceof V2SensingPuckJobDto || job instanceof V1SensingPuckJobDto) {
      jobService.add(job);
    } else {
      throw new IllegalArgumentException(
          "The /v2 web API can not create a version 3 SensingPuck job. Please use /v3 instead.");
    }
  }

  /**
   * Updates a job in the database using the given one.
   *
   * @param job The updated job.
   */
  @PutMapping
  public void updateJob(@RequestBody SensingPuckJobDtoBase job) {
    if (job instanceof V2SensingPuckJobDto || job instanceof V1SensingPuckJobDto) {
      jobService.update(job);
    } else {
      throw new IllegalArgumentException(
          "The /v2 web API can not update a version 3 SensingPuck job. Please use /v3 instead.");
    }
  }

  /**
   * Returns the {@link V1SensingPuckJobDto} or {@link V2SensingPuckJobDto} with the given job ID.
   *
   * @param jobId The ID of the job to fetch.
   * @return The {@link V1SensingPuckJobDto} or {@link V2SensingPuckJobDto} with the given job ID.
   */
  @GetMapping("/{jobId}")
  public SensingPuckJobDtoBase fetchJob(@PathVariable long jobId) {
    SensingPuckJobDtoBase job = jobService.findDtoByJobId(jobId);
    if (job instanceof V2SensingPuckJobDto || job instanceof V1SensingPuckJobDto) {
      return job;
    } else {
      throw new JobNotFoundException(jobId);
    }
  }


  /**
   * Returns a list of sensing puck job IDs.
   *
   * @param deviceId The ID of the sensing puck device to fetch job IDs for.
   * @return A list of sensing puck {@link V1SensingPuckJobDto} or {@link V2SensingPuckJobDto} IDs.
   */
  @GetMapping("/ids")
  public List<Long> fetchJobIds(@RequestParam long deviceId) {
    return jobService.findAllByDeviceId(deviceId).stream()
        .filter(job -> job instanceof V2SensingPuckJobDto || job instanceof V1SensingPuckJobDto)
        .map(SensingPuckJobDtoBase::getJobId)
        .collect(Collectors.toList());
  }

  /**
   * Updates {@link ProtocolVersion#V1} or {@link ProtocolVersion#V2} jobs associated with the device
   * with the given ID and sets their state to {@link JobState#FINISHED}.
   *
   * @param deviceId The ID of the sensing puck device for which the state of associated jobs should
   *                 be set to {@link JobState#FINISHED}.
   */
  @PutMapping("/finish")
  public void updateJobStatesFinished(@RequestParam long deviceId) {
    // collect and update each v1 or v2 job
    List<SensingPuckJobDtoBase> jobs = jobService.findAllByDeviceId(deviceId).stream()
        .filter(job -> job instanceof V2SensingPuckJobDto || job instanceof V1SensingPuckJobDto)
        .collect(Collectors.toList());
    for (var job : jobs) {
      job.setJobState(JobState.FINISHED);
      jobService.update(job);
    }
  }

  /**
   * Deletes all {@link ProtocolVersion#V1} or {@link ProtocolVersion#V2} jobs in the database.
   */
  @DeleteMapping
  public void deleteJobs() {
    jobService.deleteAll(jobEntitiesFilter);
  }
}
