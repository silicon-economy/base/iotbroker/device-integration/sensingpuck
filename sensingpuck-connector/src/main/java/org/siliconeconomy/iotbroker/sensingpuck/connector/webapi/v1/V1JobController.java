/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.webapi.v1;

import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobNotFoundException;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Provides REST endpoints to add, query and update sensing puck jobs.
 * <p>
 * The implemented endpoints are limited to handling {@link ProtocolVersion#V1} jobs.
 *
 * @author D. Ronnenberg
 */
@RestController
public class V1JobController {
  /**
   * Used to retrieve/add/update jobs from the database.
   */
  private final JobService jobService;

  @Autowired
  public V1JobController(JobService jobService) {
    this.jobService = jobService;
  }

  /**
   * Returns a single {@link V1SensingPuckJobDto} for a given job ID.
   *
   * @param jobId The ID of the requested job.
   * @return The {@link V1SensingPuckJobDto} for the given ID.
   */
  @GetMapping("/jobs/{jobId}")
  @ResponseStatus(HttpStatus.OK)
  public V1SensingPuckJobDto getJob(@PathVariable("jobId") long jobId) {
    SensingPuckJobDtoBase job = jobService.findDtoByJobId(jobId);
    if (job instanceof V1SensingPuckJobDto) {
      return (V1SensingPuckJobDto) job;
    }

    throw new JobNotFoundException(jobId);
  }

  /**
   * Add a new job.
   *
   * @param newJob The job as {@link V1SensingPuckJobDto} to add.
   */
  @PostMapping("/jobs")
  @ResponseStatus(HttpStatus.CREATED)
  public void addJob(@RequestBody V1SensingPuckJobDto newJob) {
    jobService.add(newJob);
  }

  /**
   * Update a job in the database with a given job.
   *
   * @param job The updated job as {@link V1SensingPuckJobDto}.
   */
  @PutMapping("/jobs")
  @ResponseStatus(HttpStatus.CREATED)
  public void updateJob(@RequestBody V1SensingPuckJobDto job) {
    jobService.update(job);
  }
}
