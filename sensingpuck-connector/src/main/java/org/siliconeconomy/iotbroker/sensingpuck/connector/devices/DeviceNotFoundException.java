/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.connector.devices;

/**
 * Thrown to indicate that a device with a specific source and tenant could not be found.
 *
 * @author M. Grzenia
 */
public class DeviceNotFoundException
    extends RuntimeException {

  public DeviceNotFoundException(String source, String tenant) {
    super(String.format("Could not find device with source '%s' and tenant '%s'.",
                        source,
                        tenant));
  }
}
