/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.AbstractRequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.springframework.stereotype.Component;

/**
 * The {@link RequestResponseDispatcher} implementation for {@link ProtocolVersion#V3} requests and
 * responses.
 * <p>
 * This class merely delegates requests and responses to the right {@link ProtocolVersion#V3}
 * components.
 *
 * @author M. Grzenia
 */
@Component
public class V3RequestResponseDispatcher
    extends AbstractRequestResponseDispatcher {

  public V3RequestResponseDispatcher(V3RequestPreProcessor requestPreProcessor,
                                     V3RequestHandler requestHandler,
                                     V3ResponseProvider responseProvider,
                                     V3ResponsePostProcessor responsePostProcessor) {
    super(requestPreProcessor, requestHandler, responseProvider, responsePostProcessor);
  }
}
