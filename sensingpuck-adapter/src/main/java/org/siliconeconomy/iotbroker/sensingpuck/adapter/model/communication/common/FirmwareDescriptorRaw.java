/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents the raw firmware description that the adapter received from a sensing puck.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class FirmwareDescriptorRaw {

  /**
   * The version number of the firmware.
   */
  private VersionNumberRaw versionNumber;
  /**
   * The first 32 bit of the firmware's commit hash (as an unsigned 32-bit integer).
   */
  private long commitHash;
  /**
   * Indicates whether the local repository used to build the firmware was dirty.
   */
  private boolean dirtyFlag;
}
