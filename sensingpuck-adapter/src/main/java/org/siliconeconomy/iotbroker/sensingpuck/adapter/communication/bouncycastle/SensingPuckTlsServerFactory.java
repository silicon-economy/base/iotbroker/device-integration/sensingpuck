/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsPSKIdentityManager;
import org.bouncycastle.tls.TlsServer;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

/**
 * A factory for instances of {@link TlsServer} that are configured to be used for sensing puck
 * client devices.
 *
 * @author M. Grzenia
 */
@Component
public class SensingPuckTlsServerFactory
    implements TlsServerFactory {

  private final TlsPSKIdentityManager pskIdentityManager;

  /**
   * Creates a new instance.
   *
   * @param pskIdentityManager The {@link TlsPSKIdentityManager} that manages PSKs for sensing puck
   *                           client devices.
   */
  public SensingPuckTlsServerFactory(SensingPuckPskIdentityManager pskIdentityManager) {
    this.pskIdentityManager = requireNonNull(pskIdentityManager, "pskIdentityManager");
  }

  @Override
  public TlsServer create() {
    return new SensingPuckPskTlsServer(pskIdentityManager);
  }
}
