/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;

/**
 * Defines methods for handling requests received from sensing puck client devices.
 *
 * @author M. Grzenia
 */
public interface RequestHandler {

  /**
   * Called when a {@link SensingPuckRequestBase} from a sensing puck client device has been
   * received.
   *
   * @param request The received request.
   */
  void onRequest(SensingPuckRequestBase request);
}
