/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;

/**
 * Defines the base information that all messages (across all protocol versions) received from
 * sensing puck devices contain.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "protocolVersion",
    visible = true
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = V1SensingPuckRequest.class, name = "1"),
    @JsonSubTypes.Type(value = V2SensingPuckRequest.class, name = "2"),
    @JsonSubTypes.Type(value = V3SensingPuckRequest.class, name = "3")
})
// On deserialization, ignore unknown properties which would otherwise cause an exception.
@JsonIgnoreProperties(ignoreUnknown = true)
public class SensingPuckRequestBase
    extends SensingPuckMessageBase {

  public SensingPuckRequestBase(int protocolVersion, long deviceId) {
    super(protocolVersion, deviceId);
  }
}
