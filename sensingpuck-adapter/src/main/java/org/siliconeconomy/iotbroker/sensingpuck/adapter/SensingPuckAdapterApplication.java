/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.CouchDbProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * The starting point for an adapter instance.
 *
 * @author M. Grzenia
 */
@SpringBootApplication(
    scanBasePackages = {
        "org.siliconeconomy.iotbroker.sensingpuck.common",
        "org.siliconeconomy.iotbroker.sensingpuck.adapter"
    }
)
@EnableConfigurationProperties({
    AdapterProperties.class,
    CouchDbProperties.class,
    DeviceIntegrationProperties.class
})
public class SensingPuckAdapterApplication {

  public static void main(String[] args) {
    SpringApplication.run(SensingPuckAdapterApplication.class);
  }
}
