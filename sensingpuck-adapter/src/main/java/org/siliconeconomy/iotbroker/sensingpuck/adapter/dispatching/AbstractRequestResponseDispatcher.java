/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;

/**
 * An abstract implementation of {@link RequestResponseDispatcher} that merely delegates requests
 * and responses to the right components.
 *
 * @author M. Grzenia
 */
public abstract class AbstractRequestResponseDispatcher
    implements RequestResponseDispatcher {

  private final RequestPreProcessor requestPreProcessor;
  private final RequestHandler requestHandler;
  private final ResponseProvider responseProvider;
  private final ResponsePostProcessor responsePostProcessor;

  protected AbstractRequestResponseDispatcher(RequestPreProcessor requestPreProcessor,
                                              RequestHandler requestHandler,
                                              ResponseProvider responseProvider,
                                              ResponsePostProcessor responsePostProcessor) {
    this.requestPreProcessor = requestPreProcessor;
    this.requestHandler = requestHandler;
    this.responseProvider = responseProvider;
    this.responsePostProcessor = responsePostProcessor;
  }

  @Override
  public SensingPuckRequestBase tryParseAndValidateMessage(String message) {
    return requestPreProcessor.tryParseAndValidateMessage(message);
  }

  @Override
  public void onRequest(SensingPuckRequestBase request) {
    requestHandler.onRequest(request);
  }

  @Override
  public SensingPuckResponseBase getResponseFor(SensingPuckRequestBase request) {
    return responseProvider.getResponseFor(request);
  }

  @Override
  public String serializeResponse(SensingPuckResponseBase response) {
    return responseProvider.serializeResponse(response);
  }

  @Override
  public void postProcessResponse(SensingPuckResponseBase response) {
    responsePostProcessor.postProcessResponse(response);
  }
}
