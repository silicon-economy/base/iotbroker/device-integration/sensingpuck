/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.UnitOfMeasurement;

/**
 * The unit for the ambient illuminance measured by a sensing puck device.
 *
 * @author M. Grzenia
 */
public class AmbientLightUnit
    extends UnitOfMeasurement {

  private static final String NAME = "Lux";
  private static final String SYMBOL = "lx";

  public AmbientLightUnit() {
    super(NAME, SYMBOL);
  }
}
