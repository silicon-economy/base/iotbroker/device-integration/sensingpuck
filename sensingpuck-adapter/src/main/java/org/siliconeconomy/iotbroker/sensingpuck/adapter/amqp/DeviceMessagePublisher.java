/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.amqp.DeviceMessageExchangeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

import static java.util.Objects.requireNonNull;

/**
 * Handles publishing of (raw) client device messages to the IoT Broker's Device Message Exchange.
 * <p>
 * Instead of publishing {@link SensingPuckRequestBase} instances as messages, which are technically
 * the actual "raw" messages, this adapter publishes {@link SensingPuckMessageDtoBase} instances
 * (or more specifically their JSON representation) to the AMQP exchange. One of the reasons for
 * this is that {@link SensingPuckRequestBase} contains only relative timestamps, which are not
 * really helpful for someone interested in the raw messages. {@link SensingPuckMessageDtoBase} not
 * only simplifies the representation of some information in {@link SensingPuckRequestBase}, but
 * also uses absolute timestamps determined by the adapter when a message is received.
 *
 * @author M. Grzenia
 */
@Component
public class DeviceMessagePublisher {

  private static final Logger LOG = LoggerFactory.getLogger(DeviceMessagePublisher.class);
  /**
   * Used for mapping {@link SensingPuckRequestBase} instances to their JSON representation.
   */
  private final ObjectMapper objectMapper = new ObjectMapper();
  private final DeviceTypeProvider deviceTypeProvider;
  private final DeviceIntegrationProperties deviceIntegrationProperties;
  private final AmqpTemplate amqpTemplate;

  /**
   * Creates a new instance.
   *
   * @param deviceTypeProvider          Provides information about the adapter's {@link DeviceType}.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   * @param amqpTemplate                The AMQP template to use for sending messages.
   */
  public DeviceMessagePublisher(DeviceTypeProvider deviceTypeProvider,
                                DeviceIntegrationProperties deviceIntegrationProperties,
                                AmqpTemplate amqpTemplate) {
    this.deviceTypeProvider = requireNonNull(deviceTypeProvider, "deviceTypeProvider");
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");
    this.amqpTemplate = requireNonNull(amqpTemplate, "amqpTemplate");

    // Configure the object mapper to properly serialize instances of the Instant class.
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    objectMapper.registerModule(new JavaTimeModule());
  }

  /**
   * Publishes the given {@link SensingPuckMessageDtoBase} via AMQP to the Device Message Exchange.
   *
   * @param message The message to publish.
   */
  public void publishToDeviceMessageExchange(String deviceId, SensingPuckMessageDtoBase message) {
    try {
      amqpTemplate.send(DeviceMessageExchangeConfiguration.NAME,
                        DeviceMessageExchangeConfiguration.formatDeviceMessageRoutingKey(
                            deviceTypeProvider.getAdapterDeviceTypeIdentifier(),
                            deviceIntegrationProperties.getDeviceSource(),
                            deviceId
                        ),
                        toAmqpMessage(message));
    } catch (AmqpException e) {
      LOG.error("Error while publishing message via AMQP.", e);
    }
  }

  private Message toAmqpMessage(SensingPuckMessageDtoBase message) {
    try {
      return MessageBuilder
          .withBody(objectMapper.writeValueAsString(message).getBytes(StandardCharsets.UTF_8))
          .setContentEncoding(StandardCharsets.UTF_8.toString())
          .setContentType(MessageProperties.CONTENT_TYPE_JSON)
          .build();
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Error while serializing data to JSON.", e);
    }
  }
}
