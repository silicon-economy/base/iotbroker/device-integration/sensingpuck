/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

/**
 * Provides methods to convert {@link DeviceType} instances to various other types.
 *
 * @author M. Grzenia
 */
public class DeviceTypeConverter {

  /**
   * Converts the given {@link DeviceType} to an instance of {@link DeviceTypeCreateInputTO}.
   *
   * @param deviceType The {@link DeviceType} to convert.
   * @return The converted {@link DeviceTypeCreateInputTO}.
   */
  public DeviceTypeCreateInputTO toDeviceTypeCreateInputTO(DeviceType deviceType) {
    var deviceTypeCreateInput = new DeviceTypeCreateInputTO()
        .setSource(deviceType.getSource())
        .setIdentifier(deviceType.getIdentifier());
    deviceTypeCreateInput.setProvidedBy(deviceType.getProvidedBy());
    deviceTypeCreateInput.setDescription(deviceType.getDescription());
    deviceTypeCreateInput.setEnabled(deviceType.isEnabled());
    deviceTypeCreateInput.setAutoRegisterDeviceInstances(
        deviceType.isAutoRegisterDeviceInstances()
    );
    deviceTypeCreateInput.setAutoEnableDeviceInstances(deviceType.isAutoEnableDeviceInstances());

    return deviceTypeCreateInput;
  }

  /**
   * Converts the given {@link DeviceType} to an instance of {@link DeviceTypeUpdateInputTO}.
   *
   * @param deviceType The {@link DeviceType} to convert.
   * @return The converted {@link DeviceTypeUpdateInputTO}.
   */
  public DeviceTypeUpdateInputTO toDeviceTypeUpdateInput(DeviceType deviceType) {
    var deviceTypeUpdateInput = new DeviceTypeUpdateInputTO();
    deviceTypeUpdateInput.setProvidedBy(deviceType.getProvidedBy());
    deviceTypeUpdateInput.setDescription(deviceType.getDescription());
    deviceTypeUpdateInput.setEnabled(deviceType.isEnabled());
    deviceTypeUpdateInput.setAutoRegisterDeviceInstances(
        deviceType.isAutoRegisterDeviceInstances()
    );
    deviceTypeUpdateInput.setAutoEnableDeviceInstances(deviceType.isAutoEnableDeviceInstances());

    return deviceTypeUpdateInput;
  }
}
