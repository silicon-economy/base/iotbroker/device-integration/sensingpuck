/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.*;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.LatLong;
import org.siliconeconomy.iotbroker.model.sensordata.observation.MeasurementResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.SimpleResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.TruthResult;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.utils.SensingPuckMessageUtils;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.*;

import static java.util.Objects.requireNonNull;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata.SensorDataMessageConstants.OBSERVATION_PARAM_KEY_MOTION_STATE;

/**
 * Provides methods for mapping {@link SensingPuckMessageEntityBase} instances to corresponding
 * {@link SensorDataMessage} instances.
 * <p>
 * A sensing puck device combines the measurements of multiple sensors into a single protocol
 * version specific data structure. These measurements are mapped to {@link Observation} instances
 * and combined into separate {@link Datastream}s each containing the same type of measurement.
 *
 * @author M. Grzenia
 */
@Component
public class MessageEntityToSensorDataMapper {

  private final SensingPuckMessageUtils sensingPuckMessageUtils;
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param sensingPuckMessageUtils     Provides utility methods for working with
   *                                    {@link SensingPuckMessageEntityBase} instances.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public MessageEntityToSensorDataMapper(SensingPuckMessageUtils sensingPuckMessageUtils,
                                         DeviceIntegrationProperties deviceIntegrationProperties) {
    this.sensingPuckMessageUtils = requireNonNull(sensingPuckMessageUtils,
                                                  "sensingPuckMessageUtils");
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");
  }

  /**
   * Maps the given {@link SensingPuckMessageEntityBase} (considering its subtype) to a
   * {@link SensorDataMessage} instance.
   * <p>
   * The ID of the returned {@link SensorDataMessage} is set to a randomly generated {@link UUID}.
   *
   * @param messageEntity The {@link SensingPuckMessageEntityBase} to map.
   * @return The mapped {@link SensorDataMessage}.
   */
  public SensorDataMessage map(SensingPuckMessageEntityBase messageEntity) {
    if (messageEntity instanceof V1SensingPuckMessageEntity) {
      return map((V1SensingPuckMessageEntity) messageEntity);
    }
    if (messageEntity instanceof V2SensingPuckMessageEntity) {
      return map((V2SensingPuckMessageEntity) messageEntity);
    }
    if (messageEntity instanceof V3SensingPuckMessageEntity) {
      return map((V3SensingPuckMessageEntity) messageEntity);
    }

    throw new IllegalArgumentException("Unknown type: " + messageEntity.getClass());
  }

  private SensorDataMessage map(V1SensingPuckMessageEntity messageEntity) {
    return new SensorDataMessage(
        UUID.randomUUID().toString(),
        sensingPuckMessageUtils.extractMessageId(messageEntity.getId()),
        deviceIntegrationProperties.getDeviceSource(),
        String.valueOf(messageEntity.getDeviceId()),
        messageEntity.getComTimestamp(),
        null,
        mapV1SensingDataToDatastreams(messageEntity)
    );
  }

  private SensorDataMessage map(V2SensingPuckMessageEntity messageEntity) {
    return new SensorDataMessage(
        UUID.randomUUID().toString(),
        sensingPuckMessageUtils.extractMessageId(messageEntity.getId()),
        deviceIntegrationProperties.getDeviceSource(),
        String.valueOf(messageEntity.getDeviceId()),
        messageEntity.getComTimestamp(),
        null,
        mapV2SensingDataToDatastreams(messageEntity)
    );
  }

  private SensorDataMessage map(V3SensingPuckMessageEntity messageEntity) {
    return new SensorDataMessage(
        UUID.randomUUID().toString(),
        sensingPuckMessageUtils.extractMessageId(messageEntity.getId()),
        deviceIntegrationProperties.getDeviceSource(),
        String.valueOf(messageEntity.getDeviceId()),
        messageEntity.getComTimestamp(),
        null,
        mapV3SensingDataToDatastreams(messageEntity)
    );
  }

  private List<Datastream<? extends ObservationResult<?>>> mapV1SensingDataToDatastreams(
      V1SensingPuckMessageEntity messageEntity) {
    List<Observation<MeasurementResult>> humidityObservations = new ArrayList<>();
    List<Observation<MeasurementResult>> temperatureObservations = new ArrayList<>();

    for (V1SensingData data : messageEntity.getData()) {
      humidityObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getHumidity(),
                                            null,
                                            data.getMotionState()));
      temperatureObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getTemperature(),
                                            null,
                                            data.getMotionState()));
    }

    return Arrays.asList(createHumidityDatastream(humidityObservations),
                         createTemperatureDatastream(temperatureObservations));
  }

  private List<Datastream<? extends ObservationResult<?>>> mapV2SensingDataToDatastreams(
      V2SensingPuckMessageEntity messageEntity) {
    List<Observation<MeasurementResult>> humidityObservations = new ArrayList<>();
    List<Observation<MeasurementResult>> temperatureObservations = new ArrayList<>();
    List<Observation<MeasurementResult>> batteryVoltageObservations = new ArrayList<>();

    for (V2SensingData data : messageEntity.getData()) {
      humidityObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getHumidity(),
                                            null,
                                            data.getMotionState()));
      temperatureObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getTemperature(),
                                            null,
                                            data.getMotionState()));
      batteryVoltageObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getVoltage(),
                                            null,
                                            data.getMotionState()));
    }

    return Arrays.asList(createHumidityDatastream(humidityObservations),
                         createTemperatureDatastream(temperatureObservations),
                         createBatteryVoltageDatastream(batteryVoltageObservations));
  }

  private List<Datastream<? extends ObservationResult<?>>> mapV3SensingDataToDatastreams(
      V3SensingPuckMessageEntity messageEntity) {
    List<Observation<MeasurementResult>> humidityObservations = new ArrayList<>();
    List<Observation<MeasurementResult>> temperatureObservations = new ArrayList<>();
    List<Observation<MeasurementResult>> batteryVoltageObservations = new ArrayList<>();
    List<Observation<MeasurementResult>> ambientLightObservations = new ArrayList<>();
    List<Observation<TruthResult>> openedObservations = new ArrayList<>();
    List<Observation<SimpleResult>> locationObservations = new ArrayList<>();

    for (V3SensingData data : messageEntity.getData()) {
      Location<LatitudeLongitudeDetails> location = new Location<>(
          "Device location",
          "The device's location at the time of this observation.",
          LatitudeLongitudeDetails.class,
          // Prevent "precision conflicts" when converting float to double. See
          // the documentation of Double.valueOf(String).
          new LatitudeLongitudeDetails(
              new LatLong(Double.valueOf(String.valueOf(data.getGpsLat())),
                          Double.valueOf(String.valueOf(data.getGpsLong())))
          )
      );
      humidityObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getHumidity(),
                                            location,
                                            data.getMotionState()));
      temperatureObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getTemperature(),
                                            location,
                                            data.getMotionState()));
      batteryVoltageObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getVoltage(),
                                            location,
                                            data.getMotionState()));
      ambientLightObservations
          .add(createMeasurementObservation(data.getTimestamp(),
                                            data.getAmbientLight(),
                                            location,
                                            data.getMotionState()));
      openedObservations
          .add(createTruthObservation(data.getTimestamp(),
                                      data.isOpenedFlag(),
                                      location,
                                      data.getMotionState()));
      locationObservations
          .add(createLocationObservation(data.getTimestamp(),
                                         location,
                                         data.getMotionState()));
    }

    return Arrays.asList(createHumidityDatastream(humidityObservations),
                         createTemperatureDatastream(temperatureObservations),
                         createBatteryVoltageDatastream(batteryVoltageObservations),
                         createAmbientLightDatastream(ambientLightObservations),
                         createOpenedDatastream(openedObservations),
                         createLocationDatastream(locationObservations));
  }

  private Observation<MeasurementResult> createMeasurementObservation(
      Instant timestamp,
      float result,
      Location<LatitudeLongitudeDetails> location,
      MotionState motionState) {
    return new Observation<>(timestamp,
                             // Prevent "precision conflicts" when converting float to double. See
                             // the documentation of Double.valueOf(String).
                             new MeasurementResult(Double.valueOf(String.valueOf(result))),
                             location,
                             Map.of(OBSERVATION_PARAM_KEY_MOTION_STATE,
                                    motionState.getStringRepresentation())
    );
  }

  private Observation<TruthResult> createTruthObservation(
      Instant timestamp,
      boolean result,
      Location<LatitudeLongitudeDetails> location,
      MotionState motionState) {
    return new Observation<>(timestamp,
                             new TruthResult(result),
                             location,
                             Map.of(OBSERVATION_PARAM_KEY_MOTION_STATE,
                                    motionState.getStringRepresentation())
    );
  }

  private Observation<SimpleResult> createLocationObservation(
      Instant timestamp,
      Location<LatitudeLongitudeDetails> location,
      MotionState motionState) {
    return new Observation<>(timestamp,
                             new SimpleResult("The result is this observation's location."),
                             location,
                             Map.of(OBSERVATION_PARAM_KEY_MOTION_STATE,
                                    motionState.getStringRepresentation())
    );
  }

  private Datastream<MeasurementResult> createHumidityDatastream(
      List<Observation<MeasurementResult>> humidityObservations) {
    return new Datastream<>(new HumidityProperty(),
                            MeasurementResult.class,
                            new HumidityUnit(),
                            humidityObservations);
  }

  private Datastream<MeasurementResult> createTemperatureDatastream(
      List<Observation<MeasurementResult>> temperatureObservations) {
    return new Datastream<>(new TemperatureProperty(),
                            MeasurementResult.class,
                            new TemperatureUnit(),
                            temperatureObservations);
  }

  private Datastream<MeasurementResult> createBatteryVoltageDatastream(
      List<Observation<MeasurementResult>> batteryVoltageObservations) {
    return new Datastream<>(new BatteryVoltageProperty(),
                            MeasurementResult.class,
                            new BatteryVoltageUnit(),
                            batteryVoltageObservations);
  }

  private Datastream<MeasurementResult> createAmbientLightDatastream(
      List<Observation<MeasurementResult>> ambientLightObservations) {
    return new Datastream<>(new AmbientLightProperty(),
                            MeasurementResult.class,
                            new AmbientLightUnit(),
                            ambientLightObservations);
  }

  private Datastream<TruthResult> createOpenedDatastream(
      List<Observation<TruthResult>> openedObservations) {
    return new Datastream<>(new OpenedProperty(),
                            TruthResult.class,
                            new NoUnit(),
                            openedObservations);
  }

  private Datastream<SimpleResult> createLocationDatastream(
      List<Observation<SimpleResult>> locationObservations) {
    return new Datastream<>(new LocationProperty(),
                            SimpleResult.class,
                            new NoUnit(),
                            locationObservations);
  }
}
