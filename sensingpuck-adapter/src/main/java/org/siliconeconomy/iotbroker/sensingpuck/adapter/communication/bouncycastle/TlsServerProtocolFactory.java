/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsServerProtocol;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;

import static java.util.Objects.requireNonNull;

/**
 * A factory for instances of {@link TlsServerProtocol}.
 *
 * @author M. Grzenia
 */
@Component
public class TlsServerProtocolFactory {

  /**
   * Creates a {@link TlsServerProtocol} instance for the given {@link Socket}.
   *
   * @param clientSocket The {@link Socket} to create a {@link TlsServerProtocol} instance for.
   * @return A {@link TlsServerProtocol} instance.
   * @throws TlsServerProtocolCreationException If the creation of the {@link TlsServerProtocol}
   *                                            instance failed.
   */
  public TlsServerProtocol create(Socket clientSocket)
      throws TlsServerProtocolCreationException {
    requireNonNull(clientSocket, "clientSocket");

    try {
      return new TlsServerProtocol(clientSocket.getInputStream(), clientSocket.getOutputStream());
    } catch (IOException e) {
      throw new TlsServerProtocolCreationException(
          String.format("Client %s:%d: Creation of TlsServerProtocol instance failed.",
                        clientSocket.getInetAddress().getHostAddress(),
                        clientSocket.getPort()),
          e
      );
    }
  }
}
