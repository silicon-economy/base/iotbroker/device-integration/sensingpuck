/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;

/**
 * Exception indicating an unsupported request type.
 *
 * @author M. Grzenia
 */
public class UnsupportedRequestTypeException
    extends IllegalArgumentException {

  /**
   * Creates a new instance.
   *
   * @param request The request that caused the exception.
   */
  public UnsupportedRequestTypeException(SensingPuckRequestBase request) {
    super(String.format("Unsupported request type: %s", request.getClass()));
  }
}
