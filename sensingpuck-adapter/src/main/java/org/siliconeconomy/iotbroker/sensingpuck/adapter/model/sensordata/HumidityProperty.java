/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.ObservedProperty;

/**
 * The observed property describing the relative humidity measured by a sensing puck device.
 *
 * @author M. Grzenia
 */
public class HumidityProperty
    extends ObservedProperty {

  private static final String NAME = "The relative humidity.";
  private static final String DESCRIPTION = "";

  public HumidityProperty() {
    super(NAME, DESCRIPTION);
  }
}
