/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication;

import lombok.*;

/**
 * Defines the base information that all messages (across all protocol versions) received from and
 * sent to sensing puck devices contain.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class SensingPuckMessageBase {

  /**
   * The protocol version (as an unsigned 8-bit integer).
   */
  private int protocolVersion;
  /**
   * The ID of the sensing puck (as an unsigned 32-bit integer).
   */
  private long deviceId;
}
