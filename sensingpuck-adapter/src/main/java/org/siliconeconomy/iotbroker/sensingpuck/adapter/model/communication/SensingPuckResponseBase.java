/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Defines the base information that all messages (across all protocol versions) sent to sensing
 * puck devices contain.
 * <p>
 * Messages of this type are sent to sensing puck devices in response to a
 * {@link SensingPuckRequestBase}.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SensingPuckResponseBase
    extends SensingPuckMessageBase {

  public SensingPuckResponseBase(int protocolVersion, long deviceId) {
    super(protocolVersion, deviceId);
  }
}
