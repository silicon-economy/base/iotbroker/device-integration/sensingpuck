/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;

/**
 * A null-object for {@link DeviceType}s.
 *
 * @author M. Grzenia
 */
public class NullDeviceType
    extends DeviceType {

  public NullDeviceType() {
    super("", "", "", Set.of(), "", false, false, false);
  }
}
