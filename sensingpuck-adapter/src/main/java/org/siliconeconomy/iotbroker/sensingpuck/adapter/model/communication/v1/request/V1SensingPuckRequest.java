/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;

import java.util.List;

import static org.siliconeconomy.iotbroker.sensingpuck.adapter.util.Assertions.checkArgument;

/**
 * Represents a {@link ProtocolVersion#V1} request that the adapter received from a sensing puck
 * device.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class V1SensingPuckRequest
    extends SensingPuckRequestBase {

  /**
   * The ID of the job that the sensing puck last received (limited to 2^53 - 1).
   */
  private long jobId;
  /**
   * The description of the sensing puck adapter's firmware.
   */
  private FirmwareDescriptorRaw firmwareDescriptor;
  /**
   * The timestamp when the request was sent by the sensing puck.
   * The timestamp is the time since the sensing puck was started (in milliseconds;
   * limited to 2^53 - 1).
   */
  private long comTimestamp;
  /**
   * The identifier for the latest event that triggered a communication.
   */
  private CommCause lastComCause;
  /**
   * The actual data measured by the sensing puck.
   */
  private List<V1SensingDataRaw> data;

  public V1SensingPuckRequest(int protocolVersion,
                              long jobId,
                              long deviceId,
                              FirmwareDescriptorRaw firmwareDescriptor,
                              long comTimestamp,
                              CommCause lastComCause,
                              List<V1SensingDataRaw> data) {
    super(protocolVersion, deviceId);
    checkArgument(protocolVersion == ProtocolVersion.V1.getVersionNumber(),
                  String.format("protocolVersion is not %s", ProtocolVersion.V1.getVersionNumber()));
    this.jobId = jobId;
    this.firmwareDescriptor = firmwareDescriptor;
    this.comTimestamp = comTimestamp;
    this.lastComCause = lastComCause;
    this.data = data;
  }

  @Override
  public void setProtocolVersion(int protocolVersion) {
    checkArgument(protocolVersion == ProtocolVersion.V1.getVersionNumber(),
                  String.format("protocolVersion is not %s", ProtocolVersion.V1.getVersionNumber()));
    super.setProtocolVersion(protocolVersion);
  }
}
