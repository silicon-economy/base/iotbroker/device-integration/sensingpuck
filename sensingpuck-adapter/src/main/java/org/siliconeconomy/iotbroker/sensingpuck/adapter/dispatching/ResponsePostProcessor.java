/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;

/**
 * Defines methods for post-processing responses that have been sent to sensing puck client devices.
 *
 * @author M. Grzenia
 */
public interface ResponsePostProcessor {

  /**
   * Performs some post-processing operations for the given {@link SensingPuckResponseBase}.
   *
   * @param response The response to post-process.
   */
  void postProcessResponse(SensingPuckResponseBase response);
}
