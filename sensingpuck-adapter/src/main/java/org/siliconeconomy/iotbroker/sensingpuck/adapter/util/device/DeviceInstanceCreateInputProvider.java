/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request.FirmwareVersionFormatter;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

/**
 * A provider for {@link DeviceInstanceCreateInputTO} instances.
 *
 * @author M. Grzenia
 */
@Component
public class DeviceInstanceCreateInputProvider {

  /**
   * Used to format the firmware version number to a string.
   */
  private final FirmwareVersionFormatter firmwareVersionFormatter = new FirmwareVersionFormatter();
  private final DeviceTypeProvider deviceTypeProvider;
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param deviceTypeProvider          Provides information about the adapter's {@link DeviceType}.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public DeviceInstanceCreateInputProvider(
      DeviceTypeProvider deviceTypeProvider,
      DeviceIntegrationProperties deviceIntegrationProperties) {
    this.deviceTypeProvider = requireNonNull(deviceTypeProvider, "deviceTypeProvider");
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");
  }

  /**
   * Provides a {@link DeviceInstanceCreateInputTO} for the given {@code tenant} and
   * {@code registrationContext}.
   * <p>
   * The given {@code registrationContext} is expected to be an instance of
   * {@link SensingPuckRequestBase}. The returned {@link DeviceInstanceCreateInputTO} then
   * represents the sensing puck that sent the {@link SensingPuckRequestBase}.
   *
   * @param tenant              The tenant
   * @param registrationContext The registration context.
   * @return A {@link DeviceInstanceCreateInputTO} instance.
   */
  public DeviceInstanceCreateInputTO get(String tenant, Object registrationContext) {
    if (registrationContext instanceof V1SensingPuckRequest) {
      return map((V1SensingPuckRequest) registrationContext);
    }
    if (registrationContext instanceof V2SensingPuckRequest) {
      return map((V2SensingPuckRequest) registrationContext);
    }
    if (registrationContext instanceof V3SensingPuckRequest) {
      return map((V3SensingPuckRequest) registrationContext);
    }

    throw new IllegalArgumentException(
        String.format("Unhandled registration context type for tenant '%s': %s",
                      tenant,
                      registrationContext.getClass())
    );
  }

  private DeviceInstanceCreateInputTO map(V1SensingPuckRequest request) {
    DeviceInstanceCreateInputTO device
        = createDefaultSensingPuckDevice(String.valueOf(request.getDeviceId()));
    configureDeviceInstance(device,
                            deviceTypeProvider.determineDeviceTypeIdentifier(request),
                            request.getFirmwareDescriptor());

    return device;
  }

  private DeviceInstanceCreateInputTO map(V2SensingPuckRequest request) {
    DeviceInstanceCreateInputTO device
        = createDefaultSensingPuckDevice(String.valueOf(request.getDeviceId()));
    configureDeviceInstance(device,
                            deviceTypeProvider.determineDeviceTypeIdentifier(request),
                            request.getFirmwareDescriptor());

    return device;
  }

  private DeviceInstanceCreateInputTO map(V3SensingPuckRequest request) {
    DeviceInstanceCreateInputTO device
        = createDefaultSensingPuckDevice(String.valueOf(request.getDeviceId()));
    configureDeviceInstance(device,
                            deviceTypeProvider.determineDeviceTypeIdentifier(request),
                            request.getFirmwareDescriptor());

    return device;
  }

  private DeviceInstanceCreateInputTO createDefaultSensingPuckDevice(String tenant) {
    DeviceInstanceCreateInputTO device = new DeviceInstanceCreateInputTO()
        .setSource(deviceIntegrationProperties.getDeviceSource())
        .setTenant(tenant)
        .setDeviceTypeIdentifier("");
    device.setLastSeen(null);
    device.setEnabled(true);
    device.setDescription(deviceIntegrationProperties.getDeviceInstanceDescription());
    device.setHardwareRevision("");
    device.setFirmwareVersion("");
    return device;
  }

  private void configureDeviceInstance(DeviceInstanceCreateInputTO device,
                                       String deviceTypeIdentifier,
                                       FirmwareDescriptorRaw firmwareDescriptorRaw) {
    long firmwareMajorVersion = firmwareDescriptorRaw.getVersionNumber().getMajor();
    String firmwareVersion = firmwareVersionFormatter.format(
        firmwareMajorVersion,
        firmwareDescriptorRaw.getVersionNumber().getMinor(),
        firmwareDescriptorRaw.getVersionNumber().getPatch()
    );

    device.setDeviceTypeIdentifier(deviceTypeIdentifier);
    device.setFirmwareVersion(firmwareVersion);
  }
}
