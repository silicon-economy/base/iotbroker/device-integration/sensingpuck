/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;

/**
 * Represents the raw {@link ProtocolVersion#V2} measurement data that the adapter receives from a
 * sensing puck device.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class V2SensingDataRaw {

  /**
   * The temperature (in °C).
   */
  private float temperature;
  /**
   * The relative humidity (in %RH).
   */
  private float humidity;
  /**
   * The battery voltage (in V).
   */
  private float voltage;
  /**
   * The timestamp when the data was captured by the sensing puck.
   * The timestamp is the time since the sensing puck was started (in ms; limited to 2^53 - 1).
   */
  private long timestamp;
  /**
   * The sensing puck's motion state at the time of measurement.
   */
  private MotionState motionState;
}
