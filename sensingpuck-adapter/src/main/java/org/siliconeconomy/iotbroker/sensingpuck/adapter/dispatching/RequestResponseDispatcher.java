/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;

/**
 * Defines methods for end-to-end processing of {@link SensingPuckRequestBase} and
 * {@link SensingPuckResponseBase} instances.
 * <p>
 * A {@link RequestResponseDispatcher} manages the distribution of request and response messages
 * and related operations to components that know how to handle them. However, the actual
 * distribution may depend on the respective message contents (such as different protocol versions).
 *
 * @author M. Grzenia
 */
public interface RequestResponseDispatcher
    extends RequestPreProcessor, RequestHandler, ResponseProvider, ResponsePostProcessor {
}
