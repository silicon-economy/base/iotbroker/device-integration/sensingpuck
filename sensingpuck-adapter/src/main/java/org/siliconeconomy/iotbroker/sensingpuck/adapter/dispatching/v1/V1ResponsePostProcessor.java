/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v1;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.ResponsePostProcessor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.UnsupportedResponseTypeException;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * The {@link ResponsePostProcessor} implementation for {@link ProtocolVersion#V1} responses.
 *
 * @author M. Grzenia
 */
@Component
public class V1ResponsePostProcessor
    implements ResponsePostProcessor {

  /**
   * The service to use for accessing the database with the device configurations.
   */
  private final JobService jobService;

  public V1ResponsePostProcessor(JobService jobService) {
    this.jobService = jobService;
  }

  @Override
  public void postProcessResponse(SensingPuckResponseBase response) {
    if (!(response instanceof V1SensingPuckResponse)) {
      throw new UnsupportedResponseTypeException(response);
    }

    if (response instanceof V1DefaultSensingPuckResponse) {
      // Don't post-process the default response.
      return;
    }

    V1SensingPuckResponse v1Response = (V1SensingPuckResponse) response;
    SensingPuckJobEntityBase baseEntity = jobService.findEntityByJobId(v1Response.getJobId());
    if (!(baseEntity instanceof V1SensingPuckJobEntity)) {
      throw new IllegalArgumentException(String.format("Unexpected entity type: %s",
                                                       baseEntity.getClass()));
    }

    // We have just sent a response to a sensing puck device. Update the corresponding database
    // entry's sentTimestamp and reset the clearAlarm flag if we just ordered the sensing puck
    // device to clear its alarm.
    V1SensingPuckJobEntity v1Entity = (V1SensingPuckJobEntity) baseEntity;
    v1Entity.setSentTimestamp(Instant.now());
    if (v1Response.isClearAlarm()) {
      v1Entity.setClearAlarm(false);
    }

    jobService.update(v1Entity);
  }
}
