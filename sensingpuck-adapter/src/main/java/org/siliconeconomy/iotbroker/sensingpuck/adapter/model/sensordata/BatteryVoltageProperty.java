/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.ObservedProperty;

/**
 * The observed property describing the battery voltage measured by a sensing puck device.
 *
 * @author M. Grzenia
 */
public class BatteryVoltageProperty
    extends ObservedProperty {

  private static final String NAME = "The battery voltage.";
  private static final String DESCRIPTION = "The battery voltage measured in V.";

  public BatteryVoltageProperty() {
    super(NAME, DESCRIPTION);
  }
}
