/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.CipherSuite;
import org.bouncycastle.tls.PSKTlsServer;
import org.bouncycastle.tls.TlsPSKIdentityManager;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;

import java.security.SecureRandom;

import static java.util.Objects.requireNonNull;

/**
 * A {@link PSKTlsServer} that is configured to be used for sensing puck client devices.
 *
 * @author M. Grzenia
 */
public class SensingPuckPskTlsServer
    extends PSKTlsServer {

  /**
   * The cipher suites that are used with sensing puck client devices.
   */
  private static final int[] SENSINGPUCK_CIPHER_SUITES = new int[]{
      CipherSuite.TLS_PSK_WITH_AES_128_CCM_8
  };

  /**
   * Creates a new instance.
   *
   * @param pskIdentityManager The {@link TlsPSKIdentityManager} that manages PSKs for sensing puck
   *                           client devices.
   */
  public SensingPuckPskTlsServer(TlsPSKIdentityManager pskIdentityManager) {
    super(new BcTlsCrypto(new SecureRandom()),
          requireNonNull(pskIdentityManager, "pskIdentityManager"));
  }

  @Override
  protected int[] getSupportedCipherSuites() {
    return SENSINGPUCK_CIPHER_SUITES;
  }
}
