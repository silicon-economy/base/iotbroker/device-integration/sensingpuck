/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.response.JobEntityToSensingPuckResponseMapper;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

/**
 * An abstract implementation of {@link ResponseProvider} that implements basic behavior that is the
 * same across all protocol versions.
 * <p>
 * Where necessary, abstract (template) methods are provided to allow for protocol-specific
 * customization.
 *
 * @author M. Grzenia
 */
@Component
public abstract class AbstractResponseProvider
    implements ResponseProvider {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(AbstractResponseProvider.class);
  /**
   * The service used to retrieve {@link SensingPuckJobEntityBase} instances.
   */
  private final JobService jobService;
  /**
   * Maps {@link SensingPuckJobEntityBase} instances to {@link SensingPuckResponseBase} instances.
   */
  private final JobEntityToSensingPuckResponseMapper mapper
      = new JobEntityToSensingPuckResponseMapper();
  /**
   * Used to serialize instances of {@link SensingPuckResponseBase}.
   */
  private final ObjectMapper objectMapper = new ObjectMapper();

  protected AbstractResponseProvider(JobService jobService) {
    this.jobService = jobService;
  }

  /**
   * Returns an appropriate {@link SensingPuckResponseBase} for the given
   * {@link SensingPuckRequestBase}.
   * <p>
   * The following rules apply in the given order:
   * <ol>
   *     <li>If there are jobs in the database with the {@link JobState} set to
   *     {@link JobState#ACTIVE}, the most recent job (according to the created timestamp) is
   *     retrieved, mapped to a {@link SensingPuckResponseBase} and then returned.</li>
   *     <li>If there are jobs in the database with the {@link JobState} set to
   *     {@link JobState#FINISHED}, the most recent job (according to the created timestamp) is
   *     retrieved, mapped to a {@link SensingPuckResponseBase} and then returned.</li>
   *     <li>If no such jobs are found in the database, the job returned by
   *     {@link #createDefaultSensingPuckResponse()} is returned.</li>
   * </ol>
   *
   * @param request The request to get a response for.
   * @return The {@link SensingPuckResponseBase}.
   */
  @Override
  public SensingPuckResponseBase getResponseFor(SensingPuckRequestBase request) {
    if (!isSupportedRequestType(request)) {
      throw new UnsupportedRequestTypeException(request);
    }

    long deviceId = request.getDeviceId();

    LOG.debug("Device ID {}: Checking for most recent active job...", deviceId);
    Optional<SensingPuckJobEntityBase> config
        = jobService.findMostRecentActiveJob(String.valueOf(deviceId))
        .filter(this::isSupportedJobEntityType);
    if (config.isPresent()) {
      LOG.debug("Device ID {}: Returning most recent active job: {}",
                deviceId,
                config.get());
      return mapper.map(config.get());
    }

    LOG.debug("Device ID {}: Checking for most recent finished job...", deviceId);
    config = jobService.findMostRecentFinishedJob(String.valueOf(deviceId))
        .filter(this::isSupportedJobEntityType);
    if (config.isPresent()) {
      LOG.debug("Device ID {}: Returning most recent finished job: {}",
                deviceId,
                config.get()
      );
      return mapper.map(config.get());
    }

    SensingPuckResponseBase defaultResponse = createDefaultSensingPuckResponse();
    defaultResponse.setDeviceId(deviceId);

    LOG.debug("Device ID {}: Returning default job...", deviceId);
    return defaultResponse;
  }

  @Override
  public String serializeResponse(SensingPuckResponseBase response) {
    if (!isSupportedResponseType(response)) {
      throw new UnsupportedResponseTypeException(response);
    }

    try {
      return objectMapper.writeValueAsString(response);
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Could not serialize response.", e);
    }
  }

  /**
   * Returns the type of {@link SensingPuckRequestBase} that this {@link ResponseProvider} supports.
   *
   * @return A class extending {@link SensingPuckRequestBase}.
   */
  protected abstract Class<? extends SensingPuckRequestBase> supportedRequestType();

  /**
   * Returns the types of {@link SensingPuckJobEntityBase} that this {@link ResponseProvider}
   * supports.
   *
   * @return A set of classes extending {@link SensingPuckJobEntityBase}.
   */
  protected abstract Set<Class<? extends SensingPuckJobEntityBase>> supportedJobEntityTypes();

  /**
   * Returns the types of {@link SensingPuckResponseBase} that this {@link ResponseProvider}
   * supports.
   *
   * @return A set of classes extending {@link SensingPuckResponseBase}.
   */
  protected abstract Set<Class<? extends SensingPuckResponseBase>> supportedResponseTypes();

  /**
   * Creates the default {@link SensingPuckResponseBase}.
   *
   * @return The default {@link SensingPuckResponseBase}.
   */
  protected abstract SensingPuckResponseBase createDefaultSensingPuckResponse();

  private boolean isSupportedRequestType(SensingPuckRequestBase request) {
    return supportedRequestType().isAssignableFrom(request.getClass());
  }

  private boolean isSupportedJobEntityType(SensingPuckJobEntityBase jobEntity) {
    return supportedJobEntityTypes().stream()
        .anyMatch(jobEntityClass -> jobEntityClass.isAssignableFrom(jobEntity.getClass()));
  }

  private boolean isSupportedResponseType(SensingPuckResponseBase response) {
    return supportedResponseTypes().stream()
        .anyMatch(responseClass -> responseClass.isAssignableFrom(response.getClass()));
  }
}
