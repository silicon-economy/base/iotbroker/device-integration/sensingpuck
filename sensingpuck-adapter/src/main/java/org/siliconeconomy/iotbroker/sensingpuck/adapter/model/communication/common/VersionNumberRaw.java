/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents the raw version number that the adapter received from a sensing puck.
 *
 * @author M. Grzenia
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class VersionNumberRaw {

  /**
   * The major version number (as an unsigned 32-bit integer).
   */
  private long major;
  /**
   * The minor version number (as an unsigned 32-bit integer).
   */
  private long minor;
  /**
   * The patch version number (as an unsigned 32-bit integer).
   */
  private long patch;
  /**
   * The number of commits since the last version tag (as an unsigned 32-bit integer).
   */
  private long commitCounter;
  /**
   * Indicates whether the last version tag was a release version tag.
   */
  private boolean releaseFlag;
}
