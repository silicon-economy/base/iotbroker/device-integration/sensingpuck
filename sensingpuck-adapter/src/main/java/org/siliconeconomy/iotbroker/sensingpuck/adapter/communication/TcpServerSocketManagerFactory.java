/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;

import static java.util.Objects.requireNonNull;

/**
 * A factory for instances of {@link TcpServerSocketManager}.
 * <p>
 * This factory basically allows {@link TcpServerSocketManager} to participate in dependency
 * injection while avoiding a circular dependency involving {@link DeviceAdapter}.
 *
 * @author M. Grzenia
 */
@Component
public class TcpServerSocketManagerFactory {

  private final ClientConnectionHandlerFactory clientConnectionHandlerFactory;
  private final ExecutorService communicationExecutorService;
  private final AdapterProperties adapterProperties;

  /**
   * Creates a new instance.
   *
   * @param clientConnectionHandlerFactory A factory for instances of
   *                                       {@link ClientConnectionHandler}.
   * @param communicationExecutorService   The {@link ExecutorService} to use for handling
   *                                       connections to and processing messages from clients.
   * @param adapterProperties              The adapter's configuration properties.
   */
  public TcpServerSocketManagerFactory(
      ClientConnectionHandlerFactory clientConnectionHandlerFactory,
      ExecutorService communicationExecutorService,
      AdapterProperties adapterProperties) {
    this.clientConnectionHandlerFactory = requireNonNull(clientConnectionHandlerFactory,
                                                         "clientConnectionHandlerFactory");
    this.communicationExecutorService = requireNonNull(communicationExecutorService,
                                                       "communicationExecutorService");
    this.adapterProperties = requireNonNull(adapterProperties, "adapterProperties");
  }

  /**
   * Creates a new instance of {@link TcpServerSocketManager}.
   *
   * @param deviceAdapter The {@link DeviceAdapter} for devices for which the
   *                      {@link TcpServerSocketManager} to create should manage TCP connections.
   * @return A {@link TcpServerSocketManager} instance.
   */
  public TcpServerSocketManager create(DeviceAdapter deviceAdapter) {
    return new TcpServerSocketManager(adapterProperties.getServerSocketPort(),
                                      communicationExecutorService,
                                      clientConnectionHandlerFactory,
                                      deviceAdapter
    );
  }
}
