/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.ResponsePostProcessor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.UnsupportedResponseTypeException;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * The {@link ResponsePostProcessor} implementation for {@link ProtocolVersion#V3} responses.
 *
 * @author M. Grzenia
 */
@Component
public class V3ResponsePostProcessor
    implements ResponsePostProcessor {

  /**
   * The service to use for accessing the database with the device configurations.
   */
  private final JobService jobService;

  public V3ResponsePostProcessor(JobService jobService) {
    this.jobService = jobService;
  }

  @Override
  public void postProcessResponse(SensingPuckResponseBase response) {
    if (!(response instanceof V3SensingPuckResponse)) {
      throw new UnsupportedResponseTypeException(response);
    }

    if (response instanceof V3DefaultSensingPuckResponse) {
      // Don't post-process the default response.
      return;
    }

    V3SensingPuckResponse v3Response = (V3SensingPuckResponse) response;
    SensingPuckJobEntityBase baseEntity = jobService.findEntityByJobId(v3Response.getJobId());
    if (!(baseEntity instanceof V3SensingPuckJobEntity)) {
      throw new IllegalArgumentException(String.format("Unexpected entity type: %s",
                                                       baseEntity.getClass()));
    }

    // We have just sent a response to a sensing puck device. Update the corresponding database
    // entry's sentTimestamp and reset the clearAlarm flag if we just ordered the sensing puck
    // device to clear its alarm.
    V3SensingPuckJobEntity v3Entity = (V3SensingPuckJobEntity) baseEntity;
    v3Entity.setSentTimestamp(Instant.now());
    if (v3Response.isClearAlarm()) {
      v3Entity.setClearAlarm(false);
    }

    jobService.update(v3Entity);
  }
}
