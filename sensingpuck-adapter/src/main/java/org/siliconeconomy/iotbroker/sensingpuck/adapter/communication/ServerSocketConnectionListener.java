/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

/**
 * A {@link Runnable} that, when executed, starts listening for client connections on a
 * {@link ServerSocket}.
 * <p>
 * It listens for connections to be made on a {@link ServerSocket} and passes accepted connections
 * (i.e. the corresponding client {@link Socket}) to a {@link Consumer}.
 * <p>
 * The {@link ServerSocket} <em>continuously</em> listens for a connection to be made to it until
 * the result of a {@link BooleanSupplier} is {@code true}.
 *
 * @author M. Grzenia
 */
public class ServerSocketConnectionListener
    implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(ServerSocketConnectionListener.class);
  private final ServerSocket serverSocket;
  private final Consumer<Socket> clientSocketHandler;
  private final BooleanSupplier stopCondition;

  /**
   * Creates a new instance.
   *
   * @param serverSocket        The {@link ServerSocket} on which to listen for connections.
   * @param clientSocketHandler Handles the {@link Socket} of a client that connected to the given
   *                            {@link ServerSocket}.
   * @param stopCondition       A {@link BooleanSupplier} representing the condition when to stop
   *                            listening for connections (indicated by a {@code true} result).
   */
  public ServerSocketConnectionListener(ServerSocket serverSocket,
                                        Consumer<Socket> clientSocketHandler,
                                        BooleanSupplier stopCondition) {
    this.serverSocket = requireNonNull(serverSocket, "serverSocket");
    this.clientSocketHandler = requireNonNull(clientSocketHandler, "clientSocketHandler");
    this.stopCondition = requireNonNull(stopCondition, "stopCondition");
  }

  @Override
  public void run() {
    listenForClientConnections(serverSocket, clientSocketHandler, stopCondition);
  }

  private void listenForClientConnections(ServerSocket serverSocket,
                                          Consumer<Socket> clientSocketHandler,
                                          BooleanSupplier stopCondition) {
    LOG.info("Listening for client connections on port {}...", serverSocket.getLocalPort());
    try {
      while (!stopCondition.getAsBoolean()) {
        Socket clientSocket = serverSocket.accept();
        clientSocketHandler.accept(clientSocket);
      }
    } catch (IOException e) {
      LOG.error("An error occurred while waiting for a client connection.", e);
    }
    LOG.info("Stopped to listen for client connections on port {}.", serverSocket.getLocalPort());
  }
}
