/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Provides information about the adapter's {@link DeviceType} (i.e. the {@link DeviceType} that the
 * adapter provides an IoT Broker integration for).
 *
 * @author M. Grzenia
 */
@Component
public class DeviceTypeProvider {

  /**
   * The version number to use for the adapter's {@link DeviceType}.
   */
  private static final int ADAPTER_DEVICE_TYPE_IDENTIFIER_VERSION = 1;
  /**
   * The format string to use for the adapter's {@link DeviceType}.
   */
  private final String adapterDeviceTypeIdentifierFormat;
  private final String adapterDeviceTypeIdentifier;
  private final DeviceIntegrationProperties deviceIntegrationProperties;
  private final AdapterIdentifierProvider adapterIdentifierProvider;

  /**
   * Creates a new instance.
   *
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   * @param adapterIdentifierProvider   Provides the adapter identifier.
   */
  public DeviceTypeProvider(DeviceIntegrationProperties deviceIntegrationProperties,
                            AdapterIdentifierProvider adapterIdentifierProvider) {
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");
    this.adapterIdentifierProvider = requireNonNull(adapterIdentifierProvider,
                                                    "adapterIdentifierProvider");
    this.adapterDeviceTypeIdentifierFormat = deviceIntegrationProperties.getDeviceSource() + "-v%s";
    this.adapterDeviceTypeIdentifier
        = getFormattedDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE_IDENTIFIER_VERSION);
  }

  /**
   * Returns the identifier string of the adapter's {@link DeviceType}.
   *
   * @return A {@link DeviceType} identifier.
   */
  public String getAdapterDeviceTypeIdentifier() {
    return adapterDeviceTypeIdentifier;
  }

  /**
   * Creates a (default) {@link DeviceType} instance representing the adapter's device type.
   * <p>
   * This method should only be used in the context of {@link DeviceType} registration.
   *
   * @return A {@link DeviceType}.
   */
  public DeviceType createDefaultAdapterDeviceType() {
    return new DeviceType("",
                          deviceIntegrationProperties.getDeviceSource(),
                          adapterDeviceTypeIdentifier,
                          Set.of(adapterIdentifierProvider.getAdapterIdentifier()),
                          deviceIntegrationProperties.getDeviceTypeDescription(),
                          true,
                          true,
                          true);
  }

  /**
   * Determines the identifier of the {@link DeviceType} that the given
   * {@link SensingPuckRequestBase} corresponds to.
   *
   * @param request The {@link SensingPuckRequestBase} to get a {@link DeviceType} identifier for.
   * @return A {@link DeviceType} identifier.
   */
  public String determineDeviceTypeIdentifier(SensingPuckRequestBase request) {
    if (request instanceof V1SensingPuckRequest
        || request instanceof V2SensingPuckRequest
        || request instanceof V3SensingPuckRequest) {
      // Consider sensing pucks that use protocol versions 1, 2 and 3 as devices (i.e. the device
      // type) supported by the adapter.
      return adapterDeviceTypeIdentifier;
    }

    // For now, encode the protocol version in the device type identifier for sensing pucks that
    // are not supported by the adapter. In the future, the device type identifier could also be
    // derived from other information. The only important thing is that in case of unsupported
    // devices the device type identifier differs from the device type supported by the adapter.
    return getFormattedDeviceTypeIdentifier(request.getProtocolVersion());
  }

  private String getFormattedDeviceTypeIdentifier(int identifierVersion) {
    return String.format(adapterDeviceTypeIdentifierFormat, identifierVersion);
  }
}
