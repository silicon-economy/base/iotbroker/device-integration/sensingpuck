/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.bouncycastle.tls.TlsServerProtocol;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.SensingPuckAdapter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle.TlsServerProtocolProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestResponseDispatcher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.net.Socket;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.Objects.requireNonNull;

/**
 * A factory for instances of {@link ClientConnectionHandler}.
 * <p>
 * This factory basically allows {@link ClientConnectionHandler} to participate in dependency
 * injection while avoiding a circular dependency involving {@link SensingPuckAdapter}.
 *
 * @author M. Grzenia
 */
@Component
public class ClientConnectionHandlerFactory {

  private final RequestResponseDispatcher requestResponseDispatcher;
  private final TlsServerProtocolProvider tlsServerProtocolProvider;
  private final ScheduledExecutorService communicationExecutorService;
  private final AdapterProperties adapterProperties;

  /**
   * Creates a new instance.
   *
   * @param tlsServerProtocolProvider    A provider for instances of {@link TlsServerProtocol}.
   * @param requestResponseDispatcher    The {@link RequestResponseDispatcher} responsible for
   *                                     processing requests and responses.
   * @param communicationExecutorService The {@link ScheduledExecutorService} to use for processing
   *                                     messages from clients.
   * @param adapterProperties            The adapter's configuration properties.
   */
  public ClientConnectionHandlerFactory(
      TlsServerProtocolProvider tlsServerProtocolProvider,
      @Qualifier("compositeRequestResponseDispatcher") RequestResponseDispatcher requestResponseDispatcher,
      ScheduledExecutorService communicationExecutorService,
      AdapterProperties adapterProperties) {
    this.tlsServerProtocolProvider = requireNonNull(tlsServerProtocolProvider,
                                                    "tlsServerProtocolProvider");
    this.requestResponseDispatcher = requireNonNull(requestResponseDispatcher,
                                                    "requestResponseDispatcher");
    this.communicationExecutorService = requireNonNull(communicationExecutorService,
                                                       "communicationExecutorService");
    this.adapterProperties = requireNonNull(adapterProperties, "adapterProperties");
  }

  /**
   * Creates a new instance of {@link ClientConnectionHandler}.
   *
   * @param clientSocket  The socket used to read data sent by a sensing puck client device.
   * @param deviceAdapter The adapter that provides an integration for sensing puck client devices.
   * @return A {@link ClientConnectionHandler} instance.
   */
  public ClientConnectionHandler create(Socket clientSocket, DeviceAdapter deviceAdapter) {
    return new ClientConnectionHandler(clientSocket,
                                       tlsServerProtocolProvider,
                                       requestResponseDispatcher,
                                       deviceAdapter,
                                       communicationExecutorService,
                                       adapterProperties);
  }
}
