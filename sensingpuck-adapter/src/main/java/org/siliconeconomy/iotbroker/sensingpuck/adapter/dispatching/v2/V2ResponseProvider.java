/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v2;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.AbstractResponseProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.ResponseProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * The {@link ResponseProvider} implementation for {@link ProtocolVersion#V2} responses.
 *
 * @author M. Grzenia
 */
@Component
public class V2ResponseProvider
    extends AbstractResponseProvider {

  public V2ResponseProvider(JobService jobService) {
    super(jobService);
  }

  @Override
  protected Class<? extends SensingPuckRequestBase> supportedRequestType() {
    return V2SensingPuckRequest.class;
  }

  @Override
  protected Set<Class<? extends SensingPuckJobEntityBase>> supportedJobEntityTypes() {
    return Set.of(V1SensingPuckJobEntity.class, V2SensingPuckJobEntity.class);
  }

  @Override
  protected Set<Class<? extends SensingPuckResponseBase>> supportedResponseTypes() {
    return Set.of(V1SensingPuckResponse.class, V2SensingPuckResponse.class);
  }

  @Override
  protected SensingPuckResponseBase createDefaultSensingPuckResponse() {
    return new V2DefaultSensingPuckResponse(0);
  }
}
