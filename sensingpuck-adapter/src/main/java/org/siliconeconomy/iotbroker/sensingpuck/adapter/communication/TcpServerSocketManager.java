/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;

import static java.util.Objects.requireNonNull;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.util.Assertions.checkArgument;

/**
 * Configures and manages the {@link ServerSocket} though which clients can connect to and
 * communicate with the adapter.
 *
 * @author M. Grzenia
 */
public class TcpServerSocketManager {

  private static final Logger LOG = LoggerFactory.getLogger(TcpServerSocketManager.class);
  private final int port;
  private final ExecutorService communicationExecutorService;
  private final ClientConnectionHandlerFactory clientConnectionHandlerFactory;
  private final DeviceAdapter deviceAdapter;
  /**
   * The {@link ServerSocket} that this {@link TcpServerSocketManager} opens and on which it
   * continuously listens for client connections.
   */
  private ServerSocket serverSocket;
  /**
   * Indicates whether this component is initialized.
   */
  private boolean initialized;

  /**
   * Creates a new instance.
   *
   * @param port                           The port to bind the {@link ServerSocket} to and on which
   *                                       to listen for client connections. Has to be >= 0.
   * @param communicationExecutorService   The {@link ExecutorService} to use for handling
   *                                       connections to and processing messages from clients.
   * @param clientConnectionHandlerFactory A factory for instances of
   *                                       {@link ClientConnectionHandler}.
   * @param deviceAdapter                  The {@link DeviceAdapter} for devices for which this
   *                                       {@link TcpServerSocketManager} manages the TCP
   *                                       connections.
   */
  public TcpServerSocketManager(int port,
                                ExecutorService communicationExecutorService,
                                ClientConnectionHandlerFactory clientConnectionHandlerFactory,
                                DeviceAdapter deviceAdapter) {
    checkArgument(port >= 0, String.format("Port has to be >= 0 but is %s.", port));
    this.port = port;
    this.communicationExecutorService = requireNonNull(communicationExecutorService,
                                                       "communicationExecutorService");
    this.clientConnectionHandlerFactory = requireNonNull(clientConnectionHandlerFactory,
                                                         "clientConnectionHandlerFactory");
    this.deviceAdapter = requireNonNull(deviceAdapter, "deviceAdapter");
  }

  /**
   * Initializes the {@link TcpServerSocketManager}.
   * <p>
   * Opens a {@link ServerSocket} and continuously listens for client connections. Accepted
   * connections are handed over to a {@link ClientConnectionHandler} instance per connection.
   */
  public void initialize() {
    if (isInitialized()) {
      return;
    }

    try {
      serverSocket = new ServerSocket(port);
    } catch (IOException e) {
      LOG.error("Failed to open server socket.", e);
      return;
    }

    LOG.info("Starting to listen for client connections...");
    communicationExecutorService.submit(
        new ServerSocketConnectionListener(
            serverSocket,
            clientSocket -> communicationExecutorService.submit(
                clientConnectionHandlerFactory.create(clientSocket, deviceAdapter)
            ),
            serverSocket::isClosed
        )
    );

    initialized = true;
  }

  /**
   * Terminates the {@link TcpServerSocketManager}.
   * <p>
   * Stops to listen for client connections and closes the {@link ServerSocket}.
   */
  public void terminate() {
    if (!isInitialized()) {
      return;
    }

    LOG.info("Stopping to listen for client connections...");
    try {
      serverSocket.close();
    } catch (IOException e) {
      LOG.warn("An error occurred while trying to close the server socket.", e);
    }
    serverSocket = null;

    initialized = false;
  }

  /**
   * Indicates whether this component is initialized.
   *
   * @return {@code true}, if this component is initialized, otherwise {@code false}.
   */
  public boolean isInitialized() {
    return initialized;
  }

  /**
   * Returns the {@link ServerSocket} the {@link TcpServerSocketManager} manages.
   *
   * @return A {@link ServerSocket}.
   */
  protected ServerSocket getServerSocket() {
    return serverSocket;
  }
}
