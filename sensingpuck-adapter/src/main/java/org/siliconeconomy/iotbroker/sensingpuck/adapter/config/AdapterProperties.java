/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import static java.util.Objects.requireNonNull;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.util.Assertions.checkArgument;

/**
 * Defines adapter-relevant (configuration) properties (that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "adapter")
@ConstructorBinding
@Getter
public class AdapterProperties {

  private final int connectionThreadPoolSize;
  private final String deviceRegistryServiceApiUrl;
  private final int serverSocketPort;
  private final String serverTlsPsk;
  private final String serverTlsPskIdentity;
  private final String serverTlsPskIdentityHint;
  private final int clientConnectionTimeout;

  /**
   * Creates a new instance.
   *
   * @param connectionThreadPoolSize    The size of the thread pool used for handling connections to
   *                                    and processing messages from client devices.
   * @param deviceRegistryServiceApiUrl The URL to the Device Registry Service's web API.
   * @param serverSocketPort            The port on which the adapter listens for client
   *                                    connections.
   * @param serverTlsPsk                The PSK to use for communication with client devices.
   * @param serverTlsPskIdentity        The PSK identity expected to be used by client devices.
   * @param serverTlsPskIdentityHint    The PSK identity hint to provide to clients (to help them in
   *                                    selecting which identity to use).
   * @param clientConnectionTimeout     The time (in seconds) until a connection to a client is
   *                                    considered timed out and ultimately closed (i.e. the time
   *                                    to wait before the adapter closes the connection to a client
   *                                    after a response has been sent to it.).
   */
  public AdapterProperties(int connectionThreadPoolSize,
                           String deviceRegistryServiceApiUrl,
                           int serverSocketPort,
                           String serverTlsPsk,
                           String serverTlsPskIdentity,
                           String serverTlsPskIdentityHint,
                           int clientConnectionTimeout) {
    this.connectionThreadPoolSize = connectionThreadPoolSize;
    this.deviceRegistryServiceApiUrl = requireNonNull(deviceRegistryServiceApiUrl,
                                                      "deviceRegistryServiceApiUrl");
    this.serverSocketPort = serverSocketPort;
    this.serverTlsPsk = requireNonNull(serverTlsPsk, "serverTlsPsk");
    this.serverTlsPskIdentity = requireNonNull(serverTlsPskIdentity, "serverTlsPskIdentity");
    this.serverTlsPskIdentityHint = requireNonNull(serverTlsPskIdentityHint,
                                                   "serverTlsPskIdentityHint");
    checkArgument(clientConnectionTimeout >= 0, "clientConnectionTimeout has to be >= 0");
    this.clientConnectionTimeout = clientConnectionTimeout;
  }
}
