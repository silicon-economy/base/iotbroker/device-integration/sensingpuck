/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

/**
 * Provides the adapter identifier (used, among other things, in the context of {@link DeviceType}
 * registration).
 *
 * @author M. Grzenia
 */
@Component
public class AdapterIdentifierProvider {

  /**
   * The version number to use for the adapter identifier.
   * <p>
   * The version number should be tied to the major version of the adapter.
   */
  private static final int ADAPTER_IDENTIFIER_VERSION = 2;
  private final String adapterIdentifier;

  /**
   * Creates a new instance.
   *
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public AdapterIdentifierProvider(DeviceIntegrationProperties deviceIntegrationProperties) {
    requireNonNull(deviceIntegrationProperties, "deviceIntegrationProperties");
    this.adapterIdentifier = String.format(
        "%s-adapter-v%s",
        deviceIntegrationProperties.getDeviceSource(),
        ADAPTER_IDENTIFIER_VERSION
    );
  }

  /**
   * Returns the adapter's identifier string.
   *
   * @return The adapter identifier.
   */
  public String getAdapterIdentifier() {
    return adapterIdentifier;
  }
}
