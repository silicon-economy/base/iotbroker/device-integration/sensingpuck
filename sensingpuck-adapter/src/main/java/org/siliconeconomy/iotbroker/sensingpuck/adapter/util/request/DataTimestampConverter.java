/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request;

import lombok.NonNull;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;

import java.time.Instant;

/**
 * Provides a method to convert the timestamp of a data entry in a {@link SensingPuckRequestBase}
 * to an instance of {@link Instant} representing the time at which the data was captured.
 *
 * @author M. Grzenia
 */
public class DataTimestampConverter {

  /**
   * Converts the given {@code dataTimestamp} to an instance of {@link Instant} by calculating the
   * difference between {@code comTimestamp} and {@code dataTimestamp} and subtracting the result
   * from {@code referenceTime}.
   *
   * @param dataTimestamp The timestamp of a data entry (in milliseconds).
   * @param comTimestamp  The timestamp of the communication attempt the data entry belongs to
   *                      (in milliseconds).
   * @param referenceTime The time to use as a reference (i.e. the time at which the
   *                      {@link SensingPuckRequestBase} was received).
   * @return The time at which the data was captured.
   */
  public Instant convert(long dataTimestamp, long comTimestamp, @NonNull Instant referenceTime) {
    return referenceTime.minusMillis(comTimestamp - dataTimestamp);
  }
}
