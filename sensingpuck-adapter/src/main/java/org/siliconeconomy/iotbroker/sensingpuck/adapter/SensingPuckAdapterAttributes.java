/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.experimental.adapter.AdapterAttributes;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

@Component
public class SensingPuckAdapterAttributes
    implements AdapterAttributes {
  private final DeviceTypeProvider deviceTypeProvider;
  private final AdapterIdentifierProvider adapterIdentifierProvider;

  public SensingPuckAdapterAttributes(DeviceTypeProvider deviceTypeProvider,
                                      AdapterIdentifierProvider adapterIdentifierProvider) {
    this.deviceTypeProvider = requireNonNull(deviceTypeProvider, "deviceTypeProvider");
    this.adapterIdentifierProvider = requireNonNull(adapterIdentifierProvider, 
                                                    "adapterIdentifierProvider");
  }

  @Override
  public @NonNull String getAdapterIdentifier() {
    return adapterIdentifierProvider.getAdapterIdentifier();
  }

  @Override
  public @NonNull String getAdapterDeviceSource() {
    return deviceTypeProvider.createDefaultAdapterDeviceType().getSource();
  }

  @Override
  public @NonNull String getAdapterDeviceTypeIdentifier() {
    return deviceTypeProvider.getAdapterDeviceTypeIdentifier();
  }
}
