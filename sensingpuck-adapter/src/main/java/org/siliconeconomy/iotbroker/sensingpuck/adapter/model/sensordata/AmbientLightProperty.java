/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.ObservedProperty;

/**
 * The observed property describing the ambient illuminance measured by a sensing puck device.
 *
 * @author M. Grzenia
 */
public class AmbientLightProperty
    extends ObservedProperty {

  private static final String NAME = "The ambient illuminance.";
  private static final String DESCRIPTION = "The ambient illuminance measured in lx.";

  public AmbientLightProperty() {
    super(NAME, DESCRIPTION);
  }
}
