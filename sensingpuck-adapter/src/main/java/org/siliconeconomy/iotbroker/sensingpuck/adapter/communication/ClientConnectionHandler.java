/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.bouncycastle.tls.TlsServerProtocol;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle.TlsServerProtocolProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * Handles the connection to a single sensing puck client device.
 * <p>
 * The client device is expected to send a message in JSON format. When a message is received, it is
 * processed utilizing a {@link RequestResponseDispatcher}. Processing steps include, for example:
 * <ul>
 *   <li>Mapping the received message to an instance of {@link SensingPuckRequestBase}</li>
 *   <li>Processing the mapped request.</li>
 *   <li>Retrieving an appropriate response for the received request and sending it to the client
 *   device.</li>
 * </ul>
 * After a message has been received and processed, the connection to the client device is closed.
 *
 * @author M. Grzenia
 */
public class ClientConnectionHandler
    implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(ClientConnectionHandler.class);
  private final Socket clientSocket;
  private final TlsServerProtocolProvider tlsServerProtocolProvider;
  private final RequestResponseDispatcher requestResponseDispatcher;
  private final DeviceAdapter deviceAdapter;
  private final ScheduledExecutorService communicationExecutorService;
  private final AdapterProperties adapterProperties;

  /**
   * Creates a new instance.
   *
   * @param clientSocket                 The socket used to read data sent by a sensing puck client
   *                                     device.
   * @param tlsServerProtocolProvider    A provider for instances of {@link TlsServerProtocol}.
   * @param requestResponseDispatcher    The {@link RequestResponseDispatcher} responsible for
   *                                     processing requests and responses.
   * @param deviceAdapter                The adapter that provides an integration for sensing puck
   *                                     client devices.
   * @param communicationExecutorService The {@link ScheduledExecutorService} to use for processing
   *                                     messages from clients.
   * @param adapterProperties            The adapter's configuration properties.
   */
  public ClientConnectionHandler(Socket clientSocket,
                                 TlsServerProtocolProvider tlsServerProtocolProvider,
                                 RequestResponseDispatcher requestResponseDispatcher,
                                 DeviceAdapter deviceAdapter,
                                 ScheduledExecutorService communicationExecutorService,
                                 AdapterProperties adapterProperties) {
    this.clientSocket = requireNonNull(clientSocket, "clientSocket");
    this.tlsServerProtocolProvider = requireNonNull(tlsServerProtocolProvider,
                                                    "tlsServerProtocolProvider");
    this.requestResponseDispatcher = requireNonNull(requestResponseDispatcher,
                                                    "requestResponseDispatcher");
    this.deviceAdapter = requireNonNull(deviceAdapter, "deviceAdapter");
    this.communicationExecutorService = requireNonNull(communicationExecutorService,
                                                       "communicationExecutorService");
    this.adapterProperties = requireNonNull(adapterProperties, "adapterProperties");
  }

  @Override
  public void run() {
    if (!clientSocket.isConnected() || clientSocket.isClosed()) {
      LOG.warn("Client not connected or connection has already been closed.");
      return;
    }

    LOG.debug("Client {}:{}: Connection established...",
              clientSocket.getInetAddress().getHostAddress(),
              clientSocket.getPort());

    TlsServerProtocol serverProtocol = tlsServerProtocolProvider.getAndAccept(clientSocket);

    LOG.debug("Client {}:{}: Waiting for client message...",
              clientSocket.getInetAddress().getHostAddress(),
              clientSocket.getPort());

    var in = new BufferedReader(new InputStreamReader(serverProtocol.getInputStream(),
                                                      StandardCharsets.UTF_8));
    var out = new PrintWriter(serverProtocol.getOutputStream(), true, StandardCharsets.UTF_8);

    try {
      // A messages from a sensing puck device is expected to be terminated by a new line
      // character.
      String inputLine = in.readLine();

      LOG.debug("Client {}:{}: Message received: {}",
                clientSocket.getInetAddress().getHostAddress(),
                clientSocket.getPort(),
                inputLine);

      // Parse the received message.
      SensingPuckRequestBase request
          = requestResponseDispatcher.tryParseAndValidateMessage(inputLine);

      String tenant = String.valueOf(request.getDeviceId());
      if (!deviceAdapter.attemptDeviceInstanceAutoRegistration(tenant, request)) {
        LOG.warn("Client {}:{}: Failed to register device instance. Discarding received message...",
                 clientSocket.getInetAddress().getHostAddress(),
                 clientSocket.getPort());
        return;
      }

      if (!deviceAdapter.isDeviceTypeEnabled() || !deviceAdapter.isDeviceInstanceEnabled(tenant)) {
        LOG.debug("Client {}:{}: Device type or instance disabled. Discarding received message...",
                  clientSocket.getInetAddress().getHostAddress(),
                  clientSocket.getPort());
        return;
      }

      // Process the received request,...
      requestResponseDispatcher.onRequest(request);

      // ...send a response to the client,...
      SensingPuckResponseBase response = requestResponseDispatcher.getResponseFor(request);
      out.println(requestResponseDispatcher.serializeResponse(response));

      // ...and perform some post-processing steps.
      requestResponseDispatcher.postProcessResponse(response);
    } catch (Exception e) {
      LOG.error("Client {}:{}: Error while communicating with client or processing its message.",
                clientSocket.getInetAddress().getHostAddress(),
                clientSocket.getPort(),
                e);
    } finally {
      // Closing the reader and writer implicitly closes the associated streams which in turn closes
      // the client socket.
      // Don't close the reader and writer immediately, but schedule them to be closed in the
      // future. This is a (rather hacky) workaround for situations where the client cannot fully
      // retrieve the data from its input stream when the server (we) has already closed its socket.
      scheduleSilentClose(in, out);
    }
  }

  private void scheduleSilentClose(Closeable reader, Closeable writer) {
    communicationExecutorService.schedule(
        () -> {
          closeSilently(reader);
          closeSilently(writer);
          LOG.debug("Client {}:{}: Connection closed.",
                    clientSocket.getInetAddress().getHostAddress(),
                    clientSocket.getPort());
        },
        adapterProperties.getClientConnectionTimeout(),
        TimeUnit.SECONDS);
  }

  private void closeSilently(Closeable closeable) {
    try {
      closeable.close();
    } catch (IOException e) {
      // Ignore exceptions.
    }
  }
}
