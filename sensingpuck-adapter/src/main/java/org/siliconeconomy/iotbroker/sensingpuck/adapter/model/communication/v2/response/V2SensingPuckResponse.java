/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.HumidityAlarm;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.LedTimings;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.MeasurementCycles;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.TemperatureAlarm;

import static org.siliconeconomy.iotbroker.sensingpuck.adapter.util.Assertions.checkArgument;

/**
 * Represents a {@link ProtocolVersion#V2} response that the adapter sends to a device in response
 * to a {@link V2SensingPuckRequest}.
 *
 * @author M. Grzenia
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class V2SensingPuckResponse
    extends SensingPuckResponseBase {

  /**
   * The ID of the job to be processed by the sensing puck (limited to 2^53 - 1).
   */
  private long jobId;
  /**
   * The current state of the job.
   */
  private JobState jobState;
  /**
   * Indicates whether the device should clear its internal alarm flag.
   */
  private boolean clearAlarm;
  /**
   * Defines the intervals in which the sensing puck's sensors will be evaluated.
   */
  private MeasurementCycles measurementCycles;
  /**
   * The amount of evaluated data that will trigger the sensing puck to start communication
   * (as an unsigned 32-bit integer).
   */
  private long dataAmount;
  /**
   * Defines temperature thresholds that will trigger the sensing puck to start communication.
   */
  private TemperatureAlarm temperatureAlarm;
  /**
   * Defines humidity thresholds that will trigger the sensing puck to start communication.
   */
  private HumidityAlarm humidityAlarm;
  /**
   * Defines the lower battery voltage threshold that will trigger the sensing puck to start
   * communication.
   */
  private float voltageAlarm;
  /**
   * Defines timings for information to be shown on the sensing puck's display.
   */
  private LedTimings ledTimings;

  // Suppress "long parameter list" warning for simple data structures.
  @SuppressWarnings("java:S107")
  public V2SensingPuckResponse(int protocolVersion,
                               long jobId,
                               JobState jobState,
                               long deviceId,
                               boolean clearAlarm,
                               MeasurementCycles measurementCycles,
                               long dataAmount,
                               TemperatureAlarm temperatureAlarm,
                               HumidityAlarm humidityAlarm,
                               float voltageAlarm,
                               LedTimings ledTimings) {
    super(protocolVersion, deviceId);
    checkArgument(protocolVersion == ProtocolVersion.V2.getVersionNumber(),
                  String.format("protocolVersion is not %s", ProtocolVersion.V2.getVersionNumber()));
    this.jobId = jobId;
    this.jobState = jobState;
    this.clearAlarm = clearAlarm;
    this.measurementCycles = measurementCycles;
    this.dataAmount = dataAmount;
    this.temperatureAlarm = temperatureAlarm;
    this.humidityAlarm = humidityAlarm;
    this.voltageAlarm = voltageAlarm;
    this.ledTimings = ledTimings;
  }

  @Override
  public void setProtocolVersion(int protocolVersion) {
    checkArgument(protocolVersion == ProtocolVersion.V2.getVersionNumber(),
                  String.format("protocolVersion is not %s", ProtocolVersion.V2.getVersionNumber()));
    super.setProtocolVersion(protocolVersion);
  }
}
