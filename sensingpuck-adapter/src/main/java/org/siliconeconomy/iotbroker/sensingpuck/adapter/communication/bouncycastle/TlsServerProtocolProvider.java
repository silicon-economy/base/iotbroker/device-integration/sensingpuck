/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsServer;
import org.bouncycastle.tls.TlsServerProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;

import static java.util.Objects.requireNonNull;

/**
 * A provider for instances of {@link TlsServerProtocol}.
 *
 * @author M. Grzenia
 */
@Component
public class TlsServerProtocolProvider {

  private static final Logger LOG = LoggerFactory.getLogger(TlsServerProtocolProvider.class);
  private final TlsServerFactory tlsServerFactory;
  private final TlsServerProtocolFactory tlsServerProtocolFactory;

  /**
   * Creates a new instance.
   *
   * @param tlsServerFactory         Creates the {@link TlsServer} instances to perform the TLS
   *                                 handshakes with.
   * @param tlsServerProtocolFactory A factory for instances of {@link TlsServerProtocol}.
   */
  public TlsServerProtocolProvider(TlsServerFactory tlsServerFactory,
                                   TlsServerProtocolFactory tlsServerProtocolFactory) {
    this.tlsServerFactory = requireNonNull(tlsServerFactory, "tlsServerFactory");
    this.tlsServerProtocolFactory = requireNonNull(tlsServerProtocolFactory,
                                                   "tlsServerProtocolFactory");
  }

  /**
   * Provides a {@link TlsServerProtocol} instance for the given {@link Socket} and waits for a TLS
   * handshake over the given {@link Socket} to complete.
   * <p>
   * In blocking mode, like {@link TlsServerProtocol#accept(TlsServer)}, this method will not return
   * until the handshake is complete.
   *
   * @param clientSocket The {@link Socket} to get a {@link TlsServerProtocol} instance for.
   * @return A {@link TlsServerProtocol} instance.
   * @throws TlsServerProtocolCreationException If the creation of the {@link TlsServerProtocol}
   *                                            instance failed.
   * @throws TlsHandshakeException              If the TLS handshake failed.
   */
  public TlsServerProtocol getAndAccept(Socket clientSocket)
      throws TlsServerProtocolCreationException,
             TlsHandshakeException {
    requireNonNull(clientSocket, "clientSocket");

    TlsServer server = tlsServerFactory.create();
    TlsServerProtocol serverProtocol = tlsServerProtocolFactory.create(clientSocket);

    try {
      LOG.debug("Client {}:{}: Waiting for TLS handshake to complete...",
                clientSocket.getInetAddress().getHostAddress(),
                clientSocket.getPort());
      serverProtocol.accept(server);
      LOG.debug("Client {}:{}: TLS handshake complete.",
                clientSocket.getInetAddress().getHostAddress(),
                clientSocket.getPort());
    } catch (IOException e) {
      try {
        clientSocket.close();
      } catch (IOException ex) {
        // At this point, we don't really care whether the socket could be closed properly.
      }
      throw new TlsHandshakeException(
          String.format("Client %s:%d: TLS handshake failed.",
                        clientSocket.getInetAddress().getHostAddress(),
                        clientSocket.getPort()),
          e
      );
    }

    return serverProtocol;
  }
}
