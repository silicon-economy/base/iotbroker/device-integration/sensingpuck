/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.DeviceMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.SensorDataMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request.SensingPuckRequestConverter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.sensordata.MessageEntityToSensorDataMapper;
import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

/**
 * An abstract implementation of {@link RequestHandler} that implements basic behavior that is the
 * same across all protocol versions.
 * <p>
 * Where necessary, abstract (template) methods are provided to allow for protocol-specific
 * customization.
 *
 * @author M. Grzenia
 */
public abstract class AbstractRequestHandler
    implements RequestHandler {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(AbstractRequestHandler.class);
  /**
   * The service to use for accessing the database with the sensing puck messages.
   */
  private final MessageService messageService;
  /**
   * Handles publishing of (raw) client device messages to the IoT Broker's Device Message Exchange.
   */
  private final DeviceMessagePublisher deviceMessagePublisher;
  /**
   * Handles publishing of client device messages (mapped to {@link SensorDataMessage}s) to the
   * IoT Broker's Sensor Data Exchange.
   */
  private final SensorDataMessagePublisher sensorDataMessagePublisher;
  /**
   * Used to convert received {@link SensingPuckRequestBase} messages to instances of
   * {@link SensingPuckMessageDtoBase}.
   */
  private final SensingPuckRequestConverter sensingPuckRequestConverter
      = new SensingPuckRequestConverter();
  /**
   * Used to map {@link SensingPuckMessageEntityBase} instances to corresponding
   * {@link SensorDataMessage} instances.
   */
  private final MessageEntityToSensorDataMapper messageEntityToSensorDataMapper;

  protected AbstractRequestHandler(
      MessageService messageService,
      DeviceMessagePublisher deviceMessagePublisher,
      SensorDataMessagePublisher sensorDataMessagePublisher,
      MessageEntityToSensorDataMapper messageEntityToSensorDataMapper) {
    this.messageService = messageService;
    this.deviceMessagePublisher = deviceMessagePublisher;
    this.sensorDataMessagePublisher = sensorDataMessagePublisher;
    this.messageEntityToSensorDataMapper = messageEntityToSensorDataMapper;
  }

  @Override
  public void onRequest(SensingPuckRequestBase request) {
    if (!isSupportedRequestType(request)) {
      throw new UnsupportedRequestTypeException(request);
    }

    LOG.debug("Data received: {}", request);
    SensingPuckMessageDtoBase message = sensingPuckRequestConverter.convert(request, Instant.now());
    deviceMessagePublisher.publishToDeviceMessageExchange(String.valueOf(request.getDeviceId()),
                                                          message);
    SensingPuckMessageEntityBase messageEntity = messageService.save(message);
    sensorDataMessagePublisher.publishToSensorDataExchange(
        messageEntityToSensorDataMapper.map(messageEntity));
  }

  /**
   * Returns the type of {@link SensingPuckRequestBase} that this {@link RequestHandler} supports.
   *
   * @return A class extending {@link SensingPuckRequestBase}.
   */
  protected abstract Class<? extends SensingPuckRequestBase> supportedRequestType();

  private boolean isSupportedRequestType(SensingPuckRequestBase request) {
    return supportedRequestType().isAssignableFrom(request.getClass());
  }
}
