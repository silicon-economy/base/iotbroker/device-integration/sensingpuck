/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

/**
 * An exception indicating that the TLS handshake with a client has failed.
 *
 * @author M. Grzenia
 */
public class TlsHandshakeException
    extends RuntimeException {

  public TlsHandshakeException(String message, Throwable cause) {
    super(message, cause);
  }
}
