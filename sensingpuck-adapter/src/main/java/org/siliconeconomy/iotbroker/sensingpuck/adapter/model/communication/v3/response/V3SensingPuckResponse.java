/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;

import static org.siliconeconomy.iotbroker.sensingpuck.adapter.util.Assertions.checkArgument;

/**
 * Represents a {@link ProtocolVersion#V3} response that the adapter sends to a device in response
 * to a {@link V3SensingPuckRequest}.
 *
 * @author M. Grzenia
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class V3SensingPuckResponse
    extends SensingPuckResponseBase {

  /**
   * The ID of the job to be processed by the sensing puck (limited to 2^53 - 1).
   */
  private long jobId;
  /**
   * The current state of the job.
   */
  private JobState jobState;
  /**
   * Indicates whether the device should clear its internal alarm flag.
   */
  private boolean clearAlarm;
  /**
   * Defines the intervals in which the sensing puck's sensors will be evaluated.
   */
  private MeasurementCycles measurementCycles;
  /**
   * The amount of evaluated data that will trigger the sensing puck to start communication
   * (as an unsigned 32-bit integer).
   */
  private long dataAmount;
  /**
   * The duration (in ms) from the activation of the device to the completion of the packaging
   * process.
   * <p>
   * After the device is activated, it won't enter the <em>opened</em> alarm state within this
   * period.
   */
  private long packagingTime;
  /**
   * Defines temperature thresholds that will trigger the sensing puck to start communication.
   */
  private TemperatureAlarm temperatureAlarm;
  /**
   * Defines humidity thresholds that will trigger the sensing puck to start communication.
   */
  private HumidityAlarm humidityAlarm;
  /**
   * Defines the lower battery voltage threshold that will trigger the sensing puck to start
   * communication.
   */
  private float voltageAlarm;
  /**
   * Defines illuminance thresholds that will trigger the device to start communication.
   */
  private LightAlarm lightAlarm;
  /**
   * Defines timings for information to be shown on the sensing puck's display.
   */
  private LedTimings ledTimings;

  // Suppress "long parameter list" warning for simple data structures.
  @SuppressWarnings("java:S107")
  public V3SensingPuckResponse(int protocolVersion,
                               long jobId,
                               JobState jobState,
                               long deviceId,
                               boolean clearAlarm,
                               MeasurementCycles measurementCycles,
                               long dataAmount,
                               long packagingTime,
                               TemperatureAlarm temperatureAlarm,
                               HumidityAlarm humidityAlarm,
                               float voltageAlarm,
                               LightAlarm lightAlarm,
                               LedTimings ledTimings) {
    super(protocolVersion, deviceId);
    checkArgument(protocolVersion == ProtocolVersion.V3.getVersionNumber(),
                  String.format("protocolVersion is not %s", ProtocolVersion.V3.getVersionNumber()));
    this.jobId = jobId;
    this.jobState = jobState;
    this.clearAlarm = clearAlarm;
    this.measurementCycles = measurementCycles;
    this.dataAmount = dataAmount;
    this.packagingTime = packagingTime;
    this.temperatureAlarm = temperatureAlarm;
    this.humidityAlarm = humidityAlarm;
    this.voltageAlarm = voltageAlarm;
    this.lightAlarm = lightAlarm;
    this.ledTimings = ledTimings;
  }

  @Override
  public void setProtocolVersion(int protocolVersion) {
    checkArgument(protocolVersion == ProtocolVersion.V3.getVersionNumber(),
                  String.format("protocolVersion is not %s", ProtocolVersion.V3.getVersionNumber()));
    super.setProtocolVersion(protocolVersion);
  }
}
