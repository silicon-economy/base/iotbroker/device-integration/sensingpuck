/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.HumidityAlarm;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.LedTimings;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.MeasurementCycles;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.TemperatureAlarm;

/**
 * The default {@link V2SensingPuckResponse} to be sent to sensing puck devices if there is no
 * appropriate device configuration in the database.
 *
 * @author M. Grzenia
 */
public class V2DefaultSensingPuckResponse
    extends V2SensingPuckResponse {

  public V2DefaultSensingPuckResponse(long deviceId) {
    super(ProtocolVersion.V2.getVersionNumber(),
          0,
          JobState.FINISHED,
          deviceId,
          false,
          new MeasurementCycles(60, 10),
          12,
          new TemperatureAlarm(35.0f, 15.0f),
          new HumidityAlarm(65.0f, 30.0f),
          3.0f,
          new LedTimings(10, 5, 5));
  }
}
