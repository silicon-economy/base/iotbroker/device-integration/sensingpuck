/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.ObservedProperty;

/**
 * The observed property describing the opened state of a package.
 *
 * @author M. Grzenia
 */
public class OpenedProperty
    extends ObservedProperty {

  private static final String NAME = "The opened state of the package.";
  private static final String DESCRIPTION
      = "A flag indicating whether the package is considered opened.";

  public OpenedProperty() {
    super(NAME, DESCRIPTION);
  }
}
