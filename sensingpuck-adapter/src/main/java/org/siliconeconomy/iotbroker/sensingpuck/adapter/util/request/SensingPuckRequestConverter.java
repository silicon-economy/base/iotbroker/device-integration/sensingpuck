/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.FirmwareDescriptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides a method to convert instances of {@link SensingPuckRequestBase} to instances of
 * {@link SensingPuckMessageDtoBase}.
 *
 * @author M. Grzenia
 */
public class SensingPuckRequestConverter {

  /**
   * Converts the timestamps of data entries in a {@link SensingPuckRequestBase}.
   */
  private final DataTimestampConverter dataTimestampConverter = new DataTimestampConverter();
  /**
   * Used to format the firmware version number to a string.
   */
  private final FirmwareVersionFormatter firmwareVersionFormatter = new FirmwareVersionFormatter();

  /**
   * Converts the given {@link SensingPuckRequestBase} to an instance of
   * {@link SensingPuckMessageDtoBase} by considering their respective subtypes.
   *
   * @param request      The {@link SensingPuckRequestBase} to convert.
   * @param timeReceived The time at which the request was received.
   * @return The converted {@link SensingPuckMessageDtoBase}.
   * @throws IllegalArgumentException If the given {@link SensingPuckRequestBase} is of an unhandled
   *                                  type.
   */
  public SensingPuckMessageDtoBase convert(SensingPuckRequestBase request, Instant timeReceived) {
    if (request instanceof V1SensingPuckRequest) {
      return convert((V1SensingPuckRequest) request, timeReceived);
    }
    if (request instanceof V2SensingPuckRequest) {
      return convert((V2SensingPuckRequest) request, timeReceived);
    }
    if (request instanceof V3SensingPuckRequest) {
      return convert((V3SensingPuckRequest) request, timeReceived);
    }

    throw new IllegalArgumentException("Unknown type: " + request.getClass());
  }

  private V1SensingPuckMessageDto convert(V1SensingPuckRequest request, Instant timeReceived) {
    List<V1SensingData> sensingData = request.getData().stream()
        .map(data -> convertV1SensingData(data, request.getComTimestamp(), timeReceived))
        .collect(Collectors.toList());

    return new V1SensingPuckMessageDto(
        request.getProtocolVersion(),
        request.getJobId(),
        request.getDeviceId(),
        convertFirmwareDescriptor(request.getFirmwareDescriptor()),
        timeReceived,
        request.getLastComCause(),
        sensingData
    );
  }

  private V2SensingPuckMessageDto convert(V2SensingPuckRequest request, Instant timeReceived) {
    List<V2SensingData> sensingData = request.getData().stream()
        .map(data -> convertV2SensingData(data, request.getComTimestamp(), timeReceived))
        .collect(Collectors.toList());

    return new V2SensingPuckMessageDto(
        request.getProtocolVersion(),
        request.getJobId(),
        request.getDeviceId(),
        convertFirmwareDescriptor(request.getFirmwareDescriptor()),
        timeReceived,
        request.getLastComCause(),
        sensingData
    );
  }

  private V3SensingPuckMessageDto convert(V3SensingPuckRequest request, Instant timeReceived) {
    List<V3SensingData> sensingData = request.getData().stream()
        .map(data -> convertV3SensingData(data, request.getComTimestamp(), timeReceived))
        .collect(Collectors.toList());

    return new V3SensingPuckMessageDto(
        request.getProtocolVersion(),
        request.getJobId(),
        request.getDeviceId(),
        convertFirmwareDescriptor(request.getFirmwareDescriptor()),
        timeReceived,
        request.getLastComCause(),
        sensingData
    );
  }

  private FirmwareDescriptor convertFirmwareDescriptor(FirmwareDescriptorRaw rawData) {
    String versionNumber
        = firmwareVersionFormatter.format(rawData.getVersionNumber().getMajor(),
                                          rawData.getVersionNumber().getMinor(),
                                          rawData.getVersionNumber().getPatch());

    return new FirmwareDescriptor(
        versionNumber,
        rawData.getVersionNumber().getCommitCounter(),
        rawData.getVersionNumber().isReleaseFlag(),
        rawData.getCommitHash(),
        rawData.isDirtyFlag()
    );
  }

  private V1SensingData convertV1SensingData(V1SensingDataRaw rawData,
                                             long comTimestamp,
                                             Instant timeReceived) {
    return new V1SensingData(
        rawData.getTemperature(),
        rawData.getHumidity(),
        dataTimestampConverter.convert(rawData.getTimestamp(), comTimestamp, timeReceived),
        rawData.getMotionState()
    );
  }

  private V2SensingData convertV2SensingData(V2SensingDataRaw rawData,
                                             long comTimestamp,
                                             Instant timeReceived) {
    return new V2SensingData(
        rawData.getTemperature(),
        rawData.getHumidity(),
        rawData.getVoltage(),
        dataTimestampConverter.convert(rawData.getTimestamp(), comTimestamp, timeReceived),
        rawData.getMotionState()
    );
  }

  private V3SensingData convertV3SensingData(V3SensingDataRaw rawData,
                                             long comTimestamp,
                                             Instant timeReceived) {
    return new V3SensingData(
        rawData.getTemperature(),
        rawData.getHumidity(),
        rawData.getVoltage(),
        rawData.getAmbientLight(),
        rawData.isOpenedFlag(),
        rawData.getGpsLat(),
        rawData.getGpsLong(),
        dataTimestampConverter.convert(rawData.getTimestamp(), comTimestamp, timeReceived),
        rawData.getMotionState()
    );
  }
}
