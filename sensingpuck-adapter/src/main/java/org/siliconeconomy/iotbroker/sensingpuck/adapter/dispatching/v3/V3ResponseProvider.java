/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.AbstractResponseProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.ResponseProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * The {@link ResponseProvider} implementation for {@link ProtocolVersion#V3} responses.
 *
 * @author M. Grzenia
 */
@Component
public class V3ResponseProvider
    extends AbstractResponseProvider {

  public V3ResponseProvider(JobService jobService) {
    super(jobService);
  }

  @Override
  protected Class<? extends SensingPuckRequestBase> supportedRequestType() {
    return V3SensingPuckRequest.class;
  }

  @Override
  protected Set<Class<? extends SensingPuckJobEntityBase>> supportedJobEntityTypes() {
    return Set.of(V3SensingPuckJobEntity.class);
  }

  @Override
  protected Set<Class<? extends SensingPuckResponseBase>> supportedResponseTypes() {
    return Set.of(V3SensingPuckResponse.class);
  }

  @Override
  protected SensingPuckResponseBase createDefaultSensingPuckResponse() {
    return new V3DefaultSensingPuckResponse(0);
  }
}
