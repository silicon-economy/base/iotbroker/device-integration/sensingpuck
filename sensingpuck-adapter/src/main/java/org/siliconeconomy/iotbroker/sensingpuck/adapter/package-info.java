/**
 * The root package of the sensing puck adapter application.
 * Contains the main class and the adapter class itself.
 *
 * @author M. Grzenia
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;
