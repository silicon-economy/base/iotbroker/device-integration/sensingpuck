/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;

/**
 * Defines methods for pre-processing messages received from sensing puck client devices.
 *
 * @author M. Grzenia
 */
public interface RequestPreProcessor {

  /**
   * Tries to parse and validate the given (raw) message received from a sensing puck client device.
   *
   * @param message The (raw) message received from a sensing puck client device.
   * @return The mapped {@link SensingPuckRequestBase}.
   * @throws IllegalArgumentException If parsing or validating the message failed.
   */
  SensingPuckRequestBase tryParseAndValidateMessage(String message);
}
