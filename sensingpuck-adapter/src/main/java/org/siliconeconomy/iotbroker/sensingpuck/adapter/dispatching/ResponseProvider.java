/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;

/**
 * Defines methods for providing appropriate responses to requests received from sensing puck client
 * devices.
 *
 * @author M. Grzenia
 */
public interface ResponseProvider {

  /**
   * Returns an appropriate {@link SensingPuckResponseBase} for a sensing puck device that sent
   * the given {@link SensingPuckRequestBase}.
   *
   * @param request The received request.
   * @return A {@link SensingPuckRequestBase}.
   */
  SensingPuckResponseBase getResponseFor(SensingPuckRequestBase request);

  /**
   * Serializes the given {@link SensingPuckResponseBase}.
   *
   * @param response The response to serialize.
   * @return The serialized response.
   */
  String serializeResponse(SensingPuckResponseBase response);
}
