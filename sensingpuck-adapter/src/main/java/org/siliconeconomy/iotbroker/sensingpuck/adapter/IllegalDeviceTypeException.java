/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

/**
 * Exception indicating that the {@link DeviceType} a {@link DeviceInstance} is associated with
 * (see {@link DeviceInstance#getDeviceTypeIdentifier()}) does not correspond to the
 * {@link DeviceType} that the {@link DeviceAdapter} provides an IoT Broker integration for.
 *
 * @author M. Grzenia
 */
public class IllegalDeviceTypeException
    extends RuntimeException {

  /**
   * Creates a new instance.
   *
   * @param source               The source of the {@link DeviceInstance}.
   * @param tenant               The tenant of the {@link DeviceInstance}.
   * @param deviceTypeIdentifier The identifier of the {@link DeviceType} the {@link DeviceInstance}
   *                             is associated with.
   * @param deviceType           The {@link DeviceType} the {@link DeviceAdapter} provides an
   *                             IoT Broker integration for.
   */
  public IllegalDeviceTypeException(String source,
                                    String tenant,
                                    String deviceTypeIdentifier,
                                    DeviceType deviceType) {
    super(String.format(
        "Device type of device instance (source=%s, tenant=%s, deviceTypeIdentifier=%s) does not " +
            "correspond to the adapter's device type (source=%s, identifier=%s).",
        source,
        tenant,
        deviceTypeIdentifier,
        deviceType.getSource(),
        deviceType.getIdentifier()
    ));
  }
}
