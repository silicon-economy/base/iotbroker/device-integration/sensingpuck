/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.UnitOfMeasurement;

/**
 * A unit for observed properties that actually have no unit.
 *
 * @author M. Grzenia
 */
public class NoUnit
    extends UnitOfMeasurement {

  public NoUnit() {
    super("", "");
  }
}
