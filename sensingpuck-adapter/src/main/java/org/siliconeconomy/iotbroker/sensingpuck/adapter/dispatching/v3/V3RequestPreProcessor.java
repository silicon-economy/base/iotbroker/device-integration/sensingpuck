/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.AbstractRequestPreProcessor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestPreProcessor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.springframework.stereotype.Component;

/**
 * The {@link RequestPreProcessor} implementation for {@link ProtocolVersion#V3} requests.
 *
 * @author M. Grzenia
 */
@Component
public class V3RequestPreProcessor
    extends AbstractRequestPreProcessor {

  @Override
  protected Class<? extends SensingPuckRequestBase> supportedRequestType() {
    return V3SensingPuckRequest.class;
  }
}
