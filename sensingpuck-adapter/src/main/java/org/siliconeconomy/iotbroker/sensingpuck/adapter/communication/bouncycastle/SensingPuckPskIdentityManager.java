/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsPSKIdentityManager;
import org.bouncycastle.util.Strings;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * The {@link TlsPSKIdentityManager} that manages PSKs for sensing puck client devices.
 *
 * @author M. Grzenia
 */
@Component
public class SensingPuckPskIdentityManager
    implements TlsPSKIdentityManager {

  private final AdapterProperties adapterProperties;

  /**
   * Creates a new instance.
   *
   * @param adapterProperties The adapter-relevant (configuration) properties.
   */
  public SensingPuckPskIdentityManager(AdapterProperties adapterProperties) {
    this.adapterProperties = requireNonNull(adapterProperties, "adapterProperties");
  }

  @Override
  public byte[] getHint() {
    return Strings.toUTF8ByteArray(adapterProperties.getServerTlsPskIdentityHint());
  }

  @Override
  public byte[] getPSK(byte[] identity) {
    requireNonNull(identity, "identity");

    String identityString = Strings.fromUTF8ByteArray(identity);
    if (!isSensingPuckIdentity(identityString)) {
      throw new IllegalArgumentException(String.format("Unknown identity: %s", identityString));
    }

    return Strings.toUTF8ByteArray(adapterProperties.getServerTlsPsk());
  }

  private boolean isSensingPuckIdentity(String identity) {
    return Objects.equals(identity, adapterProperties.getServerTlsPskIdentity());
  }
}
