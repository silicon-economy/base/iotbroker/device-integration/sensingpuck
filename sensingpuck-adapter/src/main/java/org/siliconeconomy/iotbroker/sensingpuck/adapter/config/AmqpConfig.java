/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.amqp.DeviceManagementExchangeConfiguration;
import org.siliconeconomy.iotbroker.amqp.DeviceMessageExchangeConfiguration;
import org.siliconeconomy.iotbroker.amqp.SensorDataExchangeConfiguration;
import org.siliconeconomy.iotbroker.jackson.ObjectMapperFactory;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.AdapterIdentifierProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.DeviceRegistrationUpdateListener;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.SensorDataMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of various AMQP related beans.
 *
 * @author M. Grzenia
 */
@Configuration
public class AmqpConfig {

  private final DeviceTypeProvider deviceTypeProvider;
  private final AdapterIdentifierProvider adapterIdentifierProvider;

  /**
   * Creates a new instance.
   *
   * @param deviceTypeProvider        Provides information about the adapter's {@link DeviceType}.
   * @param adapterIdentifierProvider Provides the adapter identifier.
   */
  public AmqpConfig(DeviceTypeProvider deviceTypeProvider,
                    AdapterIdentifierProvider adapterIdentifierProvider) {
    this.deviceTypeProvider = requireNonNull(deviceTypeProvider, "deviceTypeProvider");
    this.adapterIdentifierProvider = requireNonNull(adapterIdentifierProvider,
                                                    "adapterIdentifierProvider");
  }

  /**
   * Configures (and creates) the AMQP exchange where messages regarding the device management are
   * published to.
   *
   * @return The configured {@link Exchange}.
   */
  @Bean
  public Exchange deviceManagementExchange() {
    return ExchangeBuilder
        .topicExchange(DeviceManagementExchangeConfiguration.NAME)
        .durable(DeviceManagementExchangeConfiguration.DURABLE)
        .build();
  }

  /**
   * Configures (and creates) the AMQP exchange where {@link SensorDataMessage}s are published to.
   *
   * @return The configured {@link Exchange}.
   */
  @Bean
  public Exchange sensorDataExchange() {
    return ExchangeBuilder
        .topicExchange(SensorDataExchangeConfiguration.NAME)
        .durable(SensorDataExchangeConfiguration.DURABLE)
        .build();
  }

  /**
   * Configures (and creates) the AMQP exchange where {@link SensingPuckRequestBase} messages are
   * published to.
   *
   * @return The configured {@link Exchange}.
   */
  @Bean
  public Exchange deviceMessageExchange() {
    return ExchangeBuilder
        .topicExchange(DeviceMessageExchangeConfiguration.NAME)
        .durable(DeviceMessageExchangeConfiguration.DURABLE)
        .build();
  }

  /**
   * Configures (and creates) the {@link Queue} used by {@link DeviceRegistrationUpdateListener}.
   *
   * @return The configured {@link Queue}.
   */
  @Bean
  public Queue deviceInstanceUpdateListenerQueue() {
    return QueueBuilder
        // Use a prefix for the queue name to indicate affiliation with the adapter, but still use
        // a random name to ensure multiple adapter instances don't share the same queue.
        .nonDurable(adapterIdentifierProvider.getAdapterIdentifier() + "-" + UUID.randomUUID())
        .autoDelete()
        .exclusive()
        .build();
  }

  /**
   * Configures (and creates) the {@link Queue} used by {@link DeviceRegistrationUpdateListener}.
   *
   * @return The configured {@link Queue}.
   */
  @Bean
  public Queue deviceTypeUpdateListenerQueue() {
    return QueueBuilder
        // Use a prefix for the queue name to indicate affiliation with the adapter, but still use
        // a random name to ensure multiple adapter instances don't share the same queue.
        .nonDurable(adapterIdentifierProvider.getAdapterIdentifier() + "-" + UUID.randomUUID())
        .autoDelete()
        .exclusive()
        .build();
  }

  /**
   * Configures the {@link Binding} from the {@link #deviceInstanceUpdateListenerQueue()} to the
   * {@link #deviceManagementExchange()}.
   *
   * @return The configured {@link Binding}.
   */
  @Bean
  public Binding deviceInstanceUpdateListenerBinding() {
    String routingKey = DeviceManagementExchangeConfiguration.formatDeviceInstanceUpdateRoutingKey(
        deviceTypeProvider.getAdapterDeviceTypeIdentifier()
    );
    return BindingBuilder
        .bind(deviceInstanceUpdateListenerQueue())
        .to(deviceManagementExchange())
        .with(routingKey)
        .noargs();
  }

  /**
   * Configures the {@link Binding} from the {@link #deviceTypeUpdateListenerQueue()} to the
   * {@link #deviceManagementExchange()}.
   *
   * @return The configured {@link Binding}.
   */
  @Bean
  public Binding deviceTypeUpdateListenerBinding() {
    String routingKey = DeviceManagementExchangeConfiguration.formatDeviceTypeUpdateRoutingKey(
        deviceTypeProvider.getAdapterDeviceTypeIdentifier()
    );
    return BindingBuilder
        .bind(deviceTypeUpdateListenerQueue())
        .to(deviceManagementExchange())
        .with(routingKey)
        .noargs();
  }

  /**
   * Configures the {@link AmqpTemplate} to be used by {@link SensorDataMessagePublisher}.
   * <p>
   * Configures a {@link RabbitTemplate} to use a {@link SimpleMessageConverter}. This is basically
   * Spring's default configuration, but it is done explicitly because there's also a
   * {@link SimpleRabbitListenerContainerFactory} being configured with a different
   * {@link MessageConverter}.
   *
   * @param connectionFactory The {@link ConnectionFactory} to use.
   * @return The configured {@link AmqpTemplate}.
   * @see #rabbitListenerContainerFactory(ConnectionFactory)
   */
  @Bean
  public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
    var rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(new SimpleMessageConverter());
    return rabbitTemplate;
  }

  /**
   * Configures the {@link SimpleRabbitListenerContainerFactory} to be used by
   * {@link DeviceRegistrationUpdateListener}.
   * <p>
   * Configures a {@link SimpleRabbitListenerContainerFactory} to use a
   * {@link Jackson2JsonMessageConverter} with a properly configured {@link ObjectMapper}. For this,
   * the Device Registry Service's {@link ObjectMapperFactory} is used, which already provides an
   * {@link ObjectMapper} for deserialization of {@link DeviceType} and {@link DeviceInstance}
   * instances.
   *
   * @param connectionFactory The {@link ConnectionFactory} to use.
   * @return The configured {@link SimpleRabbitListenerContainerFactory}.
   */
  @Bean
  public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
      ConnectionFactory connectionFactory) {
    // A properly configured message converter is required for deserialization of AMQP messages to
    // Sensor Data Message instances. We could define the message converter as a bean and Spring
    // would 'automagically' set it, but this way it's more explicit. Also, for methods annotated
    // with @RabbitListener, this way we don't have to explicitly specify the message converter to
    // use.
    var objectMapper = ObjectMapperFactory.createObjectMapper();
    var messageConverter = new Jackson2JsonMessageConverter(objectMapper);

    var factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(connectionFactory);
    factory.setMessageConverter(messageConverter);

    return factory;
  }
}
