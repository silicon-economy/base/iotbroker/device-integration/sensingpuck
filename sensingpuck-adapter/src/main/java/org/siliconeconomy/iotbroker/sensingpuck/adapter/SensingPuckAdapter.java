/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.experimental.adapter.AbstractDeviceAdapter;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateListener;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateListener;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.TcpServerSocketManager;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.TcpServerSocketManagerFactory;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device.DeviceInstanceCreateInputProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device.DeviceTypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.ServerSocket;
import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * A {@link DeviceAdapter} implementation for sensing puck devices.
 * <p>
 * Implementation remarks:
 * <ul>
 *   <li>Communication with the sensing puck client devices is done via a simple TCP connection.
 *   Connections to the devices are managed by a {@link TcpServerSocketManager}.</li>
 * </ul>
 *
 * @author M. Grzenia
 */
@Component
public class SensingPuckAdapter
    extends AbstractDeviceAdapter {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(SensingPuckAdapter.class);
  /**
   * The client to use for access to the Device Registry Service.
   */
  private final DeviceRegistryServiceClient deviceRegistryServiceClient;
  /**
   * A provider for {@link DeviceInstanceCreateInputTO} instances.
   */
  private final DeviceInstanceCreateInputProvider deviceInstanceCreateInputProvider;
  /**
   * Used to convert {@link DeviceType} instances.
   */
  private final DeviceTypeConverter deviceTypeConverter = new DeviceTypeConverter();
  /**
   * Provides information about the adapter's {@link DeviceType}.
   */
  private final DeviceTypeProvider deviceTypeProvider;
  /**
   * A factory for instances of TcpServerSocketManager.
   */
  private final TcpServerSocketManagerFactory serverChannelManagerFactory;
  /**
   * The {@link TcpServerSocketManager} instance managing the adapter's {@link ServerSocket}.
   */
  private TcpServerSocketManager serverChannelManager;

  @Autowired
  public SensingPuckAdapter(DeviceRegistryServiceClient deviceRegistryServiceClient,
                            DeviceInstanceCreateInputProvider deviceInstanceCreateInputProvider,
                            TcpServerSocketManagerFactory serverChannelManagerFactory,
                            SensingPuckAdapterAttributes sensingPuckadapterAttributes,
                            DeviceTypeProvider deviceTypeProvider,
                            DeviceTypeUpdateListener deviceTypeUpdateListener,
                            DeviceInstanceUpdateListener deviceInstanceUpdateListener) {
    super(sensingPuckadapterAttributes, deviceTypeUpdateListener, deviceInstanceUpdateListener);
    this.deviceRegistryServiceClient = deviceRegistryServiceClient;
    this.deviceInstanceCreateInputProvider = deviceInstanceCreateInputProvider;
    this.serverChannelManagerFactory = serverChannelManagerFactory;
    this.deviceTypeProvider = deviceTypeProvider;
  }

  @PostConstruct
  @Override
  public void initialize() {
    if (isInitialized()) {
      return;
    }

    super.initialize();
  }

  @PreDestroy
  @Override
  public void terminate() {
    if (!isInitialized()) {
      return;
    }

    super.terminate();
  }

  @Override
  public synchronized void registerDeviceType() {
    LOG.debug("Registering device type...");
    if (isDeviceTypeRegistered()) {
      LOG.debug("Device type is already registered.");
      return;
    }

    DeviceTypeCreateInputTO deviceTypeCreateInput = deviceTypeConverter.toDeviceTypeCreateInputTO(
        deviceTypeProvider.createDefaultAdapterDeviceType()
    );
    deviceRegistryServiceClient.createDeviceType(deviceTypeCreateInput);
  }

  @Override
  public synchronized void updateDeviceTypeProvidedByAdapter() {
    if (!isDeviceTypeRegistered()) {
      throw new IllegalStateException("Cannot update device type that is not registered.");
    }

    DeviceType deviceType = fetchAdapterDeviceType().orElseThrow(
        () -> new IllegalStateException("Cannot update device type that is not registered.")
    );

    DeviceTypeUpdateInputTO deviceTypeUpdateInput
        = deviceTypeConverter.toDeviceTypeUpdateInput(deviceType);
    Set<String> providedBy = new HashSet<>(deviceTypeUpdateInput.getProvidedBy());
    providedBy.add(getAdapterAttributes().getAdapterIdentifier());
    deviceTypeUpdateInput.setProvidedBy(providedBy);
    deviceRegistryServiceClient.updateDeviceType(deviceType.getId(), deviceTypeUpdateInput);
  }

  @Override
  public synchronized void registerDeviceInstance(@NonNull String tenant,
                                                  @Nullable Object registrationContext)
      throws IllegalDeviceTypeException {
    requireNonNull(tenant, "tenant");

    LOG.debug("Registering device instance with tenant '{}'...", tenant);
    if (isDeviceInstanceRegistered(tenant)) {
      LOG.debug("Device instance with tenant '{}' is already registered.", tenant);
      return;
    }

    DeviceInstanceCreateInputTO deviceInstanceCreateInput
        = deviceInstanceCreateInputProvider.get(tenant, registrationContext);
    DeviceType adapterDeviceType = fetchAdapterDeviceType().orElseGet(NullDeviceType::new);

    // Ensure that the device instance to be registered is associated with to the adapter's device
    // type.
    if (!isSupportedDeviceType(deviceInstanceCreateInput.getSource(),
                               deviceInstanceCreateInput.getDeviceTypeIdentifier())) {
      throw new IllegalDeviceTypeException(deviceInstanceCreateInput.getSource(),
                                           deviceInstanceCreateInput.getTenant(),
                                           deviceInstanceCreateInput.getDeviceTypeIdentifier(),
                                           adapterDeviceType);
    }

    // Consider the device type's current configuration regarding the 'enabled' state for newly
    // registered device instances.
    deviceInstanceCreateInput.setEnabled(adapterDeviceType.isAutoEnableDeviceInstances());
    deviceRegistryServiceClient.createDeviceInstance(deviceInstanceCreateInput);
  }

  @Override
  protected void openCommunicationChannel() {
    if (serverChannelManager != null) {
      return;
    }

    serverChannelManager = serverChannelManagerFactory.create(this);
    serverChannelManager.initialize();
  }

  @Override
  protected void closeCommunicationChannel() {
    if (serverChannelManager == null) {
      return;
    }

    serverChannelManager.terminate();
    serverChannelManager = null;
  }

  @Override
  protected @NonNull Optional<DeviceType> fetchAdapterDeviceType() {
    return deviceRegistryServiceClient.fetchDeviceTypes(
        getAdapterAttributes().getAdapterDeviceSource(),
        getAdapterAttributes().getAdapterDeviceTypeIdentifier(),
        null,
        null,
        null,
        null).stream()
        .findFirst();
  }

  @Override
  protected @NonNull Optional<DeviceInstance> fetchAdapterDeviceInstance(String tenant) {
    requireNonNull(tenant, "tenant");

    return deviceRegistryServiceClient.fetchDeviceInstances(
        getAdapterAttributes().getAdapterDeviceSource(),
        tenant,
        null,
        null,
        null,
        null).stream().findFirst();
  }

  private boolean isSupportedDeviceType(String source, String deviceTypeIdentifier) {
    return Objects.equals(getAdapterAttributes().getAdapterDeviceSource(), source)
        && Objects.equals(deviceTypeProvider.getAdapterDeviceTypeIdentifier(),
                          deviceTypeIdentifier);
  }
}
