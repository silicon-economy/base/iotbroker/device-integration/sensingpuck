/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp;

import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateListener;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateListener;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AmqpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Configures a {@link RabbitListener} for {@link DeviceInstanceUpdate} messages.
 *
 * @author M. Grzenia
 */
@Component
public class DeviceRegistrationUpdateListener {

  private static final Logger LOG = LoggerFactory.getLogger(DeviceRegistrationUpdateListener.class);

  private final DeviceTypeUpdateListener deviceTypeUpdateListener;
  private final DeviceInstanceUpdateListener deviceInstanceUpdateListener;


  /**
   * Creates a new instance.
   */
  public DeviceRegistrationUpdateListener(
      DeviceTypeUpdateListener deviceTypeUpdateListener,
      DeviceInstanceUpdateListener deviceInstanceUpdateListener) {
    this.deviceTypeUpdateListener = deviceTypeUpdateListener;
    this.deviceInstanceUpdateListener = deviceInstanceUpdateListener;
  }

  /**
   * The {@link RabbitListener} callback method for {@link DeviceInstanceUpdate} messages published
   * via the IoT Broker's AMQP broker.
   * <p>
   * The {@link RabbitListener} ignores {@link DeviceInstanceUpdate} messages that do not
   * correspond to the {@link DeviceType} the adapter provides an IoT Broker integration for.
   *
   * @param deviceInstanceUpdate The {@link DeviceInstanceUpdate} message received via AMQP.
   * @see AmqpConfig
   */
  @RabbitListener(queues = "#{deviceInstanceUpdateListenerQueue}")
  public void onDeviceInstanceUpdate(DeviceInstanceUpdate deviceInstanceUpdate) {
    LOG.debug("Received device instance update message: {}", deviceInstanceUpdate);
    deviceInstanceUpdateListener.onDeviceInstanceUpdate(deviceInstanceUpdate);
  }

  /**
   * The {@link RabbitListener} callback method for {@link DeviceTypeUpdate} messages published
   * via the IoT Broker's AMQP broker.
   * <p>
   * The {@link RabbitListener} ignores {@link DeviceTypeUpdate} messages that do not
   * correspond to the {@link DeviceType} the adapter provides an IoT Broker integration for.
   *
   * @param deviceTypeUpdate The {@link DeviceTypeUpdate} message received via AMQP.
   * @see AmqpConfig
   */
  @RabbitListener(queues = "#{deviceTypeUpdateListenerQueue}")
  public void onDeviceTypeUpdate(DeviceTypeUpdate deviceTypeUpdate) {
    LOG.debug("Received device type update message: {}", deviceTypeUpdate);
    deviceTypeUpdateListener.onDeviceTypeUpdate(deviceTypeUpdate);
  }
}
