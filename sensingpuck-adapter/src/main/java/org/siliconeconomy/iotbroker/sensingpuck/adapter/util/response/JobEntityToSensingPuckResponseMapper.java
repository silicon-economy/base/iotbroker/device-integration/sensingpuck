/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.response;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

/**
 * Provides methods for mapping {@link SensingPuckJobEntityBase} instances retrieved from the
 * database to corresponding instances of {@link SensingPuckResponseBase} that can be sent to
 * sensing puck devices.
 *
 * @author M. Grzenia
 */
public class JobEntityToSensingPuckResponseMapper {

  /**
   * Maps the given {@link SensingPuckJobEntityBase} to an instance of
   * {@link SensingPuckResponseBase} by considering their subtypes.
   *
   * @param jobEntity The {@link SensingPuckJobEntityBase} to map.
   * @return The mapped {@link SensingPuckResponseBase}.
   */
  public SensingPuckResponseBase map(SensingPuckJobEntityBase jobEntity) {
    if (jobEntity instanceof V1SensingPuckJobEntity) {
      return map((V1SensingPuckJobEntity) jobEntity);
    }
    if (jobEntity instanceof V2SensingPuckJobEntity) {
      return map((V2SensingPuckJobEntity) jobEntity);
    }
    if (jobEntity instanceof V3SensingPuckJobEntity) {
      return map((V3SensingPuckJobEntity) jobEntity);
    }

    throw new IllegalArgumentException("Unknown type: " + jobEntity.getClass());
  }

  private V1SensingPuckResponse map(V1SensingPuckJobEntity config) {
    return new V1SensingPuckResponse(config.getProtocolVersion(),
                                     config.getJobId(),
                                     config.getJobState(),
                                     config.getDeviceId(),
                                     config.isClearAlarm(),
                                     config.getMeasurementCycles(),
                                     config.getDataAmount(),
                                     config.getTemperatureAlarm(),
                                     config.getHumidityAlarm(),
                                     config.getLedTimings());
  }

  private V2SensingPuckResponse map(V2SensingPuckJobEntity config) {
    return new V2SensingPuckResponse(config.getProtocolVersion(),
                                     config.getJobId(),
                                     config.getJobState(),
                                     config.getDeviceId(),
                                     config.isClearAlarm(),
                                     config.getMeasurementCycles(),
                                     config.getDataAmount(),
                                     config.getTemperatureAlarm(),
                                     config.getHumidityAlarm(),
                                     config.getVoltageAlarm(),
                                     config.getLedTimings());
  }

  private V3SensingPuckResponse map(V3SensingPuckJobEntity config) {
    return new V3SensingPuckResponse(config.getProtocolVersion(),
                                     config.getJobId(),
                                     config.getJobState(),
                                     config.getDeviceId(),
                                     config.isClearAlarm(),
                                     config.getMeasurementCycles(),
                                     config.getDataAmount(),
                                     config.getPackagingTime(),
                                     config.getTemperatureAlarm(),
                                     config.getHumidityAlarm(),
                                     config.getVoltageAlarm(),
                                     config.getLightAlarm(),
                                     config.getLedTimings());
  }
}
