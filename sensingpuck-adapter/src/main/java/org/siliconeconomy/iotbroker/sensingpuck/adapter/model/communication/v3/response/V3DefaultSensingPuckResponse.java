/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response;

import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;

/**
 * The default {@link V3SensingPuckResponse} to be sent to sensing puck devices if there is no
 * appropriate device configuration in the database.
 *
 * @author M. Grzenia
 */
public class V3DefaultSensingPuckResponse
    extends V3SensingPuckResponse {

  public V3DefaultSensingPuckResponse(long deviceId) {
    super(ProtocolVersion.V3.getVersionNumber(),
          0,
          JobState.FINISHED,
          deviceId,
          false,
          new MeasurementCycles(60, 10),
          12,
          600000,
          new TemperatureAlarm(35.0f, 15.0f),
          new HumidityAlarm(65.0f, 30.0f),
          3.0f,
          new LightAlarm(600.0f, 300.0f),
          new LedTimings(10, 5, 5));
  }
}
