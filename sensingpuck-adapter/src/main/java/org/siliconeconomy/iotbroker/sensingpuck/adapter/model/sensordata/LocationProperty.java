/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.ObservedProperty;

/**
 * The observed property describing the device's location.
 *
 * @author M. Grzenia
 */
public class LocationProperty
    extends ObservedProperty {

  private static final String NAME = "The device's location.";
  private static final String DESCRIPTION
      = "The device's location in latitude and longitude (GPS) coordinates.";

  public LocationProperty() {
    super(NAME, DESCRIPTION);
  }
}
