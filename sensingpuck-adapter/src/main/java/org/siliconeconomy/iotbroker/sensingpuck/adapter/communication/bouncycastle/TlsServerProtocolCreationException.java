/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsServerProtocol;

/**
 * An exception indicating that the creation of a {@link TlsServerProtocol} instance has failed.
 *
 * @author M. Grzenia
 */
public class TlsServerProtocolCreationException
    extends RuntimeException {

  public TlsServerProtocolCreationException(String message, Throwable cause) {
    super(message, cause);
  }
}
