/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.config;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClientBuilder;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching.*;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.*;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.SensingPuckAdapterAttributes;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.CompositeRequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v1.V1RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v2.V2RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3.V3RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.util.LoggingScheduledThreadPoolExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of various application beans.
 *
 * @author M. Grzenia
 */
@Configuration
public class ApplicationConfig {

  private final AdapterProperties adapterProperties;

  /**
   * Creates a new instance.
   *
   * @param adapterProperties The adapter's configuration properties.
   */
  public ApplicationConfig(AdapterProperties adapterProperties) {
    this.adapterProperties = requireNonNull(adapterProperties, "adapterProperties");
  }

  /**
   * Returns the {@link ScheduledExecutorService} to use for handling connections to and processing
   * messages from sensing puck client devices.
   *
   * @return A {@link ScheduledExecutorService}.
   */
  @Bean
  public ScheduledExecutorService communicationExecutorService() {
    return new LoggingScheduledThreadPoolExecutor(adapterProperties.getConnectionThreadPoolSize());
  }

  /**
   * Returns a map of {@link RequestResponseDispatcher} implementations mapped to their respective
   * {@link ProtocolVersion}.
   * <p>
   * This bean is used by {@link CompositeRequestResponseDispatcher}.
   *
   * @param v1RequestResponseDispatcher The {@link RequestResponseDispatcher} implementation for
   *                                    {@link ProtocolVersion#V1} requests and responses.
   * @param v2RequestResponseDispatcher The {@link RequestResponseDispatcher} implementation for
   *                                    {@link ProtocolVersion#V2} requests and responses.
   * @param v3RequestResponseDispatcher The {@link RequestResponseDispatcher} implementation for
   *                                    {@link ProtocolVersion#V3} requests and responses.
   * @return A map of {@link RequestResponseDispatcher} implementations mapped to their respective
   * {@link ProtocolVersion}.
   */
  @Bean
  public Map<ProtocolVersion, RequestResponseDispatcher> requestResponseDispatchers(
      V1RequestResponseDispatcher v1RequestResponseDispatcher,
      V2RequestResponseDispatcher v2RequestResponseDispatcher,
      V3RequestResponseDispatcher v3RequestResponseDispatcher) {
    Map<ProtocolVersion, RequestResponseDispatcher> requestResponseDispatchers
        = new EnumMap<>(ProtocolVersion.class);
    requestResponseDispatchers.put(ProtocolVersion.V1, v1RequestResponseDispatcher);
    requestResponseDispatchers.put(ProtocolVersion.V2, v2RequestResponseDispatcher);
    requestResponseDispatchers.put(ProtocolVersion.V3, v3RequestResponseDispatcher);
    return requestResponseDispatchers;
  }

  @Bean
  public DeviceRegistryServiceClient deviceRegistryServiceClient(DeviceRegistryCache cache) {
    var client = new DeviceRegistryServiceClientBuilder()
        .baseUrl(adapterProperties.getDeviceRegistryServiceApiUrl())
        .build();

    return new CachingDeviceRegistryServiceClient(client, cache);
  }

  @Bean
  public DeviceRegistryCache deviceRegistryCache(
      SensingPuckAdapterAttributes sensingPuckAdapterAttributes) {
    return new DeviceRegistryCache(
        new DeviceTypeFilter(sensingPuckAdapterAttributes.getAdapterDeviceSource(),
                             sensingPuckAdapterAttributes.getAdapterDeviceTypeIdentifier()),
        new DeviceInstanceFilter(sensingPuckAdapterAttributes.getAdapterDeviceSource(),
                                 sensingPuckAdapterAttributes.getAdapterDeviceTypeIdentifier())
    );
  }

  @Bean
  public DeviceRegistryCacheUpdater deviceRegistryCacheUpdater(DeviceRegistryCache cache) {
    return new DeviceRegistryCacheUpdater(cache);
  }

  @Bean
  public DeviceInstanceUpdateListener deviceInstanceUpdateListener(
      SensingPuckAdapterAttributes sensingPuckAdapterAttributes,
      DeviceRegistryCacheUpdater deviceRegistryCacheUpdater) {
    DeviceInstanceUpdateListener listener = new DefaultDeviceInstanceUpdateListener(
        new RelevantDeviceInstanceUpdateFilter(
            sensingPuckAdapterAttributes.getAdapterDeviceSource(),
            sensingPuckAdapterAttributes.getAdapterDeviceTypeIdentifier()
        )
    );

    // Ensure that update events are first handed to the cache updater. Otherwise, the adapter
    // may access the cache in an outdated state.
    listener.addDeviceInstanceUpdateHandler(deviceRegistryCacheUpdater);

    return listener;
  }

  @Bean
  public DeviceTypeUpdateListener deviceTypeUpdateListener(
      SensingPuckAdapterAttributes sensingPuckAdapterAttributes,
      DeviceRegistryCacheUpdater deviceRegistryCacheUpdater) {
    DeviceTypeUpdateListener listener = new DefaultDeviceTypeUpdateListener(
        new RelevantDeviceTypeUpdateFilter(
            sensingPuckAdapterAttributes.getAdapterDeviceSource(),
            sensingPuckAdapterAttributes.getAdapterDeviceTypeIdentifier()
        )
    );

    // Ensure that update events are first handed to the cache updater. Otherwise, the adapter
    // may access the cache in an outdated state.
    listener.addDeviceTypeUpdateHandler(deviceRegistryCacheUpdater);

    return listener;
  }


}
