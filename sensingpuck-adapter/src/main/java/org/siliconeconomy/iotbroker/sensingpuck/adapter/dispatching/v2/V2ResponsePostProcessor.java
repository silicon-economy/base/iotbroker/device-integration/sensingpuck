/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v2;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.ResponsePostProcessor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.UnsupportedResponseTypeException;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * The {@link ResponsePostProcessor} implementation for {@link ProtocolVersion#V2} responses.
 *
 * @author M. Grzenia
 */
@Component
public class V2ResponsePostProcessor
    implements ResponsePostProcessor {

  /**
   * The service to use for accessing the database with the device configurations.
   */
  private final JobService jobService;

  public V2ResponsePostProcessor(JobService jobService) {
    this.jobService = jobService;
  }

  @Override
  public void postProcessResponse(SensingPuckResponseBase response) {
    if (!(response instanceof V2SensingPuckResponse)) {
      throw new UnsupportedResponseTypeException(response);
    }

    if (response instanceof V2DefaultSensingPuckResponse) {
      // Don't post-process the default response.
      return;
    }

    V2SensingPuckResponse v2Response = (V2SensingPuckResponse) response;
    SensingPuckJobEntityBase baseEntity = jobService.findEntityByJobId(v2Response.getJobId());
    if (!(baseEntity instanceof V2SensingPuckJobEntity)) {
      throw new IllegalArgumentException(String.format("Unexpected entity type: %s",
                                                       baseEntity.getClass()));
    }

    // We have just sent a response to a sensing puck device. Update the corresponding database
    // entry's sentTimestamp and reset the clearAlarm flag if we just ordered the sensing puck
    // device to clear its alarm.
    V2SensingPuckJobEntity v2Entity = (V2SensingPuckJobEntity) baseEntity;
    v2Entity.setSentTimestamp(Instant.now());
    if (v2Response.isClearAlarm()) {
      v2Entity.setClearAlarm(false);
    }

    jobService.update(v2Entity);
  }
}
