/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request;

/**
 * Provides a method to format the firmware version (as sent by sensing puck devices) into a version
 * string.
 *
 * @author M. Grzenia
 */
public class FirmwareVersionFormatter {

  /**
   * Formats the given major, minor and patch version numbers into a version string.
   *
   * @param major The major version number.
   * @param minor The minor version number
   * @param patch The patch version number.
   * @return The formatted version string.
   */
  public String format(long major, long minor, long patch) {
    return String.format("%d.%d.%d", major, minor, patch);
  }
}
