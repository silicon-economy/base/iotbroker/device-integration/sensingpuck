/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.amqp.DeviceManagementExchangeConfiguration;

/**
 * Defines methods that a component interested in updates in the context of the registration of
 * {@link DeviceType}s and {@link DeviceInstance}s must implement. Usually, a {@link DeviceAdapter}
 * is such a component.
 * <p>
 * The callback methods defined by this interface (e.g.
 * {@link #onDeviceTypeModified(DeviceType, DeviceType)} or
 * {@link #onDeviceInstanceModified(DeviceInstance, DeviceInstance)}) are expected to be invoked
 * only on relevant updates. In the case of a {@link DeviceAdapter}, for example, an update is
 * considered relevant if it corresponds to the {@link DeviceType} for which the
 * {@link DeviceAdapter} provides an IoT Broker integration.
 *
 * @author M. Grzenia
 */
public interface DeviceRegistrationUpdateHandler {

  /**
   * Adds a listener for {@link DeviceTypeUpdate} messages.
   * <p>
   * Messages of this type are published via the IoT Broker's AMQP broker. For information on the
   * configuration of the corresponding AMQP exchange, refer to
   * {@link DeviceManagementExchangeConfiguration}.
   */
  void addDeviceTypeUpdateListener();

  /**
   * Removes the listener for {@link DeviceTypeUpdate} messages.
   */
  void removeDeviceTypeUpdateListener();

  /**
   * Adds a listener for {@link DeviceInstanceUpdate} messages.
   * <p>
   * Messages of this type are published via the IoT Broker's AMQP broker. For information on the
   * configuration of the corresponding AMQP exchange, refer to
   * {@link DeviceManagementExchangeConfiguration}.
   */
  void addDeviceInstanceUpdateListener();

  /**
   * Removes the listener for {@link DeviceInstanceUpdate} messages.
   */
  void removeDeviceInstanceUpdateListener();

  /**
   * Invoked when a {@link DeviceType} has been created.
   * <p>
   * Corresponds to {@link DeviceTypeUpdate.Type#CREATED}.
   *
   * @param newDeviceType The new {@link DeviceType} state.
   */
  void onDeviceTypeCreated(@NonNull DeviceType newDeviceType);

  /**
   * Invoked when a {@link DeviceType} has been modified.
   * <p>
   * Corresponds to {@link DeviceTypeUpdate.Type#MODIFIED}.
   *
   * @param oldDeviceType The old {@link DeviceType} state.
   * @param newDeviceType The new {@link DeviceType} state.
   */
  void onDeviceTypeModified(@NonNull DeviceType oldDeviceType, @NonNull DeviceType newDeviceType);

  /**
   * Invoked when a {@link DeviceType} has been deleted.
   * <p>
   * Corresponds to {@link DeviceTypeUpdate.Type#DELETED}.
   *
   * @param oldDeviceType The old {@link DeviceType} state.
   */
  void onDeviceTypeDeleted(@NonNull DeviceType oldDeviceType);

  /**
   * Invoked when a {@link DeviceInstance} has been created.
   * <p>
   * Corresponds to {@link DeviceInstanceUpdate.Type#CREATED}.
   *
   * @param newDeviceInstance The new {@link DeviceInstance} state.
   */
  void onDeviceInstanceCreated(@NonNull DeviceInstance newDeviceInstance);

  /**
   * Invoked when a {@link DeviceInstance} has been modified.
   * <p>
   * Corresponds to {@link DeviceInstanceUpdate.Type#MODIFIED}.
   *
   * @param oldDeviceInstance The old {@link DeviceInstance} state.
   * @param newDeviceInstance The new {@link DeviceInstance} state.
   */
  void onDeviceInstanceModified(@NonNull DeviceInstance oldDeviceInstance,
                                @NonNull DeviceInstance newDeviceInstance);

  /**
   * Invoked when a {@link DeviceInstance} has been deleted.
   * <p>
   * Corresponds to {@link DeviceInstanceUpdate.Type#DELETED}.
   *
   * @param oldDeviceInstance The old {@link DeviceInstance} state.
   */
  void onDeviceInstanceDeleted(@NonNull DeviceInstance oldDeviceInstance);
}
