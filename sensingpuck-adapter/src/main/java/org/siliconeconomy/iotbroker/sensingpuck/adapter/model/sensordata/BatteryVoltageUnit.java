/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.UnitOfMeasurement;

/**
 * The unit for the battery voltage measured by a sensing puck device.
 *
 * @author M. Grzenia
 */
public class BatteryVoltageUnit
    extends UnitOfMeasurement {

  private static final String NAME = "Voltage";
  private static final String SYMBOL = "V";

  public BatteryVoltageUnit() {
    super(NAME, SYMBOL);
  }
}
