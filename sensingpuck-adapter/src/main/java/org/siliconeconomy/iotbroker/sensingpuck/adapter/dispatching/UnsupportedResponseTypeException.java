/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;

/**
 * Exception indicating an unsupported response type.
 *
 * @author M. Grzenia
 */
public class UnsupportedResponseTypeException
    extends IllegalArgumentException {

  /**
   * Creates a new instance.
   *
   * @param response The response that caused the exception.
   */
  public UnsupportedResponseTypeException(SensingPuckResponseBase response) {
    super(String.format("Unsupported response type: %s", response.getClass()));
  }
}
