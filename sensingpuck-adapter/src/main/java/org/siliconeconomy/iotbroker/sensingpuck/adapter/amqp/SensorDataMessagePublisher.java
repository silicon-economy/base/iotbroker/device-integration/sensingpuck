/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.amqp.SensorDataExchangeConfiguration;
import org.siliconeconomy.iotbroker.jackson.ObjectMapperFactory;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * Handles publishing of client device messages (mapped to {@link SensorDataMessage}s) to the
 * IoT Broker's Sensor Data Exchange.
 *
 * @author M. Grzenia
 */
@Component
public class SensorDataMessagePublisher {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(SensorDataMessagePublisher.class);
  /**
   * For mapping {@link SensorDataMessage}s to JSON a separate object mapper with a different
   * configuration is needed.
   */
  private final ObjectMapper sensorDataMapper = ObjectMapperFactory.createObjectMapper();
  /**
   * The AMQP template to use for sending messages.
   */
  private final AmqpTemplate amqpTemplate;

  @Autowired
  public SensorDataMessagePublisher(AmqpTemplate amqpTemplate) {
    this.amqpTemplate = amqpTemplate;
  }

  /**
   * Publishes the given {@link SensorDataMessage} via AMQP to the Sensor Data Exchange.
   *
   * @param message The message to publish.
   */
  public void publishToSensorDataExchange(SensorDataMessage message) {
    try {
      amqpTemplate.send(SensorDataExchangeConfiguration.NAME,
                        SensorDataExchangeConfiguration.ROUTING_KEY,
                        toAmqpMessage(message));
    } catch (AmqpException e) {
      LOG.error("Error while publishing message via AMQP.", e);
    }
  }

  private Message toAmqpMessage(SensorDataMessage message) {
    try {
      return MessageBuilder
          .withBody(sensorDataMapper.writeValueAsString(message).getBytes(StandardCharsets.UTF_8))
          .setContentEncoding(StandardCharsets.UTF_8.toString())
          .setContentType(MessageProperties.CONTENT_TYPE_JSON)
          .build();
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Error while serializing data to JSON.", e);
    }
  }
}
