/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v1;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.DeviceMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.SensorDataMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.AbstractRequestHandler;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestHandler;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.sensordata.MessageEntityToSensorDataMapper;
import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.springframework.stereotype.Component;

/**
 * The {@link RequestHandler} implementation for {@link ProtocolVersion#V1} requests.
 *
 * @author M. Grzenia
 */
@Component
public class V1RequestHandler
    extends AbstractRequestHandler {

  public V1RequestHandler(MessageService sensingPuckMessageService,
                          DeviceMessagePublisher deviceMessagePublisher,
                          SensorDataMessagePublisher sensorDataMessagePublisher,
                          MessageEntityToSensorDataMapper messageEntityToSensorDataMapper) {
    super(sensingPuckMessageService,
          deviceMessagePublisher,
          sensorDataMessagePublisher,
          messageEntityToSensorDataMapper);
  }

  @Override
  protected Class<? extends SensingPuckRequestBase> supportedRequestType() {
    return V1SensingPuckRequest.class;
  }
}
