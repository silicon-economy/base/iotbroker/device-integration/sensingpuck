/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsServer;

/**
 * A factory for instances of {@link TlsServer}.
 *
 * @author M. Grzenia
 */
public interface TlsServerFactory {

  /**
   * Creates a {@link TlsServer} instance.
   *
   * @return A {@link TlsServer} instance.
   */
  TlsServer create();
}
