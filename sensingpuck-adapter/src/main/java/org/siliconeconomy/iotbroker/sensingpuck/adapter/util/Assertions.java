/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util;

/**
 * Provides methods for checking preconditions.
 *
 * @author M. Grzenia
 */
public class Assertions {

  private Assertions() {
  }

  /**
   * Checks whether the given expression is {@code true}.
   *
   * @param expression   The expression to check.
   * @param errorMessage The error message to use in case the given expression is {@code false}.
   * @throws IllegalArgumentException If the given expression is {@code false}.
   */
  public static void checkArgument(boolean expression, String errorMessage) {
    if (!expression) {
      throw new IllegalArgumentException(errorMessage);
    }
  }
}
