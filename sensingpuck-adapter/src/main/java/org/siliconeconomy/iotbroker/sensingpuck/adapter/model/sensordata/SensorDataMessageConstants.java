/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;

/**
 * Defines constants related to the {@link SensorDataMessage}s published by the sensing puck
 * adapter.
 *
 * @author M. Grzenia
 */
public class SensorDataMessageConstants {

  /**
   * The parameter key for observations describing the {@link MotionState}.
   */
  public static final String OBSERVATION_PARAM_KEY_MOTION_STATE = "motionState";

  private SensorDataMessageConstants() {
  }
}
