/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckMessageBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

/**
 * A composite of all available version-specific {@link RequestResponseDispatcher} implementations.
 * <p>
 * This class delegates requests and responses to the appropriate {@link RequestResponseDispatcher}
 * implementation, depending on the protocol version specified in the respective request or
 * response.
 *
 * @author M. Grzenia
 */
@Component
public class CompositeRequestResponseDispatcher
    implements RequestResponseDispatcher {

  /**
   * A map of all available version-specific {@link RequestResponseDispatcher} implementations
   * mapped to their respective {@link ProtocolVersion}.
   */
  private final Map<ProtocolVersion, RequestResponseDispatcher> requestResponseDispatchers;
  /**
   * Used for initial mapping of received messages to instances of {@link SensingPuckRequestBase}.
   */
  private final ObjectMapper objectMapper = new ObjectMapper();

  public CompositeRequestResponseDispatcher(
      Map<ProtocolVersion, RequestResponseDispatcher> requestResponseDispatchers) {
    this.requestResponseDispatchers = requestResponseDispatchers;
  }

  @Override
  public SensingPuckRequestBase tryParseAndValidateMessage(String message) {
    SensingPuckRequestBase request;
    try {
      request = objectMapper.readValue(message, SensingPuckRequestBase.class);
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Could not parse message.", e);
    }

    checkProtocolVersion(request);

    return requestResponseDispatchers
        .get(ProtocolVersion.fromVersionNumber(request.getProtocolVersion()))
        .tryParseAndValidateMessage(message);
  }

  @Override
  public void onRequest(SensingPuckRequestBase request) {
    checkProtocolVersion(request);

    requestResponseDispatchers
        .get(ProtocolVersion.fromVersionNumber(request.getProtocolVersion()))
        .onRequest(request);
  }

  @Override
  public SensingPuckResponseBase getResponseFor(SensingPuckRequestBase request) {
    checkProtocolVersion(request);

    return requestResponseDispatchers
        .get(ProtocolVersion.fromVersionNumber(request.getProtocolVersion()))
        .getResponseFor(request);
  }

  @Override
  public String serializeResponse(SensingPuckResponseBase response) {
    checkProtocolVersion(response);

    return requestResponseDispatchers
        .get(ProtocolVersion.fromVersionNumber(response.getProtocolVersion()))
        .serializeResponse(response);
  }

  @Override
  public void postProcessResponse(SensingPuckResponseBase response) {
    checkProtocolVersion(response);

    requestResponseDispatchers
        .get(ProtocolVersion.fromVersionNumber(response.getProtocolVersion()))
        .postProcessResponse(response);
  }

  /**
   * Checks whether the protocol version specified in the given {@link SensingPuckMessageBase} is
   * supported according to {@link #supportedProtocolVersions()}.
   *
   * @param message The message for which the protocol version is to be checked.
   * @throws IllegalArgumentException If the protocol version specified in the given
   *                                  {@link SensingPuckMessageBase} is <em>not</em> supported
   *                                  according to {@link #supportedProtocolVersions()}.
   */
  private void checkProtocolVersion(SensingPuckMessageBase message) {
    boolean versionSupported = supportedProtocolVersions().stream()
        .mapToInt(ProtocolVersion::getVersionNumber)
        .anyMatch(versionNumber -> versionNumber == message.getProtocolVersion());

    if (!versionSupported) {
      throw new IllegalArgumentException(
          String.format("Unsupported protocol version: %s (Supported protocol versions: %s)",
                        message.getProtocolVersion(),
                        supportedProtocolVersions())
      );
    }
  }

  /**
   * Returns the set of all supported {@link ProtocolVersion}s.
   *
   * @return The set of all supported {@link ProtocolVersion}s.
   */
  private Set<ProtocolVersion> supportedProtocolVersions() {
    return requestResponseDispatchers.keySet();
  }
}
