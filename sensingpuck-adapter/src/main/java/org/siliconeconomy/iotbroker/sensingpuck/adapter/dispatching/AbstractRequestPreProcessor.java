/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;

/**
 * An abstract implementation of {@link RequestPreProcessor} that implements basic behavior that is
 * the same across all protocol versions.
 * <p>
 * Where necessary, abstract (template) methods are provided to allow for protocol-specific
 * customization.
 *
 * @author M. Grzenia
 */
public abstract class AbstractRequestPreProcessor
    implements RequestPreProcessor {

  /**
   * Used to map received messages to instances of {@link SensingPuckRequestBase}.
   */
  private final ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public SensingPuckRequestBase tryParseAndValidateMessage(String message) {
    SensingPuckRequestBase result;
    try {
      result = objectMapper.readValue(message, supportedRequestType());
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException("Could not parse message.", e);
    }

    return result;
  }

  /**
   * Returns the type of {@link SensingPuckRequestBase} that this {@link RequestHandler} supports.
   * <p>
   * The returned type will be used to parse received messages.
   *
   * @return A class extending {@link SensingPuckRequestBase}.
   */
  protected abstract Class<? extends SensingPuckRequestBase> supportedRequestType();
}
