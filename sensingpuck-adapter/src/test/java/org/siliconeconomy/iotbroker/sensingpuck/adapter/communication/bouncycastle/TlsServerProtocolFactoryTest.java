/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsServerProtocol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link TlsServerProtocolFactory}.
 *
 * @author M. Grzenia
 */
class TlsServerProtocolFactoryTest {

  /**
   * Class under test.
   */
  private TlsServerProtocolFactory factory;

  @BeforeEach
  void setUp() {
    factory = new TlsServerProtocolFactory();
  }

  @Test
  void create_whenCreationSucceeds_thenReturnsTlsServerProtocolInstance()
      throws IOException {
    // Arrange
    Socket socket = configureSocketMock();

    // Act
    TlsServerProtocol result = factory.create(socket);

    // Assert
    assertThat(result).isNotNull();
  }

  @Test
  void create_whenCreationFails_thenThrowsException()
      throws IOException {
    // Arrange
    Socket socket = configureSocketMock();
    when(socket.getInputStream()).thenThrow(IOException.class);

    // Act & Assert
    assertThatThrownBy(() -> factory.create(socket))
        .isInstanceOf(TlsServerProtocolCreationException.class);
  }

  private Socket configureSocketMock()
      throws IOException {
    Socket socket = mock(Socket.class);
    when(socket.isConnected()).thenReturn(true);
    when(socket.isClosed()).thenReturn(false);
    when(socket.getInetAddress()).thenReturn(InetAddress.getLoopbackAddress());
    when(socket.getPort()).thenReturn(50000);
    InputStream in = new ByteArrayInputStream("dummy-string".getBytes(StandardCharsets.UTF_8));
    when(socket.getInputStream()).thenReturn(in);
    OutputStream out = mock(OutputStream.class);
    when(socket.getOutputStream()).thenReturn(out);
    return socket;
  }
}
