/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v2.V2ResponseProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.*;

/**
 * Test cases for {@link V3ResponseProvider}.
 *
 * @author M. Grzenia
 */
class V3ResponseProviderTest {

  /**
   * Class under test.
   */
  private V3ResponseProvider responseProvider;
  /**
   * Test dependencies.
   */
  private JobService service;

  @BeforeEach
  void setUp() {
    service = mock(JobService.class);
    responseProvider = new V3ResponseProvider(service);
  }

  @Test
  void getResponseFor_whenActiveV3JobAvailable_thenReturnsMostRecentActiveJob() {
    // Arrange
    V3SensingPuckJobEntity jobEntity = v3JobEntityWithDefaults();
    jobEntity.setJobState(JobState.ACTIVE);
    when(service.findMostRecentActiveJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v3RequestWithDefaults());

    // Assert
    V3SensingPuckResponse expectedResponse = v3ResponseWithDefaults();
    expectedResponse.setJobState(JobState.ACTIVE);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V3SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenOnlyFinishedV3JobsAvailable_thenReturnsMostRecentFinishedJob() {
    // Arrange
    V3SensingPuckJobEntity jobEntity = v3JobEntityWithDefaults();
    jobEntity.setJobState(JobState.FINISHED);
    when(service.findMostRecentFinishedJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v3RequestWithDefaults());

    // Assert
    V3SensingPuckResponse expectedResponse = v3ResponseWithDefaults();
    expectedResponse.setJobState(JobState.FINISHED);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V3SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenNoJobsAvailable_thenReturnsDefaultJob() {
    // Arrange
    V3SensingPuckRequest request = v3RequestWithDefaults();
    request.setDeviceId(4711L);

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(request);

    // Assert
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V3SensingPuckResponse.class))
        .extracting(V3SensingPuckResponse::getDeviceId)
        .isEqualTo(4711L);
  }

  @Test
  void getResponseFor_whenUnsupportedRequestType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new UnsupportedRequestType();

    // Act & Assert
    assertThatThrownBy(() -> responseProvider.getResponseFor(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void serializeResponse_whenValidV3Response_thenSerializesSuccessfully() {
    // Arrange
    SensingPuckResponseBase response = v3ResponseWithDefaults();

    // Act & Assert
    assertThatNoException().isThrownBy(() -> responseProvider.serializeResponse(response));
  }

  @Test
  void serializeResponse_whenUnsupportedResponseType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new UnsupportedResponseType();

    // Act & Assert
    assertThatThrownBy(() -> responseProvider.serializeResponse(response))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedRequestType
      extends SensingPuckRequestBase {

    public UnsupportedRequestType() {
      super(ProtocolVersion.V3.getVersionNumber(), 0);
    }
  }

  private static class UnsupportedResponseType
      extends SensingPuckResponseBase {

    public UnsupportedResponseType() {
      super(ProtocolVersion.V3.getVersionNumber(), 0);
    }
  }
}
