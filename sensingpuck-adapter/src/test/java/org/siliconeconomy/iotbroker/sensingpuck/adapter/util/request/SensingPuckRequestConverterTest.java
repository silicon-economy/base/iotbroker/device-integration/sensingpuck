/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.VersionNumberRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageDto;

import java.time.Instant;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases for {@link SensingPuckRequestConverter}.
 *
 * @author M. Grzenia
 */
class SensingPuckRequestConverterTest {

  /**
   * Class under test.
   */
  private SensingPuckRequestConverter converter;

  @BeforeEach
  void setUp() {
    converter = new SensingPuckRequestConverter();
  }

  @Test
  void convert_whenV1Request_thenReturnsV1MessageDto() {
    // Arrange
    VersionNumberRaw versionNumber = new VersionNumberRaw(1, 2, 3, 45, true);
    FirmwareDescriptorRaw firmwareDescriptor = new FirmwareDescriptorRaw(
        versionNumber,
        123456789,
        false
    );
    V1SensingPuckRequest request = new V1SensingPuckRequest(
        1,
        987654321L,
        111,
        firmwareDescriptor,
        1000000L,
        CommCause.DATA,
        Arrays.asList(
            new V1SensingDataRaw(10.0f, 44.0f, 900000L, MotionState.STATIC),
            new V1SensingDataRaw(9.0f, 45.0f, 800000L, MotionState.MOVING)
        )
    );
    Instant timeReceived = Instant.now();

    // Act
    SensingPuckMessageDtoBase messageDto = converter.convert(request, timeReceived);

    // Assert
    assertThat(messageDto).isInstanceOf(V1SensingPuckMessageDto.class);
    V1SensingPuckMessageDto v1MessageDto = (V1SensingPuckMessageDto) messageDto;
    SoftAssertions.assertSoftly(softly -> {
      assertThat(v1MessageDto.getProtocolVersion()).isEqualTo(request.getProtocolVersion());
      assertThat(v1MessageDto.getJobId()).isEqualTo(request.getJobId());
      assertThat(v1MessageDto.getDeviceId()).isEqualTo(request.getDeviceId());
      assertThat(v1MessageDto.getFirmwareDescriptor().getVersionNumber()).isEqualTo("1.2.3");
      assertThat(v1MessageDto.getFirmwareDescriptor().getCommitCounter())
          .isEqualTo(request.getFirmwareDescriptor().getVersionNumber().getCommitCounter());
      assertThat(v1MessageDto.getFirmwareDescriptor().isReleaseFlag())
          .isEqualTo(request.getFirmwareDescriptor().getVersionNumber().isReleaseFlag());
      assertThat(v1MessageDto.getFirmwareDescriptor().getCommitHash())
          .isEqualTo(request.getFirmwareDescriptor().getCommitHash());
      assertThat(v1MessageDto.getFirmwareDescriptor().isDirtyFlag())
          .isEqualTo(request.getFirmwareDescriptor().isDirtyFlag());
      assertThat(v1MessageDto.getComTimestamp()).isEqualTo(timeReceived);
      assertThat(v1MessageDto.getLastComCause()).isEqualTo(CommCause.DATA);
      assertThat(v1MessageDto.getData())
          .isNotEmpty()
          .hasSize(2)
          .contains(
              new V1SensingData(10.0f, 44.0f, timeReceived.minusMillis(100000), MotionState.STATIC),
              new V1SensingData(9.0f, 45.0f, timeReceived.minusMillis(200000), MotionState.MOVING)
          );
    });
  }

  @Test
  void convert_whenV2Request_thenReturnsV2MessageDto() {
    // Arrange
    VersionNumberRaw versionNumber = new VersionNumberRaw(4, 5, 6, 78, true);
    FirmwareDescriptorRaw firmwareDescriptor = new FirmwareDescriptorRaw(
        versionNumber,
        987654321,
        false
    );
    V2SensingPuckRequest request = new V2SensingPuckRequest(
        2,
        123456798L,
        222,
        firmwareDescriptor,
        1000000L,
        CommCause.DATA,
        Arrays.asList(
            new V2SensingDataRaw(20.0f, 54.0f, 2.8f, 900000L, MotionState.STATIC),
            new V2SensingDataRaw(19.0f, 55.0f, 2.5f, 800000L, MotionState.MOVING)
        )
    );
    Instant timeReceived = Instant.now();

    // Act
    SensingPuckMessageDtoBase messageDto = converter.convert(request, timeReceived);

    // Assert
    assertThat(messageDto).isInstanceOf(V2SensingPuckMessageDto.class);
    V2SensingPuckMessageDto v2MessageDto = (V2SensingPuckMessageDto) messageDto;
    SoftAssertions.assertSoftly(softly -> {
      assertThat(v2MessageDto.getProtocolVersion()).isEqualTo(request.getProtocolVersion());
      assertThat(v2MessageDto.getJobId()).isEqualTo(request.getJobId());
      assertThat(v2MessageDto.getDeviceId()).isEqualTo(request.getDeviceId());
      assertThat(v2MessageDto.getFirmwareDescriptor().getVersionNumber()).isEqualTo("4.5.6");
      assertThat(v2MessageDto.getFirmwareDescriptor().getCommitCounter())
          .isEqualTo(request.getFirmwareDescriptor().getVersionNumber().getCommitCounter());
      assertThat(v2MessageDto.getFirmwareDescriptor().isReleaseFlag())
          .isEqualTo(request.getFirmwareDescriptor().getVersionNumber().isReleaseFlag());
      assertThat(v2MessageDto.getFirmwareDescriptor().getCommitHash())
          .isEqualTo(request.getFirmwareDescriptor().getCommitHash());
      assertThat(v2MessageDto.getFirmwareDescriptor().isDirtyFlag())
          .isEqualTo(request.getFirmwareDescriptor().isDirtyFlag());
      assertThat(v2MessageDto.getComTimestamp()).isEqualTo(timeReceived);
      assertThat(v2MessageDto.getLastComCause()).isEqualTo(CommCause.DATA);
      assertThat(v2MessageDto.getData())
          .isNotEmpty()
          .hasSize(2)
          .contains(
              new V2SensingData(20.0f, 54.0f, 2.8f, timeReceived.minusMillis(100000), MotionState.STATIC),
              new V2SensingData(19.0f, 55.0f, 2.5f, timeReceived.minusMillis(200000), MotionState.MOVING)
          );
    });
  }

  @Test
  void convert_whenV3Request_thenReturnsV3MessageDto() {
    // Arrange
    VersionNumberRaw versionNumber = new VersionNumberRaw(4, 5, 6, 78, true);
    FirmwareDescriptorRaw firmwareDescriptor = new FirmwareDescriptorRaw(
        versionNumber,
        987654321,
        false
    );
    V3SensingPuckRequest request = new V3SensingPuckRequest(
        3,
        123456798L,
        222,
        firmwareDescriptor,
        1000000L,
        CommCause.DATA,
        Arrays.asList(
            new V3SensingDataRaw(20.0f, 54.0f, 2.8f, 476.0f, false, 51.4f, 7.4f, 900000L, MotionState.STATIC),
            new V3SensingDataRaw(19.0f, 55.0f, 2.5f, 489.0f, false, 51.4f, 7.4f, 800000L, MotionState.MOVING)
        )
    );
    Instant timeReceived = Instant.now();

    // Act
    SensingPuckMessageDtoBase messageDto = converter.convert(request, timeReceived);

    // Assert
    assertThat(messageDto).isInstanceOf(V3SensingPuckMessageDto.class);
    V3SensingPuckMessageDto v3MessageDto = (V3SensingPuckMessageDto) messageDto;
    SoftAssertions.assertSoftly(softly -> {
      assertThat(v3MessageDto.getProtocolVersion()).isEqualTo(request.getProtocolVersion());
      assertThat(v3MessageDto.getJobId()).isEqualTo(request.getJobId());
      assertThat(v3MessageDto.getDeviceId()).isEqualTo(request.getDeviceId());
      assertThat(v3MessageDto.getFirmwareDescriptor().getVersionNumber()).isEqualTo("4.5.6");
      assertThat(v3MessageDto.getFirmwareDescriptor().getCommitCounter())
          .isEqualTo(request.getFirmwareDescriptor().getVersionNumber().getCommitCounter());
      assertThat(v3MessageDto.getFirmwareDescriptor().isReleaseFlag())
          .isEqualTo(request.getFirmwareDescriptor().getVersionNumber().isReleaseFlag());
      assertThat(v3MessageDto.getFirmwareDescriptor().getCommitHash())
          .isEqualTo(request.getFirmwareDescriptor().getCommitHash());
      assertThat(v3MessageDto.getFirmwareDescriptor().isDirtyFlag())
          .isEqualTo(request.getFirmwareDescriptor().isDirtyFlag());
      assertThat(v3MessageDto.getComTimestamp()).isEqualTo(timeReceived);
      assertThat(v3MessageDto.getLastComCause()).isEqualTo(CommCause.DATA);
      assertThat(v3MessageDto.getData())
          .isNotEmpty()
          .hasSize(2)
          .contains(
              new V3SensingData(20.0f, 54.0f, 2.8f, 476.0f, false, 51.4f, 7.4f, timeReceived.minusMillis(100000), MotionState.STATIC),
              new V3SensingData(19.0f, 55.0f, 2.5f, 489.0f, false, 51.4f, 7.4f, timeReceived.minusMillis(200000), MotionState.MOVING)
          );
    });
  }

  @Test
  void convert_whenUnsupportedRequestType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new UnsupportedSensingPuckRequest();
    Instant timeReceived = Instant.now();

    // Act & Assert
    assertThatThrownBy(() -> converter.convert(request, timeReceived))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckRequest
      extends SensingPuckRequestBase {
  }
}
