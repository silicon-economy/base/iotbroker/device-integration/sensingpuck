/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases for {@link V3RequestPreProcessor}.
 *
 * @author M. Grzenia
 */
class V3RequestPreProcessorTest {

  /**
   * Class under test.
   */
  private V3RequestPreProcessor requestPreProcessor;

  @BeforeEach
  void setUp() {
    requestPreProcessor = new V3RequestPreProcessor();
  }

  @Test
  void tryParseAndValidateMessage_whenValidJsonString_thenReturnsRequest() {
    // Arrange
    String validJsonString = loadResourceAsString("/testdata/V3SensingPuckRequest.json");

    // Act
    SensingPuckRequestBase request = requestPreProcessor.tryParseAndValidateMessage(validJsonString);

    // Assert
    assertThat(request)
        .isInstanceOf(V3SensingPuckRequest.class)
        .isNotNull();
  }

  @Test
  void tryParseAndValidateMessage_whenInvalidJsonString_thenThrowsIllegalArgumentException() {
    // Arrange
    String invalidJsonString = "not-a-json-string";

    // Act & Assert
    assertThatThrownBy(() -> requestPreProcessor.tryParseAndValidateMessage(invalidJsonString))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void tryParseAndValidateMessage_whenInvalidProtocolVersion_thenThrowsIllegalArgumentException() {
    // Arrange
    String invalidProtocolVersionJsonString
        = loadResourceAsString("/testdata/V3SensingPuckRequest_invalidProtocolVersion.json");

    // Act & Assert
    assertThatThrownBy(() -> requestPreProcessor.tryParseAndValidateMessage(invalidProtocolVersionJsonString))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private String loadResourceAsString(String resourcePath) {
    try {
      return new String(this.getClass().getResourceAsStream(resourcePath).readAllBytes());
    } catch (IOException e) {
      throw new RuntimeException("Failed to load resource: " + resourcePath, e);
    }
  }
}
