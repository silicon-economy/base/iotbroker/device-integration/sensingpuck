/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request.DataTimestampConverter;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link DataTimestampConverter}.
 *
 * @author M. Grzenia
 */
class DataTimestampConverterTest {

  /**
   * Class under test.
   */
  private DataTimestampConverter converter;

  @BeforeEach
  void setUp() {
    converter = new DataTimestampConverter();
  }

  @Test
  void convert_dataTimestampInThePast() {
    // Arrange
    Instant referenceTime = Instant.now();
    // Act
    Instant result = converter.convert(9000, 10000, referenceTime);
    // Assert
    assertThat(result).isEqualTo(referenceTime.minusMillis(1000));
  }

  @Test
  void convert_dataTimestampInTheFuture() {
    // Arrange
    Instant referenceTime = Instant.now();
    // Act
    Instant result = converter.convert(11000, 10000, referenceTime);
    // Assert
    assertThat(result).isEqualTo(referenceTime.plusMillis(1000));
  }

  /**
   * The sensing puck uses unsigned data types for the timestamps.
   * Since we convert these timestamps to signed data types, we need to take care of negative numbers as well.
   */
  @Test
  void convert_negativeTimestamps() {
    // Arrange
    Instant referenceTime = Instant.now();
    // Act
    Instant result = converter.convert(Long.MIN_VALUE, Long.MIN_VALUE + 1000, referenceTime);
    // Assert
    assertThat(result).isEqualTo(referenceTime.minusMillis(1000));
  }

  @Test
  void convert_negativeTimestamps_Overflow() {
    // Arrange
    Instant referenceTime = Instant.now();
    // Act
    Instant result = converter.convert(Long.MAX_VALUE, Long.MIN_VALUE + 1000, referenceTime);
    // Assert
    assertThat(result).isEqualTo(referenceTime.minusMillis(1001));
  }
}
