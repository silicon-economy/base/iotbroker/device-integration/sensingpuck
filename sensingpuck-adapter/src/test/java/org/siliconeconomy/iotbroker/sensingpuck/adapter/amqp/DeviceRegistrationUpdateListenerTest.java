/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateListener;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateListener;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

import java.time.Instant;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Test cases for {@link DeviceRegistrationUpdateListener}.
 *
 * @author M. Grzenia
 */
class DeviceRegistrationUpdateListenerTest {

  /**
   * Class under test.
   */
  private DeviceRegistrationUpdateListener listener;
  /**
   * Test dependencies.
   */
  private DeviceTypeUpdateListener deviceTypeUpdateListener;

  private DeviceInstanceUpdateListener deviceInstanceUpdateListener;

  @BeforeEach
  void setUp() {
    deviceTypeUpdateListener = mock(DeviceTypeUpdateListener.class);
    deviceInstanceUpdateListener = mock(DeviceInstanceUpdateListener.class);

    listener = new DeviceRegistrationUpdateListener(deviceTypeUpdateListener,
                                                    deviceInstanceUpdateListener);
  }

  @Test
  void onDeviceInstanceUpdate() {
    // Arrange
    DeviceInstance newDeviceInstance = dummyDeviceInstance().withSource("not-the-sensing-puck");
    DeviceInstance oldDeviceInstance = null;
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         DeviceInstanceUpdate.Type.CREATED);

    // Act
    listener.onDeviceInstanceUpdate(deviceInstanceUpdate);

    // Assert
    verify(deviceInstanceUpdateListener).onDeviceInstanceUpdate(deviceInstanceUpdate);
  }

  @Test
  void onDeviceTypeUpdate() {
    DeviceType newDeviceType = dummyDeviceType().withSource("not-the-sensing-puck");
    DeviceType oldDeviceType = null;
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(newDeviceType,
                                                             oldDeviceType,
                                                             DeviceTypeUpdate.Type.CREATED);

    // Act
    listener.onDeviceTypeUpdate(deviceTypeUpdate);

    // Assert
    verify(deviceTypeUpdateListener).onDeviceTypeUpdate(deviceTypeUpdate);
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("", "", "", "", Instant.EPOCH, Instant.EPOCH, false, "", "", "");
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("", "", "", Set.of(), "", false, false, false);
  }
}
