/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.siliconeconomy.iotbroker.model.sensordata.Datastream;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.Observation;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.observation.MeasurementResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.SimpleResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.TruthResult;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;

import java.time.Instant;
import java.util.List;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.tuple;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.model.sensordata.SensorDataMessageConstants.OBSERVATION_PARAM_KEY_MOTION_STATE;

/**
 * Defines some assertions related to {@link Datastream}s.
 *
 * @author M. Grzenia
 */
public class DatastreamsAssert
    extends AbstractAssert<DatastreamsAssert, List<Datastream<? extends ObservationResult<?>>>> {

  private DatastreamsAssert(List<Datastream<? extends ObservationResult<?>>> datastreams) {
    super(datastreams, DatastreamsAssert.class);
  }

  public static DatastreamsAssert assertThat(
      List<Datastream<? extends ObservationResult<?>>> datastreams) {
    requireNonNull(datastreams, "datastreams");
    return new DatastreamsAssert(datastreams);
  }

  public DatastreamsAssert containsOneTemperatureDatastream() {
    isNotNull();
    Assertions.assertThat(actual)
        .filteredOn(this::isTemperatureDatastream)
        .hasSize(1);
    return this;
  }

  public DatastreamsAssert containsOneHumidityDatastream() {
    isNotNull();
    Assertions.assertThat(actual)
        .filteredOn(this::isHumidityDatastream)
        .hasSize(1);
    return this;
  }

  public DatastreamsAssert containsOneBatteryVoltageDatastream() {
    isNotNull();
    Assertions.assertThat(actual)
        .filteredOn(this::isBatteryVoltageDatastream)
        .hasSize(1);
    return this;
  }

  public DatastreamsAssert containsOneAmbientLightDatastream() {
    isNotNull();
    Assertions.assertThat(actual)
        .filteredOn(this::isAmbientLightDatastream)
        .hasSize(1);
    return this;
  }

  public DatastreamsAssert containsOneOpenedDatastream() {
    isNotNull();
    Assertions.assertThat(actual)
        .filteredOn(this::isOpenedDatastream)
        .hasSize(1);
    return this;
  }

  public DatastreamsAssert containsOneLocationDatastream() {
    isNotNull();
    Assertions.assertThat(actual)
        .filteredOn(this::isLocationDatastream)
        .hasSize(1);
    return this;
  }

  public DatastreamsAssert containsTemperatureObservationWith(
      Instant timestamp,
      Double measurementResult,
      LatitudeLongitudeDetails locationDetails,
      MotionState motionState) {
    isNotNull();
    containsObservationWith(this::isTemperatureDatastream,
                            timestamp,
                            measurementResult,
                            locationDetails,
                            motionState);
    return this;
  }

  public DatastreamsAssert containsHumidityObservationWith(Instant timestamp,
                                                           double measurementResult,
                                                           LatitudeLongitudeDetails locationDetails,
                                                           MotionState motionState) {
    isNotNull();
    containsObservationWith(this::isHumidityDatastream,
                            timestamp,
                            measurementResult,
                            locationDetails,
                            motionState);
    return this;
  }

  public DatastreamsAssert containsBatteryVoltageObservationWith(
      Instant timestamp,
      Double measurementResult,
      LatitudeLongitudeDetails locationDetails,
      MotionState motionState) {
    isNotNull();
    containsObservationWith(this::isBatteryVoltageDatastream,
                            timestamp,
                            measurementResult,
                            locationDetails,
                            motionState);
    return this;
  }

  public DatastreamsAssert containsAmbientLightObservationWith(
      Instant timestamp,
      Double measurementResult,
      LatitudeLongitudeDetails locationDetails,
      MotionState motionState) {
    isNotNull();
    containsObservationWith(this::isAmbientLightDatastream,
                            timestamp,
                            measurementResult,
                            locationDetails,
                            motionState);
    return this;
  }

  public DatastreamsAssert containsOpenedObservationWith(
      Instant timestamp,
      Boolean truthResult,
      LatitudeLongitudeDetails locationDetails,
      MotionState motionState) {
    isNotNull();
    containsObservationWith(this::isOpenedDatastream,
                            timestamp,
                            truthResult,
                            locationDetails,
                            motionState);
    return this;
  }

  public DatastreamsAssert containsLocationObservationWith(Instant timestamp,
                                                           String simpleResult,
                                                           LatitudeLongitudeDetails locationDetails,
                                                           MotionState motionState) {
    isNotNull();
    containsObservationWith(this::isLocationDatastream,
                            timestamp,
                            simpleResult,
                            locationDetails,
                            motionState);
    return this;
  }

  private boolean isTemperatureDatastream(Datastream<?> datastream) {
    return datastream.getObservedProperty() instanceof TemperatureProperty
        && datastream.getUnitOfMeasurement() instanceof TemperatureUnit
        && datastream.getObservationType().equals(MeasurementResult.class);
  }

  private boolean isHumidityDatastream(Datastream<?> datastream) {
    return datastream.getObservedProperty() instanceof HumidityProperty
        && datastream.getUnitOfMeasurement() instanceof HumidityUnit
        && datastream.getObservationType().equals(MeasurementResult.class);
  }

  private boolean isBatteryVoltageDatastream(Datastream<?> datastream) {
    return datastream.getObservedProperty() instanceof BatteryVoltageProperty
        && datastream.getUnitOfMeasurement() instanceof BatteryVoltageUnit
        && datastream.getObservationType().equals(MeasurementResult.class);
  }

  private boolean isAmbientLightDatastream(Datastream<?> datastream) {
    return datastream.getObservedProperty() instanceof AmbientLightProperty
        && datastream.getUnitOfMeasurement() instanceof AmbientLightUnit
        && datastream.getObservationType().equals(MeasurementResult.class);
  }

  private boolean isOpenedDatastream(Datastream<?> datastream) {
    return datastream.getObservedProperty() instanceof OpenedProperty
        && datastream.getUnitOfMeasurement() instanceof NoUnit
        && datastream.getObservationType().equals(TruthResult.class);
  }

  private boolean isLocationDatastream(Datastream<?> datastream) {
    return datastream.getObservedProperty() instanceof LocationProperty
        && datastream.getUnitOfMeasurement() instanceof NoUnit
        && datastream.getObservationType().equals(SimpleResult.class);
  }

  // Suppress "Unchecked generics array creation for varargs parameter"
  // For this test, the warning can be ignored.
  @SuppressWarnings("unchecked")
  private void containsObservationWith(
      Predicate<? super Datastream<? extends ObservationResult<?>>> datastreamPredicate,
      Instant timestamp,
      Object result,
      LocationDetails<?> locationDetails,
      MotionState motionState) {
    Assertions.assertThat(actual)
        .filteredOn(datastreamPredicate)
        .flatExtracting(Datastream::getObservations)
        .anySatisfy(observation -> {
          Assertions.assertThat(observation.getTimestamp()).isEqualTo(timestamp);
          Assertions.assertThat(observation.getResult().getResult()).isEqualTo(result);
          if (observation.getLocation() != null || locationDetails != null) {
            Assertions.assertThat(observation.getLocation().getDetails()).isEqualTo(locationDetails);
          }
          Assertions.assertThat(observation.getParameter(OBSERVATION_PARAM_KEY_MOTION_STATE))
              .isEqualTo(motionState.getStringRepresentation());
        });
  }
}
