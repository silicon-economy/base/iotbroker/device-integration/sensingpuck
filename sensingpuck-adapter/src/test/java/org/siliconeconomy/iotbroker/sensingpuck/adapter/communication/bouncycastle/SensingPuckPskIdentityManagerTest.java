/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases {@link SensingPuckPskIdentityManager}.
 *
 * @author M. Grzenia
 */
class SensingPuckPskIdentityManagerTest {

  /**
   * Class under test.
   */
  private SensingPuckPskIdentityManager pskIdentityManager;

  @BeforeEach
  void setUp() {
    AdapterProperties adapterProperties = new AdapterProperties(1,
                                                                "localhost",
                                                                5683,
                                                                "abc123",
                                                                "sensingpuck-device",
                                                                "sensingpuck-adapter",
                                                                0);
    pskIdentityManager = new SensingPuckPskIdentityManager(adapterProperties);
  }

  @Test
  void getHint() {
    // Act
    byte[] result = pskIdentityManager.getHint();

    // Assert
    byte[] expectedIdentityHint = "sensingpuck-adapter".getBytes(StandardCharsets.UTF_8);
    assertThat(result).isEqualTo(expectedIdentityHint);
  }

  @Test
  void getPSK_whenKnownIdentity_thenReturnsPsk() {
    // Arrange
    byte[] identity = "sensingpuck-device".getBytes(StandardCharsets.UTF_8);

    // Act
    byte[] result = pskIdentityManager.getPSK(identity);

    // Assert
    byte[] expectedPsk = "abc123".getBytes(StandardCharsets.UTF_8);
    assertThat(result).isEqualTo(expectedPsk);
  }

  @Test
  void getPSK_whenUnknownIdentity_thenThrowsException() {
    // Arrange
    byte[] identity = "unknown-identity".getBytes(StandardCharsets.UTF_8);

    // Act & Assert
    assertThatThrownBy(() -> pskIdentityManager.getPSK(identity))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
