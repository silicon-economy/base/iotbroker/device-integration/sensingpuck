/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckJobEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Test cases for {@link JobEntityToSensingPuckResponseMapper}.
 *
 * @author M. Grzenia
 */
class JobEntityToSensingPuckResponseMapperTest {

  /**
   * Class under test.
   */
  private JobEntityToSensingPuckResponseMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new JobEntityToSensingPuckResponseMapper();
  }

  @Test
  void map_whenV1JobEntity_thenReturnsV1Response() {
    // Arrange
    V1SensingPuckJobEntity jobEntity = new V1SensingPuckJobEntity();
    jobEntity.setJobId(0);
    jobEntity.setSource("some-source");
    jobEntity.setTenant("1");
    jobEntity.setCreatedTimestamp(Instant.now());
    jobEntity.setSentTimestamp(null);
    jobEntity.setProtocolVersion(1);
    jobEntity.setJobState(JobState.ACTIVE);
    jobEntity.setDeviceId(1);
    jobEntity.setClearAlarm(false);
    jobEntity.setMeasurementCycles(new MeasurementCycles(3, 5));
    jobEntity.setDataAmount(8);
    jobEntity.setTemperatureAlarm(new TemperatureAlarm(13.0f, 21.0f));
    jobEntity.setHumidityAlarm(new HumidityAlarm(34.0f, 55.0f));
    jobEntity.setLedTimings(new LedTimings(89, 144, 233));

    // Act
    SensingPuckResponseBase response = mapper.map(jobEntity);

    // Assert
    assertThat(response).isInstanceOf(V1SensingPuckResponse.class);
    V1SensingPuckResponse v1Response = (V1SensingPuckResponse) response;
    assertSoftly(softAssertions -> {
      assertThat(v1Response.getProtocolVersion()).isEqualTo(1);
      assertThat(v1Response.getJobId()).isZero();
      assertThat(v1Response.getJobState()).isEqualTo(JobState.ACTIVE);
      assertThat(v1Response.getDeviceId()).isEqualTo(1);
      assertThat(v1Response.isClearAlarm()).isFalse();
      assertThat(v1Response.getMeasurementCycles().getStorage()).isEqualTo(3);
      assertThat(v1Response.getMeasurementCycles().getTransport()).isEqualTo(5);
      assertThat(v1Response.getDataAmount()).isEqualTo(8);
      assertThat(v1Response.getTemperatureAlarm().getUpperBound()).isEqualTo(13.0f);
      assertThat(v1Response.getTemperatureAlarm().getLowerBound()).isEqualTo(21.0f);
      assertThat(v1Response.getHumidityAlarm().getUpperBound()).isEqualTo(34.0f);
      assertThat(v1Response.getHumidityAlarm().getLowerBound()).isEqualTo(55.0f);
      assertThat(v1Response.getLedTimings().getShowId()).isEqualTo(89);
      assertThat(v1Response.getLedTimings().getShowAlarm()).isEqualTo(144);
      assertThat(v1Response.getLedTimings().getShowTemp()).isEqualTo(233);
    });
  }

  @Test
  void map_whenV2JobEntity_thenReturnsV2Response() {
    // Arrange
    V2SensingPuckJobEntity jobEntity = new V2SensingPuckJobEntity();
    jobEntity.setJobId(6);
    jobEntity.setSource("some-source");
    jobEntity.setTenant("5");
    jobEntity.setCreatedTimestamp(Instant.now());
    jobEntity.setSentTimestamp(null);
    jobEntity.setProtocolVersion(2);
    jobEntity.setJobState(JobState.FINISHED);
    jobEntity.setDeviceId(9);
    jobEntity.setClearAlarm(true);
    jobEntity.setMeasurementCycles(new MeasurementCycles(7, 2));
    jobEntity.setDataAmount(4);
    jobEntity.setTemperatureAlarm(new TemperatureAlarm(1.0f, 16.3f));
    jobEntity.setHumidityAlarm(new HumidityAlarm(15.7f, 5.0f));
    jobEntity.setVoltageAlarm(2.3f);
    jobEntity.setLedTimings(new LedTimings(5, 24, 73));

    // Act
    SensingPuckResponseBase response = mapper.map(jobEntity);

    // Assert
    assertThat(response).isInstanceOf(V2SensingPuckResponse.class);
    V2SensingPuckResponse v2Response = (V2SensingPuckResponse) response;
    assertSoftly(softAssertions -> {
      assertThat(v2Response.getProtocolVersion()).isEqualTo(2);
      assertThat(v2Response.getJobId()).isEqualTo(6);
      assertThat(v2Response.getJobState()).isEqualTo(JobState.FINISHED);
      assertThat(v2Response.getDeviceId()).isEqualTo(9);
      assertThat(v2Response.isClearAlarm()).isTrue();
      assertThat(v2Response.getMeasurementCycles().getStorage()).isEqualTo(7);
      assertThat(v2Response.getMeasurementCycles().getTransport()).isEqualTo(2);
      assertThat(v2Response.getDataAmount()).isEqualTo(4);
      assertThat(v2Response.getTemperatureAlarm().getUpperBound()).isEqualTo(1.0f);
      assertThat(v2Response.getTemperatureAlarm().getLowerBound()).isEqualTo(16.3f);
      assertThat(v2Response.getHumidityAlarm().getUpperBound()).isEqualTo(15.7f);
      assertThat(v2Response.getHumidityAlarm().getLowerBound()).isEqualTo(5.0f);
      assertThat(v2Response.getVoltageAlarm()).isEqualTo(2.3f);
      assertThat(v2Response.getLedTimings().getShowId()).isEqualTo(5);
      assertThat(v2Response.getLedTimings().getShowAlarm()).isEqualTo(24);
      assertThat(v2Response.getLedTimings().getShowTemp()).isEqualTo(73);
    });
  }

  @Test
  void map_whenV3JobEntity_thenReturnsV3Response() {
    // Arrange
    V3SensingPuckJobEntity jobEntity = new V3SensingPuckJobEntity();
    jobEntity.setJobId(6);
    jobEntity.setSource("some-source");
    jobEntity.setTenant("5");
    jobEntity.setCreatedTimestamp(Instant.now());
    jobEntity.setSentTimestamp(null);
    jobEntity.setProtocolVersion(3);
    jobEntity.setJobState(JobState.FINISHED);
    jobEntity.setDeviceId(9);
    jobEntity.setClearAlarm(true);
    jobEntity.setMeasurementCycles(new MeasurementCycles(7, 2));
    jobEntity.setDataAmount(4);
    jobEntity.setPackagingTime(1000);
    jobEntity.setTemperatureAlarm(new TemperatureAlarm(1.0f, 16.3f));
    jobEntity.setHumidityAlarm(new HumidityAlarm(15.7f, 5.0f));
    jobEntity.setVoltageAlarm(2.3f);
    jobEntity.setLightAlarm(new LightAlarm(600.0f, 300.0f));
    jobEntity.setLedTimings(new LedTimings(5, 24, 73));

    // Act
    SensingPuckResponseBase response = mapper.map(jobEntity);

    // Assert
    assertThat(response).isInstanceOf(V3SensingPuckResponse.class);
    V3SensingPuckResponse v3Response = (V3SensingPuckResponse) response;
    assertSoftly(softAssertions -> {
      assertThat(v3Response.getProtocolVersion()).isEqualTo(3);
      assertThat(v3Response.getJobId()).isEqualTo(6);
      assertThat(v3Response.getJobState()).isEqualTo(JobState.FINISHED);
      assertThat(v3Response.getDeviceId()).isEqualTo(9);
      assertThat(v3Response.isClearAlarm()).isTrue();
      assertThat(v3Response.getMeasurementCycles().getStorage()).isEqualTo(7);
      assertThat(v3Response.getMeasurementCycles().getTransport()).isEqualTo(2);
      assertThat(v3Response.getDataAmount()).isEqualTo(4);
      assertThat(v3Response.getPackagingTime()).isEqualTo(1000);
      assertThat(v3Response.getTemperatureAlarm().getUpperBound()).isEqualTo(1.0f);
      assertThat(v3Response.getTemperatureAlarm().getLowerBound()).isEqualTo(16.3f);
      assertThat(v3Response.getHumidityAlarm().getUpperBound()).isEqualTo(15.7f);
      assertThat(v3Response.getHumidityAlarm().getLowerBound()).isEqualTo(5.0f);
      assertThat(v3Response.getVoltageAlarm()).isEqualTo(2.3f);
      assertThat(v3Response.getLightAlarm().getUpperBound()).isEqualTo(600.0f);
      assertThat(v3Response.getLightAlarm().getLowerBound()).isEqualTo(300.0f);
      assertThat(v3Response.getLedTimings().getShowId()).isEqualTo(5);
      assertThat(v3Response.getLedTimings().getShowAlarm()).isEqualTo(24);
      assertThat(v3Response.getLedTimings().getShowTemp()).isEqualTo(73);
    });
  }

  @Test
  void convert_whenUnsupportedJobEntityType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckJobEntityBase jobEntity = new UnsupportedSensingPuckJobEntity();

    // Act & Assert
    assertThatThrownBy(() -> mapper.map(jobEntity))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckJobEntity
      extends SensingPuckJobEntityBase {
  }
}
