/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link FirmwareVersionFormatter}.
 *
 * @author M. Grzenia
 */
class FirmwareVersionFormatterTest {

  /**
   * Class under test.
   */
  private FirmwareVersionFormatter formatter;

  @BeforeEach
  void setUp() {
    formatter = new FirmwareVersionFormatter();
  }

  @ParameterizedTest
  @MethodSource("versionNumbers")
  void format(long major, long minor, long patch) {
    // Act
    String formattedFirmwareVersion = formatter.format(major, minor, patch);

    // Assert
    assertThat(formattedFirmwareVersion).isEqualTo(major + "." + minor + "." + patch);
  }

  private static Stream<Arguments> versionNumbers() {
    return Stream.of(
        Arguments.arguments(1L, 2L, 3L),
        Arguments.arguments(3L, 2L, 1L),
        Arguments.arguments(7L, 11L, 13L)
    );
  }
}
