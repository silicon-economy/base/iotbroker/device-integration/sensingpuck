/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v2JobEntityWithDefaults;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v2ResponseWithDefaults;

/**
 * Test cases for {@link V2ResponsePostProcessor}.
 *
 * @author M. Grzenia
 */
class V2ResponsePostProcessorTest {

  /**
   * Class under test.
   */
  private V2ResponsePostProcessor responsePostProcessor;
  /**
   * Test dependencies.
   */
  private JobService jobService;

  @BeforeEach
  void setUp() {
    jobService = mock(JobService.class);
    responsePostProcessor = new V2ResponsePostProcessor(jobService);
  }

  @Test
  void postProcessResponse_whenValidResponse_thenUpdatesJobInDatabase() {
    // Arrange
    V2SensingPuckResponse response = v2ResponseWithDefaults();
    response.setJobId(4711);
    response.setClearAlarm(true);
    V2SensingPuckJobEntity jobEntity = v2JobEntityWithDefaults();
    jobEntity.setJobId(4711);
    jobEntity.setClearAlarm(true);
    when(jobService.findEntityByJobId(response.getJobId())).thenReturn(jobEntity);

    // Act
    responsePostProcessor.postProcessResponse(response);

    // Assert
    ArgumentCaptor<V2SensingPuckJobEntity> jobEntityCaptor
        = ArgumentCaptor.forClass(V2SensingPuckJobEntity.class);
    verify(jobService).update(jobEntityCaptor.capture());
    assertThat(jobEntityCaptor.getValue().isClearAlarm()).isFalse();
  }

  @Test
  void postProcessResponse_whenDefaultSensingPuckResponse_thenDoesNothing() {
    // Arrange
    V2DefaultSensingPuckResponse response = new V2DefaultSensingPuckResponse(1);

    // Act
    responsePostProcessor.postProcessResponse(response);

    // Verify
    verifyNoInteractions(jobService);
  }

  @Test
  void postProcessResponse_whenUnsupportedResponseType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new UnsupportedResponseType();

    // Act & Assert
    assertThatThrownBy(() -> responsePostProcessor.postProcessResponse(response))
        .isInstanceOf(IllegalArgumentException.class);

    // Verify
    verifyNoInteractions(jobService);
  }

  private static class UnsupportedResponseType
      extends SensingPuckResponseBase {

    public UnsupportedResponseType() {
      super(ProtocolVersion.V2.getVersionNumber(), 0);
    }
  }
}
