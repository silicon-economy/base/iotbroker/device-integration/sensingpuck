/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.sensordata;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.sensordata.Datastream;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.LatLong;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DatastreamsAssert;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.utils.SensingPuckMessageUtils;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.*;

/**
 * Test cases for {@link MessageEntityToSensorDataMapper}.
 *
 * @author M. Grzenia
 */
class MessageEntityToSensorDataMapperTest {

  /**
   * Class under test.
   */
  private MessageEntityToSensorDataMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new MessageEntityToSensorDataMapper(
        new SensingPuckMessageUtils(TestConstants.DEVICE_INTEGRATION_PROPERTIES),
        TestConstants.DEVICE_INTEGRATION_PROPERTIES
    );
  }

  @Test
  void map_whenV1MessageEntity_thenReturnsSensorDataMessage() {
    // Arrange
    Instant time = Instant.now();
    V1SensingPuckMessageEntity messageEntity = v1MessageEntityWithDefaults();
    messageEntity.setId("sensingpuck-1:6474e121-e44a-4425-900b-f5a107835cc9");
    messageEntity.setDeviceId(3);
    messageEntity.setComTimestamp(time);
    messageEntity.setData(List.of(
        new V1SensingData(10.0f, 20.0f, time, MotionState.STATIC),
        new V1SensingData(15.0f, 28.0f, time, MotionState.MOVING)
    ));

    // Act
    SensorDataMessage result = mapper.map(messageEntity);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      assertThat(result.getId()).isNotEmpty();
      assertThatNoException().isThrownBy(() -> UUID.fromString(result.getId()));
      assertThat(result.getOriginId()).isEqualTo("6474e121-e44a-4425-900b-f5a107835cc9");
      assertThat(result.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      assertThat(result.getTenant()).isEqualTo("3");
      assertThat(result.getTimestamp()).isEqualTo(time);
      assertThat(result.getLocation()).isNull();
      assertThat(result.getDatastreams())
          .hasSize(2)
          .flatExtracting(Datastream::getObservations)
          .hasSize(4);
      DatastreamsAssert.assertThat(result.getDatastreams())
          .containsOneTemperatureDatastream()
          .containsOneHumidityDatastream()
          .containsTemperatureObservationWith(time, 10.0, null, MotionState.STATIC)
          .containsHumidityObservationWith(time, 20.0, null, MotionState.STATIC)
          .containsTemperatureObservationWith(time, 15.0, null, MotionState.MOVING)
          .containsHumidityObservationWith(time, 28.0, null, MotionState.MOVING);
    });
  }

  @Test
  void map_whenV2MessageEntity_thenReturnsSensorDataMessage() {
    // Arrange
    Instant time = Instant.now();
    V2SensingPuckMessageEntity messageEntity = v2MessageEntityWithDefaults();
    messageEntity.setId("sensingpuck-1:d9891a86-ee0f-4e68-9c9d-6fa8ba81901a");
    messageEntity.setDeviceId(4);
    messageEntity.setComTimestamp(time);
    messageEntity.setData(List.of(
        new V2SensingData(20.0f, 30.0f, 2.8f, time, MotionState.STATIC),
        new V2SensingData(25.0f, 38.0f, 1.3f, time, MotionState.MOVING)
    ));

    // Act
    SensorDataMessage result = mapper.map(messageEntity);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      assertThat(result.getId()).isNotEmpty();
      assertThatNoException().isThrownBy(() -> UUID.fromString(result.getId()));
      assertThat(result.getOriginId()).isEqualTo("d9891a86-ee0f-4e68-9c9d-6fa8ba81901a");
      assertThat(result.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      assertThat(result.getTenant()).isEqualTo("4");
      assertThat(result.getTimestamp()).isEqualTo(time);
      assertThat(result.getLocation()).isNull();
      assertThat(result.getDatastreams())
          .hasSize(3)
          .flatExtracting(Datastream::getObservations)
          .hasSize(6);
      DatastreamsAssert.assertThat(result.getDatastreams())
          .containsOneTemperatureDatastream()
          .containsOneHumidityDatastream()
          .containsOneBatteryVoltageDatastream()
          .containsTemperatureObservationWith(time, 20.0, null, MotionState.STATIC)
          .containsHumidityObservationWith(time, 30.0, null, MotionState.STATIC)
          .containsBatteryVoltageObservationWith(time, 2.8, null, MotionState.STATIC)
          .containsTemperatureObservationWith(time, 25.0, null, MotionState.MOVING)
          .containsHumidityObservationWith(time, 38.0, null, MotionState.MOVING)
          .containsBatteryVoltageObservationWith(time, 1.3, null, MotionState.MOVING);
    });
  }

  @Test
  void map_whenV3MessageEntity_thenReturnsSensorDataMessage() {
    // Arrange
    Instant time = Instant.now();
    V3SensingPuckMessageEntity messageEntity = v3MessageEntityWithDefaults();
    messageEntity.setId("sensingpuck-1:d9891a86-ee0f-4e68-9c9d-6fa8ba81901a");
    messageEntity.setDeviceId(4);
    messageEntity.setComTimestamp(time);
    messageEntity.setData(List.of(
        new V3SensingData(30.0f, 40.0f, 3.8f, 476.0f, false, 51.4f, 7.4f, time, MotionState.STATIC),
        new V3SensingData(23.0f, 48.0f, 2.3f, 489.0f, true, 51.4f, 7.4f, time, MotionState.MOVING)
    ));

    // Act
    SensorDataMessage result = mapper.map(messageEntity);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      assertThat(result.getId()).isNotEmpty();
      assertThatNoException().isThrownBy(() -> UUID.fromString(result.getId()));
      assertThat(result.getOriginId()).isEqualTo("d9891a86-ee0f-4e68-9c9d-6fa8ba81901a");
      assertThat(result.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      assertThat(result.getTenant()).isEqualTo("4");
      assertThat(result.getTimestamp()).isEqualTo(time);
      assertThat(result.getLocation()).isNull();
      assertThat(result.getDatastreams())
          .hasSize(6)
          .flatExtracting(Datastream::getObservations)
          .hasSize(12);
      LatitudeLongitudeDetails longitudeDetails = new LatitudeLongitudeDetails(new LatLong(51.4, 7.4));
      DatastreamsAssert.assertThat(result.getDatastreams())
          .containsOneTemperatureDatastream()
          .containsOneHumidityDatastream()
          .containsOneBatteryVoltageDatastream()
          .containsOneAmbientLightDatastream()
          .containsOneOpenedDatastream()
          .containsOneLocationDatastream()
          .containsTemperatureObservationWith(time, 30.0, longitudeDetails, MotionState.STATIC)
          .containsHumidityObservationWith(time, 40.0, longitudeDetails, MotionState.STATIC)
          .containsBatteryVoltageObservationWith(time, 3.8, longitudeDetails, MotionState.STATIC)
          .containsAmbientLightObservationWith(time, 476.0, longitudeDetails, MotionState.STATIC)
          .containsOpenedObservationWith(time, false, longitudeDetails, MotionState.STATIC)
          .containsLocationObservationWith(time,
                                           "The result is this observation's location.",
                                           longitudeDetails,
                                           MotionState.STATIC)
          .containsTemperatureObservationWith(time, 23.0, longitudeDetails, MotionState.MOVING)
          .containsHumidityObservationWith(time, 48.0, longitudeDetails, MotionState.MOVING)
          .containsBatteryVoltageObservationWith(time, 2.3, longitudeDetails, MotionState.MOVING)
          .containsAmbientLightObservationWith(time, 489.0, longitudeDetails, MotionState.MOVING)
          .containsOpenedObservationWith(time, true, longitudeDetails, MotionState.MOVING)
          .containsLocationObservationWith(time,
                                           "The result is this observation's location.",
                                           longitudeDetails,
                                           MotionState.MOVING);
    });
  }

  @Test
  void map_whenUnsupportedMessageEntityType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckMessageEntityBase messageEntity = new UnsupportedSensingPuckMessageEntity();

    // Act & Assert
    assertThatThrownBy(() -> mapper.map(messageEntity))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckMessageEntity
      extends SensingPuckMessageEntityBase {
  }
}
