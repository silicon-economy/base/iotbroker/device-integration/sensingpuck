/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;

/**
 * @author M. Grzenia
 */
class DeviceTypeConverterTest {

  /**
   * Class under test.
   */
  private DeviceTypeConverter converter;

  @BeforeEach
  void setUp() {
    converter = new DeviceTypeConverter();
  }

  @Test
  void toDeviceTypeCreateInputTO() {
    // Arrange
    DeviceType deviceType = new DeviceType("some-id",
                                           "some-source",
                                           "some-identifier",
                                           Set.of("some-adapter-identifier"),
                                           "some-description",
                                           true,
                                           true,
                                           true);

    // Act
    DeviceTypeCreateInputTO result = converter.toDeviceTypeCreateInputTO(deviceType);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(result.getSource()).isEqualTo("some-source");
      softly.assertThat(result.getIdentifier()).isEqualTo("some-identifier");
      softly.assertThat(result.getProvidedBy())
          .hasSize(1)
          .containsExactly("some-adapter-identifier");
      softly.assertThat(result.getDescription()).isEqualTo("some-description");
      softly.assertThat(result.getEnabled()).isEqualTo(true);
      softly.assertThat(result.getAutoRegisterDeviceInstances()).isEqualTo(true);
      softly.assertThat(result.getAutoEnableDeviceInstances()).isEqualTo(true);
    });
  }

  @Test
  void toDeviceTypeUpdateInput() {
    // Arrange
    DeviceType deviceType = new DeviceType("some-id",
                                           "some-source",
                                           "some-identifier",
                                           Set.of("some-adapter-identifier"),
                                           "some-description",
                                           true,
                                           true,
                                           true);

    // Act
    DeviceTypeUpdateInputTO result = converter.toDeviceTypeUpdateInput(deviceType);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      softly.assertThat(result.getProvidedBy())
          .hasSize(1)
          .containsExactly("some-adapter-identifier");
      softly.assertThat(result.getDescription()).isEqualTo("some-description");
      softly.assertThat(result.getEnabled()).isEqualTo(true);
      softly.assertThat(result.getAutoRegisterDeviceInstances()).isEqualTo(true);
      softly.assertThat(result.getAutoEnableDeviceInstances()).isEqualTo(true);
    });
  }
}
