/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.amqp.DeviceMessageExchangeConfiguration;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link DeviceMessagePublisher}.
 *
 * @author M. Grzenia
 */
class DeviceMessagePublisherTest {

  /**
   * Class under test.
   */
  private DeviceMessagePublisher deviceMessagePublisher;
  /**
   * Test dependencies.
   */
  private DeviceTypeProvider deviceTypeProvider;
  private AmqpTemplate amqpTemplate;

  @BeforeEach
  void setUp() {
    deviceTypeProvider = mock(DeviceTypeProvider.class);
    amqpTemplate = mock(AmqpTemplate.class);
    deviceMessagePublisher = new DeviceMessagePublisher(deviceTypeProvider,
                                                        TestConstants.DEVICE_INTEGRATION_PROPERTIES,
                                                        amqpTemplate);
  }

  @Test
  void publishToDeviceMessageExchange_shouldPublishToCorrectExchangeAndWithCorrectRoutingKey() {
    // Arrange
    SensingPuckMessageDtoBase message = TestDataFactory.v1MessageDtoWithDefaults();
    when(deviceTypeProvider.getAdapterDeviceTypeIdentifier())
        .thenReturn(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);

    // Act
    deviceMessagePublisher.publishToDeviceMessageExchange("1", message);

    // Assert & Verify
    ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
    verify(amqpTemplate).send(
        eq(DeviceMessageExchangeConfiguration.NAME),
        eq("sensingpuck-vX.sensingpuck.1"),
        messageCaptor.capture()
    );
    assertThat(messageCaptor.getValue().getMessageProperties())
        .extracting(MessageProperties::getContentEncoding, MessageProperties::getContentType)
        .contains(StandardCharsets.UTF_8.toString(), MessageProperties.CONTENT_TYPE_JSON);
  }
}
