/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle;

import org.bouncycastle.tls.TlsServer;
import org.bouncycastle.tls.TlsServerProtocol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link TlsServerProtocol}.
 *
 * @author M. Grzenia
 */
class TlsServerProtocolProviderTest {

  /**
   * Class under test.
   */
  private TlsServerProtocolProvider tlsServerProtocolProvider;
  /**
   * Test dependencies.
   */
  private TlsServerFactory tlsServerFactory;
  private TlsServerProtocolFactory tlsServerProtocolFactory;

  @BeforeEach
  void setUp() {
    tlsServerFactory = mock(TlsServerFactory.class);
    tlsServerProtocolFactory = mock(TlsServerProtocolFactory.class);
    tlsServerProtocolProvider = new TlsServerProtocolProvider(tlsServerFactory,
                                                              tlsServerProtocolFactory);
  }

  @Test
  void getAndAccept_whenTlsHandshakeSucceeds_thenReturnsTlsServerProtocolInstance()
      throws IOException {
    // Arrange
    Socket socket = configureSocketMock();
    TlsServer tlsServer = mock(TlsServer.class);
    when(tlsServerFactory.create()).thenReturn(tlsServer);
    TlsServerProtocol tlsServerProtocol = mock(TlsServerProtocol.class);
    when(tlsServerProtocolFactory.create(socket)).thenReturn(tlsServerProtocol);

    // Act
    TlsServerProtocol result = tlsServerProtocolProvider.getAndAccept(socket);

    // Assert & Verify
    assertThat(result)
        .isNotNull()
        .isEqualTo(tlsServerProtocol);
    verify(tlsServerProtocol).accept(tlsServer);
  }

  @Test
  void getAndAccept_whenTlsHandshakeFails_thenThrowsException()
      throws IOException {
    // Arrange
    Socket socket = configureSocketMock();
    TlsServer tlsServer = mock(TlsServer.class);
    when(tlsServerFactory.create()).thenReturn(tlsServer);
    TlsServerProtocol tlsServerProtocol = mock(TlsServerProtocol.class);
    doThrow(IOException.class).when(tlsServerProtocol).accept(tlsServer);
    when(tlsServerProtocolFactory.create(socket)).thenReturn(tlsServerProtocol);

    // Act & Assert
    assertThatThrownBy(() -> tlsServerProtocolProvider.getAndAccept(socket))
        .isInstanceOf(TlsHandshakeException.class);
  }

  private Socket configureSocketMock()
      throws IOException {
    Socket socket = mock(Socket.class);
    when(socket.isConnected()).thenReturn(true);
    when(socket.isClosed()).thenReturn(false);
    when(socket.getInetAddress()).thenReturn(InetAddress.getLoopbackAddress());
    when(socket.getPort()).thenReturn(50000);
    InputStream in = new ByteArrayInputStream("dummy-string".getBytes(StandardCharsets.UTF_8));
    when(socket.getInputStream()).thenReturn(in);
    OutputStream out = mock(OutputStream.class);
    when(socket.getOutputStream()).thenReturn(out);
    return socket;
  }
}
