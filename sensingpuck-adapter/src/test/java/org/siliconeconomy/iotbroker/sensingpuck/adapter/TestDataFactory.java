/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.VersionNumberRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.response.V3SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.job.*;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.FirmwareDescriptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.job.V3SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v3.message.V3SensingPuckMessageEntity;

import java.time.Instant;
import java.util.List;

/**
 * A factory that provides convenient methods for creating test data/objects.
 *
 * @author M. Grzenia
 */
public class TestDataFactory {

  public static V1SensingPuckRequest v1RequestWithDefaults() {
    FirmwareDescriptorRaw firmwareDescriptor
        = new FirmwareDescriptorRaw(new VersionNumberRaw(1, 2, 3, 4, false), 123, false);
    return new V1SensingPuckRequest(1,
                                    1,
                                    1,
                                    firmwareDescriptor,
                                    Instant.now().toEpochMilli(),
                                    CommCause.DATA,
                                    List.of());
  }

  public static V1SensingPuckResponse v1ResponseWithDefaults() {
    return new V1SensingPuckResponse(
        1,
        1,
        JobState.FINISHED,
        1,
        false,
        new MeasurementCycles(5, 5),
        10,
        new TemperatureAlarm(10.0f, 10.0f),
        new HumidityAlarm(20.0f, 20.0f),
        new LedTimings(5, 5, 5)
    );
  }

  public static V1SensingPuckMessageDto v1MessageDtoWithDefaults() {
    return new V1SensingPuckMessageDto(
        1,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        List.of()
    );
  }

  public static V1SensingPuckMessageEntity v1MessageEntityWithDefaults() {
    return new V1SensingPuckMessageEntity(
        "sensingpuck-1:36a83ab4-cf8f-416a-b226-801af1080913",
        "some-revision",
        1,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        List.of()
    );
  }

  public static V1SensingPuckJobEntity v1JobEntityWithDefaults() {
    return new V1SensingPuckJobEntity(
        "1",
        "some-revision",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        null,
        1,
        JobState.FINISHED,
        1,
        false,
        new MeasurementCycles(5, 5),
        10,
        new TemperatureAlarm(10.0f, 10.0f),
        new HumidityAlarm(20.0f, 20.0f),
        new LedTimings(5, 5, 5)
    );
  }

  public static V2SensingPuckRequest v2RequestWithDefaults() {
    FirmwareDescriptorRaw firmwareDescriptor
        = new FirmwareDescriptorRaw(new VersionNumberRaw(1, 2, 3, 4, false), 123, false);
    return new V2SensingPuckRequest(2,
                                    1,
                                    1,
                                    firmwareDescriptor,
                                    Instant.now().toEpochMilli(),
                                    CommCause.DATA,
                                    List.of());
  }

  public static V2SensingPuckResponse v2ResponseWithDefaults() {
    return new V2SensingPuckResponse(
        2,
        1,
        JobState.FINISHED,
        1,
        false,
        new MeasurementCycles(5, 5),
        10,
        new TemperatureAlarm(10.0f, 10.0f),
        new HumidityAlarm(20.0f, 20.0f),
        2.7f,
        new LedTimings(5, 5, 5)
    );
  }

  public static V2SensingPuckMessageEntity v2MessageEntityWithDefaults() {
    return new V2SensingPuckMessageEntity(
        "sensingpuck-1:33965119-c2cc-4ccd-a00f-e30f44cbab76",
        "some-revision",
        2,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        List.of()
    );
  }

  public static V2SensingPuckJobEntity v2JobEntityWithDefaults() {
    return new V2SensingPuckJobEntity(
        "1",
        "some-revision",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        null,
        2,
        JobState.FINISHED,
        1,
        false,
        new MeasurementCycles(5, 5),
        10,
        new TemperatureAlarm(10.0f, 10.0f),
        new HumidityAlarm(20.0f, 20.0f),
        2.7f,
        new LedTimings(5, 5, 5)
    );
  }

  public static V3SensingPuckRequest v3RequestWithDefaults() {
    FirmwareDescriptorRaw firmwareDescriptor
        = new FirmwareDescriptorRaw(new VersionNumberRaw(1, 2, 3, 4, false), 123, false);
    return new V3SensingPuckRequest(3,
                                    1,
                                    1,
                                    firmwareDescriptor,
                                    Instant.now().toEpochMilli(),
                                    CommCause.DATA,
                                    List.of());
  }

  public static V3SensingPuckResponse v3ResponseWithDefaults() {
    return new V3SensingPuckResponse(
        3,
        1,
        JobState.FINISHED,
        1,
        false,
        new MeasurementCycles(5, 5),
        10,
        1000,
        new TemperatureAlarm(10.0f, 10.0f),
        new HumidityAlarm(20.0f, 20.0f),
        2.7f,
        new LightAlarm(600.0f, 300.0f),
        new LedTimings(5, 5, 5)
    );
  }

  public static V3SensingPuckMessageEntity v3MessageEntityWithDefaults() {
    return new V3SensingPuckMessageEntity(
        "sensingpuck-1:33965119-c2cc-4ccd-a00f-e30f44cbab76",
        "some-revision",
        3,
        1,
        1,
        new FirmwareDescriptor(
            "1.2.3",
            4,
            false,
            123,
            false
        ),
        Instant.EPOCH,
        CommCause.DATA,
        List.of()
    );
  }

  public static V3SensingPuckJobEntity v3JobEntityWithDefaults() {
    return new V3SensingPuckJobEntity(
        "1",
        "some-revision",
        TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
        "1",
        Instant.EPOCH,
        null,
        3,
        JobState.FINISHED,
        1,
        false,
        new MeasurementCycles(5, 5),
        10,
        1000,
        new TemperatureAlarm(10.0f, 10.0f),
        new HumidityAlarm(20.0f, 20.0f),
        2.7f,
        new LightAlarm(600.0f, 300.0f),
        new LedTimings(5, 5, 5)
    );
  }
}
