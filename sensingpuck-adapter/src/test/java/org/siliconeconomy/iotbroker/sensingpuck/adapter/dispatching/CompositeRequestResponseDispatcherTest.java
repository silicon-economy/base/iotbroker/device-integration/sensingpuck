/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link CompositeRequestResponseDispatcher}.
 *
 * @author M. Grzenia
 */
class CompositeRequestResponseDispatcherTest {

  /**
   * Class under test.
   */
  private CompositeRequestResponseDispatcher requestResponseDispatcher;
  /**
   * Test dependencies.
   */
  private RequestResponseDispatcher v1RequestResponseDispatcher;
  private RequestResponseDispatcher v2RequestResponseDispatcher;
  /**
   * Test utilities.
   */
  private final ObjectMapper objectMapper = new ObjectMapper();

  @BeforeEach
  void setUp() {
    v1RequestResponseDispatcher = mock(RequestResponseDispatcher.class);
    v2RequestResponseDispatcher = mock(RequestResponseDispatcher.class);
    Map<ProtocolVersion, RequestResponseDispatcher> requestResponseDispatchers = Map.of(
        ProtocolVersion.V1, v1RequestResponseDispatcher,
        ProtocolVersion.V2, v2RequestResponseDispatcher
    );
    requestResponseDispatcher = new CompositeRequestResponseDispatcher(requestResponseDispatchers);
  }

  @Test
  void tryParseAndValidateMessage_whenV1Request_thenDelegatesToV1Dispatcher()
      throws JsonProcessingException {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(ProtocolVersion.V1.getVersionNumber(),
                                                                99);
    String requestJsonString = objectMapper.writeValueAsString(request);

    // Act
    requestResponseDispatcher.tryParseAndValidateMessage(requestJsonString);

    // Verify
    verify(v1RequestResponseDispatcher).tryParseAndValidateMessage(requestJsonString);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void tryParseAndValidateMessage_whenV2Request_thenDelegatesToV2Dispatcher()
      throws JsonProcessingException {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(ProtocolVersion.V2.getVersionNumber(),
                                                                99);
    String requestJsonString = objectMapper.writeValueAsString(request);

    // Act
    requestResponseDispatcher.tryParseAndValidateMessage(requestJsonString);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verify(v2RequestResponseDispatcher).tryParseAndValidateMessage(requestJsonString);
  }

  @Test
  void tryParseAndValidateMessage_whenInvalidMessage_thenThrowsIllegalArgumentException()
      throws JsonProcessingException {
    // Arrange
    String requestJsonString = "not-a-json-string";

    // Act & Assert
    assertThatThrownBy(() -> requestResponseDispatcher.tryParseAndValidateMessage(requestJsonString))
        .isInstanceOf(IllegalArgumentException.class);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void tryParseAndValidateMessage_whenUnsupportedProtocolVersion_thenThrowsIllegalArgumentException()
      throws JsonProcessingException {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(9999, 99);
    String requestJsonString = objectMapper.writeValueAsString(request);

    // Act & Assert
    assertThatThrownBy(() -> requestResponseDispatcher.tryParseAndValidateMessage(requestJsonString))
        .isInstanceOf(IllegalArgumentException.class);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void onRequest_whenV1Request_thenDelegatesToV1Dispatcher() {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(ProtocolVersion.V1.getVersionNumber(),
                                                                99);

    // Act
    requestResponseDispatcher.onRequest(request);

    // Verify
    verify(v1RequestResponseDispatcher).onRequest(request);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void onRequest_whenV2Request_thenDelegatesToV2Dispatcher() {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(ProtocolVersion.V2.getVersionNumber(),
                                                                99);

    // Act
    requestResponseDispatcher.onRequest(request);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verify(v2RequestResponseDispatcher).onRequest(request);
  }


  @Test
  void onRequest_whenUnsupportedProtocolVersion_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(9999, 99);

    // Act & Assert
    assertThatThrownBy(() -> requestResponseDispatcher.onRequest(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void getResponseFor_whenV1Request_thenDelegatesToV1Dispatcher() {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(ProtocolVersion.V1.getVersionNumber(),
                                                                99);

    // Act
    requestResponseDispatcher.getResponseFor(request);

    // Verify
    verify(v1RequestResponseDispatcher).getResponseFor(request);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void getResponseFor_whenV2Request_thenDelegatesToV2Dispatcher() {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(ProtocolVersion.V2.getVersionNumber(),
                                                                99);

    // Act
    requestResponseDispatcher.getResponseFor(request);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verify(v2RequestResponseDispatcher).getResponseFor(request);
  }

  @Test
  void getResponseFor_whenUnsupportedProtocolVersion_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new SensingPuckRequestBase(9999, 99);

    // Act & Assert
    assertThatThrownBy(() -> requestResponseDispatcher.getResponseFor(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void serializeResponse_whenV1Request_thenDelegatesToV1Dispatcher() {
    // Arrange
    SensingPuckResponseBase response = new SensingPuckResponseBase(ProtocolVersion.V1.getVersionNumber(),
                                                                   99);

    // Act
    requestResponseDispatcher.serializeResponse(response);

    // Verify
    verify(v1RequestResponseDispatcher).serializeResponse(response);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void serializeResponse_whenV2Request_thenDelegatesToV2Dispatcher() {
    // Arrange
    SensingPuckResponseBase response = new SensingPuckResponseBase(ProtocolVersion.V2.getVersionNumber(),
                                                                   99);

    // Act
    requestResponseDispatcher.serializeResponse(response);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verify(v2RequestResponseDispatcher).serializeResponse(response);
  }

  @Test
  void serializeResponse_whenUnsupportedProtocolVersion_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new SensingPuckResponseBase(9999, 99);

    // Act & Assert
    assertThatThrownBy(() -> requestResponseDispatcher.serializeResponse(response))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void postProcessResponse_whenV1Request_thenDelegatesToV1Dispatcher() {
    // Arrange
    SensingPuckResponseBase response = new SensingPuckResponseBase(ProtocolVersion.V1.getVersionNumber(),
                                                                   99);

    // Act
    requestResponseDispatcher.postProcessResponse(response);

    // Verify
    verify(v1RequestResponseDispatcher).postProcessResponse(response);
    verifyNoInteractions(v2RequestResponseDispatcher);
  }

  @Test
  void postProcessResponse_whenV2Request_thenDelegatesToV2Dispatcher() {
    // Arrange
    SensingPuckResponseBase response = new SensingPuckResponseBase(ProtocolVersion.V2.getVersionNumber(),
                                                                   99);

    // Act
    requestResponseDispatcher.postProcessResponse(response);

    // Verify
    verifyNoInteractions(v1RequestResponseDispatcher);
    verify(v2RequestResponseDispatcher).postProcessResponse(response);
  }

  @Test
  void postProcessResponse_whenUnsupportedProtocolVersion_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new SensingPuckResponseBase(9999, 99);

    // Act & Assert
    assertThatThrownBy(() -> requestResponseDispatcher.postProcessResponse(response))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
