/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v2;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.response.V2SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.job.V2SensingPuckJobEntity;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.*;

/**
 * Test cases for {@link V2ResponseProvider}.
 *
 * @author M. Grzenia
 */
class V2ResponseProviderTest {

  /**
   * Class under test.
   */
  private V2ResponseProvider responseProvider;
  /**
   * Test dependencies.
   */
  private JobService service;

  @BeforeEach
  void setUp() {
    service = mock(JobService.class);
    responseProvider = new V2ResponseProvider(service);
  }

  @Test
  void getResponseFor_whenActiveV2JobAvailable_thenReturnsMostRecentActiveJob() {
    // Arrange
    V2SensingPuckJobEntity jobEntity = v2JobEntityWithDefaults();
    jobEntity.setJobState(JobState.ACTIVE);
    when(service.findMostRecentActiveJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v2RequestWithDefaults());

    // Assert
    V2SensingPuckResponse expectedResponse = v2ResponseWithDefaults();
    expectedResponse.setJobState(JobState.ACTIVE);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V2SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenActiveV1JobAvailable_thenReturnsMostRecentActiveJob() {
    // Arrange
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();
    jobEntity.setJobState(JobState.ACTIVE);
    when(service.findMostRecentActiveJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v2RequestWithDefaults());

    // Assert
    V1SensingPuckResponse expectedResponse = v1ResponseWithDefaults();
    expectedResponse.setJobState(JobState.ACTIVE);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V1SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenOnlyFinishedV2JobsAvailable_thenReturnsMostRecentFinishedJob() {
    // Arrange
    V2SensingPuckJobEntity jobEntity = v2JobEntityWithDefaults();
    jobEntity.setJobState(JobState.FINISHED);
    when(service.findMostRecentFinishedJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v2RequestWithDefaults());

    // Assert
    V2SensingPuckResponse expectedResponse = v2ResponseWithDefaults();
    expectedResponse.setJobState(JobState.FINISHED);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V2SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenOnlyFinishedV1JobsAvailable_thenReturnsMostRecentFinishedJob() {
    // Arrange
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();
    jobEntity.setJobState(JobState.FINISHED);
    when(service.findMostRecentFinishedJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v2RequestWithDefaults());

    // Assert
    V1SensingPuckResponse expectedResponse = v1ResponseWithDefaults();
    expectedResponse.setJobState(JobState.FINISHED);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V1SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenNoJobsAvailable_thenReturnsDefaultJob() {
    // Arrange
    V2SensingPuckRequest request = v2RequestWithDefaults();
    request.setDeviceId(4711L);

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(request);

    // Assert
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V2SensingPuckResponse.class))
        .extracting(V2SensingPuckResponse::getDeviceId)
        .isEqualTo(4711L);
  }

  @Test
  void getResponseFor_whenUnsupportedRequestType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new UnsupportedRequestType();

    // Act & Assert
    assertThatThrownBy(() -> responseProvider.getResponseFor(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void serializeResponse_whenValidV2Response_thenSerializesSuccessfully() {
    // Arrange
    SensingPuckResponseBase response = v2ResponseWithDefaults();

    // Act & Assert
    assertThatNoException().isThrownBy(() -> responseProvider.serializeResponse(response));
  }

  @Test
  void serializeResponse_whenValidV1Response_thenSerializesSuccessfully() {
    // Arrange
    SensingPuckResponseBase response = v1ResponseWithDefaults();

    // Act & Assert
    assertThatNoException().isThrownBy(() -> responseProvider.serializeResponse(response));
  }

  @Test
  void serializeResponse_whenUnsupportedResponseType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new UnsupportedResponseType();

    // Act & Assert
    assertThatThrownBy(() -> responseProvider.serializeResponse(response))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedRequestType
      extends SensingPuckRequestBase {

    public UnsupportedRequestType() {
      super(ProtocolVersion.V2.getVersionNumber(), 0);
    }
  }

  private static class UnsupportedResponseType
      extends SensingPuckResponseBase {

    public UnsupportedResponseType() {
      super(ProtocolVersion.V2.getVersionNumber(), 0);
    }
  }
}
