/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bouncycastle.tls.BasicTlsPSKIdentity;
import org.bouncycastle.tls.CipherSuite;
import org.bouncycastle.tls.PSKTlsClient;
import org.bouncycastle.tls.TlsClientProtocol;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;
import org.bouncycastle.util.Strings;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.VersionNumberRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingDataRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;
import org.siliconeconomy.iotbroker.util.LoggingScheduledThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A test for emulating sensing puck devices.
 * <p>
 * This test creates a configurable amount of emulated sensing puck client instances. Each client
 * instance then tries to connect to an adapter via TCP. When a connection is established each
 * client sends a message to the adapter and closes the connection afterwards.
 *
 * @author M. Grzenia
 */
class TestClient {

  /**
   * This class's logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(TestClient.class);
  /**
   * Provides the configuration options to use for running the {@link TestClient}.
   * <p>
   * Adjust these as needed. Hint:
   * <ul>
   *   <li>Use e.g. {@code IntStream.range(1, 11)} to emulate 10 devices with the IDs 1 to 10.</li>
   *   <li>Use e.g. {@code IntStream.of(3, 8, 14)} to emulate 3 device with the IDs 3, 8 and 14.</li>
   * </ul>
   */
  private static final ClientConfiguration CONFIGURATION
      = new ClientConfiguration(ProtocolVersion.V3,
                                IntStream.of(1),
                                "localhost",
                                5683,
                                "sensingpuck-device");
  /**
   * Maps {@link ProtocolVersion}s to functions to be used for creating
   * {@link SensingPuckRequestBase} instances.
   */
  private static final Map<ProtocolVersion, Function<Integer, SensingPuckRequestBase>> REQUEST_FACTORIES
      = Map.of(ProtocolVersion.V1, TestClient::createV1DummyRequest,
               ProtocolVersion.V2, TestClient::createV2DummyRequest,
               ProtocolVersion.V3, TestClient::createV3DummyRequest);
  /**
   * Used to map instances of {@link SensingPuckRequestBase} to JSON.
   */
  private static final ObjectMapper MAPPER = new ObjectMapper();
  /**
   * The executor service to use for managing the test client threads.
   */
  private final ExecutorService executor = new LoggingScheduledThreadPoolExecutor(1);

  @Test
  @Disabled("Available adapter instance required. Use for manual tests only.")
  void startTestClients()
      throws InterruptedException {
    CONFIGURATION.getClientIds().forEach(clientId -> executor.submit(new Client(clientId)));

    executor.shutdown();
    executor.awaitTermination(10, TimeUnit.SECONDS);

    assertThat(executor.isShutdown()).isTrue();
  }

  private static class Client
      implements Runnable {

    /**
     * The ID of the client.
     */
    private final int clientId;

    public Client(int clientId) {
      this.clientId = clientId;
    }

    @Override
    public void run() {
      try (Socket clientSocket = new Socket(CONFIGURATION.getAdapterHost(),
                                            CONFIGURATION.getAdapterPort())) {
        TlsClientProtocol protocol = new TlsClientProtocol(clientSocket.getInputStream(),
                                                           clientSocket.getOutputStream());
        protocol.connect(new SensingPuckPskTlsClient());
        LOG.info("Client {}: Connected to server via {}:{}",
                 clientId,
                 clientSocket.getInetAddress().getHostAddress(),
                 clientSocket.getLocalPort());

        BufferedReader in = new BufferedReader(new InputStreamReader(protocol.getInputStream()));
        PrintWriter out = new PrintWriter(protocol.getOutputStream(), true);

        SensingPuckRequestBase request = REQUEST_FACTORIES.get(CONFIGURATION.getProtocolVersion())
            .apply(clientId);
        LOG.info("Client {}: Sending request: {}", clientId, MAPPER.writeValueAsString(request));
        out.print(MAPPER.writeValueAsString(request) + "\n");
        out.flush();

        LOG.info("Client {}: Waiting for response from IoT broker...", clientId);
        StringBuilder sb = new StringBuilder();
        String inputLine;
        // Read the input stream until it gets closed.
        while ((inputLine = in.readLine()) != null) {
          sb.append(inputLine);
        }

        LOG.info("Client {}: Received response: {}", clientId, sb);
        LOG.info("Client {}: Closing connection...", clientId);
        protocol.close();
        in.close();
        out.close();
      } catch (IOException e) {
        LOG.warn("Client {}: Connection error.", clientId, e);
      }
    }

    private static class SensingPuckPskTlsClient
        extends PSKTlsClient {

      private static final int[] SENSINGPUCK_CIPHER_SUITES = new int[]{
          CipherSuite.TLS_PSK_WITH_AES_128_CCM_8
      };

      public SensingPuckPskTlsClient() {
        super(new BcTlsCrypto(new SecureRandom()),
              new BasicTlsPSKIdentity(CONFIGURATION.getPskIdentity(),
                                      Strings.toUTF8ByteArray("")));
      }

      @Override
      protected int[] getSupportedCipherSuites() {
        return SENSINGPUCK_CIPHER_SUITES;
      }
    }
  }

  private static V1SensingPuckRequest createV1DummyRequest(int clientId) {
    VersionNumberRaw versionNumber = new VersionNumberRaw(1,
                                                          2,
                                                          3,
                                                          4,
                                                          false);
    FirmwareDescriptorRaw firmwareDescriptor = new FirmwareDescriptorRaw(versionNumber,
                                                                         123,
                                                                         false);
    return new V1SensingPuckRequest(
        1,
        123L,
        clientId,
        firmwareDescriptor,
        10000L,
        CommCause.DATA,
        List.of(new V1SensingDataRaw(10.0f, 40.0f, 9000L, MotionState.STATIC))
    );
  }

  private static V2SensingPuckRequest createV2DummyRequest(int clientId) {
    VersionNumberRaw versionNumber = new VersionNumberRaw(1,
                                                          2,
                                                          3,
                                                          4,
                                                          false);
    FirmwareDescriptorRaw firmwareDescriptor = new FirmwareDescriptorRaw(versionNumber,
                                                                         123,
                                                                         false);
    return new V2SensingPuckRequest(
        2,
        123L,
        clientId,
        firmwareDescriptor,
        10000L,
        CommCause.DATA,
        List.of(new V2SensingDataRaw(10.0f, 40.0f, 2.3f, 9000L, MotionState.STATIC))
    );
  }

  private static V3SensingPuckRequest createV3DummyRequest(int clientId) {
    VersionNumberRaw versionNumber = new VersionNumberRaw(1,
                                                          2,
                                                          3,
                                                          4,
                                                          false);
    FirmwareDescriptorRaw firmwareDescriptor = new FirmwareDescriptorRaw(versionNumber,
                                                                         123,
                                                                         false);
    return new V3SensingPuckRequest(
        3,
        123L,
        clientId,
        firmwareDescriptor,
        10000L,
        CommCause.DATA,
        List.of(
            new V3SensingDataRaw(10.0f,
                                 40.0f,
                                 2.3f,
                                 500.0f,
                                 false,
                                 51.493770315432144f,
                                 7.407194955748563f,
                                 9000L,
                                 MotionState.STATIC)
        )
    );
  }

  /**
   * Provides the configuration options to use for running the {@link TestClient}.
   */
  @AllArgsConstructor
  @Getter
  private static class ClientConfiguration {

    /**
     * The protocol version the client should use.
     */
    private final ProtocolVersion protocolVersion;
    /**
     * The IDs of the clients to emulate.
     */
    private final IntStream clientIds;
    /**
     * The hostname of the adapter to connect to.
     */
    private final String adapterHost;
    /**
     * The port of the adapter to connect to.
     */
    private final int adapterPort;
    /**
     * The PSK identity the client should use.
     */
    private final String pskIdentity;
  }
}
