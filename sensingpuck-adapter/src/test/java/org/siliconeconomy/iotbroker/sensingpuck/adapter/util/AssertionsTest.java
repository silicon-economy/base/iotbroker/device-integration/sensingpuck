/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Test cases for {@link Assertions}.
 *
 * @author M. Grzenia
 */
class AssertionsTest {

  @Test
  void checkArgument_whenExpressionTrue_thenDoesNotThrowException() {
    assertDoesNotThrow(() -> Assertions.checkArgument(true, ""));
  }

  @Test
  void checkArgument_whenExpressionFalse_thenThrowsException() {
    assertThatThrownBy(() -> Assertions.checkArgument(false,
                                                      "my error message"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("my error message");
  }
}
