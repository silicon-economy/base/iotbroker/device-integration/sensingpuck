/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateListener;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateListener;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.TcpServerSocketManager;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.TcpServerSocketManagerFactory;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device.DeviceInstanceCreateInputProvider;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link SensingPuckAdapter}.
 *
 * @author M. Grzenia
 */
class SensingPuckAdapterTest {

  /**
   * Class under test.
   */
  private SensingPuckAdapter adapter;
  /**
   * Test dependencies.
   */
  private DeviceRegistryServiceClient deviceRegistryServiceClient;
  private DeviceInstanceCreateInputProvider deviceInstanceCreateInputProvider;
  private TcpServerSocketManagerFactory tcpServerSocketManagerFactory;

  @BeforeEach
  void setUp() {
    deviceRegistryServiceClient = mock(DeviceRegistryServiceClient.class);
    deviceInstanceCreateInputProvider = mock(DeviceInstanceCreateInputProvider.class);
    tcpServerSocketManagerFactory = mock(TcpServerSocketManagerFactory.class);
    DeviceTypeProvider deviceTypeProvider = mock(DeviceTypeProvider.class);
    AdapterIdentifierProvider adapterIdentifierProvider = mock(AdapterIdentifierProvider.class);
    DeviceTypeUpdateListener deviceTypeUpdateListener = mock(DeviceTypeUpdateListener.class);
    DeviceInstanceUpdateListener deviceInstanceUpdateListener = mock(DeviceInstanceUpdateListener.class);
    adapter = new SensingPuckAdapter(
        deviceRegistryServiceClient,
        deviceInstanceCreateInputProvider,
        tcpServerSocketManagerFactory,
        new SensingPuckAdapterAttributes(deviceTypeProvider, adapterIdentifierProvider),
        deviceTypeProvider,
        deviceTypeUpdateListener,
        deviceInstanceUpdateListener);

    when(deviceTypeProvider.getAdapterDeviceTypeIdentifier())
        .thenReturn(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
    when(deviceTypeProvider.createDefaultAdapterDeviceType())
        .thenReturn(sensingPuckDeviceType());
    when(adapterIdentifierProvider.getAdapterIdentifier())
        .thenReturn(TestConstants.ADAPTER_IDENTIFIER);
  }

  @Test
  void registerDeviceType_whenDeviceTypeNotRegistered_thenRegistersDeviceType() {
    // Arrange
    when(fetchSensingPuckDeviceTypes()).thenReturn(List.of());
    when(deviceRegistryServiceClient.createDeviceType(any())).thenReturn(sensingPuckDeviceType());

    // Act
    adapter.registerDeviceType();

    // Verify
    verify(deviceRegistryServiceClient).createDeviceType(any());
  }

  @Test
  void updateDeviceTypeProvidedByAdapter_whenDeviceTypeNotRegistered_thenThrowsException() {
    // Arrange
    when(fetchSensingPuckDeviceTypes()).thenReturn(List.of());

    // Act & Assert
    assertThatThrownBy(() -> adapter.updateDeviceTypeProvidedByAdapter())
        .isInstanceOf(IllegalStateException.class);
  }

  @Test
  void updateDeviceTypeProvidedByAdapter_whenDeviceTypeRegistered_thenAppendsAdapterIdentifierToProvidedBySet() {
    // Arrange
    DeviceType deviceType = sensingPuckDeviceType()
        .withId("deviceType-1")
        .withProvidedBy(Set.of("adapter-xyz"))
        .withDescription("")
        .withEnabled(true)
        .withAutoRegisterDeviceInstances(true)
        .withAutoEnableDeviceInstances(false);
    when(fetchSensingPuckDeviceTypes()).thenReturn(List.of(deviceType));
    when(deviceRegistryServiceClient.updateDeviceType(any(), any()))
        .thenReturn(sensingPuckDeviceType());

    // Act
    adapter.updateDeviceTypeProvidedByAdapter();

    // Verify
    DeviceTypeUpdateInputTO expectedDeviceTypeUpdateInputTO = new DeviceTypeUpdateInputTO();
    expectedDeviceTypeUpdateInputTO
        .setProvidedBy(Set.of("adapter-xyz", TestConstants.ADAPTER_IDENTIFIER))
        .setDescription("")
        .setEnabled(true)
        .setAutoRegisterDeviceInstances(true)
        .setAutoEnableDeviceInstances(false);
    verify(deviceRegistryServiceClient).updateDeviceType("deviceType-1",
                                                         expectedDeviceTypeUpdateInputTO);
  }

  @Test
  void registerDeviceInstance_whenDeviceInstanceNotRegistered_thenRegistersDeviceInstance() {
    // Arrange
    SensingPuckRequestBase registrationContext = TestDataFactory.v1RequestWithDefaults();
    when(fetchSensingPuckDeviceInstances("1")).thenReturn(List.of());
    when(deviceInstanceCreateInputProvider.get(eq("1"), any()))
        .thenReturn(sensingPuckDeviceInstanceCreateInput());
    when(deviceRegistryServiceClient.createDeviceInstance(any()))
        .thenReturn(sensingPuckDeviceInstance());
    // Ensure that the cached device type is set.
    when(fetchSensingPuckDeviceTypes()).thenReturn(List.of(sensingPuckDeviceType()));
    adapter.registerDeviceType();

    // Act
    adapter.registerDeviceInstance("1", registrationContext);

    // Verify
    verify(deviceRegistryServiceClient).createDeviceInstance(any());
  }

  @Test
  void registerDeviceInstance_whenDeviceInstanceRegistered_thenDoesNotRegisterDeviceInstanceAgain() {
    // Arrange
    SensingPuckRequestBase registrationContext = TestDataFactory.v1RequestWithDefaults();
    DeviceInstance deviceInstance = sensingPuckDeviceInstance().withTenant("1");
    when(fetchSensingPuckDeviceInstances("1")).thenReturn(List.of(deviceInstance));

    // Act
    adapter.registerDeviceInstance("1", registrationContext);

    // Verify
    verify(deviceRegistryServiceClient, never()).createDeviceInstance(any());
  }

  @Test
  void registerDeviceInstance_whenDeviceInstanceIsNotAssociatedWithAdapter_doThrowException() {
    // Arrange
    SensingPuckRequestBase registrationContext = TestDataFactory.v1RequestWithDefaults();
    DeviceInstanceCreateInputTO deviceInstanceCreateInputTO = sensingPuckDeviceInstanceCreateInput()
        .setSource("iPhone");
    DeviceInstance deviceInstance = sensingPuckDeviceInstance()
        .withTenant("1")
        .withSource("iPhone");
    when(deviceInstanceCreateInputProvider.get(eq("1"), any()))
        .thenReturn(deviceInstanceCreateInputTO);
    when(fetchSensingPuckDeviceInstances("1")).thenReturn(List.of());

    // Act & verify
    assertThatThrownBy(() -> adapter.registerDeviceInstance("1", deviceInstance))
        .isInstanceOf(IllegalDeviceTypeException.class);
  }

  @Test
  void openCommunicationChannel_initializesServerChannelManager() {
    // Arrange
    TcpServerSocketManager serverSocketManager = mock(TcpServerSocketManager.class);
    when(tcpServerSocketManagerFactory.create(any())).thenReturn(serverSocketManager);

    // Act
    adapter.openCommunicationChannel();

    // Verify
    verify(serverSocketManager).initialize();
  }

  @Test
  void closeCommunicationChannel_terminatesServerChannelManager() {
    // Arrange
    TcpServerSocketManager serverSocketManager = mock(TcpServerSocketManager.class);
    when(tcpServerSocketManagerFactory.create(any())).thenReturn(serverSocketManager);
    adapter.openCommunicationChannel();

    // Act
    adapter.closeCommunicationChannel();

    // Verify
    verify(serverSocketManager).terminate();
  }

  private DeviceType sensingPuckDeviceType() {
    return new DeviceType("",
                          TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
                          TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER,
                          Set.of(TestConstants.ADAPTER_IDENTIFIER),
                          "",
                          false,
                          false,
                          false);
  }

  private DeviceInstance sensingPuckDeviceInstance() {
    return new DeviceInstance("",
                              TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource(),
                              "",
                              TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER,
                              Instant.EPOCH,
                              Instant.EPOCH,
                              false,
                              "",
                              "",
                              "");
  }

  private DeviceInstanceCreateInputTO sensingPuckDeviceInstanceCreateInput() {
    return new DeviceInstanceCreateInputTO()
        .setSource(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource())
        .setTenant("")
        .setDeviceTypeIdentifier(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
  }

  private List<DeviceType> fetchSensingPuckDeviceTypes() {
    return deviceRegistryServiceClient
        .fetchDeviceTypes(eq(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource()),
                          eq(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER),
                          any(),
                          any(),
                          any(),
                          any());
  }

  private List<DeviceInstance> fetchSensingPuckDeviceInstances(String tenant) {
    return deviceRegistryServiceClient
        .fetchDeviceInstances(eq(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource()),
                              eq(tenant),
                              any(),
                              any(),
                              any(),
                              any());
  }
}
