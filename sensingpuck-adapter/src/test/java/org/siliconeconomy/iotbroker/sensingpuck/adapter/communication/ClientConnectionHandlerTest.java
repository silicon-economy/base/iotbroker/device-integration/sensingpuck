/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.bouncycastle.tls.TlsServerProtocol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.communication.bouncycastle.TlsServerProtocolProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.config.AdapterProperties;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.RequestResponseDispatcher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.util.LoggingScheduledThreadPoolExecutor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v1RequestWithDefaults;

/**
 * Test cases for {@link ClientConnectionHandler}.
 *
 * @author M. Grzenia
 */
class ClientConnectionHandlerTest {

  /**
   * Class under test.
   */
  private ClientConnectionHandler clientConnectionHandler;
  /**
   * Test dependencies.
   */
  private Socket clientSocket;
  private TlsServerProtocolProvider tlsServerProtocolProvider;
  private RequestResponseDispatcher requestResponseDispatcher;
  private DeviceAdapter deviceAdapter;
  private ScheduledExecutorService executorService = new LoggingScheduledThreadPoolExecutor(1);

  @BeforeEach
  void setUp() {
    clientSocket = mock(Socket.class);
    tlsServerProtocolProvider = mock(TlsServerProtocolProvider.class);
    requestResponseDispatcher = mock(RequestResponseDispatcher.class);
    deviceAdapter = mock(DeviceAdapter.class);
    AdapterProperties adapterProperties = new AdapterProperties(1,
                                                                "localhost",
                                                                5683,
                                                                "",
                                                                "",
                                                                "",
                                                                0);
    clientConnectionHandler = new ClientConnectionHandler(clientSocket,
                                                          tlsServerProtocolProvider,
                                                          requestResponseDispatcher,
                                                          deviceAdapter,
                                                          executorService,
                                                          adapterProperties);
  }

  @Test
  void run_whenInvalidMessage_thenStopsFurtherProcessing()
      throws IOException, InterruptedException {
    // Arrange
    configureSocketMock();
    TlsServerProtocol tlsServerProtocol = configureTlsServerProtocolMock();
    when(requestResponseDispatcher.tryParseAndValidateMessage(anyString()))
        .thenThrow(new IllegalArgumentException(""));

    // Act
    clientConnectionHandler.run();

    // Wait for execution of any scheduled task.
    executorService.shutdown();
    executorService.awaitTermination(5, TimeUnit.SECONDS);

    // Verify
    verify(requestResponseDispatcher).tryParseAndValidateMessage(anyString());
    verifyNoMoreInteractions(requestResponseDispatcher);
    verifyNoInteractions(deviceAdapter);
    verify(tlsServerProtocol.getOutputStream()).close();
    verifyNoMoreInteractions(tlsServerProtocol.getOutputStream());
  }

  @Test
  void run_whenAutoRegistrationNotSuccessful_thenStopsFurtherProcessing()
      throws IOException, InterruptedException {
    // Arrange
    configureSocketMock();
    TlsServerProtocol tlsServerProtocol = configureTlsServerProtocolMock();
    SensingPuckRequestBase request = v1RequestWithDefaults();
    String tenant = String.valueOf(request.getDeviceId());
    when(requestResponseDispatcher.tryParseAndValidateMessage(anyString())).thenReturn(request);
    when(deviceAdapter.attemptDeviceInstanceAutoRegistration(tenant, request))
        .thenReturn(false);

    // Act
    clientConnectionHandler.run();

    // Wait for execution of any scheduled task.
    executorService.shutdown();
    executorService.awaitTermination(5, TimeUnit.SECONDS);

    // Assert & Verify
    verify(requestResponseDispatcher).tryParseAndValidateMessage(anyString());
    verify(deviceAdapter).attemptDeviceInstanceAutoRegistration(tenant, request);
    verifyNoMoreInteractions(requestResponseDispatcher);
    verifyNoMoreInteractions(deviceAdapter);
    verify(tlsServerProtocol.getOutputStream()).close();
    verifyNoMoreInteractions(tlsServerProtocol.getOutputStream());
  }


  @Test
  void run_whenDeviceTypeDisabled_thenStopsFurtherProcessing()
      throws IOException, InterruptedException {
    // Arrange
    configureSocketMock();
    TlsServerProtocol tlsServerProtocol = configureTlsServerProtocolMock();
    SensingPuckRequestBase request = v1RequestWithDefaults();
    String tenant = String.valueOf(request.getDeviceId());
    when(requestResponseDispatcher.tryParseAndValidateMessage(anyString())).thenReturn(request);
    when(deviceAdapter.attemptDeviceInstanceAutoRegistration(tenant, request))
        .thenReturn(true);
    when(deviceAdapter.isDeviceTypeEnabled()).thenReturn(false);

    // Act
    clientConnectionHandler.run();

    // Wait for execution of any scheduled task.
    executorService.shutdown();
    executorService.awaitTermination(5, TimeUnit.SECONDS);

    // Assert & Verify
    verify(requestResponseDispatcher).tryParseAndValidateMessage(anyString());
    verify(deviceAdapter).attemptDeviceInstanceAutoRegistration(tenant, request);
    verify(deviceAdapter).isDeviceTypeEnabled();
    verifyNoMoreInteractions(requestResponseDispatcher);
    verifyNoMoreInteractions(deviceAdapter);
    verify(tlsServerProtocol.getOutputStream()).close();
    verifyNoMoreInteractions(tlsServerProtocol.getOutputStream());
  }

  @Test
  void run_whenDeviceInstanceDisabled_thenStopsFurtherProcessing()
      throws IOException, InterruptedException {
    // Arrange
    configureSocketMock();
    TlsServerProtocol tlsServerProtocol = configureTlsServerProtocolMock();
    SensingPuckRequestBase request = v1RequestWithDefaults();
    String tenant = String.valueOf(request.getDeviceId());
    when(requestResponseDispatcher.tryParseAndValidateMessage(anyString())).thenReturn(request);
    when(deviceAdapter.attemptDeviceInstanceAutoRegistration(tenant, request))
        .thenReturn(true);
    when(deviceAdapter.isDeviceTypeEnabled()).thenReturn(true);
    when(deviceAdapter.isDeviceInstanceEnabled(tenant)).thenReturn(false);

    // Act
    clientConnectionHandler.run();

    // Wait for execution of any scheduled task.
    executorService.shutdown();
    executorService.awaitTermination(5, TimeUnit.SECONDS);

    // Assert & Verify
    verify(requestResponseDispatcher).tryParseAndValidateMessage(anyString());
    verify(deviceAdapter).attemptDeviceInstanceAutoRegistration(tenant, request);
    verify(deviceAdapter).isDeviceTypeEnabled();
    verify(deviceAdapter).isDeviceInstanceEnabled(tenant);
    verifyNoMoreInteractions(requestResponseDispatcher);
    verify(tlsServerProtocol.getOutputStream()).close();
    verifyNoMoreInteractions(tlsServerProtocol.getOutputStream());
  }

  @Test
  void run_whenValidMessageAndDeviceRegisteredAndEnabled_thenProcessesCompletely()
      throws IOException, InterruptedException {
    // Arrange
    configureSocketMock();
    TlsServerProtocol tlsServerProtocol = configureTlsServerProtocolMock();
    SensingPuckRequestBase request = v1RequestWithDefaults();
    String tenant = String.valueOf(request.getDeviceId());
    when(requestResponseDispatcher.tryParseAndValidateMessage(anyString())).thenReturn(request);
    when(deviceAdapter.attemptDeviceInstanceAutoRegistration(tenant, request))
        .thenReturn(true);
    when(deviceAdapter.isDeviceTypeEnabled()).thenReturn(true);
    when(deviceAdapter.isDeviceInstanceEnabled(tenant)).thenReturn(true);

    // Act
    clientConnectionHandler.run();

    // Wait for execution of any scheduled task.
    executorService.shutdown();
    executorService.awaitTermination(5, TimeUnit.SECONDS);

    // Assert & Verify
    verify(requestResponseDispatcher).tryParseAndValidateMessage(anyString());
    verify(deviceAdapter).attemptDeviceInstanceAutoRegistration(tenant, request);
    verify(deviceAdapter).isDeviceTypeEnabled();
    verify(deviceAdapter).isDeviceInstanceEnabled(tenant);
    verify(requestResponseDispatcher).onRequest(any());
    verify(requestResponseDispatcher).getResponseFor(any());
    verify(requestResponseDispatcher).postProcessResponse(any());
    verify(tlsServerProtocol.getOutputStream()).flush();
  }

  private void configureSocketMock()
      throws IOException {
    when(clientSocket.isConnected()).thenReturn(true);
    when(clientSocket.isClosed()).thenReturn(false);
    when(clientSocket.getInetAddress()).thenReturn(InetAddress.getLoopbackAddress());
    when(clientSocket.getPort()).thenReturn(50000);
    InputStream in = new ByteArrayInputStream("dummy-string".getBytes(StandardCharsets.UTF_8));
    when(clientSocket.getInputStream()).thenReturn(in);
    OutputStream out = mock(OutputStream.class);
    when(clientSocket.getOutputStream()).thenReturn(out);
  }

  private TlsServerProtocol configureTlsServerProtocolMock() {
    TlsServerProtocol tlsServerProtocol = mock(TlsServerProtocol.class);
    when(tlsServerProtocolProvider.getAndAccept(clientSocket)).thenReturn(tlsServerProtocol);
    InputStream in = new ByteArrayInputStream("dummy-string".getBytes(StandardCharsets.UTF_8));
    when(tlsServerProtocol.getInputStream()).thenReturn(in);
    OutputStream out = mock(OutputStream.class);
    when(tlsServerProtocol.getOutputStream()).thenReturn(out);
    return tlsServerProtocol;
  }
}
