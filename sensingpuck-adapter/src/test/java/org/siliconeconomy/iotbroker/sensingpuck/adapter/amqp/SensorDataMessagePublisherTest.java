/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.amqp.SensorDataExchangeConfiguration;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link SensorDataMessagePublisher}.
 *
 * @author M. Grzenia
 */
class SensorDataMessagePublisherTest {

  /**
   * Class under test.
   */
  private SensorDataMessagePublisher sensorDataMessagePublisher;
  /**
   * Test dependencies.
   */
  private AmqpTemplate amqpTemplate;

  @BeforeEach
  void setUp() {
    amqpTemplate = mock(AmqpTemplate.class);
    sensorDataMessagePublisher = new SensorDataMessagePublisher(amqpTemplate);
  }

  @Test
  void testConvertAndPublishToSensorDataExchange() {
    // Arrange
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
    SensorDataMessage messageToPublish = createDummyMessage();

    // Act
    sensorDataMessagePublisher.publishToSensorDataExchange(messageToPublish);

    // Assert & Verify
    ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
    verify(amqpTemplate).send(
        eq(SensorDataExchangeConfiguration.NAME),
        eq(SensorDataExchangeConfiguration.ROUTING_KEY),
        messageCaptor.capture()
    );
    assertThat(messageCaptor.getValue().getMessageProperties())
        .extracting(MessageProperties::getContentEncoding, MessageProperties::getContentType)
        .contains(StandardCharsets.UTF_8.toString(), MessageProperties.CONTENT_TYPE_JSON);
  }

  private SensorDataMessage createDummyMessage() {
    return new SensorDataMessage("message-id",
                                 "origin-id",
                                 "source-1",
                                 "tenant-1",
                                 Instant.now(),
                                 null,
                                 new ArrayList<>());
  }
}
