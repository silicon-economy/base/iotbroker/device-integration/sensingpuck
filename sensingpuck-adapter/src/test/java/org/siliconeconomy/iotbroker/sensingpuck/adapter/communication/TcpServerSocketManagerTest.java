/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;

import java.util.concurrent.ExecutorService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;

/**
 * Test cases for {@link TcpServerSocketManager}.
 *
 * @author M. Grzenia
 */
class TcpServerSocketManagerTest {

  /**
   * Class under test.
   */
  private TcpServerSocketManager serverSocketManager;
  /**
   * Test dependencies.
   */
  private ExecutorService executorService;
  private ClientConnectionHandlerFactory clientConnectionHandlerFactory;
  private DeviceAdapter deviceAdapter;

  @BeforeEach
  void setUp() {
    executorService = mock(ExecutorService.class);
    clientConnectionHandlerFactory = mock(ClientConnectionHandlerFactory.class);
    deviceAdapter = mock(DeviceAdapter.class);
    serverSocketManager = new TcpServerSocketManager(0,
                                                     executorService,
                                                     clientConnectionHandlerFactory,
                                                     deviceAdapter);
  }

  @Test
  void shouldNotAllowPortsLessThanZeroOnInstantiation() {
    assertThatThrownBy(() -> new TcpServerSocketManager(-1,
                                                        executorService,
                                                        clientConnectionHandlerFactory,
                                                        deviceAdapter)
    ).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void shouldOpenServerSocketOnInitializationAndCloseItOnTermination() {
    assertThat(serverSocketManager.getServerSocket()).isNull();

    serverSocketManager.initialize();
    assertThat(serverSocketManager.getServerSocket()).isNotNull();
    assertThat(serverSocketManager.getServerSocket().isClosed()).isFalse();

    serverSocketManager.terminate();
    assertThat(serverSocketManager.getServerSocket()).isNull();
  }
}
