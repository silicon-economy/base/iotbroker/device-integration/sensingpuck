/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.util.device;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.FirmwareDescriptorRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.common.VersionNumberRaw;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v2.request.V2SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v3.request.V3SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceInstanceCreateInputProvider}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceCreateInputProviderTest {

  /**
   * Class under test.
   */
  private DeviceInstanceCreateInputProvider provider;
  /**
   * Test dependencies.
   */
  private DeviceTypeProvider deviceTypeProvider;

  @BeforeEach
  void setUp() {
    deviceTypeProvider = mock(DeviceTypeProvider.class);
    provider = new DeviceInstanceCreateInputProvider(deviceTypeProvider,
                                                     TestConstants.DEVICE_INTEGRATION_PROPERTIES);
  }

  @Test
  void map_whenV1Request_thenReturnsDevice() {
    // Arrange
    when(deviceTypeProvider.determineDeviceTypeIdentifier(any()))
        .thenReturn(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
    SensingPuckRequestBase request = new V1SensingPuckRequest(
        1,
        123,
        1234,
        new FirmwareDescriptorRaw(
            new VersionNumberRaw(
                1,
                2,
                3,
                4,
                true
            ),
            12345,
            false
        ),
        1000,
        CommCause.DATA,
        new ArrayList<>());

    // Act
    DeviceInstanceCreateInputTO result = provider.get("123", request);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      assertThat(result.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      assertThat(result.getTenant()).isEqualTo("1234");
      assertThat(result.getDeviceTypeIdentifier())
          .isEqualTo(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
      assertThat(result.getLastSeen()).isNull();
      assertThat(result.getEnabled()).isTrue();
      assertThat(result.getDescription())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceInstanceDescription());
      assertThat(result.getHardwareRevision()).isEmpty();
      assertThat(result.getFirmwareVersion()).isEqualTo("1.2.3");
    });
  }

  @Test
  void map_whenV2Request_thenReturnsDevice() {
    // Arrange
    when(deviceTypeProvider.determineDeviceTypeIdentifier(any()))
        .thenReturn(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
    SensingPuckRequestBase request = new V2SensingPuckRequest(
        2,
        123,
        1234,
        new FirmwareDescriptorRaw(
            new VersionNumberRaw(
                1,
                2,
                3,
                4,
                true
            ),
            12345,
            false
        ),
        1000,
        CommCause.DATA,
        new ArrayList<>());

    // Act
    DeviceInstanceCreateInputTO result = provider.get("123", request);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      assertThat(result.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      assertThat(result.getTenant()).isEqualTo("1234");
      assertThat(result.getDeviceTypeIdentifier())
          .isEqualTo(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
      assertThat(result.getLastSeen()).isNull();
      assertThat(result.getEnabled()).isTrue();
      assertThat(result.getDescription())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceInstanceDescription());
      assertThat(result.getHardwareRevision()).isEmpty();
      assertThat(result.getFirmwareVersion()).isEqualTo("1.2.3");
    });
  }

  @Test
  void map_whenV3Request_thenReturnsDevice() {
    // Arrange
    when(deviceTypeProvider.determineDeviceTypeIdentifier(any()))
        .thenReturn(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
    SensingPuckRequestBase request = new V3SensingPuckRequest(
        3,
        123,
        1234,
        new FirmwareDescriptorRaw(
            new VersionNumberRaw(
                1,
                2,
                3,
                4,
                true
            ),
            12345,
            false
        ),
        1000,
        CommCause.DATA,
        new ArrayList<>());

    // Act
    DeviceInstanceCreateInputTO result = provider.get("123", request);

    // Assert
    SoftAssertions.assertSoftly(softly -> {
      assertThat(result.getSource())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
      assertThat(result.getTenant()).isEqualTo("1234");
      assertThat(result.getDeviceTypeIdentifier())
          .isEqualTo(TestConstants.ADAPTER_DEVICE_TYPE_IDENTIFIER);
      assertThat(result.getLastSeen()).isNull();
      assertThat(result.getEnabled()).isTrue();
      assertThat(result.getDescription())
          .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceInstanceDescription());
      assertThat(result.getHardwareRevision()).isEmpty();
      assertThat(result.getFirmwareVersion()).isEqualTo("1.2.3");
    });
  }

  @Test
  void map_whenUnsupportedRegistrationContextType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new UnsupportedSensingPuckRequest();

    // Act & Assert
    assertThatThrownBy(() -> provider.get("", request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedSensingPuckRequest
      extends SensingPuckRequestBase {
  }
}
