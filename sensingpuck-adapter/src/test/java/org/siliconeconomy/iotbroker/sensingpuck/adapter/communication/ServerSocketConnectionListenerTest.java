/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.communication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

/**
 * Test cases for {@link ServerSocketConnectionListener}.
 *
 * @author M. Grzenia
 */
class ServerSocketConnectionListenerTest {

  /**
   * Class under test.
   */
  private ServerSocketConnectionListener connectionListener;
  /**
   * Test dependencies.
   */
  private ServerSocket serverSocket;
  private Consumer<Socket> clientSocketHandler;

  @BeforeEach
  void setUp() {
    serverSocket = mock(ServerSocket.class);
    clientSocketHandler = (Consumer<Socket>) mock(Consumer.class);
    connectionListener = new ServerSocketConnectionListener(serverSocket,
                                                            clientSocketHandler,
                                                            new TrueAfterOneInvocation());
  }

  @Test
  void listenForClientConnections_whenClientConnects_thenPassesSocketToHandler()
      throws IOException {
    // Arrange
    when(serverSocket.isClosed()).thenReturn(false);
    Socket clientSocket = new Socket();
    when(serverSocket.accept()).thenReturn(clientSocket);

    // Act
    connectionListener.run();

    // Assert
    verify(serverSocket).accept();
    verify(clientSocketHandler).accept(clientSocket);
  }

  @Test
  void listenForClientConnections_whenClientConnectionFails_thenDoesntPassSocketToHandler()
      throws IOException {
    // Arrange
    when(serverSocket.isClosed()).thenReturn(false);
    Socket clientSocket = new Socket();
    when(serverSocket.accept()).thenThrow(IOException.class);

    // Act
    connectionListener.run();

    // Assert
    verify(serverSocket).accept();
    verify(clientSocketHandler, never()).accept(clientSocket);
  }

  private static class TrueAfterOneInvocation
      implements BooleanSupplier {
    private boolean executed = false;

    @Override
    public boolean getAsBoolean() {
      if (executed) {
        return true;
      }

      executed = true;
      return false;
    }
  }
}
