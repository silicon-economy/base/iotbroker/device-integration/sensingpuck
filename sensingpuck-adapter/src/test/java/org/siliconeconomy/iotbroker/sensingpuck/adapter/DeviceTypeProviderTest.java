/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceTypeProvider}.
 *
 * @author M. Grzenia
 */
class DeviceTypeProviderTest {

  /**
   * Class under test.
   */
  private DeviceTypeProvider deviceTypeProvider;

  @BeforeEach
  void setUp() {
    AdapterIdentifierProvider adapterIdentifierProvider = mock(AdapterIdentifierProvider.class);
    deviceTypeProvider = new DeviceTypeProvider(TestConstants.DEVICE_INTEGRATION_PROPERTIES,
                                                adapterIdentifierProvider);

    when(adapterIdentifierProvider.getAdapterIdentifier())
        .thenReturn(TestConstants.ADAPTER_IDENTIFIER);
  }

  @Test
  void getAdapterDeviceTypeIdentifier_shouldConsiderConfiguration() {
    assertThat(deviceTypeProvider.getAdapterDeviceTypeIdentifier())
        .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource() + "-v1");
  }

  @Test
  void createDefaultAdapterDeviceType_shouldConsiderConfiguration() {
    DeviceType defaultDeviceType = deviceTypeProvider.createDefaultAdapterDeviceType();
    assertThat(defaultDeviceType.getSource())
        .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource());
    assertThat(defaultDeviceType.getIdentifier())
        .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource() + "-v1");
    assertThat(defaultDeviceType.getProvidedBy())
        .containsExactly(TestConstants.ADAPTER_IDENTIFIER);
    assertThat(defaultDeviceType.getDescription())
        .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceTypeDescription());
  }

  @Test
  void determineDeviceTypeIdentifier_whenV1orV2Request_thenReturnsAdapterDeviceTypeIdentifier() {
    assertThat(deviceTypeProvider.determineDeviceTypeIdentifier(
        TestDataFactory.v1RequestWithDefaults()
    ))
        .isEqualTo(deviceTypeProvider.getAdapterDeviceTypeIdentifier());

    assertThat(deviceTypeProvider.determineDeviceTypeIdentifier(
        TestDataFactory.v2RequestWithDefaults()
    ))
        .isEqualTo(deviceTypeProvider.getAdapterDeviceTypeIdentifier());
  }

  @Test
  void determineDeviceTypeIdentifier_whenUnknownRequest_thenReturnsIndividualDeviceTypeIdentifier() {
    assertThat(deviceTypeProvider.determineDeviceTypeIdentifier(
        new SensingPuckRequestBase(99, 0)
    ))
        .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource() + "-v99");
  }
}
