/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1DefaultSensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v1JobEntityWithDefaults;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v1ResponseWithDefaults;

/**
 * Test cases for {@link V1ResponsePostProcessor}.
 *
 * @author M. Grzenia
 */
class V1ResponsePostProcessorTest {

  /**
   * Class under test.
   */
  private V1ResponsePostProcessor responsePostProcessor;
  /**
   * Test dependencies.
   */
  private JobService jobService;

  @BeforeEach
  void setUp() {
    jobService = mock(JobService.class);
    responsePostProcessor = new V1ResponsePostProcessor(jobService);
  }

  @Test
  void postProcessResponse_whenValidResponse_thenUpdatesJobInDatabase() {
    // Arrange
    V1SensingPuckResponse response = v1ResponseWithDefaults();
    response.setJobId(4711);
    response.setClearAlarm(true);
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();
    jobEntity.setJobId(4711);
    jobEntity.setClearAlarm(true);
    when(jobService.findEntityByJobId(response.getJobId())).thenReturn(jobEntity);

    // Act
    responsePostProcessor.postProcessResponse(response);

    // Assert
    ArgumentCaptor<V1SensingPuckJobEntity> jobEntityCaptor
        = ArgumentCaptor.forClass(V1SensingPuckJobEntity.class);
    verify(jobService).update(jobEntityCaptor.capture());
    assertThat(jobEntityCaptor.getValue().isClearAlarm()).isFalse();
  }

  @Test
  void postProcessResponse_whenDefaultSensingPuckResponse_thenDoesNothing() {
    // Arrange
    V1DefaultSensingPuckResponse response = new V1DefaultSensingPuckResponse(1);

    // Act
    responsePostProcessor.postProcessResponse(response);

    // Verify
    verifyNoInteractions(jobService);
  }

  @Test
  void postProcessResponse_whenUnsupportedResponseType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new UnsupportedResponseType();

    // Act & Assert
    assertThatThrownBy(() -> responsePostProcessor.postProcessResponse(response))
        .isInstanceOf(IllegalArgumentException.class);

    // Verify
    verifyNoInteractions(jobService);
  }

  private static class UnsupportedResponseType
      extends SensingPuckResponseBase {

    public UnsupportedResponseType() {
      super(ProtocolVersion.V1.getVersionNumber(), 0);
    }
  }
}
