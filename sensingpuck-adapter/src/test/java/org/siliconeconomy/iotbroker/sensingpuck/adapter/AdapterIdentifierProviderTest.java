/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link AdapterIdentifierProvider}.
 *
 * @author M. Grzenia
 */
class AdapterIdentifierProviderTest {

  /**
   * Class under test.
   */
  private AdapterIdentifierProvider adapterIdentifierProvider;

  @BeforeEach
  void setUp() {
    adapterIdentifierProvider
        = new AdapterIdentifierProvider(TestConstants.DEVICE_INTEGRATION_PROPERTIES);
  }

  @Test
  void getAdapterIdentifier_shouldConsiderConfiguration() {
    assertThat(adapterIdentifierProvider.getAdapterIdentifier())
        .isEqualTo(TestConstants.DEVICE_INTEGRATION_PROPERTIES.getDeviceSource() + "-adapter-v2");
  }
}
