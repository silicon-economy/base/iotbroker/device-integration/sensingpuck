/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.TestConstants;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.DeviceMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.amqp.SensorDataMessagePublisher;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.util.sensordata.MessageEntityToSensorDataMapper;
import org.siliconeconomy.iotbroker.sensingpuck.common.messages.MessageService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageEntityBase;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.utils.SensingPuckMessageUtils;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v1MessageEntityWithDefaults;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.v1RequestWithDefaults;

/**
 * Test cases for {@link V1RequestHandler}.
 *
 * @author M. Grzenia
 */
class V1RequestHandlerTest {

  /**
   * Class under test.
   */
  private V1RequestHandler requestHandler;
  /**
   * Test dependencies.
   */
  private MessageService messageService;
  private DeviceMessagePublisher deviceMessagePublisher;
  private SensorDataMessagePublisher sensorDataMessagePublisher;

  @BeforeEach
  void setUp() {
    messageService = mock(MessageService.class);
    deviceMessagePublisher = mock(DeviceMessagePublisher.class);
    sensorDataMessagePublisher = mock(SensorDataMessagePublisher.class);
    requestHandler = new V1RequestHandler(
        messageService,
        deviceMessagePublisher,
        sensorDataMessagePublisher,
        new MessageEntityToSensorDataMapper(
            new SensingPuckMessageUtils(TestConstants.DEVICE_INTEGRATION_PROPERTIES),
            TestConstants.DEVICE_INTEGRATION_PROPERTIES
        )
    );
  }

  @Test
  void onRequest_whenValidRequest_thenPersistsAndForwardsMessage() {
    // Arrange
    SensingPuckMessageEntityBase messageEntity = v1MessageEntityWithDefaults();
    when(messageService.save(any())).thenReturn(messageEntity);

    // Act
    requestHandler.onRequest(v1RequestWithDefaults());

    // Assert
    verify(messageService).save(any(V1SensingPuckMessageDto.class));
    verify(deviceMessagePublisher).publishToDeviceMessageExchange(
        eq("1"),
        any(V1SensingPuckMessageDto.class)
    );
    verify(sensorDataMessagePublisher).publishToSensorDataExchange(any(SensorDataMessage.class));
  }

  @Test
  void onRequest_whenUnsupportedRequestType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new UnsupportedRequestType();

    // Act & Assert
    assertThatThrownBy(() -> requestHandler.onRequest(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedRequestType
      extends SensingPuckRequestBase {

    public UnsupportedRequestType() {
      super(ProtocolVersion.V1.getVersionNumber(), 0);
    }
  }
}
