/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.sensingpuck.adapter.dispatching.v1;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckRequestBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.SensingPuckResponseBase;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.request.V1SensingPuckRequest;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.model.communication.v1.response.V1SensingPuckResponse;
import org.siliconeconomy.iotbroker.sensingpuck.common.jobs.JobService;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.JobState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.ProtocolVersion;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.job.V1SensingPuckJobEntity;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.siliconeconomy.iotbroker.sensingpuck.adapter.TestDataFactory.*;

/**
 * Test cases for {@link V1ResponseProvider}.
 *
 * @author M. Grzenia
 */
class V1ResponseProviderTest {

  /**
   * Class under test.
   */
  private V1ResponseProvider responseProvider;
  /**
   * Test dependencies.
   */
  private JobService service;

  @BeforeEach
  void setUp() {
    service = mock(JobService.class);
    responseProvider = new V1ResponseProvider(service);
  }

  @Test
  void getResponseFor_whenActiveJobAvailable_thenReturnsMostRecentActiveJob() {
    // Arrange
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();
    jobEntity.setJobState(JobState.ACTIVE);
    when(service.findMostRecentActiveJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v1RequestWithDefaults());

    // Assert
    V1SensingPuckResponse expectedResponse = v1ResponseWithDefaults();
    expectedResponse.setJobState(JobState.ACTIVE);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V1SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenOnlyFinishedJobsAvailable_thenReturnsMostRecentFinishedJob() {
    // Arrange
    V1SensingPuckJobEntity jobEntity = v1JobEntityWithDefaults();
    jobEntity.setJobState(JobState.FINISHED);
    when(service.findMostRecentFinishedJob(anyString()))
        .thenReturn(Optional.of(jobEntity));

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(v1RequestWithDefaults());

    // Assert
    V1SensingPuckResponse expectedResponse = v1ResponseWithDefaults();
    expectedResponse.setJobState(JobState.FINISHED);
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V1SensingPuckResponse.class))
        .isEqualTo(expectedResponse);
  }

  @Test
  void getResponseFor_whenNoJobsAvailable_thenReturnsDefaultJob() {
    // Arrange
    V1SensingPuckRequest request = v1RequestWithDefaults();
    request.setDeviceId(4711L);

    // Act
    SensingPuckResponseBase result = responseProvider.getResponseFor(request);

    // Assert
    assertThat(result)
        .asInstanceOf(InstanceOfAssertFactories.type(V1SensingPuckResponse.class))
        .extracting(V1SensingPuckResponse::getDeviceId)
        .isEqualTo(4711L);
  }

  @Test
  void getResponseFor_whenUnsupportedRequestType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckRequestBase request = new UnsupportedRequestType();

    // Act & Assert
    assertThatThrownBy(() -> responseProvider.getResponseFor(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void serializeResponse_whenValidResponse_thenSerializesSuccessfully() {
    // Arrange
    SensingPuckResponseBase response = v1ResponseWithDefaults();

    // Act & Assert
    assertThatNoException().isThrownBy(() -> responseProvider.serializeResponse(response));
  }

  @Test
  void serializeResponse_whenUnsupportedResponseType_thenThrowsIllegalArgumentException() {
    // Arrange
    SensingPuckResponseBase response = new UnsupportedResponseType();

    // Act & Assert
    assertThatThrownBy(() -> responseProvider.serializeResponse(response))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private static class UnsupportedRequestType
      extends SensingPuckRequestBase {

    public UnsupportedRequestType() {
      super(ProtocolVersion.V1.getVersionNumber(), 0);
    }
  }

  private static class UnsupportedResponseType
      extends SensingPuckResponseBase {

    public UnsupportedResponseType() {
      super(ProtocolVersion.V1.getVersionNumber(), 0);
    }
  }
}
